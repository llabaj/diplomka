/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.objects;

import messif.algorithm.Algorithm;
import messif.algorithm.impl.SequentialScan;
import messif.data.DataObject;
import messif.distance.DataObjectDistanceFunc;
import messif.distance.DistanceFunc;
import messif.distance.impl.L2DistanceFloats;
import messif.utility.json.JSONWriter;
import messif.data.util.DataObjectList;
import messif.data.util.StreamDataObjectIterator;
import messif.operation.answer.AbstractAnswer;
import messif.operation.OperationBuilder;
import messif.operation.crud.InsertOperation;
import messif.operation.search.KNNOperation;
import messif.utility.proxy.ProxyConverter;
import messif.record.ModifiableRecord;
import messif.record.ModifiableRecordImpl;

import java.util.List;

/**
 * Allows to compute a distance histogram of a given metric space.
 * The metric space is represented by the data object class, the distances
 * are measured by taking random object pairs from a given data file.
 *
 * <p>
 * The whole data is read in and then random pairs are identified in the data.
 * </p>
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class OperationTest {

    /**
     * Main method that deals with the parameters and computes the histogram.
     * @param args command line parameters (see usage)
     */
    public static void main(String[] args) {
        try (JSONWriter outWriter = new JSONWriter(System.out, true)) {
            List<DataObject> objects = new DataObjectList(new StreamDataObjectIterator("src/test/resources/imagedata.json"));

            DistanceFunc<DataObject> distanceFunc = new DataObjectDistanceFunc<>("decaf_sparse_4096", new L2DistanceFloats());

            InsertOperation insertOp = OperationBuilder.create(InsertOperation.class)
                    .addParam(InsertOperation.OBJECTS_FIELD, objects.toArray(new DataObject[objects.size()]))
                    .build();


            ModifiableRecord kNNParams = new ModifiableRecordImpl();
            kNNParams.setField(KNNOperation.QUERY_OBJECT_FIELD, objects.get(0));
            kNNParams.setField(KNNOperation.K_FIELD, 10);
            kNNParams.setField(KNNOperation.DISTANCE_FIELD, distanceFunc);

            KNNOperation knnOperation = ProxyConverter.convertChecked(kNNParams, KNNOperation.class);


            Algorithm seqScan = new SequentialScan(distanceFunc);
            seqScan.init();

            seqScan.evaluate(insertOp);

            AbstractAnswer answer = seqScan.evaluate(knnOperation);
            answer.writeJSON(outWriter);


        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
