/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data;

import messif.record.ModifiableRecord;
import messif.record.Record;
import messif.record.RecordImpl;

/**
 * A marking interface of the data object, which is a {@link Record} with an appointed field with a unique identifier.
 */
public interface DataObject extends Record {

    /** Name of the appointed field with a data object unique identifier. */
    String ID_FIELD = "_id".intern();

    /** A shortcut method to access the unique identifier of the data object. */
    default String getID() {
        return getField(ID_FIELD, String.class);
    }

    /**
     * Creates a new data object that has just given ID.
     * @param id unique string identifier
     * @return a new created data object (unmodifiable) with on field (ID)
     */
    static DataObject objectWithID(String id) {
        return new RecordImpl(new String [] {ID_FIELD}, new Object[] {id});
    }

    /**
     * Creates a new data object (or modify the passed one, if modifiable) to contain all fields from given object
     *  plus the given field.
     */
    static DataObject addField(DataObject source, String field, Object value) {
        if (source instanceof ModifiableRecord) {
            ((ModifiableRecord) source).setField(field, value);
            return source;
        }
        return new RecordImpl(source, field, value);
    }

}
