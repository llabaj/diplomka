/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.processing.impl;

import messif.algorithm.Algorithm;
import messif.algorithm.AlgorithmMethodException;
import messif.operation.helpers.AnswerProvider;
import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.processing.OperationProcessor;

import java.util.Collection;

/**
 * Basic implementation of {@link OperationProcessor} that processes any {@link AbstractOperation}
 * on a set of processing items (e.g. buckets).
 * <p>
 * <h4>Parallelism</h4>
 * The implementation is thread-safe, if operation cloning is enabled. Otherwise,
 * the operation answer needs to be synchronized.
 *
 * @param <TRequest> the type of the operation that are processed by this operation processor
 * @param <TAnswer>  class of answer returned by this processor
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class AlgorithmsOperationProcessor<TRequest extends AbstractOperation, TAnswer extends AbstractAnswer>
        extends QueueOperationProcessor<TRequest, TAnswer, Algorithm> {

    /**
     * The operation processor to merge the answers and to produce the final answer.
     */
    protected final AnswerProvider<TAnswer> answerProvider;

    /**
     * Create a new bucket navigation processor.
     * The processor is {@link #isQueueClosed() closed} and contains only the specified buckets.
     * No additional buckets can be added.
     *
     * @param mergingProcessor the operation processor to be executed on each bucket
     * @param algorithms       the buckets on which to process
     */
    public AlgorithmsOperationProcessor(TRequest operation, AnswerProvider<TAnswer> mergingProcessor, Collection<Algorithm> algorithms) {
        super(operation, algorithms);
        this.answerProvider = mergingProcessor;
    }

    /**
     * Create a new bucket navigation processor.
     * The processor does not contain any buckets and the processing
     * will block in method {@link #processStep()}. Additional buckets must be
     * added via {@link #addProcessingItem} methods and then {@link #queueClose() closed}
     * in order to be able to finish the processing.
     *
     * @param answerProvider the operation processor to be executed on each bucket
     */
    public AlgorithmsOperationProcessor(TRequest operation, AnswerProvider<TAnswer> answerProvider) {
        super(operation);
        this.answerProvider = answerProvider;
    }


    @Override
    protected void processItem(Algorithm algorithm) throws AlgorithmMethodException {
        try {
            final AbstractAnswer partialAnswer = algorithm.evaluate(operation);
            if (partialAnswer != null)
                answerProvider.updateAnswer(partialAnswer);
        } catch (Exception e) {
            throw new AlgorithmMethodException(e);
        }
    }

    @Override
    public TAnswer finish() {
        return answerProvider.getAnswer();
    }
}
