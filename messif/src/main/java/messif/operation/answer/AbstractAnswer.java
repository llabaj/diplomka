/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.answer;

import messif.operation.RecordProxy;
import messif.record.ModifiableRecord;
import messif.record.ModifiableRecordImpl;
import messif.utility.ProcessingStatus;
import messif.utility.net.HttpStatusCodeProvider;
import messif.utility.proxy.ProxyConverter;
import messif.utility.proxy.annotations.Get;

import javax.annotation.Nonnull;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface AbstractAnswer extends RecordProxy {

    // region  ***********************    Generic ProcessingStatus codes   used by all operations   ******************

    /** Generic OK status. */
    ProcessingStatus OPERATION_OK = new ProcessingStatus("OK", 200, HttpStatusCodeProvider.STATUS_CODE_OK);

    /** This status code means that the search (query) answer is empty, but no error was signaled. */
    ProcessingStatus EMPTY_ANSWER = new ProcessingStatus("empty answer returned", 201, HttpStatusCodeProvider.STATUS_CODE_OK);

    /** The processing was aborted by the user. */
    ProcessingStatus ABORTED_BY_USER = new ProcessingStatus("aborted by user", 401, HttpStatusCodeProvider.STATUS_CODE_INTERNAL_ERROR);

    /** The processing of the operation was aborted by unknown error. */
    ProcessingStatus ABORTED_BY_ERROR = new ProcessingStatus("aborted by error", 402, HttpStatusCodeProvider.STATUS_CODE_INTERNAL_ERROR);

    /** The query object was wrong or empty. */
    ProcessingStatus QUERY_OBJ_ERROR = new ProcessingStatus("wrong query record", 403, HttpStatusCodeProvider.STATUS_CODE_INVALID_ARGUMENT);

    // endregion


    String STATUS_FIELD = "status";

    @Get(field = STATUS_FIELD)
    ProcessingStatus getStatus();

    /**
     * Returns <tt>true</tt> if this operation has canFinish its processing - either successfully or unsuccessfully.
     * In other words, <tt>true</tt> is returned if the error code of this operation is set.
     * If the operation has not canFinish yet (i.e. the error code is {@link ProcessingStatus#NOT_SET not set},
     * <tt>false</tt> is returned.
     *
     * @return <tt>true</tt> if this operation has canFinish its processing
     */
    default boolean isFinished() {
        return getStatus().isSet();
    }

    default boolean wasSuccessful() {
        return isFinished() && getStatus().equals(OPERATION_OK);
    }

    static ModifiableRecord createMap(ProcessingStatus status) {
        ModifiableRecord fieldMap = new ModifiableRecordImpl();
        fieldMap.setField(STATUS_FIELD, status);
        return fieldMap;
    }

    @Nonnull
    static AbstractAnswer createAnswer(ProcessingStatus status) {
        return createAnswer(status, AbstractAnswer.class);
    }

    @Nonnull
    static AbstractAnswer createAnswer() {
        return createAnswer(AbstractAnswer.class);
    }

    @Nonnull
    static <TAnswer extends AbstractAnswer> TAnswer createAnswer(Class<TAnswer> answerClass) {
        return createAnswer(ProcessingStatus.NOT_SET, answerClass);
    }

    @Nonnull
    static <TAnswer extends AbstractAnswer> TAnswer createAnswer(ProcessingStatus status, Class<TAnswer> answerClass) {
        return ProxyConverter.convert(createMap(status), answerClass);
    }
}
