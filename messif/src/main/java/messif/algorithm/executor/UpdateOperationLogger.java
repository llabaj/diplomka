/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.algorithm.executor;

import messif.algorithm.Algorithm;
import messif.algorithm.AlgorithmMethodException;
import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.crud.ModifyingOperation;
import messif.operation.processing.OperationExecutor;
import messif.record.Record;
import messif.utility.ExtendedProperties;

import java.util.Iterator;
import java.util.logging.Level;


/**
 * Management of the REDO operation log that is applied on the {@link UpdateOperationLogger}
 */
public class UpdateOperationLogger extends ExecutorChainItem {

    public static final String CONFIG_LOG_FILE_NAME = "redolog.filename";

    protected String logFileName;

    /**
     * Operation log for update operations.
     */
    private transient UpdateOperationLog updateOperationLog;


    @Override
    public void init(ExtendedProperties properties, Algorithm algorithm) throws AlgorithmMethodException {
        super.init(properties, algorithm);
        this.logFileName = properties.getRequiredProperty(CONFIG_LOG_FILE_NAME);
        setAndReplayLog(algorithm);
    }

    @Override
    public AbstractAnswer execute(AbstractOperation operation, Iterator<OperationExecutor> iterator) throws Exception {
        if (operation instanceof ModifyingOperation && updateOperationLog != null)
            updateOperationLog.log((ModifyingOperation) operation);
        // do not call executeNext() since it is called in the predecessor.
        // Change this if this class does not extend RunningOperationMonitor
        return executeNext(operation, iterator);
    }

    /**
     * Start using the update operation log {@link UpdateOperationLog} and use given file;
     *  if the file contains some operations, they are applied to this algorithm.
     * @throws AlgorithmMethodException if the log is already set and used
     */
    protected void setAndReplayLog(Algorithm handler) throws AlgorithmMethodException {
////        if (maximalConcurrentOperations > 0) {
////            updateOperationsSemaphore.acquireUninterruptibly(maximalConcurrentOperations);
////        } else {
////            throw new AlgorithmMethodException("cannot use the update log without the update semaphore");
////        }
//        try {
        if (! isUpdateLogEmpty()) {
            throw new AlgorithmMethodException("the update operation log is already set and not empty (execute Algorithm.storeToFile() first to clear the log)");
        }
        if (logFileName == null) {
            this.updateOperationLog = null;
            return;
        }
        UpdateOperationLog createdLog = new UpdateOperationLog(logFileName);
        try {
            Iterator<Record> operations = createdLog.readOperations();
            // replay the log
            if (operations != null) {
                Algorithm.log.log(Level.WARNING, "replaying the update operation log for: {0}", algorithmName);
                while (operations.hasNext()) {
                    Record operation = operations.next();
                    handler.evaluate(operation);
                    //Algorithm.log.log(Level.INFO, "{0} processed: {1}", new Object[]{algorithmName, operation});
                }
            }
            this.updateOperationLog = createdLog;
        } catch (Exception ex) {
            Algorithm.log.log(Level.SEVERE, null, ex);
        }
//        } finally {
//            updateOperationsSemaphore.release(maximalConcurrentOperations);
//        }
    }

    /**
     * Checks whether the update operation log is used (it is set) but there is no update operation
     *  written there. If true, the extending algorithm can be sure that there was no update operation
     *  since last serialization of the algorithm.
     * @return true only if the {@link #updateOperationLog} is used (not null) and {@link UpdateOperationLog#isUsed() }.
     */
    public boolean isUpdateLogEmpty() {
        return updateOperationLog == null || ! updateOperationLog.isUsed();
    }

    public void clearLog() {
        if (! isUpdateLogEmpty()) {
            updateOperationLog.clearLog();
        }
    }

}
