/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.objects.impl;

import java.io.IOException;
import java.util.Collection;
import junit.framework.TestCase;
import messif.objects.DistanceFunction;
import messif.objects.LocalAbstractObject;
import messif.objects.MetaObject;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class MetaObjectFixedMapTest extends TestCase {
    
    public MetaObjectFixedMapTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    /**
     * Test of getObject method, of class MetaObjectFixedMap.
     */
    public void testGetObject() throws IOException {
        ObjectFloatVectorL2 object1 = new ObjectFloatVectorL2(new float [] {1f, 2f});
        ObjectFloatVectorL2 object2 = new ObjectFloatVectorL2(new float [] {1f, 2f});
        MetaObjectFixedMap metaObjectFixedMap = new MetaObjectFixedMap(new String [] {"caffe", "other"}, new LocalAbstractObject[] {object1, object2}, "key");
        
        ObjectString objStr = new ObjectString("string");
        MetaObjectFixedMap changed = new MetaObjectFixedMap(metaObjectFixedMap, objStr, "caffe");
        changed.write(System.out);
    }
    
}
