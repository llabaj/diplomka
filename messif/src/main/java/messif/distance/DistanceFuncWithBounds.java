/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.distance;

/**
 * @param <T> the type of the distance function arguments
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface DistanceFuncWithBounds<T> extends DistanceFunc<T> {
    
    /**
     * Returns the upper bound of a distance between two objects without evaluating it.
     *
     * @param o1 the 1st object to compute the upper-bound distance
     * @param o2 the 2nd object to compute the upper-bound distance
     * @return the upper bound of the distance between the passed arguments
     */
    float getDistanceUpperBound(T o1, T o2);
    
    /**
     * Returns the lower bound of a distance between two objects without evaluating it.
     *
     * @param o1 the 1st object to compute the upper-bound distance
     * @param o2 the 2nd object to compute the upper-bound distance
     * @return the lower bound of the distance between the passed objects
     */
    float getDistanceLowerBound(T o1, T o2);
    
}
