package com.me.diplomka.fixedpivotswebapp.domain;

public class FilePto {
    private String file;

    public FilePto(String file) {
        this.file = file;
    }

    public String getFile() {
        return file;
    }

    @Override
    public String toString() {
        return "FilePto{" +
                "file='" + file + '\'' +
                '}';
    }
}
