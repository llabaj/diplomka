package com.me.diplomka.fixedpivots;

import com.hazelcast.core.IMap;
import messif.data.DataObject;
import messif.distance.DataObjectDistanceFunc;
import messif.distance.impl.L2DistanceFloats;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static com.hazelcast.query.Predicates.*;

public class KNNService {
    private IMap<String, IndexedDataObject> data;
    private PivotService pivotService;
    private DataObjectDistanceFunc<float[]> distanceFunc;
    private final int candidateSetThreshold;

    public KNNService(IMap<String, IndexedDataObject> data, PivotService pivotService, int candidateSetThreshold) {
        this.pivotService = pivotService;
        this.data = data;
        this.candidateSetThreshold = candidateSetThreshold;
        data.addIndex("pivotPrefix", false);
        data.addIndex("tags[any]", false);
        distanceFunc = new DataObjectDistanceFunc<>("descriptor_256", new L2DistanceFloats());
    }

    public List<Pair<DataObject, Float>> search(DataObject query, int limit, int candidateSet, String... tags) {
        Objects.requireNonNull(query);
        var candidatePP = pivotService.findCandidatePivotPrefixes(query, candidateSet, tags);

        var predicate = in("pivotPrefix", candidatePP);

        if (tags.length > 0) {
            var tagsPredicate = Stream.of(tags)
                    .map(tag -> equal("tags[any]", tag))
                    .reduce((acc, pred) -> and(acc, pred))
                    .orElseThrow();
            predicate = and(predicate, tagsPredicate);
        }

        return data.aggregate(new KNNAggregator(query, limit, distanceFunc), predicate);
    }

    public List<Pair<IndexedDataObject, Float>> fullScan(DataObject query, int limit, String... tags) {
        Objects.requireNonNull(query);
        if (tags.length > 0) {
            var tagsPredicate = Stream.of(tags)
                    .map(tag -> equal("tags[any]", tag))
                    .reduce((acc, pred) -> and(acc, pred))
                    .orElseThrow();
            return data.aggregate(new KNNAggregator(query, limit, distanceFunc), tagsPredicate);
        } else {
            return data.aggregate(new KNNAggregator(query, limit, distanceFunc));
        }
    }
}
