/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.objects;

import messif.data.DataObject;
import messif.utility.json.JSONWriter;
import messif.data.util.StreamDataObjectIterator;

/**
 * This class simply reads in the data file and prints the IDs of the objects.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class CheckData {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        args = new String [2];
        args[0] = "src/test/resources/imagedata.json";
        args[1] = "-print";
        if (args.length < 2) {
            System.err.println("Usage: CheckData <datafile>[.gz] [-print]");
            return;
        }
        try (JSONWriter outWriter = new JSONWriter(System.out, false)) {

            StreamDataObjectIterator iterator = new StreamDataObjectIterator(args[0]);
            boolean printOutput = ((args.length > 1) && ("-print".equals(args[1])));
            
            long counter = 0;
            while (iterator.hasNext()) {
                DataObject obj = iterator.next();
                if (printOutput) {
                    obj.writeJSON(outWriter);
                    System.out.println();
                }
                counter ++;
            }
            System.out.println("Finally processed " + counter + " objects");

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
