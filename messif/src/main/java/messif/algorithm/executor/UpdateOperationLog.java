/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.algorithm.executor;

import messif.algorithm.AlgorithmMethodException;
import messif.operation.AbstractOperation;
import messif.operation.crud.ModifyingOperation;
import messif.record.ModifiableRecord;
import messif.record.Record;
import messif.utility.json.JSONWriter;
import messif.utility.json.RecordIterator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Operation log to keep track of all update operations that are processed on given algorithm.
 * This class is not serializable and is expected to be created always with a provided file name.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class UpdateOperationLog {
    
    /** The name of the operation log file. */
    private final File logFile;
    
    /** Opened storage with the log. */
    //private DiskStorage<Record> log;
    private JSONWriter log;

    private ReadWriteLock lock = new ReentrantReadWriteLock();

    /** 
     * Constructor given a file name to store the operation log to.
     * @param logFileName name of the file to store the log to (the file can exist)
     */
    public UpdateOperationLog(String logFileName) {
        this.logFile = new File(logFileName);
    }
    
    /**
     * Test if the log is opened and open it if not.
     * @throws IOException 
     */
    private void initLog() throws IOException {
        if (log != null) {
            return;
        }
        //synchronized (this) {
            if (log == null) {
                log = new JSONWriter(new FileOutputStream(logFile, true), false);
//                log = new DiskStorage(Record.class, logFile, false, 16*1024, false, 1,
//                        0L, Long.MAX_VALUE, new ObjectSerializator());//, new MultiClassSerializator(ModifyingOperation.class));
            }
        //}
    }
    
    /**
     * Stores given operation to the disk operation log.
     * @param operation operation to store
     * @return true if the operation was actually stored, false otherwise
     */
    public boolean log(ModifyingOperation operation) {
        lock.writeLock().lock();
        try {
            initLog();
            // ensure that the operation has field "_type"
            ModifiableRecord params = operation.getParams();
            if (! params.containsField(AbstractOperation.TYPE_FIELD)) {
                params.setField(AbstractOperation.TYPE_FIELD, operation.getType());
            }
            log.write(params);
            log.write('\n');
            //log.store(params);
            log.flush();
            return true;
        } catch (IOException ex) {
            Logger.getLogger(UpdateOperationLog.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            lock.writeLock().unlock();
        }
    }
    
    /**
     * Read all operations from the log and returns an "iterator" over these operations.
     * @return MESSIF search object with the update operations or null, if the log is empty
     * @throws AlgorithmMethodException
     * @throws NoSuchMethodException 
     */
    public Iterator<Record> readOperations() throws AlgorithmMethodException, NoSuchMethodException {
        lock.readLock().lock();
        try {
            initLog();
            if (logFile.length() > 0) {
                return new RecordIterator(logFile);
                //return log.search();
            }
            return null;
        } catch (IOException ex) {
            Logger.getLogger(UpdateOperationLog.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            lock.readLock().unlock();
        }
    }
    
    /**
     * Clear existing operation log, remove the file and set the {@link #log} to null.
     */
    public void clearLog() {
        lock.writeLock().lock();
        try {
            if (log != null) {
                try {
                    log.close();
                    logFile.delete();
                    //log.destroy();
                } catch (Throwable ex) {
                    Logger.getLogger(UpdateOperationLog.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            log = null;
        } finally {
            lock.writeLock().unlock();
        }
    }
    
    /**
     * Return true if the log is open and there is some operation stored in the log.
     * @return true/false if the log is actually used
     */
    public boolean isUsed() {
        //return log != null && log.size() > 0;
        return log != null && logFile.length() > 0;
    }
    
}
