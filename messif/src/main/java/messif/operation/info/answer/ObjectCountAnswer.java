/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.info.answer;

import messif.operation.answer.AbstractAnswer;
import messif.record.ModifiableRecord;
import messif.record.Record;
import messif.utility.ProcessingStatus;
import messif.utility.proxy.ProxyConverter;
import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Required;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface ObjectCountAnswer extends AbstractAnswer {

    /** Name of a {@link Record} (JSON) field to contain the number of objects. */
    String ANSWER_OBJ_COUNT_FIELD = "answer_count";

    /**
     * This method provides a convenient access to the field with name {@link #ANSWER_OBJ_COUNT_FIELD}.
     * @return the number of objects returned by the operation
     */
    @Get(field = ANSWER_OBJ_COUNT_FIELD)
    @Required
    int getAnswerCount();


    @Override
    default boolean wasSuccessful() {
        return getStatus().in(AbstractAnswer.OPERATION_OK);
    }

    static ObjectCountAnswer createFailed(ProcessingStatus status) {
        return createFailed(status, ObjectCountAnswer.class);
    }

    static <TAnswer extends ObjectCountAnswer> TAnswer createFailed(ProcessingStatus status, Class<TAnswer> answerClass) {
        ModifiableRecord fieldMap = AbstractAnswer.createMap(status);
        fieldMap.setField(ANSWER_OBJ_COUNT_FIELD, -1);
        return ProxyConverter.convert(fieldMap, answerClass);
    }

    static ObjectCountAnswer createSuccessful(long objectCount) {
        ModifiableRecord fieldMap = AbstractAnswer.createMap(AbstractAnswer.OPERATION_OK);
        fieldMap.setField(ANSWER_OBJ_COUNT_FIELD, objectCount);
        return ProxyConverter.convert(fieldMap, ObjectCountAnswer.class);
    }

}
