/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.crud.answer;

import messif.utility.net.HttpStatusCodeProvider;
import messif.utility.ProcessingStatus;

/**
 * Answer with statuses specific to insert operation.
 */
public interface InsertAnswer extends CRUDAnswer {

    /**
     * Object has been successfully inserted causing no capacity overflow.
     */
    ProcessingStatus OBJECT_INSERTED = new ProcessingStatus("records inserted", 210, HttpStatusCodeProvider.STATUS_CODE_CREATED);

    /** Some object have been successfully inserted, but some failed. */
    ProcessingStatus SOME_OBJECTS_INSERTED = new ProcessingStatus("some records inserted", 211, HttpStatusCodeProvider.STATUS_CODE_PARTIAL);

    /** Object has been inserted but the soft-capacity has been reached. Overflow of the hard-capacity is reported as a CapacityFullException exception. */
    ProcessingStatus SOFTCAPACITY_EXCEEDED = new ProcessingStatus("soft capacity exceeded", 212, HttpStatusCodeProvider.STATUS_CODE_CREATED);


    /** Object cannot be inserted due to some limits of structure. */
    ProcessingStatus OBJECT_REFUSED = new ProcessingStatus("records refused", 410, HttpStatusCodeProvider.STATUS_CODE_INTERNAL_ERROR);

    /** Objects were not inserted because its copy is already present. */
    ProcessingStatus OBJECT_DUPLICATE = new ProcessingStatus("record duplicate", 411, HttpStatusCodeProvider.STATUS_CODE_CONFLICT);

    /** Object was not inserted because the hard-capacity has been exceeded. This is usually reported as a CapacityFullException exception, but it can caught, so this error code allows it to be reported. */
    ProcessingStatus HARDCAPACITY_EXCEEDED = new ProcessingStatus("hard capacity exceeded", 412, HttpStatusCodeProvider.STATUS_CODE_INTERNAL_ERROR);


    @Override
    default boolean wasSuccessful() {
        return getStatus().in(OBJECT_INSERTED, SOFTCAPACITY_EXCEEDED, SOME_OBJECTS_INSERTED);
    }
}
