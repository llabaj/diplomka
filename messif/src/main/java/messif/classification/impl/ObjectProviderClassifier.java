/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.classification.impl;

import messif.classification.Classification;
import messif.classification.ClassificationException;
import messif.classification.Classifier;
import messif.distance.DistanceFunc;
import messif.data.DataObject;
import messif.data.util.RankedDataObject;
import messif.record.Record;

import java.util.Iterator;

/**
 * Implementation of a classifier that computes the classification using a {@link Iterable provider}
 * of a classified set by measuring distances. The actual classification is computed
 * from the distances by a given {@link Classifier}.
 *
 * @param <C> the class of instances that represent the classification categories
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class ObjectProviderClassifier<C> implements Classifier<DataObject, C> {

    /** Classifier used to compute the object classification */
    private final Classifier<? super Iterator<? extends RankedDataObject>, C> classifier;
    /** Provider of the classified objects */
    private final Iterable<? extends DataObject> classifiedObjects;
    /** Distance function to compute distances between given object to classify and all data objects. */
    private final DistanceFunc<? super Record> distanceFunc;

    /**
     * Creates a new kNN classifier.
     * @param classifier the classifier used to compute the object classification
     * @param classifiedObjects the provider of the classified objects
     */
    public ObjectProviderClassifier(Classifier<? super Iterator<? extends RankedDataObject>, C> classifier, Iterable<? extends DataObject> classifiedObjects, DistanceFunc<? super Record> distanceFunc) {
        this.classifier = classifier;
        this.classifiedObjects = classifiedObjects;
        this.distanceFunc = distanceFunc;
    }

    @Override
    public Classification<C> classify(final DataObject object, Record parameters) throws ClassificationException {
        final Iterator<? extends DataObject> iterator = classifiedObjects.iterator();
        return classifier.classify(new Iterator<RankedDataObject>() {
            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }
            @Override
            public RankedDataObject next() {
                return new RankedDataObject(object, distanceFunc, iterator.next());
            }
            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported.");
            }
        }, parameters);
    }

    @Override
    public Class<? extends C> getCategoriesClass() {
        return classifier.getCategoriesClass();
    }

}
