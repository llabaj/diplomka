/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.indexing.pivotselection;

import messif.data.DataObject;
import messif.data.util.DataObjectIterator;
import messif.data.util.DataObjectList;
import messif.distance.DistanceFunc;
import messif.indexing.IndexObject;
import messif.indexing.PrecompDistPerforatedArrayFilter;
import messif.utility.ListUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * This class uses the k-means algorithm adapted for metric spaces to cluster the objects
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class KMeansPivotChooser extends AbstractPivotChooser {
    
    /** Size of the sample set to select a pivot from in each iteration of the k-means */
    public static final int PIVOTS_SAMPLE_SIZE = 1000;
    
    /** Threshold to consider 2 pivots the same */
    public static final float PIVOTS_DISTINCTION_THRESHOLD = 0.1f;
    
    /** Maximal number of iterations to let run */
    public static final int MAX_ITERATIONS = 100;

    /** Name of the filter parameter used to store the object-pivot distances. */
    public static final String FILTER = "k_means_pivot_chooser_filter".intern();


    /** List of initial pivots */
    protected List<IndexObject> initialPivots;
    
    protected List<List<IndexObject>> actualClusters;

    /**
     * Creates a new instance of KMeansPivotChooser with empty initial list of pivots.
     */
    public KMeansPivotChooser(DistanceFunc<DataObject> distanceFunc) {
        this(null, distanceFunc);
    }
    
    /**
     * Creates a new instance of KMeansPivotChooser.
     * @param initialPivots the list of initial pivots
     */
    public KMeansPivotChooser(DataObjectList initialPivots, DistanceFunc<DataObject> distanceFunc) {
        super(distanceFunc);
        this.initialPivots = IndexObject.convertObjects(initialPivots);
    }

    public List<List<IndexObject>> getClusters() {
        return actualClusters;
    }
    
    /**
     *  This method only uses the preselected pivots as initial pivots for k-means and rewrites the pivots completely
     */
    @Override
    protected void selectPivot(int count, DataObjectIterator sampleSetIterator) {
        
        // Store all passed objects temporarily
        List<IndexObject> objectList = IndexObject.convertObjects(new DataObjectList(sampleSetIterator));
        
        List<IndexObject> pivots = new ArrayList(count);
        // initially select "count" pivots at random - or use (partly) preselected pivots
        if (initialPivots != null) {
            for (IndexObject preselPivot : initialPivots) {
                if (count > pivots.size()) {
                    pivots.add(preselPivot);
                    System.err.println("Adding preselected pivot: "+preselPivot.getDataObject().getID());
                }
            }
        }
        if (count > pivots.size()) {
            System.err.println("Selecting: "+(count - pivots.size()) +" pivots at random");
            pivots.addAll(ListUtils.randomList(objectList, count - pivots.size(), true));
        }
        //printPivots("Initial pivots:", pivots);
        
        boolean continueKMeans = true;
        
        // one step of the k-means algorithm
        int nIterations = 0;
        while (continueKMeans && (nIterations++ < MAX_ITERATIONS)) {
            System.err.println("Running "+nIterations+"th iteration");
            System.err.print("    Voronoi partitioning... ");
            actualClusters = voronoiLikePartitioning(objectList, pivots);
            System.err.println("done");
            StringBuffer buf = new StringBuffer("       cluster sizes:");
            for (List<IndexObject> cluster : actualClusters) {
                buf.append(" ").append(cluster.size());
            }
            System.err.println(buf.toString());
            
            System.err.println("    Selecting clustroids...");
            // now calculate the new pivots for the new clusters
            int i = 0;
            List<SelectClustroidThread> selectingThreads = new ArrayList<SelectClustroidThread>();
            for (List<IndexObject> cluster : actualClusters) {
                SelectClustroidThread thread = new SelectClustroidThread(cluster, pivots.get(i++), distanceFunc);
                thread.start();
                selectingThreads.add(thread);
            }
            
            i = 0;
            continueKMeans = false;
            for (SelectClustroidThread thread : selectingThreads) {
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (thread.clustroid == null) {
                    System.err.println("        WARNING: no clustroid selected - empty cluster?: "+ actualClusters.get(i).size());
                    //System.out.println("          selecting the pivot at random");
                    continueKMeans = true;
                } else {
                    float pivotShiftDist = distanceFunc.getDistance(pivots.get(i).getDataObject(), thread.clustroid.getDataObject());
                    if (pivotShiftDist > PIVOTS_DISTINCTION_THRESHOLD) {
                        System.err.println("        pivot "+ i +" shifted by "+pivotShiftDist);
                        pivots.set(i, thread.clustroid);
                        continueKMeans = true;
                    }
                }
                i++;
            }
            //printPivots("Current pivots:", pivots);
        }
        
        //this.preselectedPivots.clear();
        for (IndexObject pivot : pivots)
            preselectedPivots.add(pivot.getDataObject());
        
    }

    /**
     * Prints all pivots selected by this chooser. Pivots are printed to <code>System.err</code>.
     * @param msg optional message printed before the pivots
     */
    public void printPivots(String msg) {
        printPivots(msg, preselectedPivots);
    }
    
    private void printPivots(String msg, List<DataObject> pivots) {
        if (msg != null)
            System.err.println(msg);
        int i = 0;
        for (DataObject p : pivots) {
            System.err.println("Pivot " + (++i) + ": " + p);
        }
    }

    /** Given a set of objects, a set of pivots, */
    private List<List<IndexObject>> voronoiLikePartitioning(List<IndexObject> objects, List<IndexObject> pivots) {
        List<List<IndexObject>> clusters =  new ArrayList(pivots.size());
        
        // precompute the mutual distances between the pivots
        for (IndexObject pivot : pivots) {
            PrecompDistPerforatedArrayFilter filter = pivot.getIndexField(FILTER, PrecompDistPerforatedArrayFilter.class);
            if (filter == null) {
                filter = new PrecompDistPerforatedArrayFilter(distanceFunc, pivots.size());
                pivot.setIndexField(FILTER, filter);
            } else filter.resetAllPrecompDist();
            
            for (IndexObject pivot2 : pivots) {
                filter.addPrecompDist(distanceFunc.getDistance(pivot.getDataObject(), pivot2.getDataObject()));
            }
            //  init the cluster for this pivot
            clusters.add(new ArrayList<>());
        }
        
        for (IndexObject object : objects) {
            PrecompDistPerforatedArrayFilter filter = object.getIndexField(FILTER, PrecompDistPerforatedArrayFilter.class);
            if (filter == null) {
                filter = new PrecompDistPerforatedArrayFilter(distanceFunc, pivots.size());
                object.setIndexField(FILTER, filter);
            } else filter.resetAllPrecompDist();
            
            float minDistance = Float.MAX_VALUE;
            int i = 0;
            int closestPivot = -1;
            float objPivotDist;
            for (IndexObject pivot : pivots) {
                if (object.excludeUsingPrecompDist(pivot, minDistance))
                    filter.addPrecompDist(DistanceFunc.UNKNOWN_DISTANCE);
                else {
                    objPivotDist = distanceFunc.getDistance(object.getDataObject(), pivot.getDataObject());
                    filter.addPrecompDist(objPivotDist);
                    if (minDistance > objPivotDist) {
                        closestPivot = i;
                        minDistance = objPivotDist;
                    }
                }
                i++;
            }
            clusters.get(closestPivot).add(object);
        }
        
        return clusters;
    }
    
    /** Internal thread for selecting the "center" of a cluster. */
    protected class SelectClustroidThread extends Thread {
        
        // parameter
        List<IndexObject> cluster;
        
        // always try the original pivot - put it to the sample pivots list
        IndexObject originalPivot;
        
        // result
        IndexObject clustroid;

        // distance function
        DistanceFunc<DataObject> distanceFunc;
        
        /**
         * Creates a new SelectClustroidThread for computing the "center" of a cluster.
         * @param cluster the list of objects that form a cluster
         * @param originalPivot the original pivot that is improved
         */
        protected SelectClustroidThread(List<IndexObject> cluster, IndexObject originalPivot, DistanceFunc<DataObject> distanceFunc) {
            this.cluster = cluster;
            this.originalPivot = originalPivot;
            this.distanceFunc = distanceFunc;
        }
        
        @Override
        public void run() {
            List<IndexObject> samplePivots = ListUtils.randomList(cluster, PIVOTS_SAMPLE_SIZE, true);
            samplePivots.add(this.originalPivot);
            
            double minRowSum = Double.MAX_VALUE;
            for (IndexObject pivot : samplePivots) {
                double rowSum = 0;
                for (IndexObject object : cluster) {
                    rowSum += Math.pow(distanceFunc.getDistance(pivot.getDataObject(), object.getDataObject()), 2);
                }
                if (minRowSum > rowSum) {
                    clustroid = pivot;
                    minRowSum = rowSum;
                }
            }
        }
    }
}
