package com.me.diplomka.fixedpivotswebapp;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.ReplicatedMap;
import com.me.diplomka.fixedpivots.IndexedDataObject;
import com.me.diplomka.fixedpivots.KNNService;
import com.me.diplomka.fixedpivots.PivotService;
import com.me.diplomka.fixedpivots.TagService;
import messif.data.DataObject;
import org.aeonbits.owner.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Configuration
public class Beans {
    private static final Logger logger = LoggerFactory.getLogger(Beans.class);

    @Autowired
    private HazelcastInstance hazelcast;

    @Bean
    public AppConfig config() {
        var configPath = System.getProperty("app.config");
        var props = new Properties();

        if (configPath != null) {
            logger.debug("loading configuration from {}", configPath);
            try (var stream = new FileInputStream(configPath)){
                props.load(stream);
            } catch (IOException e) {
                logger.error("loading configuration failed, falling back to defaults", e);
            }
        } else {
            logger.debug("app.config not set, using default configuration");
        }
        var appConfig = ConfigFactory.create(AppConfig.class, props);
        logger.debug("appconfig: {}", appConfig);
        return appConfig;
    }

    @Bean
    public IMap<String, IndexedDataObject> dataMap() {
        return hazelcast.getMap(config().dataMap());
    }

    @Bean
    public KNNService knnService(){
        return new KNNService(dataMap(), pivotService(), config().candidateSetThreshold());
    }

    @Bean
    public PivotService pivotService() {
        return new PivotService(pivotMap(), pivotPrefixCountsMap(), config().prefixLength(), tagService());
    }

    @Bean
    public ReplicatedMap<String, DataObject> pivotMap() {
        return hazelcast.getReplicatedMap(config().pivotMap());
    }

    @Bean
    public IMap<String, Integer> pivotPrefixCountsMap() {
        return hazelcast.getMap(config().pivotPrefixCountsMap());
    }

    @Bean
    public TagService tagService() {
        return new TagService(pivotPrefixTagsMap(), config().tagIntersectCoefficient());
    }

    @Bean
    public ReplicatedMap<String, Integer> pivotPrefixTagsMap() {
        return hazelcast.getReplicatedMap(config().pivotPrefixTagsMap());
    }
}
