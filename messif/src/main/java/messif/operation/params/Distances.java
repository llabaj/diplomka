/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.params;

import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Required;
import messif.record.Record;

/**
 * An interface mostly for answers that contains a list of ranking scores (floats),
 *  typically query-object distances. These distances are stored in a separate array.
 */
public interface Distances {

    /**
     * Name of a {@link Record} (JSON) field to contain the list of distances that ranked the objects.
     */
    String ANSWER_DISTANCES_FIELD = "answer_distances";

    /**
     * This method provides a convenient access to the field with name {@link #ANSWER_DISTANCES_FIELD}.
     * @return an array of float distances
     */
    @Get(field = ANSWER_DISTANCES_FIELD)
    @Required
    float [] getAnswerDistances();

}
