/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.bucket.index;

import messif.data.DataObject;
import messif.operation.OperationBuilder;
import messif.operation.ReturnDataOperation;
import messif.operation.crud.GetObjectsOperation;
import messif.operation.search.GetObjectsByIDPrefixOperation;
import messif.record.RecordImpl;

import java.io.Serializable;
import java.util.Collection;

/**
 * Default orders of {@link DataObject} based on attributes.
 * Specifically, order can be defined on object IDs, locators, data or keys.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public enum LocalAbstractObjectOrder implements IndexComparator<DataObject, DataObject>, Serializable {
    /**
     * Order defined by object locator URIs
     */
    ID,
    /**
     * Order defined by object data hash codes
     */
    DATA;

    @Override
    public int indexCompare(DataObject o1, DataObject o2) {
        switch (this) {
            case ID:
                return o1.getID().compareTo(o2.getID());
            case DATA:
                int cmp = o1.dataHashCode() - o2.dataHashCode();
                return cmp;
            default:
                throw new InternalError("Compare method is not implemented for order " + this);
        }
    }

    @Override
    public int compare(DataObject o1, DataObject o2) {
        return indexCompare(o1, o2);
    }

    @Override
    public DataObject extractKey(DataObject object) {
        return object;
    }

    /**
     * Index order defined by object locators
     */
    public static final OperationIndexComparator<String> idToObjectComparator = new OperationIndexComparator<String>() {
        /** Class serial id for serialization. */
        private static final long serialVersionUID = 25103L;

        @Override
        public int indexCompare(String o1, DataObject o2) {
            return compare(o1, o2.getID());
        }

        @Override
        public int compare(String o1, String o2) {
            return o1.compareTo(o2);
        }

        @Override
        public String extractKey(DataObject object) {
            return object.getID();
        }

        @Override
        public ReturnDataOperation createIndexOperation(Collection<? extends String> locators) {
            final DataObject[] objectIDs = new DataObject[locators.size()];
            int i = 0;
            for (String id : locators) {
                objectIDs[i++] = new RecordImpl(new String[]{DataObject.ID_FIELD}, new Object[]{id});
            }
            return OperationBuilder.create(GetObjectsOperation.class).addParam(GetObjectsOperation.OBJECTS_FIELD, objectIDs).build();
        }

        @Override
        public boolean equals(Object obj) {
            return obj.getClass() == this.getClass();
        }

        @Override
        public int hashCode() {
            return getClass().hashCode();
        }
    };

    /**
     * Index order defined by object locator prefixes
     */
    public static final OperationIndexComparator<String> locatorPrefixToLocalObjectComparator = new OperationIndexComparator<String>() {
        /** Class serial id for serialization. */
        private static final long serialVersionUID = 25104L;

        @Override
        public int indexCompare(String o1, DataObject o2) {
            return compare(o1, o2.getID());
        }

        @Override
        public int compare(String o1, String o2) {
            if (o2.startsWith(o1))
                return 0;
            return o1.compareTo(o2);
        }

        @Override
        public String extractKey(DataObject object) {
            return object.getID();
        }

        @Override
        public ReturnDataOperation createIndexOperation(Collection<? extends String> locators) {
            if (locators.size() != 1)
                throw new UnsupportedOperationException("Prefix locator operation works only on one locator");
            return OperationBuilder.create(GetObjectsByIDPrefixOperation.class)
                    .addParam(GetObjectsByIDPrefixOperation.ID_PREFIX_FIELD, locators.iterator().next()).build();
        }

        @Override
        public boolean equals(Object obj) {
            return obj.getClass() == this.getClass();
        }

        @Override
        public int hashCode() {
            return getClass().hashCode();
        }
    };
//
//    /** Index order defined by object keys */
//    public static final IndexComparator<AbstractObjectKey, DataObject> keyToLocalObjectComparator = new IndexComparator<AbstractObjectKey, DataObject>() {
//        /** Class serial id for serialization. */
//        private static final long serialVersionUID = 25104L;
//
//        @Override
//        public int indexCompare(AbstractObjectKey o1, DataObject o2) {
//            return compare(o1, o2.getObjectKey());
//        }
//
//        @Override
//        public int compare(AbstractObjectKey o1, AbstractObjectKey o2) {
//            return o1.compareTo(o2);
//        }
//
//        @Override
//        public AbstractObjectKey extractKey(DataObject object) {
//            return object.getObjectKey();
//        }
//
//        @Override
//        public boolean equals(Object obj) {
//            return obj.getClass() == this.getClass();
//        }
//
//        @Override
//        public int hashCode() {
//            return getClass().hashCode();
//        }
//    };

    /**
     * Index order defined by the object itself via {@link Comparable} interface.
     * Note that the compare methods can throw {@link ClassCastException}s.
     */
    public static final IndexComparator<Comparable<?>, Object> trivialObjectComparator = new IndexComparator<Comparable<?>, Object>() {
        private static final long serialVersionUID = 25105L;

        @SuppressWarnings("unchecked")
        @Override
        public int indexCompare(Comparable k, Object o) {
            return k.compareTo(o);
        }

        @Override
        public Comparable<?> extractKey(Object object) {
            return (Comparable<?>) object;
        }

        @SuppressWarnings("unchecked")
        @Override
        public int compare(Comparable o1, Comparable o2) {
            return o1.compareTo(o2);
        }

    };

    //****************** Search wrappers ******************//

    /**
     * Search the specified {@code index} for the object with given locator.
     *
     * @param <T>     type of objects stored in the index
     * @param index   the index to search
     * @param locator the locator to search for
     * @return the object or <tt>null</tt> if it was not found in the index
     * @throws IllegalStateException if there was a problem reading objects from the index
     */
    public static <T extends DataObject> T searchIndexByLocator(Index<T> index, String locator) throws IllegalStateException {
        Search<T> search = index.search(idToObjectComparator, locator);
        if (!search.next())
            return null;
        return search.getCurrentObject();
    }

//    /**
//     * Search the specified {@code index} for the object with given key.
//     * @param <T> type of objects stored in the index
//     * @param index the index to search
//     * @param key the key to search for
//     * @return the object or <tt>null</tt> if it was not found in the index
//     * @throws IllegalStateException if there was a problem reading objects from the index
//     */
//    public static <T extends DataObject> T searchIndexByKey(Index<T> index, AbstractObjectKey key) throws IllegalStateException {
//        Search<T> search = index.search(keyToLocalObjectComparator, key);
//        if (!search.next())
//            return null;
//        return search.getCurrentObject();
//    }
}
