/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.processing.impl;

import messif.algorithm.AlgorithmMethodException;
import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;

/**
 * An abstract base of operation processors that have only one step.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @param <TRequest> class of the operation to be processed by the specific processor
 * @param <TAnswer> class of answer returned by this processor
 */
public abstract class OneStepProcessor<TRequest extends AbstractOperation, TAnswer extends AbstractAnswer>
       extends SimpleOperationProcessor<TRequest, TAnswer> {

    /**
     * Creates this processor given the operation to be processed.
     * @param operation operation to be executed
     */
    public OneStepProcessor(TRequest operation) {
        super(operation);
    }

    @Override
    public final boolean processStep() throws Exception {
        if (canFinish) {
            return false;
        }
        canFinish = true;
        process();
        return true;
    }

    /**
     * Do the one processing step.
     * @throws AlgorithmMethodException 
     */
    protected abstract void process() throws Exception;
}
