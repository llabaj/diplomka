package com.me.diplomka.fixedpivotswebapp.controller;

import com.hazelcast.core.IMap;
import com.me.diplomka.fixedpivots.IndexedDataObject;
import com.me.diplomka.fixedpivots.KNNService;
import com.me.diplomka.fixedpivotswebapp.AppConfig;
import com.me.diplomka.fixedpivotswebapp.RandomAggregator;
import com.me.diplomka.fixedpivotswebapp.domain.ImagePto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static java.lang.Math.min;
import static java.util.stream.Collectors.toList;

@Controller
public class HomeController {
    @Autowired
    private KNNService knnService;

    @Autowired
    private AppConfig config;

    @Autowired
    private IMap<String, IndexedDataObject> data;

    private ImagePto notFound = new ImagePto("0", "Not Found", new String[0], "http://web-hosting-blog.rackservers.com.au/wp-content/uploads/2012/08/internet-error-404-file-not-found.jpg", -1);

    @RequestMapping("")
    public String home(Model model,
                       @RequestParam(required = false) String query,
                       @RequestParam(defaultValue = "20") int limit,
                       @RequestParam(defaultValue = "") String tags) {

        if (query == null) {
            var values = data.aggregate(new RandomAggregator(limit))
                    .stream()
                    .map(dataObject -> ImagePto.fromDataObject(dataObject, -1))
                    .collect(toList());

            model.addAttribute("images", values.subList(0, min(values.size(), limit)));
        } else {
            var queryObject = data.get(query);
            if (queryObject != null) {
                var tagArray = !tags.isEmpty() ? tags.split(" ") : new String[0];
                var result = knnService.search(queryObject, limit, config.candidateSetThreshold(), tagArray)
                        .stream()
                        .map(pair -> ImagePto.fromDataObject(pair.getLeft(), pair.getRight()))
                        .collect(toList());

                model.addAttribute("images", result);
            } else {
                model.addAttribute("images", List.of(notFound));
            }
        }

        return "index";
    }
}
