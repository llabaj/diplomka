/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.search;

import messif.algorithm.AlgorithmMethodException;
import messif.data.DataObject;
import messif.operation.OperationBuilder;
import messif.operation.ProcessObjectsOperation;
import messif.operation.answer.AbstractAnswer;
import messif.operation.answer.ProcessObjectsAnswer;
import messif.operation.answer.RankedAnswer;
import messif.operation.crud.answer.CRUDAnswer;
import messif.operation.crud.answer.InsertAnswer;
import messif.operation.params.*;
import messif.operation.processing.OperationEvaluator;
import messif.operation.processing.impl.QueueOperationProcessor;
import messif.record.ModifiableRecordImpl;
import messif.utility.ProcessingStatus;

import javax.annotation.Nullable;

/**
 * An operation that checks near duplicate objects for all objects in the list (for instance, during the insert chain).
 */
public interface CheckNearDupsOperation extends ProcessObjectsOperation, Range, ApproximateSearch, DistanceFunction {

    String TYPE_STRING = "find_near_duplicates";

    @Override
    default String getInnerType() {
        return TYPE_STRING;
    }

    /**
     * A simple helper that sets specific alright/partial/error codes.
     */
    class Helper extends ProcessObjectsOperation.Helper<CheckNearDupsOperation, ProcessObjectsAnswer> {

        protected Helper(CheckNearDupsOperation operation) {
            super(operation, ProcessObjectsAnswer.class, CRUDAnswer.SOME_OBJECT_NOT_FOUND,
                    new ProcessingStatus[] {ProcessObjectsAnswer.OK_PROCESSED},
                    new ProcessingStatus[] {InsertAnswer.OBJECT_DUPLICATE});
        }
    }

    /**
     * A processor that takes the operation and runs single {@link RangeOperationApprox} operations on a given
     *  algorithm. The overall {@link CheckNearDupsOperation} operation's answer is composed based on the individual
     *  results.
     */
    class Processor extends QueueOperationProcessor<CheckNearDupsOperation, ProcessObjectsAnswer, RangeOperationApprox> {

        private final OperationEvaluator algorithmToProcess;
        final Helper helper;

        public Processor(CheckNearDupsOperation operation, OperationEvaluator evaluator) {
            super(operation);
            this.algorithmToProcess = evaluator;
            this.helper = new Helper(operation);
        }

        @Override
        protected void initProcessor() throws AlgorithmMethodException {
            super.initProcessor();

            // create a range query operation for each of the data object
            for (DataObject queryObject : operation.getObjects()) {
                final ModifiableRecordImpl singleOpParams = new ModifiableRecordImpl(operation.getParams());
                singleOpParams.removeField(DataObjects.OBJECTS_FIELD);
                singleOpParams.setField(QueryObject.QUERY_OBJECT_FIELD, queryObject);

                RangeOperationApprox singleOp = OperationBuilder.build(singleOpParams, RangeOperationApprox.class, false);
                addProcessingItem(singleOp);
            }
            queueClose();
        }

        @Override
        protected void processItem(RangeOperationApprox processingItem) throws AlgorithmMethodException {
            try {
                final AbstractAnswer singleAnswer = algorithmToProcess.evaluate(processingItem);
                // if there are some objects in the near-duplicates range
                if (singleAnswer instanceof RankedAnswer && ((RankedAnswer) singleAnswer).getAnswerCount() > 0) {
                    helper.skipObject(processingItem.getQueryObject(), singleAnswer);
                    helper.errorCodeHelper.updateErrorCode(InsertAnswer.OBJECT_DUPLICATE);
                } else {
                    helper.objectProcessed(processingItem.getQueryObject(), ProcessObjectsAnswer.OK_PROCESSED);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Nullable
        @Override
        public ProcessObjectsAnswer finish() {
            return helper.getAnswer();
        }
    }

}
