/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.processing;


import messif.data.DataObject;
import messif.record.Record;
import messif.utility.json.JSONReader;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

/**
 * Given a float matrix, this class converts a neural network Caffe descriptor to lower dimension
 *  by multiplying it with the matrix.
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class PCAConverter extends AddFieldProcessor {

    public static final String MATRIX_FIELD = "matrix";
    public static final String MEAN_FIELD = "mean";

    // vector conversion matrix
    protected final float [] [] matrix;

    /**
     * A mean vector to be substracted from the input vector prior multiplication (if not null).
     */
    protected final float [] mean;

    /**
     * Creates new converted given a file with serialized float matrix to be used.
     * @param matrixFile name of file (with full or relative path) with serialized float [] [] matrix
     * @throws IOException if the file is not readable or is corrupted
     * @throws ClassNotFoundException if the matrix cannot be deserialized
     */
    public PCAConverter(String matrixFile, String requiredField, String outputField, boolean substitute) throws IOException, InstantiationException {
        super(requiredField, outputField, substitute);

        // read the text representation that is supposted to contain a JSON object with two fields:
        // 1) matrix (an array of arrays of floats in JSON format)
        // 2) mean vector (float array)
        InputStream inputStream = new FileInputStream(matrixFile);
        if (matrixFile.endsWith(".gz"))
            inputStream = new GZIPInputStream(inputStream);
        try (JSONReader reader = new JSONReader(inputStream, false)) {
            final Record record = reader.readRecord();
            if (record == null || ! record.containsField(MATRIX_FIELD)) {
                throw new InstantiationException("file " + matrixFile + " does not contain valid JSON object with matrix (and mean vector)");
            }
            // read the matrix
            Object [] field = record.getField(MATRIX_FIELD, Object[].class);
            matrix = new float[field.length][];
            for (int i = 0; i < field.length; i++) {
                matrix[i] = (float []) field[i];
            }
            // read the mean vector
            if (record.containsField(MEAN_FIELD)) {
                mean = record.getField(MEAN_FIELD, float[].class);
            } else {
                mean = new float [matrix.length];
            }
        }
    }

    protected PCAConverter(float [] [] matrix, String requiredField, String outputField, boolean substitute) {
        this(matrix, new float [matrix.length], requiredField, outputField, substitute);
    }

    protected PCAConverter(float [] [] matrix, float [] mean, String requiredField, String outputField, boolean substitute) {
        super(requiredField, outputField, substitute);
        this.matrix = matrix;
        this.mean = mean;
    }

    @Override
    protected Object getFieldValue(DataObject record) {
        final float[] fieldToTransform = record.getField(requiredField, float[].class);
        return applyPCA(fieldToTransform);
    }

    protected float [] applyPCA(float [] source) {
        float [] retVector = new float [matrix[0].length];

        // do the vector x matrix multiplication
        for (int i = 0; i < source.length; i++) {
            float s = source[i] - mean[i];
            for (int j = 0; j < retVector.length; j++) {
                retVector[j] += s * matrix[i][j];
            }
        }
        return retVector;
    }
}
