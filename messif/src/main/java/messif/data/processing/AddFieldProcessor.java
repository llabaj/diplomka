/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.processing;

import messif.data.DataObject;
import messif.record.ModifiableRecord;
import messif.record.ModifiableRecordImpl;
import messif.record.processing.RecordProcessingException;
import messif.record.processing.RecordProcessor;

/**
 * A data object processor that requires a single field and adds another field.
 */
abstract class AddFieldProcessor implements RecordProcessor.Single<DataObject> {

    protected final String requiredField;

    protected final String outputField;

    protected final String conditionString;

    protected final String[] modifiedFields;

    private final boolean removeSourceField;

    /**
     * Create a new processor given the source field and output field.
     */
    protected AddFieldProcessor(String requiredField, String outputField) {
        this(requiredField, outputField, false);
    }

    /**
     * Create a new processor given the source field, output field and flag about removing the source field.
     */
    protected AddFieldProcessor(String requiredField, String outputField, boolean removeSourceField) {
        this.requiredField = requiredField;
        this.outputField = outputField;
        this.conditionString = "field '" + requiredField + "' required";
        this.modifiedFields = removeSourceField ? new String [] {outputField, requiredField} : new String[]{outputField};
        this.removeSourceField = removeSourceField && ! requiredField.equals(outputField);
    }

    /**
     * Creates and returns the data to be added to the new field.
     * @param record the data object to create the new field for
     * @return data to be added to the new field
     */
    protected abstract Object getFieldValue(DataObject record);

    @Override
    public DataObject process(DataObject record) throws RecordProcessingException {
        if (! preCondition(record)) {
            return record;
        }
        Object fieldData = getFieldValue(record);
        if (!(record instanceof ModifiableRecord)) {
            record = new ModifiableRecordImpl(record);
        }
        if (removeSourceField) {
            ((ModifiableRecord) record).removeField(requiredField);
        }
        ((ModifiableRecord) record).setField(outputField, fieldData);
        return record;
    }

    @Override
    public boolean preCondition(DataObject record) {
        return requiredField == null || record.getField(requiredField) != null;
    }

    @Override
    public String preConditionDescription() {
        return conditionString;
    }

    @Override
    public String[] modifiedFields() {
        return modifiedFields;
    }
}
