/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.json;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.Map;
import junit.framework.TestCase;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class JSONReaderTest extends TestCase {
    
    private JSONReader reader;
    
    public JSONReaderTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        URL systemResource = JSONReaderTest.class.getClassLoader().getResource("messif/json/testdata.json");
        BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(systemResource.toURI())));
        
        this.reader = new JSONReader(bufferedReader);
        System.out.println("successful setUp");
    }

    /**
     * Test of getUnderlyingReader method, of class JSONReader.
     */
    public void testGetUnderlyingReader() {
        System.out.println("getUnderlyingReader");
        try {
            setUp();
            assertNotNull(reader.getUnderlyingReader());
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    /**
     * Test of readJSONObject method, of class JSONReader.
     */
    public void testReadJSONObject() {
        System.out.println("readJSONObject");
        try {
            setUp();
            Map<String, Object> readJSONObject = reader.readJSONObject();
            assertNotNull(readJSONObject);
            System.out.println(readJSONObject.toString());
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    /**
     * Test of isEOF method, of class JSONReader.
     */
//    public void testIsEOF() {
//        System.out.println("isEOF");
//        JSONReader instance = null;
//        boolean expResult = false;
//        boolean result = instance.isEOF();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default execute to fail.
//        fail("The test case is a prototype.");
//    }
    
}
