/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.record.processing;

import messif.record.Record;

import java.util.function.Predicate;

/**
 * Filter to check a condition on a given record.
 *
 * @param <T> type of records this process process
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface RecordFilter<T extends Record> extends Predicate<T> {

    /**
     * Test the record, if it can be processed by this processor.
     * @param record a record to be tested
     * @return true if the record can be processed by this processor, false otherwise
     */
    boolean preCondition(T record);

    /**
     * Get a string description of the condition checked by {@link #preCondition(Record)}.
     * @return a string description of the condition checked by {@link #preCondition(Record)}.
     */
    String preConditionDescription();

    @Override
    default boolean test(T t) {
        return preCondition(t);
    }
}
