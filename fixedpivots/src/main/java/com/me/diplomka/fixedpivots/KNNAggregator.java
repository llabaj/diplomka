package com.me.diplomka.fixedpivots;

import com.hazelcast.aggregation.Aggregator;
import messif.data.DataObject;
import messif.distance.DataObjectDistanceFunc;
import org.apache.commons.lang3.tuple.Pair;

import java.io.Serializable;
import java.util.*;

import static java.util.stream.Collectors.toList;

public class KNNAggregator extends Aggregator<Map.Entry<String, IndexedDataObject>, List<Pair<IndexedDataObject, Float>>> {
    private int limit;
    private DataObjectDistanceFunc<float[]> distanceFunc;
    private DataObject query;
    private Queue<Pair<IndexedDataObject, Float>> queue;

    public KNNAggregator(DataObject query, int limit, DataObjectDistanceFunc<float[]> distanceFunc) {
        this.query = query;
        this.limit = limit;
        this.distanceFunc = distanceFunc;

        queue = new PriorityQueue<>(limit + 1, new PairComparator().reversed());
    }

    @Override
    public void accumulate(Map.Entry<String, IndexedDataObject> input) {
        var dataObject = input.getValue();
        var distance = distanceFunc.getDistance(dataObject, query);
        queue.add(Pair.of(dataObject, distance));
        if (queue.size() > limit) {
            queue.poll();
        }
    }

    @Override
    public void combine(Aggregator aggregator) {
        var other = (KNNAggregator) aggregator;
        queue.addAll(other.queue);
        while (queue.size() > limit) {
            queue.poll();
        }
    }

    @Override
    public List<Pair<IndexedDataObject, Float>> aggregate() {
        return queue.stream()
                .sorted(new PairComparator())
                .collect(toList());
    }

    private static class PairComparator implements Comparator<Pair<IndexedDataObject, Float>>, Serializable {
        @Override
        public int compare(Pair<IndexedDataObject, Float> o1, Pair<IndexedDataObject, Float> o2) {
            return Float.compare(o1.getRight(), o2.getRight());
        }
    }
}
