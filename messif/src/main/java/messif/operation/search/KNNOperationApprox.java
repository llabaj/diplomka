/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.search;

import messif.operation.params.ApproximateSearch;

/**
 * This class encapsulates operation that has all params like {@link KNNOperation} and additionally params from
 *  {@link ApproximateSearch}. 
 */
public interface KNNOperationApprox extends KNNOperation, ApproximateSearch {

    String TYPE_STRING = "knn_query_approx";

    @Override
    default String getInnerType() {
        return TYPE_STRING;
    }

}
