/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.serialization;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import java.util.logging.Logger;
import messif.serialization.io.BinaryInput;
import messif.serialization.io.BinaryOutput;
import messif.serialization.io.BufferInputStream;
import messif.serialization.io.BufferOutputStream;
import messif.serialization.io.ChannelInputStream;
import messif.serialization.io.ChannelOutputStream;

/**
 * This class provides a framework for {@link BinarySerializable binary serialization} of objects.
 * It operates on any {@link BinaryInput}/{@link BinaryOutput}.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @see BufferInputStream
 * @see BufferOutputStream
 * @see ChannelInputStream
 * @see ChannelOutputStream
 */
public class BasicTypesSerializator  implements Serializable {
        
    /** Class serial id for Java serialization. */
    private static final long serialVersionUID = 1L;
    
    /** Logger for serializators. */
    protected static final Logger log = Logger.getLogger("messif.serialization");
    
    //************************ Serializing methods for primitive types ************************//

    /**
     * Writes a <code>boolean</code> value to the specified output.
     * @param output the output buffer to write the value into
     * @param value the <code>boolean</code> value to be written
     * @return the number of bytes written
     * @throws IOException if there was an I/O error
     */
    public int write(BinaryOutput output, boolean value) throws IOException {
        write(output, (byte)(value?1:0));
        return 1;
    }

    /**
     * Writes a <code>byte</code> value to the specified output.
     * @param output the output buffer to write the value into
     * @param value the <code>byte</code> value to be written
     * @return the number of bytes written
     * @throws IOException if there was an I/O error
     */
    public int write(BinaryOutput output, byte value) throws IOException {
        output.prepareOutput(1).put(value);
        return 1;
    }

    /**
     * Writes a <code>short</code> value to the specified output.
     * @param output the output buffer to write the value into
     * @param value the <code>short</code> value to be written
     * @return the number of bytes written
     * @throws IOException if there was an I/O error
     */
    public int write(BinaryOutput output, short value) throws IOException {
        output.prepareOutput(2).putShort(value);
        return 2;
    }

    /**
     * Writes a <code>char</code> value to the specified output.
     * @param output the output buffer to write the value into
     * @param value the <code>char</code> value to be written
     * @return the number of bytes written
     * @throws IOException if there was an I/O error
     */
    public int write(BinaryOutput output, char value) throws IOException {
        output.prepareOutput(2).putChar(value);
        return 2;
    }

    /**
     * Writes a <code>int</code> value to the specified output.
     * @param output the output buffer to write the value into
     * @param value the <code>int</code> value to be written
     * @return the number of bytes written
     * @throws IOException if there was an I/O error
     */
    public int write(BinaryOutput output, int value) throws IOException {
        output.prepareOutput(4).putInt(value);
        return 4;
    }

    /**
     * Writes a <code>long</code> value to the specified output.
     * @param output the output buffer to write the value into
     * @param value the <code>long</code> value to be written
     * @return the number of bytes written
     * @throws IOException if there was an I/O error
     */
    public int write(BinaryOutput output, long value) throws IOException {
        output.prepareOutput(8).putLong(value);
        return 8;
    }

    /**
     * Writes a <code>float</code> value to the specified output.
     * @param output the output buffer to write the value into
     * @param value the <code>float</code> value to be written
     * @return the number of bytes written
     * @throws IOException if there was an I/O error
     */
    public int write(BinaryOutput output, float value) throws IOException {
        output.prepareOutput(4).putFloat(value);
        return 4;
    }

    /**
     * Writes a <code>double</code> value to the specified output.
     * @param output the output buffer to write the value into
     * @param value the <code>double</code> value to be written
     * @return the number of bytes written
     * @throws IOException if there was an I/O error
     */
    public int write(BinaryOutput output, double value) throws IOException {
        output.prepareOutput(8).putDouble(value);
        return 8;
    }

    /**
     * Writes a <code>BigInteger</code> value to the specified output.
     *
     * @param output the output buffer to write the value into
     * @param value the <code>BigInteger</code> value to be written
     * @return the number of bytes written
     * @throws IOException if there was an I/O error
     */
    public int write(BinaryOutput output, BigInteger value) throws IOException {
        byte[] array = value.toByteArray();
        write(output, array);
        return array.length;
    }

    //************************ Serializing methods for primitive arrays ************************//

    /**
     * Writes a <code>boolean</code> array to the specified output.
     * @param output the output buffer to write the array into
     * @param array the <i>boolean</i> array to write
     * @param index the start index in the <i>boolean</i> array
     * @param length the number of array items to write
     * @return the number of bytes written to the output
     * @throws IOException if there was an I/O error
     * @throws NullPointerException if the specified array is <tt>null</tt>
     * @throws IndexOutOfBoundsException if the <code>index</code> or <code>length</code> are invalid for the specified array
     */
    public int write(BinaryOutput output, boolean[] array, int index, int length) throws IOException, NullPointerException, IndexOutOfBoundsException {
        byte[] bytes = new byte[length];
        for (int i = 0; i < length; i++)
            bytes[i] = array[index + i]?(byte)1:(byte)0;
        return write(output, bytes);
    }

    /**
     * Writes a <code>boolean</code> array to the specified output.
     * @param output the output buffer to write the array into
     * @param array the <i>boolean</i> array to write
     * @return the number of bytes written to the output
     * @throws IOException if there was an I/O error
     */
    public int write(BinaryOutput output, boolean[] array) throws IOException {
        if (array != null)
            return write(output, array, 0, array.length);
        write(output, -1);
        return 4;
    }

    /**
     * Writes a <code>byte</code> array to the specified output.
     * @param output the output buffer to write the array into
     * @param array the <i>byte</i> array to write
     * @param index the start index in the <i>byte</i> array
     * @param length the number of array items to write
     * @return the number of bytes written to the output
     * @throws IOException if there was an I/O error
     * @throws NullPointerException if the specified array is <tt>null</tt>
     * @throws IndexOutOfBoundsException if the <code>index</code> or <code>length</code> are invalid for the specified array
     */
    public int write(BinaryOutput output, byte[] array, int index, int length) throws IOException, NullPointerException, IndexOutOfBoundsException {
        int ret = write(output, length) + length;
        while (length > 0) {
            ByteBuffer buffer = output.prepareOutput(1);
            int lenToWrite = Math.min(length, buffer.remaining());
            buffer.put(array, index, lenToWrite);
            index += lenToWrite;
            length -= lenToWrite;
        }
        return ret;
    }

    /**
     * Writes a <code>byte</code> array to the specified output.
     * @param output the output buffer to write the array into
     * @param array the <i>byte</i> array to write
     * @return the number of bytes written to the output
     * @throws IOException if there was an I/O error
     */
    public int write(BinaryOutput output, byte[] array) throws IOException {
        if (array != null)
            return write(output, array, 0, array.length);
        write(output, -1);
        return 4;
    }

    /**
     * Writes a <code>short</code> array to the specified output.
     * @param output the output buffer to write the array into
     * @param array the <i>short</i> array to write
     * @param index the start index in the <i>short</i> array
     * @param length the number of array items to write
     * @return the number of bytes written to the output
     * @throws IOException if there was an I/O error
     * @throws NullPointerException if the specified array is <tt>null</tt>
     * @throws IndexOutOfBoundsException if the <code>index</code> or <code>length</code> are invalid for the specified array
     */
    public int write(BinaryOutput output, short[] array, int index, int length) throws IOException, NullPointerException, IndexOutOfBoundsException {
        int bytes = 4 + (length << 1);
        write(output, length);

        while (length > 0) {
            ByteBuffer byteBuffer = output.prepareOutput(2);
            ShortBuffer buffer = byteBuffer.asShortBuffer();
            int lenToWrite = Math.min(length, buffer.remaining());
            buffer.put(array, index, lenToWrite);
            index += lenToWrite;
            length -= lenToWrite;
            byteBuffer.position(byteBuffer.position() + (lenToWrite << 1));
        }

        return bytes;
    }

    /**
     * Writes a <code>short</code> array to the specified output.
     * @param output the output buffer to write the array into
     * @param array the <i>short</i> array to write
     * @return the number of bytes written to the output
     * @throws IOException if there was an I/O error
     */
    public int write(BinaryOutput output, short[] array) throws IOException {
        if (array != null)
            return write(output, array, 0, array.length);
        write(output, -1);
        return 4;
    }

    /**
     * Writes a <code>char</code> array to the specified output.
     * @param output the output buffer to write the array into
     * @param array the <i>char</i> array to write
     * @param index the start index in the <i>char</i> array
     * @param length the number of array items to write
     * @return the number of bytes written to the output
     * @throws IOException if there was an I/O error
     * @throws NullPointerException if the specified array is <tt>null</tt>
     * @throws IndexOutOfBoundsException if the <code>index</code> or <code>length</code> are invalid for the specified array
     */
    public int write(BinaryOutput output, char[] array, int index, int length) throws IOException, NullPointerException, IndexOutOfBoundsException {
        int bytes = 4 + (length << 1);
        write(output, length);

        while (length > 0) {
            ByteBuffer byteBuffer = output.prepareOutput(2);
            CharBuffer buffer = byteBuffer.asCharBuffer();
            int lenToWrite = Math.min(length, buffer.remaining());
            buffer.put(array, index, lenToWrite);
            index += lenToWrite;
            length -= lenToWrite;
            byteBuffer.position(byteBuffer.position() + (lenToWrite << 1));
        }

        return bytes;
    }

    /**
     * Writes a <code>char</code> array to the specified output.
     * @param output the output buffer to write the array into
     * @param array the <i>char</i> array to write
     * @return the number of bytes written to the output
     * @throws IOException if there was an I/O error
     */
    public int write(BinaryOutput output, char[] array) throws IOException {
        if (array != null)
            return write(output, array, 0, array.length);
        write(output, -1);
        return 4;
    }

    /**
     * Writes a <code>int</code> array to the specified output.
     * @param output the output buffer to write the array into
     * @param array the <i>int</i> array to write
     * @param index the start index in the <i>int</i> array
     * @param length the number of array items to write
     * @return the number of bytes written to the output
     * @throws IOException if there was an I/O error
     * @throws NullPointerException if the specified array is <tt>null</tt>
     * @throws IndexOutOfBoundsException if the <code>index</code> or <code>length</code> are invalid for the specified array
     */
    public int write(BinaryOutput output, int[] array, int index, int length) throws IOException, NullPointerException, IndexOutOfBoundsException {
        int bytes = 4 + (length << 2);
        write(output, length);

        while (length > 0) {
            ByteBuffer byteBuffer = output.prepareOutput(4);
            IntBuffer buffer = byteBuffer.asIntBuffer();
            int lenToWrite = Math.min(length, buffer.remaining());
            buffer.put(array, index, lenToWrite);
            index += lenToWrite;
            length -= lenToWrite;
            byteBuffer.position(byteBuffer.position() + (lenToWrite << 2));
        }

        return bytes;
    }

    /**
     * Writes a <code>int</code> array to the specified output.
     * @param output the output buffer to write the array into
     * @param array the <i>int</i> array to write
     * @return the number of bytes written to the output
     * @throws IOException if there was an I/O error
     */
    public int write(BinaryOutput output, int[] array) throws IOException {
        if (array != null)
            return write(output, array, 0, array.length);
        write(output, -1);
        return 4;
    }

    /**
     * Writes a <code>long</code> array to the specified output.
     * @param output the output buffer to write the array into
     * @param array the <i>long</i> array to write
     * @param index the start index in the <i>long</i> array
     * @param length the number of array items to write
     * @return the number of bytes written to the output
     * @throws IOException if there was an I/O error
     * @throws NullPointerException if the specified array is <tt>null</tt>
     * @throws IndexOutOfBoundsException if the <code>index</code> or <code>length</code> are invalid for the specified array
     */
    public int write(BinaryOutput output, long[] array, int index, int length) throws IOException, NullPointerException, IndexOutOfBoundsException {
        int bytes = 4 + (length << 3);
        write(output, length);

        while (length > 0) {
            ByteBuffer byteBuffer = output.prepareOutput(8);
            LongBuffer buffer = byteBuffer.asLongBuffer();
            int lenToWrite = Math.min(length, buffer.remaining());
            buffer.put(array, index, lenToWrite);
            index += lenToWrite;
            length -= lenToWrite;
            byteBuffer.position(byteBuffer.position() + (lenToWrite << 3));
        }

        return bytes;
    }

    /**
     * Writes a <code>long</code> array to the specified output.
     * @param output the output buffer to write the array into
     * @param array the <i>long</i> array to write
     * @return the number of bytes written to the output
     * @throws IOException if there was an I/O error
     */
    public int write(BinaryOutput output, long[] array) throws IOException {
        if (array != null)
            return write(output, array, 0, array.length);
        write(output, -1);
        return 4;
    }

    /**
     * Writes a <code>float</code> array to the specified output.
     * @param output the output buffer to write the array into
     * @param array the <i>float</i> array to write
     * @param index the start index in the <i>float</i> array
     * @param length the number of array items to write
     * @return the number of bytes written to the output
     * @throws IOException if there was an I/O error
     * @throws NullPointerException if the specified array is <tt>null</tt>
     * @throws IndexOutOfBoundsException if the <code>index</code> or <code>length</code> are invalid for the specified array
     */
    public int write(BinaryOutput output, float[] array, int index, int length) throws IOException, NullPointerException, IndexOutOfBoundsException {
        int bytes = 4 + (length << 2);
        write(output, length);

        while (length > 0) {
            ByteBuffer byteBuffer = output.prepareOutput(4);
            FloatBuffer buffer = byteBuffer.asFloatBuffer();
            int lenToWrite = Math.min(length, buffer.remaining());
            buffer.put(array, index, lenToWrite);
            index += lenToWrite;
            length -= lenToWrite;
            byteBuffer.position(byteBuffer.position() + (lenToWrite << 2));
        }

        return bytes;
    }

    /**
     * Writes a <code>float</code> array to the specified output.
     * @param output the output buffer to write the array into
     * @param array the <i>float</i> array to write
     * @return the number of bytes written to the output
     * @throws IOException if there was an I/O error
     */
    public int write(BinaryOutput output, float[] array) throws IOException {
        if (array != null)
            return write(output, array, 0, array.length);
        write(output, -1);
        return 4;
    }

    /**
     * Writes a <code>double</code> array to the specified output.
     * @param output the output buffer to write the array into
     * @param array the <i>double</i> array to write
     * @param index the start index in the <i>double</i> array
     * @param length the number of array items to write
     * @return the number of bytes written to the output
     * @throws IOException if there was an I/O error
     * @throws NullPointerException if the specified array is <tt>null</tt>
     * @throws IndexOutOfBoundsException if the <code>index</code> or <code>length</code> are invalid for the specified array
     */
    public int write(BinaryOutput output, double[] array, int index, int length) throws IOException, NullPointerException, IndexOutOfBoundsException {
        int bytes = 4 + (length << 3);
        write(output, length);

        while (length > 0) {
            ByteBuffer byteBuffer = output.prepareOutput(8);
            DoubleBuffer buffer = byteBuffer.asDoubleBuffer();
            int lenToWrite = Math.min(length, buffer.remaining());
            buffer.put(array, index, lenToWrite);
            index += lenToWrite;
            length -= lenToWrite;
            byteBuffer.position(byteBuffer.position() + (lenToWrite << 3));
        }

        return bytes;
    }

    /**
     * Writes a <code>double</code> array to the specified output.
     * @param output the output buffer to write the array into
     * @param array the <i>double</i> array to write
     * @return the number of bytes written to the output
     * @throws IOException if there was an I/O error
     */
    public int write(BinaryOutput output, double[] array) throws IOException {
        if (array != null)
            return write(output, array, 0, array.length);
        write(output, -1);
        return 4;
    }


    //************************ Serializing methods for generic objects ************************//

    /**
     * Writes a {@link String} to the specified output.
     * @param output the buffer to write the string into
     * @param string the {@link String} to be written
     * @return the number of bytes written
     * @throws IOException if there was an I/O error
     */
    public int write(BinaryOutput output, String string) throws IOException {
        return write(output, string == null ? null : string.toCharArray());
    }

    /**
     * Writes a {@link String} array to the specified output.
     * @param output the buffer to write the string array into
     * @param array the {@link String} array to be written
     * @return the number of bytes written
     * @throws IOException if there was an I/O error
     */
    public int write(BinaryOutput output, String[] array) throws IOException {
        int size = write(output, (array == null) ? 0 : array.length);
        if (size != 0) {
            for (String item : array) {
                size += write(output, item);
            }
        }
        return size;
    }

//    /**
//     * Writes an {@link Enum} to the specified output.
//     * @param output the buffer to write the enum into
//     * @param enumInstance the enum to be written
//     * @return the number of bytes written
//     * @throws IOException if there was an I/O error
//     */
//    public int write(BinaryOutput output, Enum<?> enumInstance) throws IOException {
//        return write(output, enumInstance == null ? -1 : enumInstance.ordinal());
//    }

    //************************ Deserializing methods for primitive types ************************//

    /**
     * Returns a <code>boolean</code> value read from the specified input.
     * @param input the input buffer to read the value from
     * @return a <code>boolean</code> value read from the input
     * @throws IOException if there was an I/O error
     */
    public boolean readBoolean(BinaryInput input) throws IOException {
        return (readByte(input) != 0);
    }

    /**
     * Returns a <code>byte</code> value read from the specified input.
     * @param input the input buffer to read the value from
     * @return a <code>byte</code> value read from the input
     * @throws IOException if there was an I/O error
     */
    public byte readByte(BinaryInput input) throws IOException {
        return input.readInput(1).get();
    }

    /**
     * Returns a <code>short</code> value read from the specified input.
     * @param input the input buffer to read the value from
     * @return a <code>short</code> value read from the input
     * @throws IOException if there was an I/O error
     */
    public short readShort(BinaryInput input) throws IOException {
        return input.readInput(2).getShort();
    }

    /**
     * Returns a <code>char</code> value read from the specified input.
     * @param input the input buffer to read the value from
     * @return a <code>char</code> value read from the input
     * @throws IOException if there was an I/O error
     */
    public char readChar(BinaryInput input) throws IOException {
        return input.readInput(2).getChar();
    }

    /**
     * Returns a <code>int</code> value read from the specified input.
     * @param input the input buffer to read the value from
     * @return a <code>int</code> value read from the input
     * @throws IOException if there was an I/O error
     */
    public int readInt(BinaryInput input) throws IOException {
        return input.readInput(4).getInt();
    }

    /**
     * Returns a <code>long</code> value read from the specified input.
     * @param input the input buffer to read the value from
     * @return a <code>long</code> value read from the input
     * @throws IOException if there was an I/O error
     */
    public long readLong(BinaryInput input) throws IOException {
        return input.readInput(8).getLong();
    }

    /**
     * Returns a <code>float</code> value read from the specified input.
     * @param input the input buffer to read the value from
     * @return a <code>float</code> value read from the input
     * @throws IOException if there was an I/O error
     */
    public float readFloat(BinaryInput input) throws IOException {
        return input.readInput(4).getFloat();
    }

    /**
     * Returns a <code>double</code> value read from the specified input.
     * @param input the input buffer to read the value from
     * @return a <code>double</code> value read from the input
     * @throws IOException if there was an I/O error
     */
    public double readDouble(BinaryInput input) throws IOException {
        return input.readInput(8).getDouble();
    }

    /**
     * Returns a <code>BigInteger</code> value read from the specified input.
     *
     * @param input the input buffer to read the value from
     * @return a <code>BigInteger</code> value read from the input
     * @throws IOException if there was an I/O error
     */
    public BigInteger readBigInteger(BinaryInput input) throws IOException {
        return new BigInteger(readByteArray(input));
    }	    

    //************************ Deserializing methods for primitive arrays ************************//

    /**
     * Returns a <code>boolean</code> array read from the specified input.
     * @param input the buffer to read the array from
     * @return a <code>boolean</code> array read from the input
     * @throws IOException if there was an I/O error
     */
    public boolean[] readBooleanArray(BinaryInput input) throws IOException {
        byte[] byteArray = readByteArray(input);
        if (byteArray == null)
            return null;
        boolean[] array = new boolean[byteArray.length];
        for (int i = 0; i < byteArray.length; i++)
            array[i] = byteArray[i] != 0;
        return array;
    }

    /**
     * Returns a <code>byte</code> array read from the specified input.
     * @param input the buffer to read the array from
     * @return a <code>byte</code> array read from the input
     * @throws IOException if there was an I/O error
     */
    public byte[] readByteArray(BinaryInput input) throws IOException {
        int len = readInt(input);
        if (len == -1)
            return null;
        byte[] array = new byte[len];
        int off = 0;
        while (len > 0) {
            ByteBuffer buffer = input.readInput(1);
            int countToRead = Math.min(len, buffer.remaining());
            buffer.get(array, off, countToRead);
            off += countToRead;
            len -= countToRead;
        }
        return array;
    }
    
    /**
     * Returns a <code>short</code> array read from the specified input.
     * @param input the buffer to read the array from
     * @return a <code>short</code> array read from the input
     * @throws IOException if there was an I/O error
     */
    public short[] readShortArray(BinaryInput input) throws IOException {
        int len = readInt(input);
        if (len == -1)
            return null;
        short[] array = new short[len];
        int off = 0;
        while (len > 0) {
            ByteBuffer byteBuffer = input.readInput(2);
            ShortBuffer buffer = byteBuffer.asShortBuffer();
            int countToRead = Math.min(len, buffer.remaining());
            buffer.get(array, off, countToRead);
            off += countToRead;
            len -= countToRead;
            byteBuffer.position(byteBuffer.position() + (countToRead << 1));
        }
        return array;
    }

    /**
     * Returns a <code>char</code> array read from the specified input.
     * @param input the buffer to read the array from
     * @return a <code>char</code> array read from the input
     * @throws IOException if there was an I/O error
     */
    public char[] readCharArray(BinaryInput input) throws IOException {
        int len = readInt(input);
        if (len == -1)
            return null;
        char[] array = new char[len];
        int off = 0;
        while (len > 0) {
            ByteBuffer byteBuffer = input.readInput(2);
            CharBuffer buffer = byteBuffer.asCharBuffer();
            int countToRead = Math.min(len, buffer.remaining());
            buffer.get(array, off, countToRead);
            off += countToRead;
            len -= countToRead;
            byteBuffer.position(byteBuffer.position() + (countToRead << 1));
        }
        return array;
    }

    /**
     * Returns a <code>int</code> array read from the specified input.
     * @param input the buffer to read the array from
     * @return a <code>int</code> array read from the input
     * @throws IOException if there was an I/O error
     */
    public int[] readIntArray(BinaryInput input) throws IOException {
        int len = readInt(input);
        if (len == -1)
            return null;
        int[] array = new int[len];
        int off = 0;
        while (len > 0) {
            ByteBuffer byteBuffer = input.readInput(4);
            IntBuffer buffer = byteBuffer.asIntBuffer();
            int countToRead = Math.min(len, buffer.remaining());
            buffer.get(array, off, countToRead);
            off += countToRead;
            len -= countToRead;
            byteBuffer.position(byteBuffer.position() + (countToRead << 2));
        }
        return array;
    }

    /**
     * Returns a <code>long</code> array read from the specified input.
     * @param input the buffer to read the array from
     * @return a <code>long</code> array read from the input
     * @throws IOException if there was an I/O error
     */
    public long[] readLongArray(BinaryInput input) throws IOException {
        int len = readInt(input);
        if (len == -1)
            return null;
        long[] array = new long[len];
        int off = 0;
        while (len > 0) {
            ByteBuffer byteBuffer = input.readInput(8);
            LongBuffer buffer = byteBuffer.asLongBuffer();
            int countToRead = Math.min(len, buffer.remaining());
            buffer.get(array, off, countToRead);
            off += countToRead;
            len -= countToRead;
            byteBuffer.position(byteBuffer.position() + (countToRead << 3));
        }
        return array;
    }

    /**
     * Returns a <code>float</code> array read from the specified input.
     * @param input the buffer to read the array from
     * @return a <code>float</code> array read from the input
     * @throws IOException if there was an I/O error
     */
    public float[] readFloatArray(BinaryInput input) throws IOException {
        int len = readInt(input);
        if (len == -1)
            return null;
        float[] array = new float[len];
        int off = 0;
        while (len > 0) {
            ByteBuffer byteBuffer = input.readInput(4);
            FloatBuffer buffer = byteBuffer.asFloatBuffer();
            int countToRead = Math.min(len, buffer.remaining());
            buffer.get(array, off, countToRead);
            off += countToRead;
            len -= countToRead;
            byteBuffer.position(byteBuffer.position() + (countToRead << 2));
        }
        return array;
    }

    /**
     * Returns a <code>double</code> array read from the specified input.
     * @param input the buffer to read the array from
     * @return a <code>double</code> array read from the input
     * @throws IOException if there was an I/O error
     */
    public double[] readDoubleArray(BinaryInput input) throws IOException {
        int len = readInt(input);
        if (len == -1)
            return null;
        double[] array = new double[len];
        int off = 0;
        while (len > 0) {
            ByteBuffer byteBuffer = input.readInput(8);
            DoubleBuffer buffer = byteBuffer.asDoubleBuffer();
            int countToRead = Math.min(len, buffer.remaining());
            buffer.get(array, off, countToRead);
            off += countToRead;
            len -= countToRead;
            byteBuffer.position(byteBuffer.position() + (countToRead << 3));
        }
        return array;
    }


    //************************ Deserializing methods for generic objects ************************//

    /**
     * Returns a {@link String} read from the specified input.
     * @param input the buffer to read the string from
     * @return a {@link String} read from the input
     * @throws IOException if there was an I/O error
     */
    public String readString(BinaryInput input) throws IOException {
        char[] stringBytes = readCharArray(input);
        if (stringBytes == null)
            return null;
        return new String(stringBytes);
    }

    /**
     * Returns a {@link String} array read from the specified input.
     * @param input the buffer to read the array from
     * @return a {@link String} array read from the input
     * @throws IOException if there was an I/O error
     */
    public String[] readStringArray(BinaryInput input) throws IOException {
        int len = readInt(input);
        if (len == -1)
            return null;
        String[] array = new String[len];
        for (int i = 0; i < len; i++)
            array[i] = readString(input);
        return array;
    }

//    /**
//     * Returns an {@link Enum} read from the specified input.
//     * @param <E> the enum class that is expected to be in the input
//     * @param input the buffer to read the enum from
//     * @param enumClass the enum class that is expected to be in the input
//     * @return an {@link Enum} read from the input
//     * @throws IOException if there was an I/O error
//     */
//    public final <E extends Enum<?>> E readEnum(BinaryInput input, Class<E> enumClass) throws IOException {
//        int ordinal = readInt(input);
//        if (ordinal == -1)
//            return null;
//        try {
//            return enumClass.getEnumConstants()[ordinal];
//        } catch (RuntimeException e) {
//            throw new IOException("Cannot read enum '" + enumClass.getName() + "' for ordinal value " + ordinal + ": " + e);
//        }
//    }
    
    
    // *****************      Skip methods     *********************** //
    // TODO: create all skip methods
    
    /**
     * Returns a <code>boolean</code> value read from the specified input.
     * @param input the input buffer to read the value from
     * @return a <code>boolean</code> value read from the input
     * @throws IOException if there was an I/O error
     */
    public int skipBoolean(BinaryInput input) throws IOException {
        return skipByte(input);
    }

    public int skipByte(BinaryInput input) throws IOException {
        return (int) input.skip(1);
    }
    
    public int skipIntArray(BinaryInput input) throws IOException {
        int len = readInt(input);
        if (len == -1)
            return 4;
        return (int) input.skip(len << 2) + 4;
    }

    
}
