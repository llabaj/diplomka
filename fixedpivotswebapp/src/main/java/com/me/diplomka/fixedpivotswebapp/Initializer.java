package com.me.diplomka.fixedpivotswebapp;

import com.hazelcast.core.IMap;
import com.hazelcast.core.ReplicatedMap;
import com.me.diplomka.fixedpivots.IndexedDataObject;
import com.me.diplomka.fixedpivots.PivotService;
import com.me.diplomka.fixedpivots.TagService;
import messif.data.DataObject;
import messif.data.util.StreamDataObjectIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class Initializer {
    private static final Logger logger = LoggerFactory.getLogger(Initializer.class);

    public static void initData(IMap<String, IndexedDataObject> data, ReplicatedMap<String, DataObject> pivots, PivotService pivotService, int pivotLimit, TagService tagService) throws IOException {
        var pivotsLocation = Initializer.class.getResourceAsStream("/json/pivots-vgg4096-pca256_2048.json");
        var queryLocation = Initializer.class.getResourceAsStream("/json/query10K-vgg4096-256pca.json");
        initData(data, pivots, pivotService, pivotLimit, tagService,
                new BufferedReader(new InputStreamReader(pivotsLocation)),
                new BufferedReader(new InputStreamReader(queryLocation)),
                0, -1);
    }

    /**
     * Initializes pivots and data from files.
     *
     * @param data   fills the map with data from file, adds "pivot_key" field to each entry
     * @param pivots fills the map with pivots from file
     */
    public static void initData(IMap<String, IndexedDataObject> data,
                                ReplicatedMap<String, DataObject> pivots,
                                PivotService pivotService,
                                int pivotLimit,
                                TagService tagService,
                                BufferedReader pivotsLocation,
                                BufferedReader queryLocation,
                                int offset,
                                int size) throws IOException {
        logger.debug("--- INIT START ---");
        if (pivotLimit <= 0) {
            pivotLimit = Integer.MAX_VALUE;
        }
        var streamSize = size > 0 ? size : Integer.MAX_VALUE;
        if (offset < 0) {
            throw new IllegalArgumentException("offset must be >= 0, found " + offset);
        }

        logger.debug("--- INIT PIVOTS ---");
        var pivotStream = new StreamDataObjectIterator(pivotsLocation);
        StreamSupport.stream(pivotStream.spliterator(), false)
                .limit(pivotLimit)
                .forEach(dataObject -> pivots.put(dataObject.getID(), dataObject));

        pivotStream.close();

        logger.debug("--- INIT DATA ---");
        var queryStream = new StreamDataObjectIterator(queryLocation);
        var pool = new ForkJoinPool(Runtime.getRuntime().availableProcessors() * 2);
        var counter = new AtomicInteger(0);

        pool.execute(
                () -> StreamSupport.stream(queryStream.spliterator(), true)
                        .skip(offset)
                        .limit(streamSize)
                        .forEach(dataObject -> {
                            var pivotPrefix = pivotService.findPivotPrefix(dataObject);
                            var indexedDataObject = IndexedDataObject.of(dataObject, pivotPrefix);
                            data.set(dataObject.getID(), indexedDataObject);

                            pivotService.incCount(pivotPrefix);
                            var tags = dataObject.getField("tags", String[].class);
                            if (tags != null) {
                                for (String tag : tags) {
                                    tagService.incCount(pivotPrefix, tag);
                                }
                            }
                            var ii = counter.getAndIncrement();
                            if (ii % 200 == 0) {
                                logger.debug(String.format("%,d", ii));
                            }
                        })
        );

        pool.shutdown();
        try {
            pool.awaitTermination(48, TimeUnit.HOURS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        queryStream.close();

        if (logger.isDebugEnabled()) {
            logger.debug("pivot count: {}", pivots.size());
            logger.debug("non-empty cells count: {}", pivotService.getCounts().size());
            logger.debug("cell size -> count");
            logger.debug("------------------");
            pivotService.getCounts()
                    .values()
                    .stream()
                    .collect(groupingBy(e -> e, counting()))
                    .entrySet()
                    .stream()
                    .sorted(Map.Entry.<Integer, Long>comparingByKey().reversed())
                    .forEach(e -> logger.debug("{} -> {}", e.getKey(), e.getValue()));


        }

        logger.debug("--- INIT END ---");
    }


}
