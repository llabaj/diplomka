/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.distance;

import messif.data.DataObject;

/**
 * A runtime exception used within the distance package.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class DistanceException extends RuntimeException {

    /**
     * Creates a new instance of <code>DistanceException</code> without detail
     * message.
     */
    public DistanceException() {
    }

    /**
     * Constructs an instance of <code>DistanceException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public DistanceException(String msg) {
        super(msg);
    }


    // region ***********************        Static builders       *********************************

    public static DistanceException fieldMissingException(DataObject object, String field) {
        return new DistanceException("object " + object.toJSONString() + " missing field '" + field +"'");
    }

    public static DistanceException fieldClassException(DataObject object, String field, Class fieldClass) {
        return new DistanceException("object " + object.toJSONString() + " has wrong class of field '" + field + "', should be " + fieldClass.getName());
    }

}
