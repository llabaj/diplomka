/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.util;

import messif.distance.DistanceFunc;
import messif.distance.DistanceException;
import messif.data.DataObject;

import java.util.Comparator;
import java.util.Iterator;

/**
 * Extension of {@link RankedSortedCollection} that always returns the threshold
 * {@link DistanceFunc#MAX_DISTANCE}.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
// TODO: seriously such things should be in MESSIF?
public class RankedSortedNoThresholdCollection extends RankedSortedCollection {
    /** class serial id for serialization */
    private static final long serialVersionUID = 1L;

    //****************** Constructors ******************//

    public RankedSortedNoThresholdCollection(int initialCapacity, int maximalCapacity, Comparator<? super RankedDataObject> comparator) throws IllegalArgumentException {
        super(initialCapacity, maximalCapacity, comparator);
    }

    public RankedSortedNoThresholdCollection(int initialCapacity, int maximalCapacity) throws IllegalArgumentException {
        super(initialCapacity, maximalCapacity);
    }

    public RankedSortedNoThresholdCollection() {
        super();
    }

    public <T extends DataObject> RankedSortedNoThresholdCollection(DistanceFunc<? super T> distanceFunc, T referenceObject, Iterator<? extends T> iterator) throws DistanceException {
        super(distanceFunc, referenceObject, iterator);
    }

    public <T extends DataObject> RankedSortedNoThresholdCollection(DistanceFunc<? super T> distanceFunc, T referenceObject, Iterable<? extends T> objectProvider) throws DistanceException {
        super(distanceFunc, referenceObject, objectProvider);
    }


    //****************** Overrides ******************//

    @Override
    public float getThresholdDistance() {
        return DistanceFunc.MAX_DISTANCE;
    }

}
