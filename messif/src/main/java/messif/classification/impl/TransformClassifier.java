/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.classification.impl;

import messif.classification.Classification;
import messif.classification.ClassificationException;
import messif.classification.Classifier;
import messif.data.DataObject;
import messif.record.ModifiableRecord;
import messif.record.Record;
import messif.record.processing.RecordProcessor;

/**
 * Implementation of a classifier that first applies a {@link messif.record.processing.RecordProcessor<DataObject>}
 *  on the object and than calls an encapsulated classifier.
 *
 * @param <C> the class of instances that represent the classification categories
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class TransformClassifier<C> implements Classifier<DataObject, C> {

    /** Classifier used to compute the object classification */
    private final Classifier<? super DataObject, C> classifier;
    /** Extractor that supplies the extracted objects */
    private final RecordProcessor<DataObject> extractor;
    /** Name of the parameter to put the modified (processed). object into when classifying. */
    private final String modifiedObjectParam;

    /**
     * Creates a new extractor classifier.
     * @param classifier the classifier used to compute the object classification
     * @param extractor the extractor that supplies the extracted objects
     * @param modifiedObjectParam the name of the parameter to put the extracted object into when classifying
     */
    public TransformClassifier(Classifier<? super DataObject, C> classifier, RecordProcessor<DataObject> extractor, String modifiedObjectParam) {
        this.classifier = classifier;
        this.extractor = extractor;
        this.modifiedObjectParam = modifiedObjectParam;
    }

    @Override
    public Classification<C> classify(DataObject object, Record parameters) throws ClassificationException {
        try {
            object = extractor.process(object);
            if (parameters instanceof ModifiableRecord && modifiedObjectParam != null)
                ((ModifiableRecord)parameters).setField(modifiedObjectParam, object);
            return classifier.classify(object, parameters);
        } catch (Exception e) {
            throw new ClassificationException("There was an error extracting object: " + e, e.getCause());
        }
    }

    @Override
    public Class<? extends C> getCategoriesClass() {
        return classifier.getCategoriesClass();
    }

    /**
     * Returns the extracted object stored by this classifier in the given parameters.
     * @param parameters the parameters to get the stored object from
     * @return the extracted object or <tt>null</tt> if no object was stored in the parameters
     */
    public DataObject getModifiedObject(Record parameters) {
        if (modifiedObjectParam == null || parameters == null)
            return null;
        return parameters.getField(modifiedObjectParam, DataObject.class);
    }
}
