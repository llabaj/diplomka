/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.objects;

import messif.data.DataObject;
import messif.distance.DataObjectDistanceFunc;
import messif.distance.DistanceFunc;
import messif.distance.impl.GPSDistance;
import messif.data.util.DataObjectList;
import messif.data.util.StreamDataObjectIterator;

import java.util.List;

/**
 * Allows to compute a distance histogram of a given metric space.
 * The metric space is represented by the data object class, the distances
 * are measured by taking random object pairs from a given data file.
 *
 * <p>
 * The whole data is read in and then random pairs are identified in the data.
 * </p>
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class DistanceTest {

    /**
     * Main method that deals with the parameters and computes the histogram.
     * @param args command line parameters (see usage)
     */
    public static void main(String[] args) {
        try {
            List<DataObject> objects = new DataObjectList(new StreamDataObjectIterator("src/test/resources/imagedata.json"));

            //Distance<DataObject> distance = new DataObjectDistance<>("decaf_sparse_4096", new L2DistanceFloats());
            DistanceFunc<DataObject> distanceFunc = new DataObjectDistanceFunc<>("GPS_coordinates", new GPSDistance());

            for (int i = 0; i < objects.size(); i++) {
                for (int j = i+1; j < objects.size(); j++) {
                    DataObject obj1 = objects.get(i);
                    DataObject obj2 = objects.get(j);
                    System.out.println("distance( " + obj1.getID() + ", " +obj2.getID()+" ) = "
                            + distanceFunc.getDistance(obj1, obj2));
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
