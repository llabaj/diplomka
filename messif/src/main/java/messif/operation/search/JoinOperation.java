/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.search;

import messif.distance.DistanceFunc;
import messif.data.DataObject;
import messif.data.util.DistanceRankedSortedCollection;
import messif.data.util.RankedJoinObject;
import messif.operation.answer.AbstractAnswer;
import messif.operation.ReturnDataOperation;
import messif.operation.helpers.AnswerProvider;
import messif.operation.helpers.BaseHelper;
import messif.operation.answer.JoinQueryAnswer;
import messif.operation.params.DistanceFunction;
import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Required;

import java.util.List;

/**
 * A request to do the similarity join.
 */
public interface JoinOperation extends ReturnDataOperation, DistanceFunction {

    String TYPE_STRING = "join_query";

    @Override
    default String getInnerType() {
        return TYPE_STRING;
    }


    /** The range (radius, \mu) of the metric join. */
    String MU_FIELD = "mu";

    @Get(field = MU_FIELD)
    @Required
    float getMu();


    String SKIP_SYMMETRIC_FIELD = "skip_symmetric_pairs";

    @Get(field = SKIP_SYMMETRIC_FIELD)
    boolean isSkipSymmetric(boolean defaultSkip);

    default boolean isSkipSymmetric() {
        return isSkipSymmetric(true);
    };


    /** The default list of fields to be returned by all underlying operations is only the object identifier. */
    String [] DEFAULT_LIST = new String [] {DataObject.ID_FIELD };

    /** The default list of fields to be returned by all underlying operations is only the object identifier. */
    default String[] fieldsToReturn() {
        return fieldsToReturn(DEFAULT_LIST);
    }


    /**
     * This class helps the operation processors to process this type of operation by providing supporting methods and
     *  continuous building of the answer.
     */
    class Helper extends BaseHelper<JoinOperation> implements AnswerProvider<JoinQueryAnswer> {

        /**
         * Distance function to calculate the distance between objects.
         */
        protected DistanceFunc<DataObject> distanceFunc;

        /**
         * Distance threshold
         */
        protected final float mu;

        /**
         * Flag whether symmetric pairs should be avoided in the answer
         */
        protected final boolean skipSymmetricPairs;

        protected final JoinQueryAnswer.Builder answerBuilder;


        public Helper(JoinOperation query, DistanceFunc<DataObject> defaultDistanceFunc) {
            this(query, null, defaultDistanceFunc);
        }

        public Helper(JoinOperation query, DistanceRankedSortedCollection<RankedJoinObject> answer, DistanceFunc<DataObject> defaultDistanceFunc) {
            super(query);
            this.distanceFunc = query.getDistanceFunction() == null ? defaultDistanceFunc : query.getDistanceFunction();
            this.mu = query.getMu();
            this.skipSymmetricPairs = query.isSkipSymmetric();
            this.answerBuilder = new JoinQueryAnswer.Builder(operation, (answer == null) ? new DistanceRankedSortedCollection<>() : answer);
        }

        // region ************        Utility methods for processing and answer manipulation    ****************** //

        /**
         * Evaluate this query on a given set of objects.
         * The objects found by this evaluation are added to answer of this query via {@link #addToAnswer}.
         *
         * @param objects the collection of objects on which to evaluate this query
         * @return number of objects satisfying the query
         */
        public int evaluate(List<DataObject> objects) {
            int beforeCount = answerBuilder.getAnswerSize();

            if (skipSymmetricPairs) {
                for (int i1 = 0; i1 < objects.size(); i1++) {
                    DataObject o1 = objects.get(i1);
                    for (int i2 = i1 + 1; i2 < objects.size(); i2++)
                        addToAnswer(o1, objects.get(i2));
                }
            } else {
                for (int i1 = 0; i1 < objects.size(); i1++) {
                    DataObject o1 = objects.get(i1);
                    for (int i2 = 0; i2 < i1; i2++)
                        addToAnswer(o1, objects.get(i2));
                    for (int i2 = i1 + 1; i2 < objects.size(); i2++)
                        addToAnswer(o1, objects.get(i2));
                }
            }

            return answerBuilder.getAnswerSize() - beforeCount;
        }

        /**
         * Add a new pair of objects to the answer. The rank of the pair is computed automatically
         * as a distance between the passed objects.
         *
         * @param leftObject  left object of the pair
         * @param rightObject right object of the pair
         * @return the distance-ranked join object that was added to answer or <tt>null</tt> if the object was not added
         */
        public RankedJoinObject addToAnswer(DataObject leftObject, DataObject rightObject) {
            if (leftObject == null || rightObject == null)
                return null;
            return answerBuilder.add(leftObject, rightObject, distanceFunc.getDistance(leftObject, rightObject, mu), mu);
        }
        // endregion

        // ********************      Implementation of {@link AnswerProvider} interface

        @Override
        public JoinQueryAnswer getAnswer() {
            return answerBuilder.getAnswer();
        }

        @Override
        public void updateAnswer(AbstractAnswer answer) throws ClassCastException {
            answerBuilder.updateAnswer(answer);
        }
    }
}
