/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.record;

/**
 * Extension of the {@link Record} interface that support modifications.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface ModifiableRecord extends Record {
    /**
     * Set an additional field with the given {@code name} to the given {@code value}.
     * Note that the previous value is <em>replaced</em> with the new one.
     * @param name the name of the additional field to set
     * @param value the new value for the field
     * @return the previous value of the field {@code name} or <tt>null</tt> if it was not set
     */
    Object setField(String name, Object value);

    /**
     * Removes an additional field with the given {@code name}.
     * @param name the name of the additional field to remove
     * @return the value of the field {@code name} that was removed or <tt>null</tt> if it was not set
     */
    Object removeField(String name);

    /**
     * Add all fields from a given record, overriding the existing as requested.
     * @param record the record from which to take the fields to insert into this record
     * @param override existing values are overwritten only if this flag is true
     * @return true if this record was actually modified
     */
    boolean addAllFields(Record record, boolean override);

}
