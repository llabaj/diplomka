/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.appl;

import com.sun.net.httpserver.*;
import messif.algorithm.Algorithm;
import messif.operation.answer.AbstractAnswer;
import messif.record.ModifiableRecord;
import messif.record.Record;
import messif.utility.Convert;
import messif.utility.net.HttpStatusCodeProvider;
import messif.utility.ProcessingStatus;
import messif.utility.executor.MethodExecutor.ExecutableMethod;
import messif.utility.json.JSONWriter;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManagerFactory;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.security.KeyStore;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Provides a HTTP extension to {@link Application} that allows to execute operations
 * via REST-like services using HTTP protocol.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class HttpApplication extends RMIApplication {

    //****************** Constants ******************//

    /** Parser regular expression for HTTP request parameters */
    private static final Pattern paramParser = Pattern.compile("([^=]+)=([^&]*)(?:&|$)");
    /** Name of the <parameter> to contain the POST data of the request. */
    public static final String POST_PARAM = "POST";

    //****************** Attributes ******************//

    /** HTTP server for algorithm API */
    private HttpServer httpServer;
    /** Remembered list of contexts */
    private Map<String, HttpContext> httpServerContexts;


    // region ****************      Thread local attributes managed for each query   and corresponding getters/setters

    /** Thread-safe operation as a {@link Record} prepared for execution (or already executed). */
    private final ThreadLocal<ModifiableRecord> lastOperationParams = new ThreadLocal<>();

    /** Answer of the last executed operation. */
    private final ThreadLocal<AbstractAnswer> lastAnswer = new ThreadLocal<>();

    /** Thread-safe current selected algorithm */
    private final ThreadLocal<Algorithm> algorithm = new ThreadLocal<>();
    // endregion


    @Override
    public ModifiableRecord getLastOpParams() {
        return lastOperationParams.get();
    }

    @Override
    public AbstractAnswer getLastAnswer() {
        return lastAnswer.get();
    }

    @Override
    protected boolean setLastOperation(ModifiableRecord lastOperationParams) {
        if (lastOperationParams == null) {
            return false;
        }
        this.lastOperationParams.set(lastOperationParams);
        return true;
    }

    @Override
    protected boolean setLastAnswer(AbstractAnswer lastAnswer) {
        if (lastAnswer == null) {
            return false;
        }
        this.lastAnswer.set(lastAnswer);
        return true;
    }

    /**
     * {@inheritDoc}
     * Note that if an algorithm was selected using {@link #algorithmSelectInThread},
     * it will be returned in that thread only. Otherwise, the globally {@link #algorithmSelect}
     * selected running algorithm will be used.
     *
     * @return {@inheritDoc}
     */
    @Override
    Algorithm getAlgorithm() {
        Algorithm alg = this.algorithm.get();
        return alg == null ? super.getAlgorithm() : alg;
    }


    // region **************************   Additional application methods available for http application

    /**
     * Select algorithm to manage in current thread.
     * That means that the running algorithm for the other operations is set only
     * for the current execution of the actual HTTP context.
     * A parameter with algorithm sequence number is required for specifying, which algorithm to select.
     * To unset the algorithm, use a value of -1.
     *
     * Example of usage:
     * <pre>
     * MESSIF &gt;&gt;&gt; algorithmSelectInThread 0
     * </pre>
     *
     * @param out a stream where the application writes information for the user
     * @param args the algorithm sequence number
     * @return <tt>true</tt> if the method completes successfully, otherwise <tt>false</tt>
     * @see #algorithmInfoAll
     */
    @ExecutableMethod(description = "select algorithm to manage", arguments = {"# of the algorithm to select"})
    public boolean algorithmSelectInThread(PrintStream out, String... args) {
        try {
            int algorithmIndex = Integer.parseInt(args[1]);
            if (algorithmIndex == -1)
                this.algorithm.remove();
            else
                this.algorithm.set(getAlgorithm(algorithmIndex));
        } catch (IndexOutOfBoundsException ignore) {
        } catch (NumberFormatException ignore) {
        }

        out.print("Algorithm # must be specified - use a number between 0 and ");
        out.println(getAlgorithmCount() - 1);

        return false;
    }

    /**
     * Adds a context to the HTTP server that is processed by the specified action.
     * 
     * If this method is called from <i>control file</i>, the first argument is automatically
     * set to the control file properties. Otherwise, the property file where the
     * action is defined must be specified as first argument.
     * Next, a name of the context must be given and then the action name to call.
     * The output of the action will be written to the response using the
     * content type as specified in the next argument and converted to the given charset.
     * 
     * Optionally, additional arguments specifying the content-type (defaults to "text/plain"),
     * and charset (defaults to "utf-8") can be specified.
     *
     * <p>
     * Example of usage:
     * <pre>
     * MESSIF &gt;&gt;&gt; httpAddContext myfile.cf /search my_search
     * </pre>
     * This will create a HTTP context that will execute the "my_search" action in "myfile.cf" and
     * the named instance "extracted_data" will contain the crud extracted by the named instances "extractor"
     * from the respective HTTP request.
     * </p>
     *
     * @param out a stream where the application writes information for the user
     * @param args the control file (only if not called from a control file), the context path, the action name to call, the map of context extractors, the content type of the output, and the charset of the output
     * @return <tt>true</tt> if the method completes successfully, otherwise <tt>false</tt>
     */
    @ExecutableMethod(description = "add HTTP server context", arguments = {"control file", "context path", "action name(s)", "content type [application/json]", "charset [utf8]", "flag if POST contains parameters [false]"})
    public boolean httpAddContext(PrintStream out, String... args) {
        Properties props = new Properties();
        try {
            props.load(new FileInputStream(args[1]));
        } catch (IndexOutOfBoundsException e) {
            out.println("Property file with action definition must be specified");
            return false;
        } catch (IOException e) {
            out.println("Error reading " + args[1] + ": " + e);
            return false;
        }
        return httpAddContext(out, props, args, 2, null);
    }

    /**
     * Internal method for adding a context to the HTTP server.
     *
     * @param out a stream where the application writes information for the user
     * @param props control-file properties where the action is defined
     * @param args the context path, the action name to call, the map of context extractors, the content type of the output, the charset of the output, and flag whether to parse POST method parameters
     * @param argIndex index in the {@code args} array where to start reading
     * @param variables list of variables set by the application
     * @return <tt>true</tt> if the method completes successfully, otherwise <tt>false</tt>
     */
    private boolean httpAddContext(PrintStream out, Properties props, String[] args, int argIndex, Map<String, String> variables) {
        if (httpServer == null) {
            out.println("There is no HTTP server started");
            return false;
        }

        if (args.length < argIndex + 2) {
            out.println("At least the context path and action name must be specified");
            return false;
        }

        if (httpServerContexts.containsKey(args[argIndex])) {
            out.println("Context '" + args[argIndex] + "' already exists");
            return false;
        }

        String[] actionNames = args[argIndex + 1].split("\\s+");
        //Map<String, String> contextExtractorNames = args.length >= argIndex + 3 ? Convert.stringToMap(args[argIndex + 2]) : null;
        String contentType = args.length >= argIndex + 3 ? args[argIndex + 2] : "application/json";
        String charset = args.length >= argIndex + 4 ? args[argIndex + 3] : "utf8";
        boolean parsePostParameters = args.length >= argIndex + 5 ? Boolean.parseBoolean(args[argIndex + 4]) : false;

        for (String actionName : actionNames) {
            if (!props.containsKey(actionName))
                throw new IllegalArgumentException("Cannot find action '" + actionName + "' in the given control file crud");
        }

        try {
            HttpContext context = httpServer.createContext(
                    args[argIndex],
                    new HttpApplicationHandler(props, actionNames, contentType, charset, parsePostParameters, variables)
            );
            httpServerContexts.put(args[argIndex], context);
            return true;
        } catch (Exception e) {
            out.println("Cannot add HTTP context " + args[argIndex] + ": " + e);
            return false;
        }
    }

    /**
     * Removes a context from the HTTP server.
     *
     * <p>
     * Example of usage:
     * <pre>
     * MESSIF &gt;&gt;&gt; httpRemoveContext /search
     * </pre>
     * </p>
     *
     * @param out a stream where the application writes information for the user
     * @param args the context path to remove
     * @return <tt>true</tt> if the method completes successfully, otherwise <tt>false</tt>
     */
    @ExecutableMethod(description = "add HTTP server context", arguments = {"context path"})
    public boolean httpRemoveContext(PrintStream out, String... args) {
        if (httpServer == null) {
            out.println("There is no HTTP server started");
            return false;
        }

        if (args.length < 2) {
            out.println("The context path must be provided");
            return false;
        }

        httpServer.removeContext(args[1]);
        httpServerContexts.remove(args[1]);
        return true;
    }

    /**
     * List current contexts of the HTTP server.
     *
     * <p>
     * Example of usage:
     * <pre>
     * MESSIF &gt;&gt;&gt; httpListContexts
     * </pre>
     * </p>
     *
     * @param out a stream where the application writes information for the user
     * @param args none
     * @return <tt>true</tt> if the method completes successfully, otherwise <tt>false</tt>
     */
    @ExecutableMethod(description = "list HTTP server contexts", arguments = {})
    public boolean httpListContexts(PrintStream out, String... args) {
        if (httpServer == null) {
            out.println("There is no HTTP server started");
            return false;
        }

        for (Map.Entry<String, HttpContext> entry : httpServerContexts.entrySet()) {
            Authenticator authenticator = entry.getValue().getAuthenticator();
            // Show authorized users if the authenticator is set
            if (authenticator != null) {
                out.print(entry.getKey());
                out.print(" (authorized users: ");
                out.print(authenticator);
                out.println(")");
            } else {
                out.println(entry.getKey());
            }
        }

        return true;
    }

    /**
     * Updates a context auth to use HTTP Basic authentication mechanism.
     * The simple username/password pairs can be added to this method to
     * authorize a given user to use the specified context path. Several
     * users can be added by subsequent calls to this method.
     * If a password is not provided for a user, the user is removed from
     * the context. If a last user is removed, the authentication is removed
     * from the context completely.
     *
     * <p>
     * Example of usage:
     * <pre>
     * MESSIF &gt;&gt;&gt; httpSetContextAuth /search myuser mypassword
     * </pre>
     * </p>
     *
     * @param out a stream where the application writes information for the user
     * @param args the context path for which to set auth, the user name and the password (optional, if not set, the user is removed)
     * @return <tt>true</tt> if the method completes successfully, otherwise <tt>false</tt>
     */
    @ExecutableMethod(description = "update authorized users for the HTTP context", arguments = {"context path", "user name", "password (if not set, user is removed)"})
    public boolean httpSetContextAuth(PrintStream out, String... args) {
        if (httpServer == null) {
            out.println("There is no HTTP server started");
            return false;
        }

        if (args.length < 3) {
            out.println("The context path and user must be provided");
            return false;
        }

        HttpContext context = httpServerContexts.get(args[1]);
        if (context == null) {
            out.println("There is no context for " + args[1]);
            return false;
        }

        HttpApplicationAuthenticator authenticator = (HttpApplicationAuthenticator)context.getAuthenticator();
        if (authenticator == null)
            authenticator = new HttpApplicationAuthenticator();
        authenticator.updateCredentials(args[2], args.length > 3 ? args[3] : null);
        context.setAuthenticator(authenticator.hasCredentials() ? authenticator : null);

        return true;
    }
    // endregion


    // region ********************    Overrides of {@link Application} methods

    @ExecutableMethod(description = "close the whole application (all connections will be closed including the HTTP server)", arguments = { })
    @Override
    public boolean quit(PrintStream out, String... args) {
        if (!super.quit(out, args))
            return false;
        httpServer.stop(0);
        return true;
    }

    @Override
    protected boolean controlFileExecuteMethod(PrintStream out, Properties props, String actionName, Map<String, String> variables, List<String> arguments) throws NoSuchMethodException, InvocationTargetException {
        if (arguments.get(0).equals("httpAddContext"))
            return httpAddContext(out, props, arguments.toArray(new String[arguments.size()]), 1, variables);
        return super.controlFileExecuteMethod(out, props, actionName, variables, arguments);
    }

    @Override
    protected String usage() {
        return "<http port> [-httpThreads <0|n>] [-httpBacklog <m>] [-sslKeyStore <file> [-sslTrustStore <file>] [-sslKeyStorePassword <password>]]" + super.usage();
    }

    @Override
    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    protected boolean parseArguments(String[] args, int argIndex) {
        if (argIndex >= args.length)
            return false;

        // Read http port argument
        int httpPort;
        try {
            httpPort = Integer.parseInt(args[argIndex]);
            argIndex++;
        } catch (NumberFormatException e) {
            System.err.println("HTTP port is not valid: " + e.getMessage());
            return false;
        }

        // Read http threads parameter
        int httpThreads = 1;
        if (argIndex < args.length && args[argIndex].equalsIgnoreCase("-httpThreads")) {
            try {
                httpThreads = Integer.parseInt(args[argIndex + 1]);
                argIndex += 2;
            } catch (IndexOutOfBoundsException e) {
                System.err.println("httpThreads parameter requires a number of threads");
                return false;
            } catch (NumberFormatException e) {
                System.err.println("Number of httpThreads is invalid: " + e.getMessage());
                return false;
            }
        }

        int httpBacklog = 0;
        if (argIndex < args.length && args[argIndex].equalsIgnoreCase("-httpBacklog")) {
            try {
                httpBacklog = Integer.parseInt(args[argIndex + 1]);
                argIndex += 2;
            } catch (IndexOutOfBoundsException e) {
                System.err.println("httpBacklog parameter requires a number of incoming connections in the backlog");
                return false;
            } catch (NumberFormatException e) {
                System.err.println("Size of httpBacklog is invalid: " + e.getMessage());
                return false;
            }
        }

        File sslKeyStore = null;
        if (argIndex < args.length && args[argIndex].equalsIgnoreCase("-sslKeyStore")) {
            try {
                if (!args[argIndex + 1].isEmpty())
                    sslKeyStore = new File(args[argIndex + 1]).getCanonicalFile();
                argIndex += 2;
            } catch (IOException e) {
                System.err.println("Cannot read keystore file: " + e);
                return false;
            }
        }

        File sslTrustStore = null;
        if (argIndex < args.length && args[argIndex].equalsIgnoreCase("-sslTrustStore")) {
            try {
                if (!args[argIndex + 1].isEmpty())
                    sslTrustStore = new File(args[argIndex + 1]).getCanonicalFile();
                argIndex += 2;
            } catch (IOException e) {
                System.err.println("Cannot read truststore file: " + e);
                return false;
            }
        }

        String sslKeyStorePassword = null;
        if (argIndex < args.length && args[argIndex].equalsIgnoreCase("-sslKeyStorePassword")) {
            if (!args[argIndex + 1].isEmpty())
                sslKeyStorePassword = args[argIndex + 1];
            argIndex += 2;
        }

        try {
            if (sslKeyStore != null) {
                HttpsServer httpsServer = HttpsServer.create(new InetSocketAddress(httpPort), httpBacklog);
                try {
                    final boolean needClientAuth = sslTrustStore != null;
                    httpsServer.setHttpsConfigurator(new HttpsConfigurator(createHttpSSLContext(sslKeyStore, sslTrustStore, sslKeyStorePassword == null ? null : sslKeyStorePassword.toCharArray())) {
                        @Override
                        public void configure(HttpsParameters params) {
                            SSLParameters defaultParams = getSSLContext().getDefaultSSLParameters();
                            // Note that there is a BUG in java server that sets the need client auth to FALSE when not passed as SSLParameters!!!!!
                            defaultParams.setNeedClientAuth(needClientAuth);
                            params.setSSLParameters(defaultParams);
                        }
                    });
                } catch (Exception e) {
                    System.err.println("Cannot initialize SSL encryption: " + e);
                    return false;
                }
                httpServer = httpsServer;
            } else {
                httpServer = HttpServer.create(new InetSocketAddress(httpPort), httpBacklog);
            }
            if (httpThreads == 0) {
                httpServer.setExecutor(Executors.newCachedThreadPool());
            } else if (httpThreads > 1) {
                httpServer.setExecutor(Executors.newFixedThreadPool(httpThreads));
            }
            httpServerContexts = new HashMap<String, HttpContext>();
        } catch (NumberFormatException e) {
            System.err.println("HTTP port is not valid: " + e.getMessage());
            return false;
        } catch (IOException e) {
            System.err.println("Cannot start HTTP server: " + e);
            return false;
        }

        boolean ret = super.parseArguments(args, argIndex);

        // Start the HTTP server
        httpServer.start();

        return ret;
    }
    // endregion

    /**
     * The standalone application's main methods - start a MESSIF application with HTTP server.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new HttpApplication().startApplication(args);
    }


    // region  ******************  HTTP requests handler

//    /**
//     * Parse search string parameters.
//     * The search string has the following format:
//     * <code>name1=value1&amp;name2=value2&amp;...</code>
//     * Note that the search string can be get by
//     * {@link HttpExchange#getRequestURI()}{@link java.net.URI#getQuery() .getQuery()}.
//     *
//     * @param query the string with URI-like search parameters
//     * @param parameters an existing hash map to add the parsed parameters to or <tt>null</tt>
//     * @return a hash map with parsed search string parameters (key represents the
//     *          parameter name and value is the parameter value)
//     */
//    public static Map<String, String> parseParameters(String query, Map<String, String> parameters) {
//        if (query == null || query.isEmpty()) {
//            if (parameters == null)
//                return Collections.emptyMap();
//            return parameters;
//        }
//        Matcher matcher = paramParser.matcher(query);
//        if (parameters == null)
//            parameters = new HashMap<>();
//        while (matcher.find())
//            try {
//                parameters.put(matcher.group(1), URLDecoder.decode(matcher.group(2), "utf-8"));
//            } catch (UnsupportedEncodingException e) {
//                throw new InternalError("Charset utf-8 should be always supported, but there was " + e);
//            }
//        return parameters;
//    }

    /**
     * Internal implementation of the {@link HttpHandler} that processes the action of the given control file.
     */
    private class HttpApplicationHandler implements HttpHandler {
        /** Control-file properties where the action is defined */
        private final Properties props;
        /** Action names to call */
        private final String[] actionNames;
        /** Content type of the output */
        private final String contentType;
        /** Charset of the output */
        private final String charset;
        /** Flag whether to parse parameters in the request body if the method is POST */
        private final boolean parsePostParameters;
        /** List of variables set by the application */
        private final Map<String, String> variables;

        /**
         * Creates a new control-file action handler executed from a HTTP context.
         * @param props the control-file properties where the action is defined
         * @param actionNames the action names to call
         * @param contentType the content type of the output
         * @param charset the charset of the output
         * @param parsePostParameters flag whether to parse parameters in the request body if the method is POST
         * @param variables list of variables set by the application
         */
        private HttpApplicationHandler(Properties props, String[] actionNames, String contentType, String charset, boolean parsePostParameters, Map<String, String> variables) {
            this.props = props;
            this.actionNames = actionNames;
            this.contentType = contentType;
            this.charset = charset;
            this.parsePostParameters = parsePostParameters;
            this.variables = new HashMap<>(variables);
        }

        /**
         * Handles a HTTP request by executing a control-file action and returns its response.
         *
         * @param exchange the HTTP request/response exchange
         * @throws IOException if there was a problem reading the HTTP request or writing the HTTP response
         */
        @Override
        public void handle(HttpExchange exchange) throws IOException {
            exchange.getResponseHeaders().set("Content-type", contentType + ";charset=" + charset);
            try {
                Map<String, String> requestVariables =  new HashMap<>(variables);
                obtainParameters(exchange, requestVariables);
                handleWithBufferedOutput(exchange, requestVariables);
            } catch (InvocationTargetException e) {
                log.log(Level.WARNING, "Http context InvocationTargetException", e);
                handleThrowable(exchange, e.getCause());
            } catch (IOException e) {
                handleThrowable(exchange, e);
                throw e;
            } catch (Exception e) {
                log.log(Level.WARNING, "Http context Exception", e);
                handleThrowable(exchange, e);
            }
        }

        /**
         * Handles a HTTP request by executing a control-file action and returns its response to buffered HTTP output.
         * Note that this method handles invalid arguments passed to action but requires more memory to buffer
         * the whole output of the action.
         *
         * @param exchange the HTTP request/response exchange
         * @param requestVariables an independent map of variables for this request
         * @throws IOException if there was a problem reading the HTTP request or writing the HTTP response
         * @throws InvocationTargetException if there was an exception during the action execution
         */
        private void handleWithBufferedOutput(HttpExchange exchange, Map<String, String> requestVariables) throws IOException, InvocationTargetException {
            ByteArrayOutputStream outBuffer = new ByteArrayOutputStream();
            PrintStream out = new PrintStream(outBuffer, true, charset);
            boolean success = true;
            for (int i = 0; success && i < actionNames.length; i++) {
                success = controlFileExecuteAction(out, props, actionNames[i], requestVariables, null, true);
            }
            int httpStatusCode = (getLastAnswer() != null ) ? getLastAnswer().getStatus().getHttpStatusCode() : HttpStatusCodeProvider.STATUS_CODE_NOT_SET;
            if (httpStatusCode == HttpStatusCodeProvider.STATUS_CODE_NOT_SET) {
                httpStatusCode = success ? HttpStatusCodeProvider.STATUS_CODE_OK : HttpStatusCodeProvider.STATUS_CODE_INVALID_ARGUMENT;
            }
            exchange.sendResponseHeaders(httpStatusCode, outBuffer.size());
            out.close();
            OutputStream response = exchange.getResponseBody();
            outBuffer.writeTo(response);
            response.close();
        }

        /**
         * Handles a HTTP request by executing a control-file action and returns its response directly to HTTP output.
         * Note that this method cannot handle invalid execution correctly.
         *
         * @param exchange the HTTP request/response exchange
         * @param requestVariables an independent map of variables for this request
         * @throws IOException if there was a problem reading the HTTP request or writing the HTTP response
         * @throws InvocationTargetException if there was an exception during the action execution
         */
        private void handleWithDirectOutput(HttpExchange exchange, Map<String, String> requestVariables) throws IOException, InvocationTargetException { // This is currently not used
            exchange.sendResponseHeaders(HttpStatusCodeProvider.STATUS_CODE_OK, 0);
            PrintStream out = new PrintStream(exchange.getResponseBody(), true, charset);
            for (String actionName : actionNames) {
                controlFileExecuteAction(out, props, actionName, requestVariables, null, true);
            }
            out.close();
        }

        /**
         * Handles a HTTP request by executing a control-file action and returns its response directly to HTTP output.
         * Note that this method cannot handle invalid execution correctly.
         *
         * @param exchange the HTTP request/response exchange
         * @param throwable the {@link Throwable} exception to handle
         * @throws IOException if there was a problem reading the HTTP request or writing the HTTP response
         * @throws NullPointerException if the given {@code throwable} was <tt>null</tt>
         */
        private void handleThrowable(HttpExchange exchange, Throwable throwable) throws IOException, NullPointerException {
            int errorCode = HttpStatusCodeProvider.STATUS_CODE_INTERNAL_ERROR;
            do {
                if ((throwable instanceof HttpStatusCodeProvider) && ((HttpStatusCodeProvider) throwable).isHttpStatusSet()) {
                    errorCode = ((HttpStatusCodeProvider) throwable).getHttpStatusCode();
                    break;
                }
                if (throwable.getCause() != null) {
                    throwable = throwable.getCause();
                }
            } while (throwable.getCause() != null);

            // create an error status with the stack trace in "error message"
            StringWriter writer = new StringWriter();
            throwable.printStackTrace(new PrintWriter(writer));
            final ProcessingStatus errorStatus = new ProcessingStatus(ProcessingStatus.UNKNOWN_ERROR, writer.toString());
            // wrap the error status into an AbstractAnswer
            AbstractAnswer answer = AbstractAnswer.createAnswer(errorStatus);

            // print it to the output
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            final JSONWriter jsonWriter = new JSONWriter(byteArrayOutputStream);
            jsonWriter.write(answer);
            jsonWriter.flush();
            byte[] outBuffer = byteArrayOutputStream.toByteArray();

            exchange.sendResponseHeaders(errorCode, outBuffer.length);
            OutputStream response = exchange.getResponseBody();
            response.write(outBuffer);
            response.close();
        }

        /**
         * Returns url-encoded parameters passed in the request.
         * @param exchange the HTTP exchange from which to get the search string
         * @return the string with encoded parameters
         * @throws IOException if there was an error reading the parameters
         */
        private void obtainParameters(HttpExchange exchange, Map<String, String> parameters) throws IOException {
            String queryString = exchange.getRequestURI().getRawQuery();
            String postData = new String(Convert.readStreamData(exchange.getRequestBody(), 0));
            if (!postData.isEmpty()) {
                // parse the post as it contains other parameters
                String requestContent = exchange.getRequestHeaders().getFirst("Content-type");
                if (parsePostParameters && "post".equalsIgnoreCase(exchange.getRequestMethod()) &&
                        (requestContent != null && requestContent.toLowerCase().startsWith("application/x-www-form-urlencoded"))) {
                    if (queryString != null && !queryString.isEmpty()) {
                        queryString += "&" + postData;
                    } else {
                        queryString = postData;
                    }
                } else { // put the POST data into a variable
                    parameters.put(POST_PARAM, postData);
                }
            }
            parseParameters(queryString, parameters);
        }

        /**
         * Parse search string parameters.
         * The search string has the following format:
         * <code>name1=value1&amp;name2=value2&amp;...</code>
         * Note that the search string can be get by
         * {@link HttpExchange#getRequestURI()}{@link java.net.URI#getQuery() .getQuery()}.
         *
         * @param query the string with URI-like search parameters
         * @param parameters an existing hash map to add the parsed parameters to or <tt>null</tt>
         * @return a hash map with parsed search string parameters (key represents the
         *          parameter name and value is the parameter value)
         */
        protected Map<String, String> parseParameters(String query, Map<String, String> parameters) {
            if (query == null || query.isEmpty()) {
                if (parameters == null)
                    return Collections.emptyMap();
                return parameters;
            }
            Matcher matcher = paramParser.matcher(query);
            if (parameters == null)
                parameters = new HashMap<>();
            while (matcher.find())
                try {
                    parameters.put(matcher.group(1), URLDecoder.decode(matcher.group(2), "utf-8"));
                } catch (UnsupportedEncodingException e) {
                    throw new InternalError("Charset utf-8 should be always supported, but there was " + e);
                }
            return parameters;
        }

//        /**
//         * Execute context extractors.
//         * @param exchange the HTTP request/response exchange
//         * @param requestVariables an independent map of variables for this request
//         * @throws ExtractorException if there was a problem with one of the context extractors
//         */
//        private void updateContextExtractors(HttpExchange exchange, Map<String, String> requestVariables) throws ExtractorException {
//            if (contextExtractors == null && contextMultiExtractors == null)
//                return;
//            ExtractorDataSource dataSource = new ExtractorDataSource(exchange.getRequestBody(), requestVariables);
//            File tempFile = null;
//            try {
//                if (postDataTemporaryFilePrefix != null) {
//                    tempFile = dataSource.pipeToTemporaryFile(postDataTemporaryFilePrefix, postDataTemporaryFileSuffix, postDataTemporaryFileDirectory);
//                    dataSource = new ExtractorDataSource(tempFile, requestVariables);
//                }
//                if (contextExtractors != null) {
//                    for (Map.Entry<ThreadLocal<LocalAbstractObject>, Extractor<?>> contextExtractor : contextExtractors.entrySet()) {
//                        contextExtractor.getKey().set(contextExtractor.getValue().extract(dataSource));
//                    }
//                }
//                if (contextMultiExtractors != null) {
//                    for (Map.Entry<ThreadLocal<Iterator<?>>, MultiExtractor<?>> contextExtractor : contextMultiExtractors.entrySet()) {
//                        contextExtractor.getKey().set(contextExtractor.getValue().extract(dataSource));
//                    }
//                }
//            } catch (IOException e) {
//                throw new ExtractorException("Error executing context extractor: " + e, e);
//            } finally {
//                if (tempFile != null)
//                    tempFile.delete();
//            }
//        }
    }

    /**
     * Authenticator for the {@link HttpServer HTTP server}
     * that provides a basic HTTP authentication based on the stored user/password
     * values.
     * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
     * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
     * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
     * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
     */
    public static class HttpApplicationAuthenticator extends BasicAuthenticator {

        //****************** Constants ******************//

        /** Default realm for the constructor */
        public static final String DEFAULT_REALM = "MESSIF HTTP Application";


        //****************** Attributes ******************//

        /** List of users and their passwords for the authentication */
        private final Map<String, String> credentials;


        //****************** Constructors ******************//

        /**
         * Creates a HttpApplicationAuthenticator for the given HTTP realm.
         * @param realm the HTTP Basic authentication realm
         */
        public HttpApplicationAuthenticator(String realm) {
            super(realm);
            this.credentials = new HashMap<String, String>();
        }

        /**
         * Creates a HttpApplicationAuthenticator for the {@link #DEFAULT_REALM default realm}.
         */
        public HttpApplicationAuthenticator() {
            this(DEFAULT_REALM);
        }

        /**
         * Updates the stored credentials of this authenticator.
         * If the password is <tt>null</tt>, the user is removed.
         * Otherwise, the user identified by the password is added to the
         * list of known users (replacing the previous password if the user
         * was already known).
         *
         * @param username the user name to add to the list
         * @param password the password for the username to add to the list
         */
        public void updateCredentials(String username, String password) {
            if (password == null)
                credentials.remove(username);
            else
                credentials.put(username, password);
        }

        /**
         * Returns whether this authenticator has at least one user set.
         * @return <tt>true</tt> if there are credentials for at least one user
         */
        public boolean hasCredentials() {
            return !credentials.isEmpty();
        }

        /**
         * Called for each incoming request to verify the given name and password
         * in the context of this Authenticator's realm.
         * @param username the username from the request
         * @param password the password from the request
         * @return <tt>true</tt> if the credentials are valid, <tt>false</tt> otherwise
         */
        @Override
        public boolean checkCredentials(String username, String password) {
            String checkPasswd = credentials.get(username);
            return checkPasswd != null && checkPasswd.equals(password);
        }

        @Override
        public String toString() {
            StringBuilder str = new StringBuilder();
            Iterator<String> iterator = credentials.keySet().iterator();
            while (iterator.hasNext()) {
                str.append(iterator.next());
                if (iterator.hasNext())
                    str.append(", ");
            }
            return str.toString();
        }
    }

    /**
     * Creates a SSL context for handling HTTPS requests.
     * A sample key store can be created using "keytool" utility:
     * <code>
     *   keytool -genkey -alias alias -keypass <em>my_password</em> -storepass <em>my_password</em> -keystore <em>my_keystore_file</em>
     * </code>
     * @param keystoreFile the key store file with the authentication keys
     * @param truststoreFile the key store file with the peer authentication trust decisions
     * @param password the password to unlock the key stores and private key
     * @return a new SSL context for handling HTTPS requests
     * @throws Exception if there was an error creating the SSL context (missing cryptography engines, protocols, etc.)
     */
    public static SSLContext createHttpSSLContext(File keystoreFile, File truststoreFile, char[] password) throws Exception {
        // Load key manager
        FileInputStream keystoreInputStream = new FileInputStream(keystoreFile);
        KeyManagerFactory kmf;
        try {
            KeyStore ks = KeyStore.getInstance("JKS");
            ks.load(keystoreInputStream, password);
            kmf = KeyManagerFactory.getInstance("SunX509");
            kmf.init(ks, password);
        } finally {
            keystoreInputStream.close();
        }

        // Load trust manager
        TrustManagerFactory tmf;
        if (truststoreFile != null) {
            FileInputStream truststoreInputStream = new FileInputStream(truststoreFile);
            try {
                KeyStore ks = KeyStore.getInstance("JKS");
                ks.load(truststoreInputStream, password);
                tmf = TrustManagerFactory.getInstance("SunX509");
                tmf.init(ks);
            } finally {
                truststoreInputStream.close();
            }
        } else {
            tmf = null;
        }

        // Create SSL context
        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(kmf.getKeyManagers(), tmf == null ? null : tmf.getTrustManagers(), null);
        return sslContext;
    }
}
