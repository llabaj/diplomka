jdk-10/bin/java \
    -Dhazelcast.config=hc.xml \
    -Dapp.config=app.config \
    --add-opens jdk.management/com.sun.management.internal=ALL-UNNAMED \
    -Dcom.sun.management.jmxremote.port=1616 \
    -Dcom.sun.management.jmxremote.rmi.port=1616 \
    -Dcom.sun.management.jmxremote.authenticate=false \
    -Dcom.sun.management.jmxremote.ssl=false \
    -Djava.rmi.server.hostname=127.0.0.1 \
    -jar webapp-full.jar \
    --server.port=8080

