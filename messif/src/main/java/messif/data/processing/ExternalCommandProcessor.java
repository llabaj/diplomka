/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.processing;

import messif.data.DataObject;
import messif.record.Record;
import messif.record.processing.BinaryDataSource;
import messif.record.processing.RecordProcessingException;
import messif.record.processing.RecordProcessors;
import messif.utility.Convert;
import messif.utility.ExternalProcessInputStream;
import messif.utility.json.JSONReader;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Connects to a given socket(s), sends the binary data either read from a file or from a URL (file) and sends it to the
 *  socket. Then it reads the response (text) and creates another field from this text.
 */
class ExternalCommandProcessor extends AddFieldProcessor {

    private final String command;
    private final boolean fileAsArgument;

    /** Suffix for the external extractor temp file. If <tt>null</tt>, no temp file can be created. */
    private String tempFileSuffix;

    /**
     *
     * @param command the external command (including all necessary arguments)
     * @param fileAsArgument if <tt>true</tt>, the "%s" argument of external command is replaced with the filename
     * @param uriField
     * @param outputField
     */
    public ExternalCommandProcessor(final String command, final boolean fileAsArgument, String tempFileSuffix, final String uriField, String outputField) {
        super(uriField, outputField);
        this.command = command;
        this.fileAsArgument = fileAsArgument;
        this.tempFileSuffix = tempFileSuffix;
    }

    /**
     * Calls an external extractor command and returns its output.
     * If {@code fileAsArgument} is <tt>true</tt>, the {@code dataSource} must
     * represent a valid file, which is passed in place of %s parameter in the {@code command}.
     * Otherwise, the external extractor receives the data from the {@code dataSource}
     * on its standard input. Note that variable substitution using question-mark enclosure
     * is applied using {@link Record#getMap()} data source parameters}, i.e.
     * every ?variable-name:default-value? is replaced with the value of {@link Record#getField(String)}("requiredField").
     *
     * @return the extracted data read from the standard output of the {@code command}
     * @throws IOException if there was a problem calling the external command or passing arguments to it
     */
    public InputStream callExternalExtractor(final Record record) throws IOException {
        Process extractorProcess;
        String finalCommand = Convert.substituteVariables(command, record.getMap());

        // send the file name (either existing or temporary) as a parameter to the command
        if (fileAsArgument) {
            File file = new File(record.getField(requiredField).toString());
            if (file.exists()) {
                extractorProcess = Runtime.getRuntime().exec(Convert.splitBySpaceWithQuotes(String.format(finalCommand, file.getAbsoluteFile())));
            } else {
                if (tempFileSuffix == null)
                    throw new IOException("Temp file suffix cannot be null");
                // External extractor requires file, but the data sources is not a file - store the data into a temporary file
                BinaryDataSource dataSource = RecordProcessors.openBinarySource(record, requiredField);
                final File tempFile = dataSource.pipeToTemporaryFile("externalExtractorTempFile_", tempFileSuffix, null);
                return new BufferedInputStream(new ExternalProcessInputStream(Runtime.getRuntime().exec(Convert.splitBySpaceWithQuotes(String.format(finalCommand, tempFile.getAbsoluteFile())))) {
                    @Override
                    protected void onExit(int processExitCode) {
                        tempFile.delete();
                    }
                });
            }
        } else {
            // open a binary source that represents a URL or a file, read the data from the URL or file and send it to STDIN of the process
            BinaryDataSource dataSource = RecordProcessors.openBinarySource(record, requiredField);
            extractorProcess = Runtime.getRuntime().exec(Convert.splitBySpaceWithQuotes(finalCommand));
            new BinaryDataSource.PipeThread(dataSource, extractorProcess.getOutputStream()).start();
        }

        return new BufferedInputStream(new ExternalProcessInputStream(extractorProcess));
    }

    @Override
    protected Object getFieldValue(DataObject record) {
        // Run the external command and read the output from it
        try (InputStream inputStream = callExternalExtractor(record)) {
            // read the text answer from the socket and create a new field from the data (JSON-formatted)
            final JSONReader jsonReader = new JSONReader(inputStream, false);
            return jsonReader.readNextObject();
        } catch (IOException e) {
            throw new RecordProcessingException("error running external command or reading its output", e);

        }
    }
}
