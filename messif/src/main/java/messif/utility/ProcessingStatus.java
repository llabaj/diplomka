/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility;

import messif.utility.json.JSONException;
import messif.utility.json.JSONWritable;
import messif.utility.json.JSONWriter;
import messif.utility.net.HttpStatusCodeProvider;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;


/**
 * Base class for all error codes.
 * Error codes represent a return value that can be used to indicate various
 * states of execution. Error codes are compared according to their text representation.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class ProcessingStatus implements Serializable, JSONWritable, HttpStatusCodeProvider {
    /** Class serial id for serialization */
    private static final long serialVersionUID = 1L;

    //***************** Standard error codes *****************//

    /** The initial value of an error code that was not set yet. */
    public static ProcessingStatus NOT_SET = new ProcessingStatus("unknown status", 0);

    /** Not specific error appeared. You may look at the source code which produced it to get some help. */
    public static ProcessingStatus UNKNOWN_ERROR = new ProcessingStatus("unknown error", 500, HttpStatusCodeProvider.STATUS_CODE_INTERNAL_ERROR);


    // ***********************   Name of the map attributes   ********************** //

    public static final String NUM_CODE_FIELD = "code";

    public static final String TEXT_CODE_FIELD = "text";

    public static final String ERROR_DESC_FIELD = "error description";

    //***************** Attributes *****************//

    /** Holder of the current error code text */
    //private final String text;

    /** Holder of the current error code text */
    private final int code;

    private final Map<String, Object> values;

    private final int httpStatusCode;


    //***************** Constructors *****************//

    /**
     * Creates a new instance of ProcessingStatus.
     * Use this constructor to create static members in classes to define
     * new error codes.
     * @param text the text representation of the new error code
     * @param code
     */
    public ProcessingStatus(String text, int code) {
        this(text, code, HttpStatusCodeProvider.STATUS_CODE_NOT_SET);
    }

    /**
     * Creates a new instance of ProcessingStatus.
     * Use this constructor to create static members in classes to define
     * new error codes.
     * @param text the text representation of the new error code
     * @param code
     */
    public ProcessingStatus(String text, int code, int httpStatusCode) {
        this.values = new ArrayMap<>(new String [] {NUM_CODE_FIELD, TEXT_CODE_FIELD}, new Object[] {code, text}, true);
        this.code = code;
        this.httpStatusCode = httpStatusCode;
    }

    /**
     * Creates a new instance of ProcessingStatus.
     * Use this constructor to create static members in classes to define
     * new error codes.
     * @param originalStatus a status to enrich by an errorDescription
     * @param errorDescription a text description of the error (can be also a stack trace)
     */
    public ProcessingStatus(ProcessingStatus originalStatus, String errorDescription) {
        this.values = new ArrayMap<>(new String [] {NUM_CODE_FIELD, TEXT_CODE_FIELD, ERROR_DESC_FIELD},
                new Object[] {originalStatus.code, originalStatus.values.get(TEXT_CODE_FIELD), errorDescription}, true);
        this.code = originalStatus.code;
        this.httpStatusCode = originalStatus.getHttpStatusCode();
    }


    //***************** State query methods *****************//

    /**
     * Returns <tt>true</tt> if the error code is not set yet (i.e. has the value of {@link #NOT_SET}).
     * @return <tt>true</tt> if the error code is not set yet
     */
    public final boolean isSet() {
        return !NOT_SET.equals(this);
    }

    @Override
    public int getHttpStatusCode() {
        return httpStatusCode;
    }

    public int getCode() {
        return code;
    }

    public String getErrorDesc() {
        return (String) values.get(ERROR_DESC_FIELD);
    }

    //***************** Overridden methods *****************//

    /**
     * Returns the textual representation of this error code.
     * @return the textual representation of this error code
     */
    @Override
    public final String toString() {
        return (String) values.get(TEXT_CODE_FIELD);
    }

    @Override
    public final int hashCode() {
        return code;
    }
    
    @Override
    public final boolean equals(Object object) {
        if (object instanceof ProcessingStatus)
            return ((ProcessingStatus) object).code == code;
            // return ((ProcessingStatus)object).text.equals(text) && (object.getClass().equals(this.getClass()));
        else return false;
    }


    // ***********************    JSONWritable   ********************


    @Override
    public JSONWriter writeJSON(JSONWriter writer) throws JSONException, IOException {
        return this.writeJSON(writer, 0);
    }

    @Override
    public JSONWriter writeJSON(JSONWriter writer, int level) throws JSONException, IOException {
        writer.write(values, level);
        return writer;
    }



    /**
     * Returns whether ther result code of this operation is one of the given {@code statuses}.
     * @param statuses the result codes to check
     * @return <tt>true</tt> if this operation's error code is equal to one of the given error codes
     *          or <tt>false</tt> if the error code is different
     */
    public boolean in(ProcessingStatus... statuses) {
        if (statuses != null)
            for (ProcessingStatus status : statuses)
                if (this == status)
                    return true;
        return false;
    }

}
