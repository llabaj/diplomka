/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.net;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

/**
 * An connector to a HTTP service that assumes a string and returns a string. More than one service URLs can be
 * specified and they are served in a round-robin manner.
 */
public class HttpClient {

    /**
     * A list of URLs to be served in a round-robin manner.
     */
    protected final URL [] urls;

    private int currentIndex;

    public HttpClient(URL url) {
        this(new URL[] {url});
    }

    public HttpClient(String url) throws MalformedURLException {
        this(new URL[] {new URL(url)});
    }

    public HttpClient(URL [] urls) {
        this.urls = urls;
    }

    /**
     * Sends given text as a POST request to the HTTP server and returns the text response.
     * @param requestString a text request to be sent to via POST
     * @return a text returned from the server
     */
    public String sendRequest(String requestString) throws IOException, HttpStatusException {
        //System.out.println("sending data: " + requestString);
        final HttpURLConnection connection = createConnection();

        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
        wr.writeBytes(requestString);
        wr.flush();
        wr.close();

        int responseCode = connection.getResponseCode();
        if (! HttpStatusCodeProvider.isOK(responseCode)) {
            throw new HttpStatusException("error sending string " + requestString, responseCode);
        }
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuffer response = new StringBuffer();
        in.lines().sequential().forEach(response::append);
        return response.toString();
    }

    /**
     * The HTTP headers can be set by extending classes.
     * @param connection HTTP connection to set the headers to
     */
    protected void setHeaders(HttpURLConnection connection) { }

    /**
     * Creates a URL connection in a round-robin fashion.
     * @return
     * @throws IOException
     */
    protected synchronized HttpURLConnection createConnection() throws IOException {
        int startIndex = currentIndex;
        // try all connections
        for (; ; ) {
            currentIndex = (currentIndex + 1) % urls.length;
            try {
                final HttpURLConnection connection = (HttpURLConnection) urls[currentIndex].openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                setHeaders(connection);
                connection.connect();
                return connection;
            } catch (IOException e) {
                if (currentIndex == startIndex)
                    throw e;
            }
        }
    }

    @Override
    public String toString() {
        return "URLs: " + Arrays.toString(urls);
    }
}
