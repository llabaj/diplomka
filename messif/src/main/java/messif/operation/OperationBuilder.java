/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation;

import messif.record.ModifiableRecord;
import messif.record.ModifiableRecordImpl;
import messif.utility.Convert;
import messif.utility.proxy.ProxyConverter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class OperationBuilder<T extends AbstractOperation> {

    /** A field that can store the operation class. */
    public static final String CLASS_FIELD = "_class".intern();

    /** A field to store other JSON fields of the operation. */
    //public static final String JSON_FIELD = "_fields".intern();


    private final Map<String, Object> params = new HashMap<>();
    //private boolean modifiable = false;
    private boolean checkFields = true;
    private Class<T> opClass;

    public static <T extends AbstractOperation> OperationBuilder<T> create(Class<T> opClass) {
        return new OperationBuilder<T>().setClass(opClass);
    }

    public static OperationBuilder create(String typeString) {
        return new OperationBuilder().addParam(AbstractOperation.TYPE_FIELD, typeString);
    }

    public static OperationBuilder create() {
        return new OperationBuilder();
    }

    // present only to seal object creation
    private OperationBuilder() {
    }

    private OperationBuilder<T> setClass(Class<T> opClass) {
        this.opClass = opClass;
        return this;
    }

    public OperationBuilder<T> addParam(String name, Object value) {
        params.put(name, value);
        return this;
    }

    public OperationBuilder<T> addParams(Map<String, Object> params) {
        this.params.putAll(params);
        return this;
    }

//    public OperationBuilder<T> modifiable(boolean modifiable) {
//        this.modifiable = modifiable;
//        return this;
//    }
//
//    public OperationBuilder<T> modifiable() {
//        return this.modifiable(true);
//    }

    public OperationBuilder<T> checkFields(boolean checkFields) {
        this.checkFields = checkFields;
        return this;
    }

    public OperationBuilder<T> doNotCheckFields() {
        return this.checkFields(false);
    }

    public T build() throws IllegalArgumentException, ClassCastException {
        if (opClass == null) {
            throw new IllegalStateException("the class of the operation is not specified");
        }
//        if (! params.containsKey(AbstractOperation.TYPE_FIELD)) {
//            try {
//                params.put(AbstractOperation.TYPE_FIELD, opClass.getField("TYPE_STRING").get(null));
//            } catch (IllegalAccessException | NoSuchFieldException ignore) { }
//        }
        final ModifiableRecord recordBase = buildMap();
        return checkFields ? ProxyConverter.convertChecked(recordBase, opClass) : ProxyConverter.convert(recordBase, opClass);
    }

    public ModifiableRecord buildMap() {
        //return modifiable ? new ModifiableRecordImpl(params, true) : new RecordImpl(params, true);
        return  new ModifiableRecordImpl(params, true);
    }

    /**
     * A static builder method that creates an instance of {@link AbstractOperation} given a field map with parameters
     *  and the operation class.
     * @param record a map with the parameters, it is used directly
     * @param opClass operation class to be used
     * @param checkFields if true, the {@link ProxyConverter} checks that the {@code record} contains all parameters
     *                    required by the operation class
     * @param <T> class of the operation
     * @return the created operation
     */
    public static <T extends AbstractOperation> T build(ModifiableRecord record, Class<T> opClass, boolean checkFields, Class<?> ... additionalClasses) {
        return checkFields ? ProxyConverter.convertChecked(record, opClass, additionalClasses) : ProxyConverter.convert(record, opClass, additionalClasses);
    }

    /**
     * A static builder method that creates an instance of {@link AbstractOperation} given a field map with parameters
     *  where the operation class should be the field {@link #CLASS_FIELD}.
     * @param record a map with the parameters, it is used directly
     * @param checkFields if true, the {@link ProxyConverter} checks that the {@code record} contains all parameters
     *                    required by the operation class
     * @return the created operation or <code>null</code> if the {@link #CLASS_FIELD} does not contain
     *                    a valid name of an operation class.
     */
    public static AbstractOperation build(ModifiableRecord record, boolean checkFields) throws ClassNotFoundException {
        String opClassName = (String) record.getField(CLASS_FIELD);
        if (opClassName == null) {
            return null;
        }
        final Class<AbstractOperation> opClass = Convert.getClassForName(opClassName, AbstractOperation.class);
        return build(record, opClass, checkFields);
    }

}
