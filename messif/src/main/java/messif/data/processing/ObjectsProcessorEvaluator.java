/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.processing;

import messif.data.DataObject;
import messif.operation.AbstractOperation;
import messif.operation.ProcessObjectsOperation;
import messif.operation.answer.AbstractAnswer;
import messif.operation.answer.ProcessObjectsAnswer;
import messif.operation.params.DataObjects;
import messif.operation.processing.OperationEvaluator;
import messif.record.Record;
import messif.record.processing.RecordProcessingException;
import messif.record.processing.RecordProcessor;
import messif.utility.ProcessingStatus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

/**
 * A simple "algorithm" (operation evaluator) that takes an operation with data objects, calls a given
 *  {@link RecordProcessor} on the data objects and the modified objects are returned in an {@link AbstractAnswer}.
 */
public class ObjectsProcessorEvaluator implements OperationEvaluator {

    /**
     * A record processor to modify the query or data objects.
     */
    final RecordProcessor<DataObject> recordProcessor;

    /**
     * If true, each record is checked also AFTER the processing using {@link RecordProcessor#preCondition(Record)}
     *  and if it is TRUE, then the object is put into the {@link ProcessObjectsAnswer#SKIPPED_OBJECTS_FIELD} field.
     */
    private final boolean checkConditionAfter;

    /**
     * Creates a new evaluator given a record processor.
     * @param recordProcessor an existing record processor to be used by this 'algorithm'
     * @param checkConditionAfter if true, then the processing condition is checked also after the processing
     *                            - see {@link #checkConditionAfter}.
     */
    public ObjectsProcessorEvaluator(RecordProcessor<DataObject> recordProcessor, boolean checkConditionAfter) {
        this.recordProcessor = recordProcessor;
        this.checkConditionAfter = checkConditionAfter;
    }

    /**
     * Takes query object or data objects from given operation, process them by given {@link HttpRecordsProcessor}, and
     * returns an answer with the returned data object(s).
     * @param operation any operation that contains query or data objects
     * @return a {@link AbstractAnswer} with the processed query or data object(s)
     * @throws Exception
     */
    @Override
    public AbstractAnswer evaluate(AbstractOperation operation) throws Exception {
        if (! (operation instanceof ProcessObjectsOperation)) {
            return null;
        }

        // objects to be directly returned without processing
        Collection<DataObject> returnObjects = new ArrayList<>(((DataObjects) operation).getObjectCount());
        // objects to be processed by the processor
        List<DataObject> objectsToProcess = new ArrayList<>(((DataObjects) operation).getObjectCount());
        for (DataObject dataObject : ((DataObjects) operation).getObjects()) {
            if (recordProcessor.preCondition(dataObject)) objectsToProcess.add(dataObject);
            else returnObjects.add(dataObject);
        }
        ProcessObjectsOperation.Helper helper = ProcessObjectsOperation.createHelper((ProcessObjectsOperation) operation);
        helper.answerBuilder.addAll(returnObjects);

        // if there are no objects to be processed
        if (objectsToProcess.isEmpty()) {
            helper.errorCodeHelper.updateErrorCode(ProcessObjectsAnswer.OPERATION_OK);
            return helper.getAnswer();
        }

        try {
            final List<DataObject> processed = recordProcessor.process(objectsToProcess);
            // now iterate over the processed objects, check the condition and if it was not processed
            // properly store it to the "skipped_objects"
            for (DataObject object : processed) {
                helper.objectProcessed(object, checkConditionAfter && recordProcessor.preCondition(object) ?
                        ProcessObjectsAnswer.ERROR_PROCESSING : ProcessObjectsAnswer.OK_PROCESSED);
            }
        } catch (RecordProcessingException ex) {
            Logger.getLogger(this.getClass().getName()).warning(ex.getMessage());
            // now iterate over the processed objects, check the condition and if it was not processed
            // properly store it to the "skipped_objects"
            for (DataObject object : objectsToProcess) {
                helper.skipObject(object, new ProcessingStatus(ProcessObjectsAnswer.ERROR_PROCESSING, ex.getMessage()));
            }
        }
        return helper.getAnswer();
    }

//    /**
//     * Check, if the record contains or not the required field(s). If it does, it's not further processed.
//     * @param record
//     * @return
//     */
//    protected boolean shouldProcess(DataObject record) {
//        return Arrays.stream(fieldsToCheck).anyMatch(field -> ! record.containsField(field));
//    }

    @Override
    public String toString() {
        return ObjectsProcessorEvaluator.class.getSimpleName() + " that processes all records by: " + recordProcessor.toString();
    }
}
