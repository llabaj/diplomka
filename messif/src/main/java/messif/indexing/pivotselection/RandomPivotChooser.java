/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.indexing.pivotselection;

import messif.data.DataObject;
import messif.data.util.DataObjectIterator;
import messif.distance.DistanceFunc;
import messif.utility.ListUtils;

import java.io.Serializable;

/**
 * RandomPivotChooser provides the capability of selecting a random object from the whole bucket.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class RandomPivotChooser extends AbstractPivotChooser implements Serializable {
    /** Class version id for serialization */
    private static final long serialVersionUID = 2L;
    
    private final boolean unique;
    
    /** 
     * Creates a new instance of RandomPivotChooser. Pivots selected by calling 
     *  {@link #selectPivot(int) } will be pairwise different.
     */
    public RandomPivotChooser(DistanceFunc<DataObject> distanceFunc) {
        this(true, distanceFunc);
    }

    /** 
     * Creates a new instance of RandomPivotChooser
     * @param unique if true (default), the pivots selected by {@link #selectPivot(int) } 
     *  are pairwise different.
     */
    public RandomPivotChooser(boolean unique, DistanceFunc<DataObject> distanceFunc) {
        super(distanceFunc);
        this.unique = unique;
    }
    
    /** Method for selecting pivots and appending to the list of pivots.
     * It is used in getPivot() function for automatic selection of missing pivots.
     *
     * Statistics are maintained automatically.
     * @param sampleSetIterator Iterator over the sample set of objects to choose new pivots from
     */
    @Override
    protected void selectPivot(int count, DataObjectIterator sampleSetIterator) {
        for (DataObject o : ListUtils.randomList(count, unique, sampleSetIterator))
            preselectedPivots.add(o);
    }
    
}
