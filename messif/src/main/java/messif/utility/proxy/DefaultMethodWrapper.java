/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.proxy;

import messif.record.ModifiableRecord;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Wrapper which allows to execute default methods implemented in converted interface.
 * <p>
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
class DefaultMethodWrapper extends IndirectMethodWrapper {

    @Nonnull
    private final MethodHandle methodHandle;

    DefaultMethodWrapper(@Nonnull Method method) {
        super(method);
        this.methodHandle = getMethodHandler(method);
    }

    @Nullable
    @Override
    public Object execute(@Nonnull ModifiableRecord record, @Nonnull Object proxy, @Nullable Object[] args) throws Throwable {
        if (args == null)
            return methodHandle.invoke(proxy);
        if (args.length == 1)
            return methodHandle.invoke(proxy, args[0]);
        if (args.length == 2)
            return methodHandle.invoke(proxy, args[0], args[1]);
        else {
            List<Object> arguments = new ArrayList<>(args.length + 1);
            arguments.add(proxy);
            arguments.addAll(Arrays.asList(args));
            return methodHandle.invoke(arguments);
        }
    }

    private static MethodHandle getMethodHandler(@Nonnull Method method) {

        try {
            final Field field = MethodHandles.Lookup.class.getDeclaredField("IMPL_LOOKUP");
            field.setAccessible(true);
            final MethodHandles.Lookup lookup = (MethodHandles.Lookup) field.get(null);
            return lookup.unreflectSpecial(method, method.getDeclaringClass());
        } catch (IllegalAccessException | NoSuchFieldException e) {
            throw new IllegalStateException("Failed to initialize default interface method " + method.getName());
        }
    }
}
