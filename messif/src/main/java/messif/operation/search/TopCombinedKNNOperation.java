/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.search;

import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Required;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface TopCombinedKNNOperation extends KNNOperation {

    String TYPE_STRING = "top_combined_KNN_query";

    @Override
    default String getInnerType() {
        return TYPE_STRING;
    }


    String N_INIT_SA_FIELD = "number_of_initial_SA";

    /** Number of sorted access objects to retrieve */
    @Get(field = N_INIT_SA_FIELD)
    @Required
    int getNumberOfInitialSA();

    String IS_PROGRESSIVE_FIELD = "number_of_initial_SA_progressive";

    /**
     * Progressive flag for the number of initial sorted accesses.
     * If set to <tt>true</tt>, the number of numberOfInitialSA is multiplied by {@link #getK()}.
     */
    @Get(field = IS_PROGRESSIVE_FIELD)
    @Required
    boolean isNumberOfInitialSAProgressive();


    String N_RA_FIELD = "number_of_RA";

    /** Number of random accesses to execute */
    @Get(field = N_RA_FIELD)
    @Required
    int getNumberOfRandomAccesses();


    /** Query operation to execute for sorted accesses */
    //protected final Class<? extends RankingQueryOperation> initialSAQueryClass;

}
