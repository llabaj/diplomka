/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.json;

import junit.framework.TestCase;
import messif.data.DataObject;
import messif.data.util.StreamDataObjectIterator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Iterator;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class JSONReaderTest extends TestCase {

    private JSONReader reader;
    
    private BufferedReader bufferedReader;
    
    public JSONReaderTest(String testName) {
        super(testName);
    }

    public static BufferedReader getJsonTestData() {
        return new BufferedReader(new InputStreamReader(JSONReaderTest.class.getResourceAsStream("/dataobjects.json")));
    }

    public static Iterator<DataObject> getDataObjects() {
        return new StreamDataObjectIterator(new BufferedReader(
                new InputStreamReader(JSONReaderTest.class.getResourceAsStream("/dataobjects.json"))));
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        bufferedReader = getJsonTestData();
        
        this.reader = new JSONReader(bufferedReader, false);
        System.out.println("successful setUp");
    }

    /**
     * Test of getUnderlyingReader method, of class JSONReader.
     */
    public void testGetUnderlyingReader() {
        System.out.println("getUnderlyingReader");
        try {
            assertNotNull(reader.getUnderlyingReader());
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    /**
     * Test of readJSONObject method, of class JSONReader.
     */
    public void testReadJSONObject() throws IOException {
        System.out.println("readJSONObject");
        try {
            DataObject jsonMessifObject = reader.readRecord();

            StringWriter w = new StringWriter();
            Object [] field = jsonMessifObject.getField("list of float lists", Object[].class, null);
            if (field != null) {
                float [] [] floats = new float[field.length][];
                for (int i = 0; i < field.length; i++) {
                    floats[i] = (float []) field[i];
                }
                System.out.print("list of float lists: ");
                System.out.println(Arrays.deepToString(floats));
            }
            new JSONWriter(w).write(jsonMessifObject.getMap());
            System.out.println(w.toString());
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }
//
//    /**
//     * Test of readJSONObject method, of class JSONReader.
//     */
//    public void testMessifObjectStreamIterator() {
//        System.out.println("objectStreamIterator");
//        try {
//            MessifObjectStreamIterator messifObjectStreamIterator = new MessifObjectStreamIterator(bufferedReader);
//            while (messifObjectStreamIterator.hasNext()) {
//                System.out.println(messifObjectStreamIterator.next().toString());
//            }
//        } catch (Exception ex) {
//            fail(ex.getMessage());
//        }
//    }
//
    /**
     * Test of isEOF method, of class JSONReader.
     */
//    public void testIsEOF() {
//        System.out.println("isEOF");
//        JSONReader instance = null;
//        boolean expResult = false;
//        boolean result = instance.isEOF();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default execute to fail.
//        fail("The test case is a prototype.");
//    }
    
}
