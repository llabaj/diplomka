/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.appl;

import junit.framework.TestCase;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class DistanceCFTest extends TestCase {

    private final static String DISTANCE_CF_FILE = "src/test/resources/distances-test.cf";

    public DistanceCFTest(String testName) {
        super(testName);
    }

    Application application;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        this.application = new Application();
    }
    public void testRunOperation() {
        //final InputStream resourceAsStream = ConfigFileTest.class.getResourceAsStream("/operations-test.cf");
        //final Properties properties = new Properties();
        //properties.load(resourceAsStream);
        //this.application.controlFileExecuteAction(System.out, properties, "approxQuery", new HashMap<>(), new HashMap<String, PrintStream >(), false);
        try {
            this.application.controlFile(System.out, "configFile", DISTANCE_CF_FILE);
        } catch (Error er) {
            assertTrue(false);
            throw er;
        }
    }


}
