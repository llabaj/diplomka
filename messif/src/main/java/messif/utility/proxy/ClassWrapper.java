/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.proxy;


import messif.record.ModifiableRecord;
import messif.record.Record;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;


/**
 * Class wrapping the proxy interface.
 * Holds the reference to constructor to speedup the proxy initialization process.
 * Provides possibility to execute individual methods on {@link ModifiableRecord}.
 * Provides possibility to verify {@link Record} to match against requirements coming from published methods.
 * <p>
 * This wrapper is stateless and shared by all proxy instances (similar to Class object from java).
 * <p>
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
class ClassWrapper<T> {
    /**
     * Constructor of proxy object. Used for performance optimization.
     */
    @Nonnull
    private final Constructor<T> constructor;
    /**
     * Map between {@link Method} and {@link MethodWrapper}.
     * This map is used during method evaluation and underlying {@link Record} verification
     */
    private final Map<Method, MethodWrapper> methodWrappers = new HashMap<>();

    /**
     * ClassWrapper constructor
     *
     * @param tClass      Class of resulting proxy object
     * @param constructor Proxy constructor
     */
    ClassWrapper(@Nonnull Class<T> tClass, @Nonnull Constructor<T> constructor) {
        this.constructor = constructor;

        for (Method method : tClass.getMethods()) {
            convertAndStore(method);
        }
    }

    @Nonnull
    Constructor<T> getConstructor() {
        return constructor;
    }

    /**
     * Executes given proxy method on incoming object.
     * Throws {@link UnsupportedOperationException} if the method could not be converted.
     *
     * @param record underlying object
     * @param proxy  proxy object
     * @param method interface (proxy) method to be executed
     * @param args   interface (proxy) method arguments   @return result of interface (proxy) method
     */
    @Nullable
    Object executeMethod(@Nonnull ModifiableRecord record, @Nonnull Object proxy, @Nonnull Method method, @Nonnull Object[] args) throws Throwable {
        MethodWrapper methodWrapper = getMethodWrapper(method);
        return methodWrapper.execute(record, proxy, args);
    }

    /**
     * Verifies the incoming object contains expected values to match proxy interface
     *
     * @param record underlying object
     */
    void verify(@Nonnull Record record) {
        for (MethodWrapper methodWrapper : methodWrappers.values())
            if (methodWrapper instanceof RecordMethodWrapper) {
                RecordMethodWrapper wrapper = (RecordMethodWrapper) methodWrapper;
                wrapper.verify(record);
            }
    }

    /**
     * Converts incoming {@link Method} method to {@link MethodWrapper} using {@link MethodConverter}.
     * Stores the wrapper into internal map to optimize performance.
     * Returns NULL if we are unable to convert the method.
     *
     * @param method Method to be converted
     * @return Method wrapper
     */
    @Nullable
    private MethodWrapper convertAndStore(@Nonnull Method method) {
        MethodWrapper methodWrapper = MethodConverter.tryToConvert(method);
        if (methodWrapper != null)
            methodWrappers.put(method, methodWrapper);
        return methodWrapper;
    }

    /**
     * Return {@link MethodWrapper} for incoming {@link Method}.
     * Throws an exception if we are not able to bring up wrapper.
     *
     * @param method Method
     * @return Wrapper
     */
    @Nonnull
    private MethodWrapper getMethodWrapper(@Nonnull Method method) {
        // get existing method wrapper
        MethodWrapper methodWrapper = methodWrappers.get(method);

        // try to create new converter - this might be useful during hot-swap
        if (methodWrapper == null)
            methodWrapper = convertAndStore(method);

        if (methodWrapper == null)
            throw new UnsupportedOperationException("Method '" + method + "' is not implemented");

        return methodWrapper;
    }
}
