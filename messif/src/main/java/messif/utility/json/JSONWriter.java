/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.json;

import java.io.*;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

public class JSONWriter extends Writer {

    /**
     * The writer that will receive the output.
     */
    protected Writer writer;

    /**
     * The default indentation of the output - typically either 0 (no indentation), 2 or 4. Default: 0.
     */
    protected final int indentFactor;

    /**
     * This parameter allows to control the depth of the JSON objects (maps) and arrays that are actually printed. If
     *  the JSON object has more levels (sub-documents), then these maps/arrays are not printed fully but just
     *  its size is printed.
     */
    protected int maxDepth = Integer.MAX_VALUE;


    // region ******************************     Constructors    ************************** //

    public JSONWriter(Writer w) {
        this(w, 0);
    }

    /**
     * Make a fresh JSONWriter. It can be used to build one JSON text.
     *
     * @param w writer to writeMap the text into
     */
    public JSONWriter(Writer w, int indentFactor) {
        this.writer = w;
        this.indentFactor = indentFactor;
    }

    public JSONWriter(Writer w, boolean prettyFormatting) {
        this(w, prettyFormatting ? 4 : 0);
    }

    public JSONWriter(Writer w, boolean prettyFormatting, int maxDepth) {
        this(w, prettyFormatting ? 4 : 0);
        this.maxDepth = maxDepth;
    }

    public JSONWriter(OutputStream outStream) {
        this(outStream, 0);
    }

    public JSONWriter(OutputStream outStream, boolean prettyFormatting) {
        this(outStream, prettyFormatting ? 4 : 0);
    }

    public JSONWriter(OutputStream outStream, int indentFactor) {
        this(new OutputStreamWriter(outStream), indentFactor);
    }
    // endregion

    // region ******************************     Static factory methods    ************************** //

    public static void writeToJSON(OutputStream outputStream, Object value,  boolean prettyFormatting) throws JSONException, IOException {
        writeToJSON(outputStream, value, prettyFormatting ? 4 : 0);
    }

    public static void writeToJSON(OutputStream outputStream, Object value, int indentFactor) throws JSONException, IOException {
        final JSONWriter jWriter = new JSONWriter(outputStream, indentFactor);
        jWriter.write(value, indentFactor, 0);
        jWriter.flush();
    }

    public static String writeToJSON(Object value, boolean prettyFormatting) throws JSONException {
        return writeToJSON(value, prettyFormatting, Integer.MAX_VALUE);
    }

    public static String writeToJSON(Object value, boolean prettyFormatting, int maxDepth) throws JSONException {
        try {
            final StringWriter stringWriter = new StringWriter();
            final JSONWriter writer = new JSONWriter(stringWriter, prettyFormatting, maxDepth);
            writer.write(value);
            return stringWriter.toString();
        } catch (IOException e) {
            throw new JSONException(e);
        }
    }

    public static String writeToJSON(JSONWritable value, boolean prettyFormatting) throws JSONException {
        try {
            final StringWriter stringWriter = new StringWriter();
            final JSONWriter writer = new JSONWriter(stringWriter, prettyFormatting);
            value.writeJSON(writer);
            return stringWriter.toString();
        } catch (IOException e) {
            throw new JSONException(e);
        }
    }
    // endregion

    // region ******************************     Getters and setters   ************************** //

    public Writer getUnderlyingWriter() {
        return writer;
    }

    public int getMaxDepth() {
        return maxDepth;
    }

//    public void setMaxDepth(int maxDepth) {
//        this.maxDepth = maxDepth;
//    }

    // endregion

    /**
     * Writes given object to this JSON writer.
     * @param value the object to be written into JSON
     * @throws JSONException
     * @throws IOException
     */
    public void write(Object value) throws JSONException, IOException {
        write(value, 0);
    }

    /**
     * Writes given object to this JSON writer.
     * @param value the object to be written into JSON
     * @param level the depth level of the object in the JSON structure
     * @throws JSONException
     * @throws IOException
     */
    public void write(Object value, int level) throws JSONException, IOException {
        write(value, indentFactor, level);
    }

    protected JSONWriter write(Object value, int indentFactor, int level) throws JSONException, IOException {
        if (value == null) {
            writer.write("null");
        } else if (value instanceof Number) {
            // float and integer numbers
            writer.write(numberToString((Number) value));
        } else if (value instanceof Boolean) {
            // boolean true/false
            writer.write(value.toString());
        } else if (value instanceof Map) {
            // map (JSON object)
            writeMap((Map<String, Object>) value, indentFactor, level);
        } else if (value.getClass().isArray()) {
            // array of values (both primitive types and Objects)
            writeArray(value, indentFactor, level);
        } else if (value instanceof Collection) {
            // create an array from collection
            writeArray(((Collection) value).toArray(), indentFactor, level);
        } else if (value instanceof JSONWritable) {
            // let the JSONWritable to write itself forwarding the level
            return ((JSONWritable) value).writeJSON(this, level);
        } else {
            // treat it like string (including case when the value is instance of String)
            writeQuoted(value.toString());
        }
        return this;
    }

    /**
     * Write the contents of the JSONObject as JSON text to a writer. For
     * compactness, no whitespace is added.
     * <p>
     * Warning: This method assumes that the data structure is acyclical.
     *
     * @return The writer.
     * @throws JSONException
     */
    protected JSONWriter writeMap(Map<String, Object> map, int indentFactor, int level) throws JSONException {
        try {
            // cover the case that the indent (depth) is higher than the maximum value (Map is written "deeper" than array)
            if (level > maxDepth) {
                writeQuoted("JSON map with " + map.size() + " entries");
                return this;
            }
            boolean commanate = false;
            final int length = map.size();
            Iterator<String> keys = map.keySet().iterator();
            writer.write('{');

            if (length == 1) {
                String key = keys.next();
                writeQuoted(key);
                writer.write(':');
                if (indentFactor > 0) {
                    writer.write(' ');
                }
                write(map.get(key), indentFactor, level);
            } else if (length != 0) {
                final int valuesIndent = (level + 1) * indentFactor;
                while (keys.hasNext()) {
                    String key = keys.next();
                    if (commanate) {
                        writer.write(',');
                    }
                    if (indentFactor > 0) {
                        writer.write('\n');
                    }
                    indent(valuesIndent);
                    writeQuoted(key);
                    writer.write(':');
                    if (indentFactor > 0) {
                        writer.write(' ');
                    }
                    write(map.get(key), indentFactor, level + 1);
                    commanate = true;
                }
                if (indentFactor > 0) {
                    writer.write('\n');
                }
                indent((level) * indentFactor);
            }
            writer.write('}');
            return this;
        } catch (IOException exception) {
            throw new JSONException(exception);
        }
    }

    /**
     * Write the contents of the JSONArray as JSON text to a writer. For
     * compactness, no whitespace is added.
     * <p>
     * Warning: This method assumes that the data structure is acyclical.
     *
     * @param indentFactor The number of spaces to add to each level of
     * indentation.
     * @param level the depth level of the array in the JSON structure
     * @return The writer.
     * @throws JSONException
     */
    protected JSONWriter writeArray(Object array, int indentFactor, int level) throws JSONException {
        try {
            // cover the case that the indent (depth) is higher than the maximum value
            if (level >= maxDepth) {
                writeQuoted("JSON array with " + Array.getLength(array) + " elements");
                return this;
            }
            boolean commanate = false;
            int length = Array.getLength(array);
            writer.write('[');

            if (length == 1) {
                write(Array.get(array, 0), indentFactor, level);
            } else if (length != 0) {
                final int valuesIndent = (level + 1) * indentFactor;

                for (int i = 0; i < length; i += 1) {
                    if (commanate) {
                        writer.write(',');
                    }
                    if (indentFactor > 0) {
                        writer.write('\n');
                    }
                    indent(valuesIndent);
                    write(Array.get(array, i), indentFactor, level + 1);
                    commanate = true;
                }
                if (indentFactor > 0) {
                    writer.write('\n');
                }
                indent(level * indentFactor);
            }
            writer.write(']');
            return this;
        } catch (IOException e) {
            throw new JSONException(e);
        }
    }

    protected Writer writeQuoted(String string) throws IOException {
        if (string == null || string.length() == 0) {
            writer.write("\"\"");
            return writer;
        }

        char b;
        char c = 0;
        String hhhh;
        int i;
        int len = string.length();

        writer.write('"');
        for (i = 0; i < len; i += 1) {
            b = c;
            c = string.charAt(i);
            switch (c) {
                case '\\':
                case '"':
                    writer.write('\\');
                    writer.write(c);
                    break;
                case '/':
                    if (b == '<') {
                        writer.write('\\');
                    }
                    writer.write(c);
                    break;
                case '\b':
                    writer.write("\\b");
                    break;
                case '\t':
                    writer.write("\\t");
                    break;
                case '\n':
                    writer.write("\\n");
                    break;
                case '\f':
                    writer.write("\\f");
                    break;
                case '\r':
                    writer.write("\\r");
                    break;
                default:
                    if (c < ' ' || (c >= '\u0080')) { //(c >= '\u0080' && c < '\u00a0')
                            //|| (c >= '\u2000' && c < '\u2100')) {
                        writer.write("\\u");
                        hhhh = Integer.toHexString(c);
                        writer.write("0000", 0, 4 - hhhh.length());
                        writer.write(hhhh);
                    } else {
                        writer.write(c);
                    }
            }
        }
        writer.write('"');
        return writer;
    }

    /**
     * Throw an exception if the object is a NaN or infinite number.
     *
     * @param o The object to test.
     * @throws JSONException If o is a non-finite number.
     */
    protected static void testValidity(Object o) throws JSONException {
        if (o != null) {
            if (o instanceof Double) {
                if (((Double) o).isInfinite() || ((Double) o).isNaN()) {
                    throw new JSONException(
                            "JSON does not allow non-finite numbers.");
                }
            } else if (o instanceof Float) {
                if (((Float) o).isInfinite() || ((Float) o).isNaN()) {
                    throw new JSONException(
                            "JSON does not allow non-finite numbers.");
                }
            }
        }
    }

    /**
     * Produce a string from a Number.
     *
     * @param number A Number
     * @return A String.
     * @throws JSONException If n is a non-finite number.
     */
    protected static String numberToString(Number number) throws JSONException {
        if (number == null) {
            throw new JSONException("Null pointer");
        }
        testValidity(number);

        // Shave off trailing zeros and decimal point, if possible.
        String string = number.toString();
//        if (string.indexOf('.') > 0 && string.indexOf('e') < 0
//                && string.indexOf('E') < 0) {
//            while (string.endsWith("0")) {
//                string = string.substring(0, string.length() - 1);
//            }
//            if (string.endsWith(".")) {
//                string = string.substring(0, string.length() - 1);
//            }
//        }
        return string;
    }

    private void indent(int indent) throws IOException {
        for (int i = 0; i < indent; i += 1) {
            writer.write(' ');
        }
    }

    // ******************    All other methods from Writer are just passed to the encapsulated Writer object   ******** //
    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {
        writer.write(cbuf, off, len);
    }

    @Override
    public void flush() throws IOException {
        writer.flush();
    }

    @Override
    public void close() throws IOException {
        writer.close();
    }

}
