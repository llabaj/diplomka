package com.me.diplomka.fixedpivotswebapp;

import com.hazelcast.aggregation.Aggregator;
import com.me.diplomka.fixedpivots.IndexedDataObject;
import messif.data.DataObject;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

public class RandomAggregator extends Aggregator<Map.Entry<String, IndexedDataObject>, List<DataObject>> {

    private final int limit;
    private DataObject[] accumulator;

    public RandomAggregator(int limit) {
        this.limit = limit;
        accumulator = new DataObject[limit];
    }

    @Override
    public void accumulate(Map.Entry<String, IndexedDataObject> input) {
        if (Math.random() < 0.1) {
            int index = (int) (limit * Math.random());
            accumulator[index] = input.getValue();
        }
    }

    @Override
    public void combine(Aggregator aggregator) {
        var other = (RandomAggregator) aggregator;
        accumulator= Stream.concat(Stream.of(accumulator), Stream.of(other.accumulator))
                .skip(limit / 2)
                .filter(Objects::nonNull)
                .limit(limit)
                .toArray(DataObject[]::new);
    }

    @Override
    public List<DataObject> aggregate() {
        return Arrays.asList(accumulator);
    }
}
