/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.record.processing;

import messif.utility.net.HttpStatusCodeProvider;
import messif.utility.net.HttpStatusException;

/**
 * Throwable that indicates an error during processing of a data object.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class RecordProcessingException extends HttpStatusException {

    /**
     * Creates a new instance of <code>ExtractorException</code> without detail message.
     */
    public RecordProcessingException() {
        super(HttpStatusCodeProvider.STATUS_CODE_NOT_SET);
    }

    /**
     * Constructs an instance of <code>ExtractorException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public RecordProcessingException(String msg) {
        super(msg, HttpStatusCodeProvider.STATUS_CODE_NOT_SET);
    }

    /**
     * Constructs an instance of <code>ExtractorException</code> with the
     * specified detail message and cause.
     * @param msg the detail message.
     * @param cause the cause which is saved for later retrieval by the
     *         {@link #getCause()} method); a <tt>null</tt> value is
     *         permitted, and indicates that the cause is nonexistent or
     *         unknown
     */
    public RecordProcessingException(String msg, Throwable cause) {
        super(msg, cause, HttpStatusCodeProvider.STATUS_CODE_NOT_SET);
    }

    /**
     * Constructs an instance of <code>ExtractorException</code> with the
     * specified detail message and cause.
     * @param msg the detail message.
     * @param cause the cause which is saved for later retrieval by the
     *         {@link #getCause()} method); a <tt>null</tt> value is
     *         permitted, and indicates that the cause is nonexistent or
     *         unknown
     */
    public RecordProcessingException(String msg, Throwable cause, int httpErrorCode) {
        super(msg, cause, httpErrorCode);
    }

    /**
     * Constructs an instance of <code>ExtractorException</code> with the specified detail message
     *  and HTTP error code to be returned.
     * @param msg the detail message.
     * @param httpErrorCode HTTP error code to be returned, if this exception is raised
     */
    public RecordProcessingException(String msg, int httpErrorCode) {
        super(msg, httpErrorCode);
    }

}
