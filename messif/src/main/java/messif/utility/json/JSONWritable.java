/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.json;

import java.io.IOException;

/**
 * The <code>JSONWritable</code> interface should be implemented by classes that are to be written 
 *  into JSON. The system assumes that this class also has a constructor with one argument {@link JSONReader}.
 */
public interface JSONWritable {

    /**
     * Make a JSON text of this object. For compactness, no whitespace is added. 
     * Warning: This method assumes that the data structure is acyclical.
     *
     * @return a printable, displayable, portable, transmittable representation of the object,
     * beginning with <code>{</code>&nbsp;<small>(left brace)</small> and ending with
     * <code>}</code>&nbsp;<small>(right brace)</small>.
     */
    default String toJSONString() {
        return toJSONString(false);
    }

    /**
     * Make a JSON text of this object. Warning: This method assumes that the data structure is acyclical.
     *
     * @param prettyFormatting if true, the JSON fields are intended (by 4 spaces)
     * @return a printable, displayable, portable, transmittable representation of the object,
     * beginning with <code>{</code>&nbsp;<small>(left brace)</small> and ending with
     * <code>}</code>&nbsp;<small>(right brace)</small>.
     */
    default String toJSONString(boolean prettyFormatting) {
        return JSONWriter.writeToJSON(this, prettyFormatting);
    }

    /**
     * Writes a JSON text of this object into given writer. For compactness, no whitespace is added. 
     * Warning: This method assumes that the data structure is acyclical.
     *
     * @param writer an output writer to write the JSON into
     * @return the writer itself (or another wrapping writer)
     * @throws java.io.IOException if the writer fails to write data
     */
    JSONWriter writeJSON(JSONWriter writer) throws JSONException, IOException;

    /**
     * Writes a JSON text of this object into given writer. For compactness, no whitespace is added.
     * Warning: This method assumes that the data structure is acyclical.
     *
     * @param writer an output writer to write the JSON into
     * @param level current level of the JSON structure
     * @return the writer itself (or another wrapping writer)
     * @throws java.io.IOException if the writer fails to write data
     */
    default JSONWriter writeJSON(JSONWriter writer, int level) throws JSONException, IOException {
        return writeJSON(writer);
    }
}
