/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.serialization.io;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Output stream that implements the {@link BinaryOutput} using
 * an internal {@link ByteBuffer buffer} which grows as required.
 *
 * @see BufferOutputStream
 * @see BufferInputStream
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class GrowingBufferOutputStream extends BufferOutputStream {

    //****************** Constants ******************//

    /** The internal buffer is growing by this ratio. */
    public static final int GROWING_RATIO = 2;


    //****************** Constructor ******************//

    /**
     * Creates a new instance of BufferOutputStream.
     * The output operates on a newly created buffer with the specified size.
     * @param bufferSize the size of the internal buffer
     * @param bufferDirect allocate the internal buffer as {@link ByteBuffer#allocateDirect direct}
     * @throws IllegalArgumentException if there specified buffer size is not valid
     */
    public GrowingBufferOutputStream(int bufferSize, boolean bufferDirect) throws IllegalArgumentException {
        super(bufferSize, bufferDirect);
    }

    public GrowingBufferOutputStream(boolean bufferDirect) throws IllegalArgumentException {
        this(MINIMAL_BUFFER_SIZE, bufferDirect);
    }

    /**
     * Creates a new instance of BufferOutputStream.
     * The output operates on the the given buffer.
     * @param buffer the internal buffer this stream
     */
    public GrowingBufferOutputStream(ByteBuffer buffer) {
        super(buffer);
    }

    //****************** Binary output implementation - override ******************//

    @Override
    public ByteBuffer prepareOutput(int minBytes) throws IOException {
        // If there is NOT enough space in the buffer, increase it
        if (minBytes > byteBuffer.remaining()) {
            ByteBuffer newByteBuffer = ByteBuffer.wrap(Arrays.copyOf(byteBuffer.array(),
                    Math.max((int) (byteBuffer.limit() * GROWING_RATIO), byteBuffer.limit() + minBytes)));
            newByteBuffer.position(byteBuffer.position());
            byteBuffer = newByteBuffer;
        }

        return byteBuffer;
    }

}
