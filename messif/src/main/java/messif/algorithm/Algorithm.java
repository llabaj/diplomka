/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.algorithm;

import messif.algorithm.executor.ExecutorChainItem;
import messif.algorithm.executor.RunningOperationMonitor;
import messif.algorithm.executor.StatisticsExecutor;
import messif.algorithm.executor.UpdateOperationLogger;
import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.OperationBuilder;
import messif.operation.info.AlgorithmInfoOperation;
import messif.operation.info.ObjectCountOperation;
import messif.operation.info.answer.InformationAnswer;
import messif.operation.info.answer.ObjectCountAnswer;
import messif.operation.info.processor.AlgorithmInfoProcessor;
import messif.operation.processing.*;
import messif.record.ModifiableRecord;
import messif.record.ModifiableRecordImpl;
import messif.record.Record;
import messif.statistic.FutureWithStatistics;
import messif.statistic.OperationStatistics;
import messif.utility.Convert;
import messif.utility.ExtendedProperties;
import messif.utility.reflection.MethodInstantiator;
import messif.utility.reflection.NoSuchInstantiatorException;

import java.io.*;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public abstract class Algorithm implements Serializable, OperationEvaluator, FieldMapEvaluator {

    //region ********************     Fields, getters and setters     ***************************************** //
    /**
     * Class id for serialization.
     */
    private static final long serialVersionUID = 2L;

    /**
     * Logger
     */
    public static final Logger log = Logger.getLogger("messif.algorithm.Algorithm");

    /**
     * Prefix of the config properties for executors.
     */
    public static final String CONFIG_EXECUTOR_PREFIX = "executors.";

    /**
     * The name of this algorithm.
     */
    private final String algorithmName;

    /**
     * Configuration of the algorithm, especially of the {@link OperationExecutor executors} chained to process operations.
     */
    protected ExtendedProperties configuration;


    // ********************        Fields that are not serialized together with the algorithm    ****************** //

    /**
     * Thread pool service to process operations in threads.
     */
    protected transient ExecutorService threadPool;

    protected transient OperationHandler handler;

    protected transient List<ExecutorChainItem> executors;

    /**
     * Flag to control that the algorithm is not initialized more than once.
     */
    protected transient volatile boolean isInitialized = false;

    public String getName() {
        return algorithmName;
    }
    //endregion


    // region *******************    Initialization of the algorithm (constructors, init)   *************************** //

    /**
     * Creates a new algorithm given a name and default configuration.
     *
     * @param algorithmName name of the algorithm
     */
    public Algorithm(String algorithmName) {
        this(algorithmName, defaultConfiguration());
    }

    /**
     * Creates a new algorithm given a name and default configuration.
     *
     * @param algorithmName name of the algorithm
     */
    public Algorithm(String algorithmName, ExtendedProperties configuration) {
        this.algorithmName = algorithmName;
        this.configuration = configuration;
    }

    /**
     * The init method must be called AFTER the algorithm object is fully created by constructor, so it cannot be
     * called directly in the constructor of the abstract algorithm. It can be called in the end of the lowest
     * level constructor, but rather from the outside. It is also to be called after deserialization of the algorithm.
     */
    public synchronized boolean init() {
        if (isInitialized)
            return false;
        isInitialized = true;
        this.threadPool = initThreadPool();
        // initialize the operation handler including the chain of executors
        this.handler = initOperationHandler();
        registerProcessors();

        // now iterate over all executors and initialize them (e.g. start using the REDO log)
        try {
            for (ExecutorChainItem executor : executors) {
                executor.init(configuration, this);
            }
        } catch (AlgorithmMethodException ex) {
            log.severe("error while initializing executor: " + ex.toString());
        }
        return true;
    }

    protected static ExtendedProperties defaultConfiguration() {
        final ExtendedProperties defaultConfig = new ExtendedProperties();
        // defaultConfig.put(CONFIG_EXECUTOR_PREFIX + "1", "messif.algorithm.executor.UpdateOperationLogger");
        defaultConfig.put(CONFIG_EXECUTOR_PREFIX + "1", "messif.algorithm.executor.RunningOperationMonitor");
        defaultConfig.put(CONFIG_EXECUTOR_PREFIX + "2", "messif.algorithm.executor.OperationLogger");
        defaultConfig.put(CONFIG_EXECUTOR_PREFIX + "3", "messif.algorithm.executor.StatisticsExecutor");
        return defaultConfig;
    }

    /**
     * Initializes the thread pool to be used by this algorithm (e.g. in the operation processing).
     */
    protected ExecutorService initThreadPool() {
        return Executors.newCachedThreadPool();
    }

    /**
     * Creates different executors to be, in turn, applied to each operation and then create a handler that will
     * apply these executor chain.
     * @return a new operation handler
     */
    protected final OperationHandler initOperationHandler() {
        this.executors = new ArrayList<>();
        int execNumber = 1;
        while (configuration.containsKey(CONFIG_EXECUTOR_PREFIX + execNumber)) {
            final Class<ExecutorChainItem> execClass = configuration.getClassProperty(CONFIG_EXECUTOR_PREFIX + execNumber, true, ExecutorChainItem.class);
            try {
                executors.add(execClass.newInstance());
                // log.info(getName() + ": just created executor: " + execClass.getSimpleName());
            } catch (IllegalAccessException | InstantiationException e) {
                log.severe("error creating instance of class " + execClass.getSimpleName());
            }
            execNumber ++;
        }
        return new OperationHandler(executors.toArray(new OperationExecutor[executors.size()]));
    }

    /**
     * This is the key method to control the ways operations are processed in the algorithm. For different operation types
     * expressed by the type strings, each algorithm can specify: <ul>
     * <li>a type of operation to be created for this type string</li>
     * <li>processor {@link OperationProcessor} to be created for this operation</li>
     * <li>sequential/parallel or other specific execution of this operation processor</li>
     * </ul>
     * This configuration is specified using {@link OperationHandler#register(ProcessorConfiguration)} method. One
     * can use the following {@link ProcessorConfiguration#create(Class, Class)} builder to create a configuration
     * to be registered in the handler.
     */
    protected void registerProcessors() {
        this.handler.register(ProcessorConfiguration.create(AlgorithmInfoOperation.class, InformationAnswer.class)
                .withProcessor(operation -> new AlgorithmInfoProcessor(operation, Algorithm.this))
                .executeSequential().build());
    }

    /**
     * Sets a new configuration of this algorithm, especially the {@link ExecutorChainItem executors} and their config.
     * This method calls {@link #init()} method.
     * @param configuration new configuration settings of this algorithm
     */
    public void setConfiguration(ExtendedProperties configuration) {
        this.configuration = configuration;
        this.isInitialized = false;
        init();
    }

    // endregion


    // region   **************************     Methods to handle individual executors     ************************* //

    protected  <C extends OperationExecutor> C getFirstExecutor(Class<C> executorClass) {
        for (OperationExecutor executor : executors) {
            if (executorClass.isInstance(executor)) {
                return executorClass.cast(executor);
            }
        }
        return null;
    }

    public StatisticsExecutor getStatisticsExecutor() {
        return getFirstExecutor(StatisticsExecutor.class);
    }

    // endregion


    // region ************    Execution of operations (and management of op. statistics)   ************************ //

    @Override
    public AbstractAnswer evaluate(Record record) throws AlgorithmMethodException, NoSuchMethodException, ClassNotFoundException {
        if (!isInitialized) {
            throw new NoSuchMethodException("Algorithm " + getName() + " has not been initialized by calling .init() method");
        }
        try {
            // try to create an operation from the record (it can be done if it contains field "_class")
            ModifiableRecord modifiableRecord = record instanceof ModifiableRecord ? (ModifiableRecord) record : new ModifiableRecordImpl(record);
            final AbstractOperation operation = OperationBuilder.build(modifiableRecord, true);
            if (operation != null) {
                return handler.evaluate(operation);
            }
            return handler.evaluate(record);
        } catch (Exception e) {
            throw new AlgorithmMethodException(e);
        }
    }

    @Override
    public AbstractAnswer evaluate(AbstractOperation operation) throws AlgorithmMethodException, NoSuchMethodException {
        if (!isInitialized) {
            throw new NoSuchMethodException("Algorithm " + getName() + " has not been initialized by calling .init() method");
        }
        try {
            return handler.evaluate(operation);
        } catch (Exception e) {
            throw new AlgorithmMethodException(e);
        }
    }

    /**
     * Execute algorithm operation on background.
     * @param opParams the operation to execute on this algorithm
     * @return a {@link Future} that can be used to wait for the execution to finish and retrieve the resulting executed operation;
     *      note that a {@link NoSuchMethodException} exception can be thrown if the operation is unsupported (there is no method for the operation)
     */
    public Future<AbstractAnswer> backgroundExecuteOperation(final Record opParams) {
        return threadPool.submit(() -> evaluate(opParams));
    }

    /**
     * Returns the statistics of the executed operations.
     * Unless you call {@link OperationStatistics#resetLocalThreadStatistics()}, the cumulative statistics
     * for all operations run in this thread are returned.
     * @return the statistics of the executed operations
     */
    public OperationStatistics getOperationStatistics() {
        return OperationStatistics.getLocalThreadStatistics();
    }

    /**
     * Returns a collection of pairs (operation type string, operation class) as used and processed by this algorithm.
     *
     * @return a collection of pairs (operation type string, operation class) as used and processed by this algorithm.
     */
    public Collection<ProcessorConfiguration> getSupportedOperations() {
        if (!isInitialized) {
            throw new IllegalStateException("Algorithm " + getName() + " has not been initialized by calling .init() method");
        }
        return handler.getSupportedOperations();
    }
    // endregion

    //region  ******************          Destructor methods          ******************  //

    /**
     * Finalize the algorithm. All transient resources associated with this
     * algorithm are released.
     * After this method is called, the behavior of executing any operation is unpredictable.
     *
     * @throws Throwable if there was an error finalizing
     */
    @Override
    public void finalize() throws Throwable {
        if (threadPool != null)
            threadPool.shutdown();
        super.finalize();
    }

    /**
     * Destroy this algorithm. This method releases all resources (transient and persistent)
     * associated with this algorithm.
     * After this method is called, the behavior of executing any operation is unpredictable.
     * <p>
     * <p>
     * This implementation defaults to execute {@link #finalize()}, but should be overridden
     * if the algorithm needs to differentiate between finalizing and destroying. In that case
     * the "super.destroy()" should <i>not</i> be called if finalizing is not part of destroy.
     * </p>
     *
     * @throws Throwable if there was an error while cleaning
     */
    public void destroy() throws Throwable {
        finalize();
    }
    //endregion


    //region    ******************          Serialization          ****************** //

    /**
     * Load the algorithm from the specified file and return it.
     *
     * @param <T>            class of the stored algorithm
     * @param algorithmClass class of the stored algorithm
     * @param filepath       the path to a file from which the algorithm should be restored
     * @return the loaded algorithm
     * @throws IOException            if the specified filename is not a readable serialized algorithm
     *                                (see {@link java.io.ObjectInputStream#readObject readObject} method for detailed description)
     * @throws NullPointerException   if the specified filename is <tt>null</tt>
     * @throws ClassNotFoundException if the class of serialized object cannot be found
     * @throws ClassCastException     if the filename doesn't contain serialized algorithm
     */
    public static <T extends Algorithm> T restoreFromFile(String filepath, Class<T> algorithmClass) throws IOException, NullPointerException, ClassNotFoundException, ClassCastException {
        ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(filepath)));
        try {
            T rtv = algorithmClass.cast(in.readObject());
            rtv.init();
            log.log(Level.INFO, "Algorithm restored from: {0}", filepath);
            return rtv;
        } finally {
            in.close();
        }
    }

    /**
     * Load the algorithm from the specified file and return it.
     *
     * @param filepath the path to file from which the algorithm should be restored
     * @return the loaded algorithm
     * @throws IOException            if the specified filename is not a readable serialized algorithm
     *                                (see {@link java.io.ObjectInputStream#readObject readObject} method for detailed description)
     * @throws NullPointerException   if the specified filename is <tt>null</tt>
     * @throws ClassNotFoundException if the class of serialized object cannot be found
     * @throws ClassCastException     if the filename doesn't contain serialized algorithm
     */
    public static Algorithm restoreFromFile(String filepath) throws IOException, NullPointerException, ClassNotFoundException, ClassCastException {
        return restoreFromFile(filepath, Algorithm.class);
    }

    /**
     * Store the algorithm to the specified file.
     *
     * @param filepath the path to a file where the algorithm should be stored. If this path is a directory,
     *                 the algorithm name (all non alphanumeric characters are replaced by underscore) with <tt>.bin</tt>
     *                 extension is appended to the path.
     * @throws IOException if the specified filename is not writable or if an error occurs during the serialization
     *                     (see {@link java.io.ObjectOutputStream#writeObject writeObject} method for detailed description)
     */
    public void storeToFile(String filepath) throws IOException {
        final RunningOperationMonitor runningOperationMonitor = getFirstExecutor(RunningOperationMonitor.class);
        // Acquire all locks, thus waiting for all currently running operations and disable additional
        if (runningOperationMonitor != null)
            runningOperationMonitor.stopUpdates();
        try {
            // Check if the file is a regular file
            File file = new File(filepath);
            if (file.isDirectory())
                file = new File(file, getName().replaceAll("[^a-zA-Z0-9.-]", "_") + ".bin");

            beforeStoreToFile(filepath);

            // create a temporary file that is renamed to the "file" only if the serialization was successfull
            File temporaryFile = File.createTempFile(file.getName(), ".tmp", file.getParentFile());
            try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(temporaryFile))) {
                out.writeObject(this);
            } catch (IOException ex) {
                afterStoreToFile(filepath, false);
                throw ex;
            }
            // rename the temp. file (the serialization was OK and the out stream is closed now)
            if (file.exists()) {
                file.delete();
            }
            temporaryFile.renameTo(file);

            afterStoreToFile(filepath, true);
            log.log(Level.INFO, "Algorithm stored to: {0} (via temporary file: {1})", new String[]{file.getAbsolutePath(), temporaryFile.getAbsolutePath()});
        } finally {
            // Unlock operations
            if (runningOperationMonitor != null)
                runningOperationMonitor.continueUpdates();
        }
    }

    /**
     * This method is executed BEFORE the method {@link #storeToFile(java.lang.String)}
     * was called. It is empty and expected to be overridden.
     *
     * @param filepath the path to a file where the algorithm was stored
     */
    protected void beforeStoreToFile(String filepath) {
    }

    /**
     * This method is executed after the method {@link #storeToFile(java.lang.String)}
     * was called. It is empty and expected to be overridden.
     *
     * @param filepath   the path to a file where the algorithm was stored
     * @param successful true, if the write to file was successful, false otherwise
     */
    protected void afterStoreToFile(String filepath, boolean successful) {
        final UpdateOperationLogger updateOperationLogger = getFirstExecutor(UpdateOperationLogger.class);
        if (updateOperationLogger != null) {
            updateOperationLogger.clearLog();
        }
    }
    //endregion


    //region ****************** Method to support execution of methods on this algorithm from a configuration file ******************//

    /**
     * Executes a given method on this algorithm and returns the result.
     *
     * @param methodName             the name of the method to execute on the remote algorithm
     * @param convertStringArguments if <tt>true</tt> the string values from the arguments are converted to proper types if possible
     * @param namedInstances         map of named instances - an instance from this map is returned if the <code>string</code> matches a key in the map
     * @param methodArguments        the arguments for the method
     * @return the method return value
     * @throws InvocationTargetException   if the executed method throws an exception
     * @throws NoSuchInstantiatorException if the there is no method for the given name and prototype
     * @throws IllegalArgumentException    if there was a problem reading the class in the remote algorithm's result
     */
    Object methodExecute(String methodName, boolean convertStringArguments, Map<String, Object> namedInstances, Object... methodArguments) throws InvocationTargetException, NoSuchInstantiatorException, IllegalArgumentException {
        try {
            return MethodInstantiator.getMethod(getClass(), methodName, convertStringArguments, true, namedInstances, methodArguments).invoke(this, methodArguments);
        } catch (IllegalAccessException e) {
            throw new InternalError("Method cannot be invoked even though it is public"); // This should never happen
        }
    }

    /**
     * Executes a given method on this algorithm and returns the result.
     * This method is a convenience method for APIs and should not be called from a native Java application.
     *
     * @param methodNameAndArguments the array that contains a method name and its arguments as string
     * @param methodNameIndex        the index in the {@code methodNameAndArguments} array where the method name is (the following arguments are considered to be the arguments)
     * @param namedInstances         map of named instances - an instance from this map is returned if the <code>string</code> matches a key in the map
     * @return the method return value
     * @throws InvocationTargetException   if the executed method throws an exception
     * @throws NoSuchInstantiatorException if the there is no method for the given name and prototype
     * @throws IllegalArgumentException    if there was a problem reading the class in the remote algorithm's result
     */
    public final Object executeMethodWithStringArguments(String[] methodNameAndArguments, int methodNameIndex, Map<String, Object> namedInstances) throws InvocationTargetException, NoSuchInstantiatorException, IllegalArgumentException {
        Object[] arguments = new Object[methodNameAndArguments.length - methodNameIndex - 1];
        System.arraycopy(methodNameAndArguments, methodNameIndex + 1, arguments, 0, arguments.length);
        return methodExecute(methodNameAndArguments[methodNameIndex], true, namedInstances, arguments);
    }

    /**
     * Executes a given method on this algorithm and returns the result.
     *
     * @param methodName      the name of the method to execute on the remote algorithm
     * @param methodArguments the arguments for the method
     * @return the method result or exception
     * @throws InvocationTargetException   if the executed method throws an exception
     * @throws NoSuchInstantiatorException if the there is no method for the given name and prototype
     * @throws IllegalArgumentException    if there was a problem reading the class in the remote algorithm's result
     */
    public final Object methodExecute(String methodName, Object[] methodArguments) throws InvocationTargetException, NoSuchInstantiatorException, IllegalArgumentException {
        return methodExecute(methodName, false, null, methodArguments);
    }
    //endregion


    //region ******************     Annotations for algorithm constructors to be used by the application ******************//

    /**
     * Annotation for algorithm constructors.
     * Each constructor, that should be accessible by auto-generated clients
     * must be annotated. Such constructor can only have parameters that can
     * be converted from a string by {@link messif.utility.Convert#stringToType stringToType}
     * method. Each constructor parameter should be annotated by a description
     * using this annotations values.
     */
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.CONSTRUCTOR)
    public @interface AlgorithmConstructor {
        /**
         * Description of an algorithm constructor.
         *
         * @return description of algorithm constructor
         */
        String description();

        /**
         * A list of descriptions for constructor parameters.
         * Each parameter should have a position-matching
         * descriptor value.
         *
         * @return list of descriptions for constructor parameters
         */
        String[] arguments();
    }

    /**
     * Returns all annotated constructors of the provided algorithm class.
     *
     * @param <E>            class of algorithm for which to get constructors
     * @param algorithmClass the class of an algorithm for which to get constructors
     * @return all annotated constructors of the provided algorithm class
     */
    public static <E extends Algorithm> List<Constructor<E>> getAnnotatedConstructors(Class<? extends E> algorithmClass) {
        List<Constructor<E>> rtv = new ArrayList<Constructor<E>>();

        // Search all its constructors for proper annotation
        for (Constructor<? extends E> constructor : Convert.getConstructors(algorithmClass))
            if (constructor.isAnnotationPresent(Algorithm.AlgorithmConstructor.class))
                rtv.add((Constructor) constructor);

        return rtv;
    }

    /**
     * Returns all annotated constructors of the provided algorithm class as array.
     *
     * @param <E>            class of algorithm for which to get constructors
     * @param algorithmClass the class of an algorithm for which to get constructors
     * @return all annotated constructors of the provided algorithm class
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public static <E extends Algorithm> Constructor<E>[] getAnnotatedConstructorsArray(Class<? extends E> algorithmClass) {
        return getAnnotatedConstructors(algorithmClass).toArray(new Constructor[1]);
    }

    /**
     * Returns constructor argument descriptions for the provided algorithm constructor.
     * List of available constructors of an algorithm class can be retrieved using {@link #getAnnotatedConstructors getAnnotatedConstructors}.
     * This is used by auto-generated clients to show description during algorithm creation.
     *
     * @param constructor an algorithm constructor to get the descriptions for
     * @return constructor argument descriptions
     */
    public static String[] getConstructorArgumentDescriptions(Constructor<? extends Algorithm> constructor) {
        Algorithm.AlgorithmConstructor annotation = constructor.getAnnotation(Algorithm.AlgorithmConstructor.class);
        return (annotation == null) ? null : annotation.arguments();
    }

    /**
     * Returns constructor description (without description of arguments) for the provided algorithm constructor.
     * List of available constructors of an algorithm class can be retrieved using {@link #getAnnotatedConstructors getAnnotatedConstructors}.
     * This is used by auto-generated clients to show description during algorithm creation.
     *
     * @param constructor an algorithm constructor to get the descriptions for
     * @return constructor description
     */
    public static String getConstructorDescriptionSimple(Constructor<? extends Algorithm> constructor) {
        Algorithm.AlgorithmConstructor annotation = constructor.getAnnotation(Algorithm.AlgorithmConstructor.class);
        if (annotation == null)
            return "";

        // Return only the description of the constructor
        return annotation.description();
    }

    /**
     * Returns algorithm constructor description including descriptions for all its arguments.
     * List of available constructors of an algorithm class can be retrieved using {@link #getAnnotatedConstructors getAnnotatedConstructors}.
     * This is used by auto-generated clients to show description during algorithm creation.
     *
     * @param constructor an algorithm constructor to get the descriptions for
     * @return constructor description including descriptions for all its arguments
     */
    public static String getConstructorDescription(Constructor<? extends Algorithm> constructor) {
        Algorithm.AlgorithmConstructor annotation = constructor.getAnnotation(Algorithm.AlgorithmConstructor.class);
        if (annotation == null)
            return "";

        // Construct the following description string: <algorithm class> [<argument description> ...]\n\t...<constructor description>
        StringBuilder rtv = new StringBuilder(constructor.getDeclaringClass().getName());
        for (String arg : annotation.arguments())
            rtv.append(" <").append(arg).append(">");
        rtv.append("\n\t...").append(annotation.description());

        return rtv.toString();
    }
    //endregion

    @Override
    public String toString() {
        return getName();
    }


    // copied from Algorithm v2.0 to be moved to individual executors or elsewhere

//    /**
//     * Sets a new {@link #threadPool thread pool} used for processing operations (via {@link NavigationProcessor}).
//     * The previously set {@link #threadPool} is shut down (destroyed).
//     * If {@code threadPool} is not null, parallel processing is used.
//     * @param threadPool the new thread pool instance to set (can be <tt>null</tt>)
//     */
//    public void setOperationsThreadPool(ExecutorService threadPool) {
//        if (this.threadPool != null && this.threadPool != threadPool && ! this.threadPool.isShutdown()) {
//            this.threadPool.shutdown();
//            log.log(Level.INFO, "shutting down threadpool: {0}", this.threadPool);
//        }
//        this.threadPool = threadPool;
//    }

//    /**
//     * Returns the current thread pool used for processing operations.
//     * @return the current thread pool instance or <tt>null</tt> if no thread pool is set
//     */
//    public ExecutorService getOperationsThreadPool() {
//        return threadPool;
//    }

//    /**
//     * Set the verbosity of the logging of the last executed operation.
//     * If set to zero (default), no executed operations are logged.
//     * If set to 1, the executed operation is logged using INFO level.
//     * If set to 2, the executed operation and all its parameters are logged using INFO level.
//     * @param opLogVerbosity the verbosity level
//     */
//    public void setExecutedOperationsLogVerbosity(int opLogVerbosity) {
//        this.opLogVerbosity = opLogVerbosity;
//    }

    // TODO: move the following methods somehow to OperationHandler
//    /**
//     * Creates a new {@link Callable} that simply runs the {@link #executeOperation} method on the given operation.
//     * @param <T> the type of operation
//     * @param operation the operation to run
//     * @return the created {@link Callable}
//     */
//    protected <T extends AbstractOperation> Callable<T> createBackgroundExecutionCallable(final T operation) {
//        return new Callable<T>() {
//            @Override
//            public T call() throws Exception {
//                return executeOperation(operation);
//            }
//        };
//    }
//
//    /**
//     * Execute algorithm operation on background.
//     * @param <T> the type of the executed operation
//     * @param operation the operation to execute on this algorithm
//     * @return a {@link Future} that can be used to wait for the execution to finish and retrieve the resulting executed operation;
//     *      note that a {@link NoSuchMethodException} exception can be thrown if the operation is unsupported (there is no method for the operation)
//     */
//    public <T extends AbstractOperation> Future<T> backgroundExecuteOperation(T operation) {
//        return threadPool.submit(createBackgroundExecutionCallable(operation));
//    }
//
//    /**
//     * Execute algorithm operation on background.
//     * @param <T> the type of the executed operation
//     * @param operation the operation to execute on this algorithm
//     * @return a {@link Future} that can be used to wait for the execution to finish and retrieve the resulting executed operation;
//     *      note that a {@link NoSuchMethodException} exception can be thrown if the operation is unsupported (there is no method for the operation)
//     */
//    public <T extends AbstractOperation> FutureWithStatistics<T> backgroundExecuteOperationWithStatistics(T operation) {
//        return FutureWithStatisticsImpl.submit(threadPool, createBackgroundExecutionCallable(operation));
//    }
//
    /**
     * // TODO: revise the background execution methods
     * Helper method for waiting for an operation executed on background.
     * Note that the operation statistics are automatically updated if the given future
     * implements {@link FutureWithStatistics}.
     * @param <T> the type of the executed operation
     * @param future a future of the previously executed operation
     * @return the executed operation
     * @throws AlgorithmMethodException if there was an exception during the background execution
     * @throws InterruptedException if the waiting was interrupted
     * @throws NoSuchMethodException if the operation is not supported
     */
    @SuppressWarnings("ThrowableResultIgnored")
    public static <T extends AbstractOperation> T waitBackgroundExecution(Future<? extends T> future) throws InterruptedException, AlgorithmMethodException, NoSuchMethodException {
        try {
            T ret = future.get();
            if (future instanceof FutureWithStatistics) {
                OperationStatistics.getLocalThreadStatistics().updateFrom((FutureWithStatistics<? extends T>)future);
            }
            return ret;
        } catch (ExecutionException ex) {
            if (ex.getCause() instanceof AlgorithmMethodException)
                throw (AlgorithmMethodException)ex.getCause();
            if (ex.getCause() instanceof NoSuchMethodException)
                throw (NoSuchMethodException)ex.getCause();
            throw new AlgorithmMethodException(ex.getCause());
        }
    }
//
//    /**
//     * Execute algorithm operation on background independently, i.e. without the
//     * possibility to wait for its finish. Use the {@link #getAllRunningOperations()}
//     * or {@link #getRunningOperationById(java.util.UUID)} to access the operation.
//     * Method {@link #terminateOperation(java.util.UUID)} can be used to stop the operation.
//     * After the operation is canFinish, there is no way to access it.
//     * @param operation the operation to execute on this algorithm
//     */
//    public void backgroundExecuteOperationIndependent(AbstractOperation operation) {
//        backgroundExecuteOperation(operation);
//    }


    // region ******************************     Utility methods

    /**
     * Returns the number of objects currently stored in the algorithm.
     * This is (by default) done by executing the {@link ObjectCountOperation}
     * but can be overridden if a more efficient method is available.
     * If the number of objects is unknown, -1 is returned.
     *
     * @return the number of objects currently stored in the algorithm
     * @throws AlgorithmMethodException if there was an error during the execution
     */
    public long getObjectCount() throws AlgorithmMethodException {
        try {
            ObjectCountAnswer answer = (ObjectCountAnswer) evaluate(OperationBuilder.create(ObjectCountOperation.class).build());
            if (answer == null) {
                return -1;
            }
            return answer.getAnswerCount();
        } catch (NoSuchMethodException | AlgorithmMethodException ignore) {
            return -1;
        }
    }
    // endregion

}
