/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.proxy;


import messif.record.ModifiableRecord;
import messif.record.Record;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Main entry point for Proxy package.
 * Provides ability to create proxy around given {@link ModifiableRecord}
 * <p>
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class ProxyConverter {

    /**
     * Creates proxy around given {@link Record}
     *
     * @param record underlying object to be wrapped by proxy
     * @param tClass class object of proxy
     * @param <T>    type of the required proxy
     * @return proxy instance of given class
     */
    @Nonnull
    public static <T> T convert(@Nonnull ModifiableRecord record, @Nonnull Class<T> tClass, Class<?> ... additionalClasses) {
        ClassWrapper<T> classWrapper = resolveClassWrapper(tClass, additionalClasses);
        return createInstance(classWrapper, record);
    }

    /**
     * Creates proxy around given {@link ModifiableRecord}.
     * Also verifies the data in incoming object to make sure it matches requested proxy interface.
     *
     * @param record underlying object to be wrapped by proxy
     * @param tClass class object of proxy
     * @param <T>    type of the required proxy
     * @throws IllegalArgumentException if the record does not successfully pass the verification
     * @return proxy instance of given class
     */
    @Nonnull
    public static <T> T convertChecked(@Nonnull ModifiableRecord record, @Nonnull Class<T> tClass, Class<?> ... additionalClasses) {
        ClassWrapper<T> classWrapper = resolveClassWrapper(tClass, additionalClasses);
        classWrapper.verify(record);
        return createInstance(classWrapper, record);
    }

    /**
     * Verifies if given object is the proxy
     *
     * @param object object to be verified
     * @return true if the object is proxy
     */
    public static boolean isProxy(@Nonnull Object object) {
        return object.getClass().getCanonicalName().contains("$Proxy");
    }

    /**
     * Return the underlying Record for given proxy object
     *
     * @param object proxy object
     * @return underlying record
     */
    @Nonnull
    public static ModifiableRecord resolveUnderlyingRecord(@Nonnull Object object) {
        return ((ClassWrapperInvocationHandler) Proxy.getInvocationHandler(object)).record;
    }

    /**
     * Uses given {@link ClassWrapper} to create Proxy instance of given record. Class of returned proxy object is driven by ClassWrapper type
     *
     * @param wrapper ClassWrapper holding necessary information to create the proxy
     * @param record  record Data
     * @param <T>     type of returned proxy instance
     * @return proxy instance
     */
    @Nonnull
    private static <T> T createInstance(@Nonnull ClassWrapper<T> wrapper, @Nonnull ModifiableRecord record) {
        try {
            InvocationHandler handler = new ClassWrapperInvocationHandler(record, wrapper);
            return wrapper.getConstructor().newInstance(handler);
        } catch (Exception e) {
            throw new RuntimeException("Failed to create proxy instance for interface " + wrapper.getClass().getGenericInterfaces()[0].getTypeName(), e);
        }
    }

    /**
     * Inner implementation {@link InvocationHandler} implementation.
     * Forwards the proxy method invocation to appropriate {@link ClassWrapper} using the {@link Record} as a data source
     */
    private static class ClassWrapperInvocationHandler implements InvocationHandler {

        @Nonnull
        private final ModifiableRecord record;
        @Nonnull
        private final ClassWrapper classWrapper;

        <T> ClassWrapperInvocationHandler(@Nonnull ModifiableRecord record, @Nonnull ClassWrapper<T> classWrapper) {
            this.record = record;
            this.classWrapper = classWrapper;
        }

        @Nullable
        @Override
        public Object invoke(@Nonnull Object proxy, @Nonnull Method method, @Nonnull Object[] args) throws Throwable {
            return classWrapper.executeMethod(record, proxy, method, args);
        }
    }

    /**
     * Lookup in {@link ProxyConverter#classWrappers} map for existing {@link ClassWrapper} for type given by incoming Class.
     * Create and store new one if necessary.
     *
     * @param tClass Class to get type of requested {@link ClassWrapper}
     * @param additionalClasses additional interfaces can be also implemented
     * @param <T>    type of requested {@link ClassWrapper}
     * @return ClassWrapper around given type
     */
    @Nonnull
    @SuppressWarnings("unchecked")
    private static <T> ClassWrapper<T> resolveClassWrapper(@Nonnull Class<T> tClass, Class<?> ... additionalClasses) {
        try {
            Class [] allClasses;
            String allClassesString;
            if (additionalClasses.length == 0) {
                allClasses = new Class[]{tClass};
                allClassesString = tClass.getName();
            } else {
                allClasses = Arrays.copyOf(additionalClasses, additionalClasses.length + 1);
                allClasses[additionalClasses.length] = tClass;
                allClassesString = Arrays.deepToString(allClasses);
            }

            ClassWrapper<T> classWrapper = classWrappers.get(allClassesString);
            if (classWrapper == null) {
                // prepare and store constructor to speed-up the proxy creation
                Class proxyClass = Proxy.getProxyClass(tClass.getClassLoader(), allClasses);
                Constructor<T> constructor = proxyClass.getConstructor(InvocationHandler.class);

                classWrapper = new ClassWrapper<>(tClass, constructor);
                classWrappers.put(allClassesString, classWrapper);
            }
            return classWrapper;
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("Failed to create proxy instance for interface(s) " + tClass.getSimpleName() + " + " + Arrays.toString(additionalClasses), e);
        }
    }


    /**
     * Global map of class {@link ClassWrapper} to save the memory consumption.
     * The key is the class name.
     */
    private static final Map<String, ClassWrapper> classWrappers = new HashMap<>();
}
