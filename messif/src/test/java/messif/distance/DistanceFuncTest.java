/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.distance;

import messif.distance.agg.WeightedDistanceFuncSum;
import messif.distance.impl.L1DistanceFloats;
import messif.distance.impl.L2DistanceInts;
import messif.utility.json.JSONReaderTest;
import messif.data.DataObject;
import messif.data.util.DataObjectList;
import messif.data.util.StreamDataObjectIterator;
import messif.record.Record;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class DistanceFuncTest {



    protected List<DataObject> objects;

    @Before
    public void setUp() throws Exception {
        this.objects = new DataObjectList(new StreamDataObjectIterator(JSONReaderTest.getJsonTestData()));
        System.out.println("successful setUp");
    }

    /**
     * Test of getDistance method, of class MessifObjectDistance.
     */
    @Test
    public void testGetDistance() throws Exception {
        System.out.println("getDistance");
        Record o1 = objects.get(0);
        Record o2 = objects.get(1);
        DistanceFunc distanceFunc = new DataObjectDistanceFunc("data", new L2DistanceInts());
        float expResult = 3.0F;
        float result = distanceFunc.getDistance(o1, o2);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getDistance method, of class MessifObjectDistance.
     */
    @Test
    public void testGetDistanceSum() throws Exception {
        System.out.println("getDistanceSumMap");
        Record o1 = objects.get(0);
        Record o2 = objects.get(1);
        DistanceFunc<DataObject>[] distances = new DistanceFunc[2];
        distances[0] = new DataObjectDistanceFunc("data", new L2DistanceInts());
        distances[1] = new DataObjectDistanceFunc("test data", new L1DistanceFloats());
        DistanceFunc distanceFunc = new WeightedDistanceFuncSum(distances, new float [] {2.0f, 1.0f});
        float expResult = 8.0F;
        float result = distanceFunc.getDistance(o1, o2);
        assertEquals(expResult, result, 0.0);
    }


}
