/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.json;

import messif.data.DataObject;
import messif.record.Record;
import messif.utility.DirectoryInputStream;

import java.io.*;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * This class represents an iterator on {@link messif.record.Record}s that are read from a file using
 * {@link JSONReader#readJSONObject()}.
 * The objects are instantiated one by one every time the {@link #next next} method is called.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class RecordIterator implements Iterator<Record>, Closeable {

    //****************** Attributes ******************//

    /** Remembered name of opened file to provide reset capability */
    protected String fileName;
    /** Instance of a next object. This is needed for implementing reading objects from a stream */
    protected Record nextObject;
    /** Instance of the current object */
    //protected DataObject currentObject;
    /** Number of objects read from the stream */
    protected int objectsRead;
    /** Reader of the JSON-formatted data objects (field maps). */
    protected JSONReader jsonReader;


    //****************** Constructors ******************//


    /**
     * Creates a new instance of RecordIterator.
     * The objects are loaded from the given stream on the fly as this iterator is iterated.
     * The constructor of <code>objClass</code> that accepts {@link BufferedReader}
     * as the first argument and all the arguments from the <code>constructorArgs</code>
     * is used to read objects from the stream.
     *
     * @param stream stream from which objects are read and instantiated
     * @throws IllegalArgumentException if the provided class does not have a proper "stream" constructor
     * @throws IllegalStateException if there was an error reading from the stream
     */
    public RecordIterator(BufferedReader stream, boolean createRecordImpl) throws IllegalArgumentException, IllegalStateException {
        this.jsonReader = new JSONReader(stream, createRecordImpl);

        // Read first object from the stream (hasNext is set automatically)
        this.nextObject = nextStreamObject();
    }

    public RecordIterator(BufferedReader stream) throws IllegalArgumentException, IllegalStateException {
        this(stream, true);
    }

    public RecordIterator(File file, boolean createRecordImpl) throws IllegalArgumentException, IOException {
        this(new BufferedReader(new InputStreamReader(new FileInputStream(file))), createRecordImpl);
        this.fileName = file.getAbsolutePath();
    }

    public RecordIterator(File file) throws IllegalArgumentException, IOException {
        this(file, true);
    }

    /**
     * Creates a new instance of StreamDataObjectIterator.
     * The objects are loaded from the given file on the fly as this iterator is iterated.
     * If the <code>fileName</code> is empty, <tt>null</tt> or dash, standard input is used.
     *
     * @param fileName the path to a file from which objects are read;
     *          if it is a directory, all files that match the glob pattern are loaded
     *          (see {@link DirectoryInputStream#open(String) DirectoryInputStream} for more informations)
     * @throws IllegalArgumentException if the provided class does not have a proper "stream" constructor
     * @throws IOException if there was an error opening the file
     */
    public RecordIterator(String fileName) throws IllegalArgumentException, IOException {
        this(new BufferedReader(new InputStreamReader(DirectoryInputStream.open(fileName))));
        // Set file name to provided value - it is used in reset functionality
        if (fileName == null || fileName.length() == 0 || fileName.equals("-"))
            this.fileName = null;
        else
            this.fileName = fileName;
    }


    //****************** Iterator methods ******************//

    /**
     * Returns the next object instance from the stream.
     *
     * @return the next object instance from the stream
     * @throws NoSuchElementException if the end-of-file was reached
     * @throws IllegalArgumentException if there was an error creating a new instance of the object
     * @throws IllegalStateException if there was an error reading from the stream
     */
    @Override
    public Record next() throws NoSuchElementException, IllegalArgumentException, IllegalStateException {
        // No next object available
        if (nextObject == null)
            nextObject = nextStreamObject();
        if (nextObject == null)
            throw new NoSuchElementException("No more objects in the stream");

        // Reading object on the fly from a stream
        //currentObject = nextObject;
        try {
            return nextObject;
        } finally {
            nextObject = null;
        }
    }

    /**
     * Returns <tt>true</tt> if the iteration has more elements. (In other
     * words, returns <tt>true</tt> if <tt>next</tt> would return an element
     * rather than throwing an exception.)
     *
     * @return <tt>true</tt> if the iterator has more elements.
     */
    @Override
    public boolean hasNext() {
        if (nextObject == null)
            nextObject = nextStreamObject();
        return nextObject != null;
    }

    //****************** Support for reading from a stream *************//

    /**
     * Returns an instance of object which would be returned by next execute to next().
     * @return Returns an instance of object of type E which would be returned by the next execute to next().
     *         If there is no additional object, null is returned.
     * @throws IllegalStateException if there was an error reading from the stream
     */
    protected final Record nextStreamObject() throws IllegalStateException {
        try {
            final DataObject ret = jsonReader.readRecord();
            if (ret != null)
                objectsRead++;
            return ret;
        } catch (JSONException e) {
            // Exception while reading the object from the stream
            throw new IllegalStateException(
                    "Cannot read instance #" + (objectsRead + 1) + " of Records from " +
                    //factory.getCreatedClass().getName() + " from " +
                    ((fileName == null) ? "STDIN" : fileName) + ": " +
                    e.getCause(), e.getCause());
        }
    }

    /**
     * Close the associated stream.
     * The iteration is finished, hasNext() will return <tt>false</tt>.
     * However, getCurrentObject is still valid if there was previous execute to next().
     * @throws IOException if there was an I/O error closing the file
     */
    @Override
    public void close() throws IOException {
        if (jsonReader != null)
            jsonReader.close();
        nextObject = null;
    }

}
