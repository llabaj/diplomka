/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.record.processing;

import messif.record.ModifiableRecord;
import messif.record.Record;
import messif.record.RecordImpl;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Provides a processor that takes a {@link Record}, changes it, and returns the modified {@link Record}. If
 *  the record is of type {@link ModifiableRecord}, the same modified object is returned. If not, a new object
 *  is created and returned - for instance, object of type {@link RecordImpl} is created. Also a batch processing
 *  of records is available.
 *
 * @param <T> type of records this process process
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface RecordProcessor<T extends Record> extends RecordFilter<T>, Function<T,T> {

    /**
     * Processes given records, modifying them and returning (can be in a different order).
     * @param records an array of records to be processed and modified
     * @return the same or different array of modified records
     */
    List <T> process(List<T> records) throws RecordProcessingException;

    /**
     * Processes given record, modifying it and returning the same instance or a new instance (if not modifiable).
     * @param record an array of records to be processed and modified
     * @return the same or different array of modified records
     */
    T process(T record) throws RecordProcessingException;

    /**
     * Get a list of record fields modified by this processor.
     * @return a list of record fields modified by this processor.
     */
    String [] modifiedFields();

    @Override
    default T apply(T t) {
        return process(t);
    }

    /**
     * Processor of a single record with a default implementation of batch processing.
     *
     * @param <T> type of records this process process
     *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
     */
    interface Single<T extends Record> extends RecordProcessor<T> {

        /**
         * Default implementation that calls the {@link #process(Record)} method.
         */
        default List<T> process(List<T> records) throws RecordProcessingException {
            return records.stream().map(this).collect(Collectors.toList());
        }
    }

    /**
     * Interface of a processor that takes a list of {@link Record}s, and returns a list of {@link Record}s.
     *
     * @param <T> type of records this process process
     *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
     * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
     * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
     * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
     * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
     */
    interface Multi<T extends Record> extends RecordProcessor<T> {

        /**
         * Default implementation that calls the {@link #process(List)} method.
         */
        default T process(T record) throws RecordProcessingException {
            return process(Collections.singletonList(record)).get(0);
        }
    }

    RecordProcessor IDENTITY = new Single<Record>() {

        @Override
        public boolean preCondition(Record record) {
            return true;
        }

        @Override
        public String preConditionDescription() {
            return "identity record processor";
        }

        @Override
        public Record process(Record record) throws RecordProcessingException {
            return record;
        }

        @Override
        public String[] modifiedFields() {
            return new String[0];
        }
    };

}
