/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.processing;

import messif.algorithm.AlgorithmMethodException;
import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.processing.impl.OneStepProcessor;

/**
 * A simple processor that executes given operation on given {@link OperationEvaluator algorithm}.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @param <TRequest> class of the operation to be processed by the specific processor
 */
public class AlgorithmOperationProcessor<TRequest extends AbstractOperation>
       extends OneStepProcessor<TRequest, AbstractAnswer> {

    /** Algorithm to run the operation on. */
    protected final OperationEvaluator algorithm;

    protected AbstractAnswer answer;

    /**
     * Creates this processor given the operation to be processed.
     * @param operation operation to be executed
     * @param algorithm
     */
    public AlgorithmOperationProcessor(TRequest operation, OperationEvaluator algorithm) {
        super(operation);
        this.algorithm = algorithm;
    }

    /**
     * Do the one processing step.
     * @throws AlgorithmMethodException 
     */
    protected void process() throws Exception {
        this.answer = algorithm.evaluate(operation);
    }

    @Override
    public AbstractAnswer finish() {
        return answer;
    }
}
