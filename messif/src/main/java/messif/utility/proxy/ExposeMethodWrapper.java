/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.proxy;

import messif.record.ModifiableRecord;
import messif.record.Record;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Ad-hoc method wrapper that allows the converted interface to return underlying {@link Record}. Such method has to be annotated with an @Expose annotation.
 * <p>
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
class ExposeMethodWrapper implements MethodWrapper {

    @Nullable
    @Override
    public Object execute(@Nonnull ModifiableRecord record, @Nonnull Object proxy, @Nullable Object[] args) throws Throwable {
        return record;
    }
}
