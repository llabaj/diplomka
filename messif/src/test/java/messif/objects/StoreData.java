/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.objects;

import messif.bucket.impl.DiskBlockBucket;
import messif.data.DataObject;
import messif.utility.json.JSONWriter;
import messif.data.util.StreamDataObjectIterator;

import java.io.File;
import java.util.Iterator;

/**
 * This class simply reads in the data file and prints the IDs of the objects.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class StoreData {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        args = new String [2];
        args[0] = "src/test/resources/imagedata.json";
        args[1] = "diskbucket.bin";
        if (args.length < 2) {
            System.err.println("Usage: CheckData <datafile>[.gz] <diskbucketfile>");
            return;
        }
        try (JSONWriter outWriter = new JSONWriter(System.out, true)) {
            File file = new File(args[1]);
            if (file.exists()) {
                file.delete();
            }
            StreamDataObjectIterator iterator = new StreamDataObjectIterator(args[0]);
            DiskBlockBucket diskBucket = new DiskBlockBucket(Long.MAX_VALUE, Long.MAX_VALUE, 0, file);
            while (iterator.hasNext()) {
                DataObject obj = iterator.next();
                diskBucket.addObject(obj);
            }
            diskBucket.finalize();

            // read the data from the bucket
            diskBucket = new DiskBlockBucket(Long.MAX_VALUE, Long.MAX_VALUE, 0, file);

            Iterator<DataObject> allObjects = diskBucket.getAllObjects();
            long counter = 0;
            while (allObjects.hasNext()) {
                DataObject obj = allObjects.next();
                obj.writeJSON(outWriter);
                System.out.println();
                counter ++;
            }
            System.out.println("Finally processed " + counter + " objects");

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
