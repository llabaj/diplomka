/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.indexing;

import messif.data.DataObject;
import messif.record.ModifiableRecord;
import messif.record.ModifiableRecordImpl;
import messif.record.Record;
import messif.utility.ArrayMap;
import messif.utility.json.JSONException;
import messif.utility.json.JSONWriter;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class IndexObject implements DataObject, IndexRecord {

    /** Name of the field used when serializing this object using JSON serializator. */
    public static final String DATA_FIELD_NAME = "data".intern();

    /** Name of the field used when serializing this object using JSON serializator. */
    public static final String INDEX_FIELD_NAME = "index".intern();

    /**
     * A data object encapsulated in this "index object".
     */
    protected final DataObject dataObject;

    /**
     * Encapsulated {@link Map} that provides the field values
     */
    //protected Map<String, Object> indexFieldMap;
    protected ModifiableRecord indexFieldMap;


    public DataObject getDataObject() {
        return dataObject;
    }


    // region ****************************       Constructors

    /**
     * Creates a new instance of RecordImpl from the given map of fields.
     *
     * @param fields         the map that provides the field values
     * @param useMapDirectly flag whether the given field map is used directly (<tt>true</tt>)
     *                       or if a {@link ArrayMap} copy is created (<tt>false</tt>)
     */
    public IndexObject(DataObject dataObject, Map<String, Object> fields, boolean useMapDirectly) {
        this.indexFieldMap = new ModifiableRecordImpl(fields, useMapDirectly);
        this.dataObject = dataObject;
    }

    /**
     * Creates a new instance of RecordImpl from the given map of fields.
     * The fields are stored in a new {@link ArrayMap} instance.
     *
     * @param fields the map that provides the field values
     */
    public IndexObject(DataObject dataObject, Map<String, Object> fields) {
        this(dataObject, fields, false);
    }

    /**
     * Create this static {@link Record} given keys and value arrays that are used directly by {@link ArrayMap}.
     * @param keys string keys of the record
     * @param values corresponding values
     */
    public IndexObject(DataObject dataObject, String[] keys, Object[] values) {
        this(dataObject, new ArrayMap<>(keys, values, true), true);
    }

    public IndexObject(DataObject dataObject) {
        this(dataObject, new HashMap<>(), true);
    }
    // endregion

    @Override
    public JSONWriter writeJSON(JSONWriter writer) throws JSONException, IOException {
        return writeJSON(writer, 0);
    }

    @Override
    public JSONWriter writeJSON(JSONWriter writer, int level) throws JSONException, IOException {
        writer.write(new ArrayMap<>(new String[]{INDEX_FIELD_NAME, DATA_FIELD_NAME}, new Object[]{indexFieldMap, dataObject}, true), level);
        writer.flush();
        return writer;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException("TODO");
    }

    // TODO
    @Override
    public boolean dataEquals(Record second) {
        return dataObject.dataEquals(second);
    }

    // TODO
    @Override
    public int dataHashCode() {
        return dataObject.dataHashCode();
    }


    // region ****************************       Implementation of the index field access

    @Override
    public Collection<String> getIndexFieldNames() {
        return Collections.unmodifiableCollection(indexFieldMap.getFieldNames());
    }

    @Override
    public boolean containsIndexField(String name) {
        return indexFieldMap.containsField(name);
    }

    @Override
    public Object getIndexField(String name) {
        return indexFieldMap.getField(name);
    }

    @Override
    public <T> T getIndexField(String name, Class<? extends T> fieldClass) {
        return indexFieldMap.getField(name, fieldClass);
    }

    @Override
    public Object setIndexField(String name, Object value) {
        return indexFieldMap.setField(name, value);
    }

    @Override
    public Object removeIndexField(String name) {
        return indexFieldMap.removeField(name);
    }

    @Override
    public Map<String, Object> getIndexMap() {
        return indexFieldMap.getMap();
    }
    // endregions


    // region ********************     Static builders

    public static List<IndexObject> convertObjects(List<DataObject> list) {
        return list.stream().map(IndexObject::new).collect(Collectors.toList());
    }


    // region  ***************************************    Delegated methods to the encapsulated data object

    @Override
    public int getFieldCount() {
        return dataObject.getFieldCount();
    }

    @Override
    public Collection<String> getFieldNames() {
        return dataObject.getFieldNames();
    }

    @Override
    public boolean containsField(String name) {
        return dataObject.containsField(name);
    }

    @Override
    public Object getField(String name) {
        return dataObject.getField(name);
    }

    @Override
    public Object getRequiredField(String name) throws IllegalArgumentException {
        return dataObject.getRequiredField(name);
    }

    @Override
    public <T> T getField(String name, Class<? extends T> fieldClass, T defaultValue) {
        return dataObject.getField(name, fieldClass, defaultValue);
    }

    @Override
    public Map<String, Object> getMap() {
        return dataObject.getMap();
    }
    // endregion
}
