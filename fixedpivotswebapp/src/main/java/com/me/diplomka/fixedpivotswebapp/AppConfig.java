package com.me.diplomka.fixedpivotswebapp;

import org.aeonbits.owner.Config;

public interface AppConfig extends Config {
    /** How many closest pivots should be used as index */
    @DefaultValue("2")
    int prefixLength();

    /** The minimal amount of entries in candidate set before refinement, if available */
    @DefaultValue("10000")
    int candidateSetThreshold();

    /** How many entries should be used from pivot file */
    @DefaultValue("128")
    int pivotCount();

    /** Name of the hazelcast map containing data objects to be searched in */
    @DefaultValue("data")
    String dataMap();

    /** Name of the hazelcast replicated map containing pivots */
    @DefaultValue("pivots")
    String pivotMap();

    /** Name of the hazelcast replicated map containing statistics for each pivot prefix */
    @DefaultValue("pivot-prefix-counts")
    String pivotPrefixCountsMap();

    /** Name of the hazelcast replicated map containing statistics for pivot prefix tag counts */
    @DefaultValue("pivot-prefix-tags")
    String pivotPrefixTagsMap();

    /** Coefficient used to estimate size of intersection when searching by tags */
    @DefaultValue("0.9")
    double tagIntersectCoefficient();
}
