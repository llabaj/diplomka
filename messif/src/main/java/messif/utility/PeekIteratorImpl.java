/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Implementation of {@link Iterator} wrapper that can peek the next value.
 * @param <T> the type of elements returned by this iterator
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class PeekIteratorImpl<T> implements PeekIterator<T> {
    /** Encapsulated iterator */
    private final Iterator<? extends T> iterator;
    /** The item that is retrieved from the next iterator iteration (and used in comparison) */
    private T nextObject;
    /** Flag whether there is a next object in the iterator */
    private boolean hasNext;

    /**
     * Creates a new instance of PeekIterator encapsulating a given iterator.
     * @param iterator the iterator to encapsulate
     */
    public PeekIteratorImpl(Iterator<? extends T> iterator) {
        this.iterator = iterator;
        this.hasNext = iterator.hasNext();
        this.nextObject = this.hasNext ? iterator.next() : null;
    }

    /**
     * Creates a new instance of PeekIterator encapsulating a given array.
     * @param <V> type of the values in the array
     * @param array the array to encapsulate
     */
    @SafeVarargs
    public <V extends T> PeekIteratorImpl(final V... array) {
        this(new Iterator<T>() {
            private int index;
            @Override
            public boolean hasNext() {
                return index < array.length;
            }
            @Override
            public T next() {
                if (index >= array.length)
                    throw new NoSuchElementException();
                return array[index++];
            }
            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        });
    }

    @Override
    public boolean hasNext() {
        return hasNext;
    }

    @Override
    public T peek() {
        if (!hasNext)
            return null;
        return nextObject;
    }

    @Override
    public T next() {
        if (!hasNext)
            return null;
        T ret = nextObject;
        if (iterator.hasNext())
            nextObject = iterator.next();
        else
            hasNext = false;
        return ret;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
