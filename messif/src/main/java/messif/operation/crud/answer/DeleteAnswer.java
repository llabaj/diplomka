/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.crud.answer;

import messif.utility.net.HttpStatusCodeProvider;
import messif.utility.ProcessingStatus;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface DeleteAnswer extends CRUDAnswer {

    /** Object has been deleted successfully. */
    ProcessingStatus OBJECT_DELETED = new ProcessingStatus("records deleted", 220, HttpStatusCodeProvider.STATUS_CODE_OK);

    /** Object has been deleted but the current capacity is less than the minimal required one (low-occupation has been reached). */
    ProcessingStatus LOWOCCUPATION_EXCEEDED = new ProcessingStatus("low occupation exceeded", 420, HttpStatusCodeProvider.STATUS_CODE_PARTIAL);


    @Override
    default boolean wasSuccessful() {
        return getStatus().in(OBJECT_DELETED, LOWOCCUPATION_EXCEEDED, CRUDAnswer.SOME_OBJECT_NOT_FOUND);
    }

}
