/**
 * Networked objects and buckets.
 * Allows to access a {@link messif.network.buckets.RemoteBucket} as a regular
 * {@link messif.bucket.Bucket} using a {@link messif.network.MessageDispatcher messaging}.
 */
package messif.network.buckets;

