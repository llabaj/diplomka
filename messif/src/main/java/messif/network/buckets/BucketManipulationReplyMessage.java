/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.network.buckets;

import messif.data.DataObject;
import messif.data.util.DataObjectIterator;
import messif.data.util.DataObjectList;
import messif.distance.DistanceFunc;
import messif.operation.ReturnDataOperation;
import messif.operation.crud.answer.DeleteAnswer;
import messif.record.Record;
import messif.utility.ProcessingStatus;

import java.util.Collection;

/**
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class BucketManipulationReplyMessage extends BucketReplyMessage {
    /** Class serial id for serialization */
    private static final long serialVersionUID = 1L;

    //****************** Attributes ******************//

    protected final ProcessingStatus errorCode;
    protected final DataObject object;
    protected final DataObjectList objects;
    protected final ReturnDataOperation query;
    protected final DistanceFunc<Record> defaultDistanceFunc;
    protected final int changeCount;
    
    //****************** Attribute access methods ******************//

    public DataObject getObject() {
        return object;
    }
    
    public DataObjectList getObjects() {
        return objects;
    }
    
    public ProcessingStatus getErrorCode() {
        return errorCode;
    }

    public ReturnDataOperation getQuery() {
        return query;
    }



    public int getChangesCount() {
        return 0;
    }


    //****************** Constructors ******************//

    /**
     * Creates a new instance of BucketManipulationReplyMessage for adding object
     */
    public BucketManipulationReplyMessage(BucketManipulationRequestMessage message, ProcessingStatus errorCode, int changeCount) {
        super(message);
        this.errorCode = errorCode;
        this.object = null;
        this.objects = null;
        this.changeCount = changeCount;
        this.query = null;
        this.defaultDistanceFunc = null;
    }

    /**
     * Creates a new instance of BucketManipulationReplyMessage for getting object
     */
    public BucketManipulationReplyMessage(BucketManipulationRequestMessage message, DataObject object) {
        this(message, object, false);
    }

    /**
     * Creates a new instance of BucketManipulationReplyMessage for getting object
     */
    public BucketManipulationReplyMessage(BucketManipulationRequestMessage message, DataObject object, boolean deleteObject) {
        super(message);
        this.errorCode = deleteObject? DeleteAnswer.OBJECT_DELETED:null;
        this.object = object;
        this.objects = null;
        this.changeCount = 0;
        this.query = null;
        this.defaultDistanceFunc = null;
    }
     
    /**
     * Creates a new instance of BucketManipulationReplyMessage for getting
     */
    public BucketManipulationReplyMessage(BucketManipulationRequestMessage message, DataObjectIterator objects) {
        super(message);
        errorCode = null;
        object = null;
        changeCount = 0;
        this.objects = new DataObjectList(objects);
        this.query = null;
        this.defaultDistanceFunc = null;
    }

    /**
     * Creates a new instance of BucketManipulationReplyMessage for getting
     */
    public BucketManipulationReplyMessage(BucketManipulationRequestMessage message, Collection<DataObject> objects, ReturnDataOperation query, DistanceFunc<Record> defaultDistanceFunc) {
        super(message);
        errorCode = null;
        object = null;
        changeCount = 0;
        this.objects = new DataObjectList(objects);
        this.query = query;
        this.defaultDistanceFunc = defaultDistanceFunc;
    }
}
