/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.distance;

import messif.data.DataObject;

/**
 * A distance function to calculate distance on a specific field of an {@link DataObject object}. The actual
 *  distance function (that takes e.g. a float []   array) is passed to this class.
 *
 * @param <T> the type of the distance function arguments
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class DataObjectDistanceFunc<T> implements DistanceFunc<DataObject> {

    /**
     * Class id for serialization.
     */
    private static final long serialVersionUID = 8198898426960390406L;

    /**
     * Name of the field to be taken from the data object.
     */
    private final String fieldName;

    /**
     * Distance to be calculated on the {@link DataObject object} field.
     */
    private final DistanceFunc<T> fieldDistance;

    /**
     * Creates a new instance of this distance function given a field name and the actual distance function.
     * @param fieldName
     * @param fieldDistance
     */
    public DataObjectDistanceFunc(String fieldName, DistanceFunc<T> fieldDistance) {
        if (fieldName == null || fieldDistance == null) {
            throw new IllegalArgumentException(DataObjectDistanceFunc.class.toString() + " cannot be constructed with null arguments");
        }
        this.fieldName = fieldName.intern();
        this.fieldDistance = fieldDistance;
    }

    /** Getter of the field name. */
    public String getFieldName() {
        return fieldName;
    }

    @Override
    public float getDistance(DataObject o1, DataObject o2, float threshold)  {
        try {
            final T subObject1 = getSubObject(o1);
            final T subObject2 = getSubObject(o2);
            if (subObject1 == null || subObject2 == null) {
                return (subObject1 == null && subObject2 == null) ? DistanceFunc.MIN_DISTANCE : getMaxDistance();
            }
            return fieldDistance.getDistance(subObject1, subObject2, threshold);
        } catch (DistanceException ex) {
            // DistanceFunc.log.log(Level.WARNING, "object missing or wrong field " + fieldName + ": " + o1.getID() + " or " + o2.getID());
            return getMaxDistance();
        }
    }

    /**
     * Returns the encapsulated object for the distance function.
     *
     * @param dataObject the meta object from which to get the encapsulated object
     * @return the encapsulated object for the distance function - can be NULL
     */
    protected final T getSubObject(DataObject dataObject) {
        return dataObject.getField(fieldName, fieldDistance.getObjectClass());
//        try {
//            T field = dataObject.getField(fieldName, fieldDistance.getObjectClass());
//            if (field == null) {
//                throw DistanceException.fieldMissingException(dataObject, fieldName);
//            }
//            return field;
//        } catch (ClassCastException ex) {
//            throw DistanceException.fieldClassException(dataObject, fieldName, fieldDistance.getObjectClass());
//        }
    }

    @Override
    public float getMaxDistance() {
        return fieldDistance.getMaxDistance();
    }

    @Override
    public Class<DataObject> getObjectClass() {
        return DataObject.class;
    }

    @Override
    public String toString() {
        return new StringBuilder(fieldDistance.toString()).append(" on field '").append(fieldName).append('\'').toString();
    }
}
