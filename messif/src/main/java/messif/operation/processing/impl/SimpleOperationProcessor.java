/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.processing.impl;

import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.processing.OperationProcessor;
import messif.utility.ProcessingStatus;

/**
 * An abstract base of operation processors that have only one step.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @param <TRequest> class of the operation to be processed by the specific processor
 * @param <TAnswer> class of answer returned by this processor
 */
public abstract class SimpleOperationProcessor<TRequest extends AbstractOperation, TAnswer extends AbstractAnswer>
        implements OperationProcessor<TRequest, TAnswer> {
    /**
     * Operation to be executed by this processor.
     */
    protected final TRequest operation;

    /**
     * Flag to determine if all the processing steps were performed (processed).
     */
    protected volatile boolean canFinish = false;

    /**
     * Continuously updated error code to be used in the final answer.
     */
    protected ProcessingStatus status = ProcessingStatus.NOT_SET;

    /**
     * Creates this processor given the operation to be processed.
     * @param operation operation to be executed
     */
    public SimpleOperationProcessor(TRequest operation) {
        this.operation = operation;
    }

    @Override
    public TRequest getOperation() {
        return operation;
    }

//    @Override
//    public boolean canFinish() {
//        return canFinish;
//    }


    /**
     * Update the error code of this operation from another operation.
     * If the updated operation was not successful, the value overrides this
     * operation's error code (so that the errors propagate up).
     * @param answer the source operation from which to get the update
     * @throws ClassCastException if the specified operation is incompatible with this operation
     */
    public void updateFrom(AbstractAnswer answer) throws ClassCastException {
        if (!status.isSet() || (answer.isFinished() && !answer.wasSuccessful()))
            status = answer.getStatus();
    }

}
