/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.algorithm.impl;

import java.io.ByteArrayInputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import messif.algorithm.Algorithm;
import messif.objects.LocalAbstractObject;
import messif.objects.impl.MetaObjectMap;
import messif.operation.data.InsertOperation;
import messif.utility.CoreApplication;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class TestVarargAlgorithm extends Algorithm {
    private final Object[] args;
    @AlgorithmConstructor(description = "test", arguments = {"vararg"})
    public TestVarargAlgorithm(String a, Object... args) throws IllegalArgumentException {
        super("Test algotithm");
        this.args = args;
        System.out.println(Arrays.toString(args));
    }

    public static void testObjectFactory() throws InvocationTargetException {
        LocalAbstractObject.TextStreamFactory<MetaObjectMap> factory = new LocalAbstractObject.TextStreamFactory<>(
                MetaObjectMap.class, true, null, new Object[] { "moje, test" }
        );
        System.out.println(factory.create("moje;messif.objects.impl.ObjectIntVectorL1\n1,2,3,4,5"));
    }

    public static void main(String[] args) throws Exception {
        System.setIn(new ByteArrayInputStream((
                "actions = alg\n"
              + "alg = algorithmStart\n"
              + "alg.param.1 = " + TestVarargAlgorithm.class.getName() + "\n"
              + "alg.param.2 = null\n"
              + "alg.param.3 = null\n"
              + "alg.param.4 = messif.operations.data.InsertOperation(null)\n"
                ).getBytes()));
        CoreApplication.main(new String[] {"-"});
        testObjectFactory();
    }
}
