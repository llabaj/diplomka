/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.distance.impl;

import messif.distance.DistanceFunc;
import messif.distance.DistanceException;
import messif.distance.Metric;

/**
 * Implementation of L_max on arrays of floats.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class LMaxDistanceFloats implements DistanceFunc<float[]>, Metric {

    /** Class id for Java serialization. */
    private static final long serialVersionUID = 1L;
    
    @Override
    public float getDistance(float[] o1, float[] o2, float threshold) throws DistanceException {
        if (o2.length != o1.length) {
            throw new DistanceException("Cannot compute distance on different vector dimensions (" + o1.length + ", " + o2.length + ")");
        }

        // Get maximum of absolute difference on all dimensions
        float rtv = 0;
        for (int i = 0; i < o1.length; i++) {
            float dist = Math.abs(o1[i] - o2[i]);
            if (rtv < dist) {
                rtv = dist;
                if (rtv > threshold) {
                    return MAX_DISTANCE;
                }
            }
        }        
        return rtv;
    }

    @Override
    public Class<float[]> getObjectClass() {
        return float [].class;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
