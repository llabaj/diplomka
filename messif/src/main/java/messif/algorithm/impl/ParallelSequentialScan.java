/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.algorithm.impl;

import messif.algorithm.Algorithm;
import messif.algorithm.AlgorithmMethodException;
import messif.bucket.BucketDispatcher;
import messif.bucket.LocalBucket;
import messif.bucket.impl.MemoryStorageBucket;
import messif.data.DataObject;
import messif.distance.DistanceFunc;
import messif.operation.crud.DeleteOperation;
import messif.operation.crud.GetObjectsOperation;
import messif.operation.crud.InsertOperation;
import messif.operation.crud.answer.DeleteAnswer;
import messif.operation.crud.answer.GetObjectsAnswer;
import messif.operation.crud.answer.InsertAnswer;
import messif.operation.info.ObjectCountOperation;
import messif.operation.info.answer.ObjectCountAnswer;
import messif.operation.processing.ProcessorConfiguration;
import messif.operation.processing.impl.BucketQueryOperationProcessor;
import messif.operation.processing.impl.OneStepProcessor;
import messif.operation.processing.impl.QueueOperationProcessor;
import messif.operation.search.KNNOperation;
import messif.operation.search.KNNRangeOperation;
import messif.operation.search.QueryObjectOperation;
import messif.operation.search.RangeOperation;
import messif.operation.answer.RankedAnswer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Implementation of the naive sequential scan algorithm.
 * <p>
 * It uses one bucket to store objects and performs operations on the bucket.
 * It also supports pivot-based filtering. The pivots can be specified in a constructor.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class ParallelSequentialScan extends Algorithm {
    /**
     * class id for serialization
     */
    private static final long serialVersionUID = 1L;

    //****************** Attributes ******************//

    /**
     * Instances of bucket where all the objects are stored
     */
    private final List<LocalBucket> buckets;

    /**
     * Index of the bucket that receives next inserted object
     */
    private int insertBucket;

    /**
     * Default distance function to be used for the distance-based queries.
     */
    protected DistanceFunc<DataObject> defaultDistanceFunc;


    /**
     * Creates a new instance of ParallelSequentialScan access structure with specific bucket class.
     * Additional parameters for the bucket class constructor can be passed.
     *
     * @param parallelization   the number of parallel buckets to create
     * @param bucketClass       the class of the storage bucket
     * @param bucketClassParams additional parameters for the bucket class constructor in the name->value form
     * @throws IllegalArgumentException if <ul><li>the provided bucketClass is not a part of LocalBucket hierarchy</li>
     *                                  <li>the bucketClass does not have a proper constructor (String,long,long)</li>
     *                                  <li>the correct constructor of bucketClass is not accessible</li>
     *                                  <li>the constructor of bucketClass has failed</li></ul>
     */
    @AlgorithmConstructor(description = "Parallel SequantialScan Access Structure", arguments = {"parallelization", "bucket class", "bucket class params"})
    public ParallelSequentialScan(int parallelization, Class<? extends LocalBucket> bucketClass, Map<String, Object> bucketClassParams) throws IllegalArgumentException {
        super("ParallelSequentialScan");

        // Check the parallelization parameter
        if (parallelization < 1)
            throw new IllegalArgumentException("Parallelization argument must be at least 1");

        // Create empty buckets (using the provided bucket class and parameters)
        buckets = new ArrayList<LocalBucket>(parallelization);
        for (int i = 0; i < parallelization; i++)
            buckets.add(BucketDispatcher.createBucket(bucketClass, Long.MAX_VALUE, Long.MAX_VALUE, 0, true, bucketClassParams));
        insertBucket = 0;
    }

    /**
     * Creates a new instance of ParallelSequentialScan access structure with specific bucket class.
     *
     * @param parallelization the number of parallel buckets to create
     * @param bucketClass     the class of the storage bucket
     * @throws IllegalArgumentException if <ul><li>the provided bucketClass is not a part of LocalBucket hierarchy</li>
     *                                  <li>the bucketClass does not have a proper constructor (String,long,long)</li>
     *                                  <li>the correct constructor of bucketClass is not accessible</li>
     *                                  <li>the constructor of bucketClass has failed</li></ul>
     */
    @AlgorithmConstructor(description = "Parallel SequantialScan Access Structure", arguments = {"parallelization", "bucket class"})
    public ParallelSequentialScan(int parallelization, Class<? extends LocalBucket> bucketClass) throws IllegalArgumentException {
        this(parallelization, bucketClass, null);
    }

    /**
     * Creates a new instance of ParallelSequentialScan access structure with {@link MemoryStorageBucket} as the storage class.
     *
     * @param parallelization the number of parallel buckets to create
     * @throws IllegalArgumentException if <ul><li>the provided bucketClass is not a part of LocalBucket hierarchy</li>
     *                                  <li>the bucketClass does not have a proper constructor (String,long,long)</li>
     *                                  <li>the correct constructor of bucketClass is not accessible</li>
     *                                  <li>the constructor of bucketClass has failed</li></ul>
     */
    @AlgorithmConstructor(description = "Parallel SequantialScan Access Structure", arguments = {"parallelization"})
    public ParallelSequentialScan(int parallelization) throws IllegalArgumentException {
        this(parallelization, MemoryStorageBucket.class);
    }

    protected ExecutorService initThreadPool() {
        return Executors.newFixedThreadPool(buckets.size());
    }


    @Override
    public void finalize() throws Throwable {
        for (LocalBucket localBucket : buckets)
            localBucket.finalize();
        super.finalize();
    }

    @Override
    public void destroy() throws Throwable {
        for (LocalBucket localBucket : buckets)
            localBucket.finalize();
        // Do not execute super.destroy(), since algorithm needs to differentiate between finalizing and destroying
    }

    @Override
    protected void registerProcessors() {
        super.registerProcessors();
        this.handler.register(ProcessorConfiguration.create(ObjectCountOperation.class, ObjectCountAnswer.class)
                .forTypeStrings(ObjectCountOperation.TYPE_STRING)
                .withProcessor(ObjectCountProcessor::new)
                .executeSequential().build());

        this.handler.register(ProcessorConfiguration.create(InsertOperation.class, InsertAnswer.class)
                .forTypeStrings(InsertOperation.TYPE_STRING)
                .withProcessor(InsertObjectsProcessor::new)
                .executeSequential().build());

        this.handler.register(ProcessorConfiguration.create(DeleteOperation.class, DeleteAnswer.class)
                .forTypeStrings(DeleteOperation.TYPE_STRING)
                .withProcessor(DeleteObjectsProcessor::new)
                .executeSequential().build());

        // TODO: make it parallel
        this.handler.register(ProcessorConfiguration.create(GetObjectsOperation.class, GetObjectsAnswer.class)
                .forTypeStrings(GetObjectsOperation.TYPE_STRING)
                .withProcessor(GetObjectProcessor::new)
                .executeSequential().build());

        // all distance-based single-query search operations
        // TODO: make it parallel
        this.handler.register(ProcessorConfiguration.create(QueryObjectOperation.class, RankedAnswer.class)
                .forTypeStrings(KNNOperation.TYPE_STRING, RangeOperation.TYPE_STRING, KNNRangeOperation.TYPE_STRING)
                .withProcessor(operation -> new BucketQueryOperationProcessor(new QueryObjectOperation.Helper(operation, defaultDistanceFunc), buckets))
                .executeSequential().build());
    }

    /**
     * Simple processor to return the number of objects in this algorithm.
     */
    protected class ObjectCountProcessor extends QueueOperationProcessor<ObjectCountOperation, ObjectCountAnswer, LocalBucket> {

        final ObjectCountOperation.Helper helper;

        public ObjectCountProcessor(ObjectCountOperation operation) {
            super(operation, buckets);
            helper = new ObjectCountOperation.Helper();
        }

        @Override
        public ObjectCountAnswer finish() {
            return helper.getAnswer();
        }

        @Override
        protected void processItem(LocalBucket bucket) throws AlgorithmMethodException {
            try {
                helper.evaluate(bucket);
            } catch (IOException e) {
                // TODO - this shouldn't be consumed
                e.printStackTrace();
            }
        }
    }

    /**
     * Processor of the insert operation.
     */
    private class InsertObjectsProcessor extends OneStepProcessor<InsertOperation, InsertAnswer> {

        final InsertOperation.Helper helper;

        InsertObjectsProcessor(InsertOperation operation) {
            super(operation);
            helper = new InsertOperation.Helper(operation);
        }

        @Override
        public InsertAnswer finish() {
            return helper.getAnswer();
        }

        @Override
        protected void process() throws Exception {
            for (DataObject object : operation.getObjects()) {
                helper.insertObjects(buckets.get(insertBucket), new DataObject[]{object});
                insertBucket = (insertBucket + 1) % buckets.size();
            }
        }
    }

    /**
     * Processor of the delete operation.
     */
    private class DeleteObjectsProcessor extends OneStepProcessor<DeleteOperation, DeleteAnswer> {

        final DeleteOperation.Helper helper;

        DeleteObjectsProcessor(DeleteOperation operation) {
            super(operation);
            helper = new DeleteOperation.Helper(operation);
        }

        @Override
        public DeleteAnswer finish() {
            return helper.getAnswer();
        }

        @Override
        protected void process() throws Exception {
            List<DataObject> objectsToDelete = new ArrayList<>(Arrays.asList(operation.getObjects()));
            for (LocalBucket bucket : buckets) {
                if (objectsToDelete.isEmpty()) {
                    break;
                }
                final List<DataObject> deleted = helper.deleteObjects(bucket, objectsToDelete.toArray(new DataObject[objectsToDelete.size()]));
                removeProcessed(objectsToDelete, deleted);
            }
        }
    }


    private void removeProcessed(List<DataObject> objectsToProcess, List<DataObject> processed) {
        int i = 0;
        int j = 0;
        while (i < objectsToProcess.size() && j < processed.size()) {
            if (objectsToProcess.get(i) == processed.get(j)) {
                objectsToProcess.remove(i);
                j++;
            } else {
                i++;
            }
        }
    }

    /**
     * Processor of the get object (by ID or data) operation.
     */
    private class GetObjectProcessor extends OneStepProcessor<GetObjectsOperation, GetObjectsAnswer> {

        final GetObjectsOperation.Helper helper;

        GetObjectProcessor(GetObjectsOperation operation) {
            super(operation);
            helper = new GetObjectsOperation.Helper(operation);
        }

        @Override
        public GetObjectsAnswer finish() {
            return helper.getAnswer();
        }

        @Override
        protected void process() throws Exception {
            List<DataObject> objectsToSearchFor = new ArrayList<>(Arrays.asList(operation.getObjects()));
            for (LocalBucket bucket : buckets) {
                if (objectsToSearchFor.isEmpty()) {
                    break;
                }
                final List<DataObject> found = helper.searchForObjects(bucket, objectsToSearchFor.toArray(new DataObject[objectsToSearchFor.size()]));
                removeProcessed(objectsToSearchFor, found);
            }
        }
    }

    /**
     * Shows the information about this algorithm.
     *
     * @return the information about this algorithm
     */
    @Override
    public String toString() {
        StringBuffer rtv;
        String lineSeparator = System.getProperty("line.separator", "\n");

        rtv = new StringBuffer();
        rtv.append("Algorithm: ").append(getName()).append(lineSeparator);
        rtv.append("Bucket Class: ").append(buckets.get(0).getClass().getName()).append(lineSeparator);
        long occupation = 0;
        int objectCount = 0;
        for (LocalBucket bucket : buckets) {
            occupation += bucket.getOccupation();
            objectCount += bucket.getObjectCount();
        }
        rtv.append("Number of buckets (threads): ").append(buckets.size()).append(lineSeparator);
        rtv.append("Bucket Occupation: ").append(occupation).append(" bytes").append(lineSeparator);
        rtv.append("Bucket Occupation: ").append(objectCount).append(" objects").append(lineSeparator);

        return rtv.toString();
    }

}
