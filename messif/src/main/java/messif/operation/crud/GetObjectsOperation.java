/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.crud;

import messif.bucket.LocalBucket;
import messif.data.DataObject;
import messif.operation.crud.answer.CRUDAnswer;
import messif.operation.crud.answer.GetObjectsAnswer;
import messif.utility.ProcessingStatus;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Encapsulation of classic "GET" operation to retrieve (a list of) objects for a given list of IDs (or other fields).
 */
public interface GetObjectsOperation extends CRUDOperation {

    String TYPE_STRING = "get";

    @Override
    default String getInnerType() {
        return TYPE_STRING;
    }

    /**
     * Helper method for the "Get" operation.
     */
    class Helper extends CRUDOperation.Helper<GetObjectsOperation, GetObjectsAnswer> {

        /**
         * Creates this processor given the operation to be processed.
         *
         * @param operation operation to be executed
         */
        public Helper(GetObjectsOperation operation) {
            super(operation, GetObjectsAnswer.class, CRUDAnswer.SOME_OBJECT_NOT_FOUND,
                    new ProcessingStatus[] { CRUDAnswer.OBJECT_FOUND },
                    new ProcessingStatus[] { CRUDAnswer.OBJECT_NOT_FOUND, CRUDAnswer.SOME_OBJECT_NOT_FOUND });
        }


        /******************
         * Helper methods to be used by other processors
         ******************/

        public List<DataObject> searchForObjects(LocalBucket bucket) {
            return searchForObjects(bucket, getOperation().getObjects());
        }

        public List<DataObject> searchForObjects(LocalBucket bucket, DataObject[] objects) {
            return super.processObjects(bucket, objects);
        }

        @Override
        protected ProcessResult processObject(LocalBucket bucket, DataObject dataObject) {
            try {
                return new ProcessResult(bucket.getObject(dataObject.getID()), CRUDAnswer.OBJECT_FOUND);
            } catch (NoSuchElementException e) {
                return new ProcessResult(dataObject, CRUDAnswer.OBJECT_NOT_FOUND);
            }
        }

    }
}
