/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.indexing.pivotselection;

import messif.bucket.BucketFilterAfterAdd;
import messif.bucket.BucketFilterAfterRemove;
import messif.bucket.LocalBucket;
import messif.distance.DistanceFunc;
import messif.data.DataObject;
import messif.data.util.DataObjectIterator;
import messif.data.util.DataObjectList;
import messif.utility.ListUtils;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Selects pivots as outliers from the sample set.
 *
 * The procedure is as follows:
 * - the first pivot is the object farthest from the other objects.
 * - the second pivots is the farthest object from the first pivot.
 * - the third pivots is the object farthest from the previous two pivots (having sum of distances to previous pivots maximal).
 * - etc...
 *
 * Based on:
 * L. Mico, J. Oncina, and E. Vidal.
 *     A new version of the nearest-neighbor approximating and eliminating search (AESA)
 *     with linear preprocessing-time and memory requirements.
 *     Pattern Recognition Letters, 15:9{17, 1994.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class OutlierPivotChooser extends AbstractPivotChooser implements Serializable, BucketFilterAfterAdd, BucketFilterAfterRemove {
    /** Class version id for serialization */
    private static final long serialVersionUID = 1L;

//    /** Size of the candidate set of pivots from which the best pivot is picked. */
//    public static int SAMPLE_PIVOT_SIZE = 100;

//    /** List of initial pivots */
//    protected DataObjectList<LocalAbstractObject> initialPivots = null;


    // *************** CONSTRUCTORS **********************

    /**
     * Creates a new instance of OutlierPivotChooser.
     */
    public OutlierPivotChooser(DistanceFunc<DataObject> distanceFunc) {
        super(distanceFunc);
    }

//    /**
//     * Creates a new instance of OutlierPivotChooser.
//     * @param initialPivots the list of initial (already selected) pivots
//     */
//    public OutlierPivotChooser(DataObjectList<LocalAbstractObject> initialPivots) {
//        this.initialPivots = initialPivots;
//    }

    // *************** PIVOT SELECTION IMPLEMENTATION **********************

    @Override
    protected void selectPivot(int count, DataObjectIterator sampleSetIterator) {
        // Store all passed objects temporarily
        DataObjectList objectList = new DataObjectList(sampleSetIterator);

//        List<LocalAbstractObject> pivots = new ArrayList<LocalAbstractObject>(count);
        // Sum of distances to previously selected pivots
        float pivotDists[] = new float[objectList.size()-1];
        Arrays.fill(pivotDists, 0f);         // Set accumulated distances to zero

//        // initially select "count" pivots at random - or use (partly) preselected pivots
//        if (initialPivots != null) {
//            // Initialize the accumulator of distances
//            for (DataObject p : initialPivots) {
//
//                if (count > pivots.size()) {
//                    pivots.add(preselPivot);
//                    System.out.println("adding preselected pivot: "+preselPivot.getID());
//                }
//            }
//        }

        selectFirstPivot(objectList);
        for (int p = 1; p < count; p++) {
            // Selects a new pivot and updates the sums of distances to previous pivots
            selectNextPivot(objectList, pivotDists);
        }
    }

    private void selectFirstPivot(DataObjectList objectList) {
        // First pivot
        DataObject pivot = null;
        float pivotDist = -1f;
        int pivotIndex = -1;

        // Take one object at random
        DataObject rand = ListUtils.randomObject(objectList);

        // The first pivot is the farthest object from rand
        int i = 0;
        for (DataObject obj : objectList) {
            float d = distanceFunc.getDistance(rand, obj);
            if (d > pivotDist) {
                pivot = obj;
                pivotDist = d;
                pivotIndex = i;
            }
            i++;
        }
        preselectedPivots.add(pivot);
        // Remove the pivot from the list of objects
        objectList.remove(pivotIndex);
    }


    private void selectNextPivot(DataObjectList objectList, float[] pivotDists) {
        // Get the last pivot
        DataObject lastPivot = preselectedPivots.get(preselectedPivots.size() - 1);

        // New pivot
        DataObject pivot = null;
        float dist = -1f;
        int pivotIndex = -1;

        // Compute distances to all objects and accumulate them in the array of distances
        int i = 0;
        for (DataObject obj : objectList) {
            pivotDists[i] += distanceFunc.getDistance(lastPivot, obj);
            if (pivotDists[i] > dist) {
                pivot = obj;
                dist = pivotDists[i];
                pivotIndex = i;
            }
            i++;
        }

        preselectedPivots.add(pivot);
        // Remove the pivot from the list of objects
        objectList.remove(pivotIndex);
        System.arraycopy(pivotDists, pivotIndex+1, pivotDists, pivotIndex, pivotDists.length-pivotIndex-1);
    }

    // *************** SUPPORT FOR ON-FLY PIVOT SELECTION **********************

    @Override
    public void filterAfterAdd(DataObject object, LocalBucket bucket) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void filterAfterRemove(DataObject object, LocalBucket bucket) {
        throw new UnsupportedOperationException("Not supported yet.");
    }


}
