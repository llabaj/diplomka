/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.net;

import junit.framework.TestCase;
import messif.data.DataObject;
import messif.data.processing.HttpRecordsProcessor;
import messif.record.RecordImpl;
import messif.utility.json.JSONWriter;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class HttpClientTest extends TestCase {

    public HttpClientTest(String testName) {
        super(testName);
    }

    public static final String urlStr = "http://localhost:4000/v2/vgg";

    protected HttpJSONClient httpClient;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        URL[] urls = new URL [] {new URL(urlStr)};
        this.httpClient = new HttpJSONClient(urls);
    }

    public void testDummy() {
        assertTrue(true);
    }

    public void ttestText() throws IOException {
        String request = "{\"record\": {\"_id\":\"1\", \"_file\":\"./mini.jpg\"}}";
        final String response = httpClient.sendRequest(request);
        System.out.println("response: " + response);
    }

    public void ttestJSON() throws IOException {
        DataObject obj1 = new RecordImpl(new String [] {"_id", "_file"}, new Object[] {"1", "./mini.jpg"});
        DataObject obj2 = new RecordImpl(new String [] {"_id", "_url"}, new Object[] {"2", "http://www.ikea.com/PIAimages/0212627_PE366708_S5.JPG"});
        final Map<String, Object[]> jsonObject = Collections.singletonMap("records", new Object[]{obj1, obj2});

        final Object response = httpClient.sendJSON(jsonObject);
        System.out.println(JSONWriter.writeToJSON(response, false));
    }

    public void ttestRecordProcessor() throws IOException {
        DataObject obj1 = new RecordImpl(new String [] {"_id", "_file"}, new Object[] {"1", "./mini.jpg"});
        DataObject obj2 = new RecordImpl(new String [] {"_id", "_url"}, new Object[] {"2", "http://www.ikea.com/PIAimages/0212627_PE366708_S5.JPG"});
        final List<DataObject> dataObjects = Arrays.asList(new DataObject [] {obj1, obj2});

        final HttpRecordsProcessor httpRecordsProcessor = new HttpRecordsProcessor(new URL[]{new URL(urlStr)});
        assertTrue(dataObjects.stream().allMatch(httpRecordsProcessor));
        final List<DataObject> results = httpRecordsProcessor.process(dataObjects);
        System.out.println(JSONWriter.writeToJSON(results.toArray(), false));
    }

}
