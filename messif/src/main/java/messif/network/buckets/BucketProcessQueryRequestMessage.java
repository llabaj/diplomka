/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.network.buckets;

import messif.bucket.BucketDispatcher;
import messif.bucket.BucketStorageException;
import messif.distance.DistanceFunc;
import messif.data.DataObject;
import messif.operation.search.RankingOperation;
import messif.operation.answer.RankedAnswer;

/**
 * Message requesting to process a query on a remote bucket.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @see NetworkBucketDispatcher
 */
public class BucketProcessQueryRequestMessage extends BucketRequestMessage<BucketProcessQueryReplyMessage> {
    /** Class serial id for serialization */
    private static final long serialVersionUID = 1L;

    //****************** Attributes ******************//

    /** Query operation to process on a remote bucket */
    private final RankingOperation query;

    /** Distance function to be used for query evaluation. */
    private final DistanceFunc<DataObject> defaultDistanceFunc;

    //****************** Constructors ******************//

    /**
     * Creates a new instance of BucketProcessQueryRequestMessage.
     * @param bucketID the ID of a remote bucket on which to process the request
     * @param query the query operation to process on a remote bucket
     * @param defaultDistanceFunc
     */
    public BucketProcessQueryRequestMessage(int bucketID, RankingOperation query, DistanceFunc<DataObject> defaultDistanceFunc) {
        super(bucketID);
        this.query = query;
        this.defaultDistanceFunc = defaultDistanceFunc;
    }


    //****************** Executing the request ******************//

    @Override
    public BucketProcessQueryReplyMessage execute(BucketDispatcher bucketDispatcher) throws RuntimeException, BucketStorageException {
        final RankedAnswer answer = bucketDispatcher.getBucket(bucketID).processQuery(query, defaultDistanceFunc);
        return new BucketProcessQueryReplyMessage(this, answer, answer.getAnswerCount());
    }

    @Override
    public Class<BucketProcessQueryReplyMessage> replyMessageClass() {
        return BucketProcessQueryReplyMessage.class;
    }

}
