/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.info;

import messif.bucket.BucketDispatcher;
import messif.bucket.LocalBucket;
import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.helpers.AnswerProvider;
import messif.operation.helpers.ErrorCodeHelper;
import messif.operation.helpers.SimpleErrorCodeMerger;
import messif.operation.info.answer.ObjectCountAnswer;

import java.io.IOException;


/**
 * Operation for retrieving the number of objects stored in indexing structure.
 *
 */
public interface ObjectCountOperation extends AbstractOperation {

    String TYPE_STRING = "get_object_count";

    @Override
    default String getInnerType() {
        return TYPE_STRING;
    }


    /**
     * A helper class for processing {@link ObjectCountOperation}s.
     */
    class Helper implements AnswerProvider<ObjectCountAnswer> {
        /**
         * The total count of objects in the algorithm, which is a result of this operation.
         */
        private volatile long objectCount;

        /**
         * Error code manager.
         * @return
         */
        protected final ErrorCodeHelper errorCodeHelper;

        public Helper() {
            errorCodeHelper = new SimpleErrorCodeMerger<ObjectCountAnswer>(ObjectCountAnswer.class);
        }

        @Override
        public ObjectCountAnswer getAnswer() {
            return ObjectCountAnswer.createSuccessful(objectCount);
        }


        /******************  Helper methods to be used by other processors   ******************/

        /**
         * Add the specified count to the answer of this operation.
         *
         * @param objectCount the count to add
         */
        public void addToAnswer(int objectCount) {
            this.objectCount += objectCount;
        }


        /**
         * Evaluate this search on a given bucket dispatcher.
         * Object counts stored in all buckets maintained by the provided dispatcher is added to this operation's answer.
         *
         * @param dispatcher the bucket dispatcher to update answer from
         * @return number of objects added to this operation, i.e. the actual dispatcher's object count
         */
        public int evaluate(BucketDispatcher dispatcher) throws IOException {
            int count = dispatcher.getObjectCount();
            objectCount += count;
            return count;
        }

        /**
         * Evaluate this search on a given bucket.
         * Object count stored in this bucket is added to this operation's answer.
         *
         * @param bucket the bucket to update answer from
         * @return number of objects added to this operation, i.e. the actual bucket's object count
         */
        public int evaluate(LocalBucket bucket) throws IOException {
            int count = bucket.getObjectCount();
            objectCount += count;
            return count;
        }

        @Override
        public void updateAnswer(AbstractAnswer answer) {
            errorCodeHelper.updateAnswer(answer);
            if (answer instanceof ObjectCountAnswer) {
                addToAnswer(((ObjectCountAnswer) answer).getAnswerCount());
            }
        }
    }
}
