/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.proxy;


import messif.record.ModifiableRecord;
import messif.record.Record;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.Method;

/**
 * Class wrapping individual proxy method. It allows to execute particular method and verify method requirement on underlying object.
 * <p>
 * Each proxy method has only one instance (like {@link Method} object from java).
 * <p>
 * These methods has to be annotated with @Get, @Set or @Remove interface.
 * <p>
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
class RecordMethodWrapper extends IndirectMethodWrapper {

    @Nonnull
    private final RecordMethodCaller caller;
    @Nonnull
    private final String fieldName;
    @Nonnull
    private final MethodVerifier methodVerifier;

    RecordMethodWrapper(@Nonnull Method method, @Nonnull RecordMethodCaller caller, @Nonnull String fieldName, @Nonnull MethodVerifier methodVerifier) {
        super(method);
        this.caller = caller;
        this.fieldName = fieldName;
        this.methodVerifier = methodVerifier;
    }

    @Nullable
    @Override
    public Object execute(@Nonnull ModifiableRecord record, @Nonnull Object proxy, @Nullable Object[] args) throws Throwable {
        return caller.execute(this, record, args);
    }

    void verify(@Nonnull Record record) {
        methodVerifier.verify(this, record);
    }

    static class MethodVerifier {
        @Nonnull
        private final Class expectedClass;
        private final boolean requiresValue;
        private final boolean modifiesInput;

        MethodVerifier(@Nonnull Class expectedClass, boolean requiresValue, boolean modifiesInput) {
            this.expectedClass = expectedClass;
            this.requiresValue = requiresValue;
            this.modifiesInput = modifiesInput;
        }

        void verify(@Nonnull RecordMethodWrapper wrapper, @Nonnull Record record) {
            if (modifiesInput && !(record instanceof ModifiableRecord))
                throw new IllegalArgumentException("Trying to instantiate modification method on unmodifiable Record " + wrapper.method.getName());

            if (requiresValue)
                record.getRequiredField(wrapper.fieldName, expectedClass);
            else
                record.getField(wrapper.fieldName, expectedClass);
        }
    }

    interface RecordMethodCaller {
        Object execute(@Nonnull RecordMethodWrapper wrapper, @Nonnull ModifiableRecord record, @Nullable Object[] args);
    }

    static Object getField(@Nonnull RecordMethodWrapper wrapper, @Nonnull Record record, @Nullable Object[] args) {
        return record.getField(wrapper.fieldName, wrapper.method.getReturnType());
    }

    static Object getDefaultField(@Nonnull RecordMethodWrapper wrapper, @Nonnull Record record, @Nullable Object[] args) {
        return record.getField(wrapper.fieldName, wrapper.method.getReturnType(), args[0]);
    }

    static Object getRequiredField(@Nonnull RecordMethodWrapper wrapper, @Nonnull Record record, @Nullable Object[] args) {
        return record.getRequiredField(wrapper.fieldName, wrapper.method.getReturnType());
    }

    static Object setField(@Nonnull RecordMethodWrapper wrapper, @Nonnull ModifiableRecord record, @Nullable Object[] args) {
        return record.setField(wrapper.fieldName, args[0]);
    }

    static Object removeField(@Nonnull RecordMethodWrapper wrapper, @Nonnull ModifiableRecord record, @Nullable Object[] args) {
        return record.removeField(wrapper.fieldName);
    }
}
