/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.serialization;

import messif.serialization.io.BinaryInput;
import messif.serialization.io.BinaryOutput;
import java.io.IOException;

/**
 * This is a serializator for a single special class which is realized via the {@link ClassReaderWriter}. It can
 * store and restore only one specified class or the standard Java-serialized objects.
 *
 * @param <T> the class of objects created by this serializator during deserialization
 * @see MultiClassSerializator
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class SingleClassSerializator<T> extends ObjectSerializator {

    /**
     * class serial id for serialization
     */
    private static final long serialVersionUID = 1L;

    protected final ClassReaderWriter<T> objectReaderWriter;

    /** A byte  that identifies this objects stored in a binary file; The byte value MUST BE GREATER than 50. */
    protected final byte classTypeByte;

    //************************ Constructor ************************//
    /**
     * Create a new instance of BinarySerializator.
     *
     * @param objectSerializator
     * @param classTypeByte
     */
    public SingleClassSerializator(ClassReaderWriter<T> objectSerializator, byte classTypeByte) {
        this.objectReaderWriter = objectSerializator;
        this.classTypeByte = classTypeByte;
    }

    //************************ Serializator methods ************************//
    @Override
    protected byte getObjectTypeByte(Object object) {
        if ((object != null) && (object.getClass() == objectReaderWriter.getReadWriteClass())) {
            return classTypeByte;
        }
        return super.getObjectTypeByte(object);
    }

    @Override
    protected int write(BinaryOutput output, Object object, byte typeByte) throws BinarySerializationException, IOException {
        if (typeByte == classTypeByte) {
            return objectReaderWriter.writeBinary((T) object, output, this);
        }
        return super.write(output, object, typeByte);
    }

    @Override
    protected Object readObject(BinaryInput input, byte typeByte) throws IOException, BinarySerializationException {
        if (typeByte == classTypeByte) {
            return objectReaderWriter.readBinary(input, this);
        }
        return super.readObject(input, typeByte);
    }

}
