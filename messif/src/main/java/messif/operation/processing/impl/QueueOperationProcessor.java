/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.processing.impl;

import messif.algorithm.AlgorithmMethodException;
import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.processing.AsyncOperationProcessor;
import messif.operation.processing.OperationProcessor;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Callable;

/**
 * Basic implementation of {@link messif.operation.processing.OperationProcessor} that processes any {@link AbstractOperation}
 * on a set of processing items (e.g. buckets).
 * 
 * <h4>Parallelism</h4>
 * The implementation is thread-safe, if operation cloning is enabled. Otherwise,
 * the operation answer needs to be synchronized.
 * 
 * @param <TRequest> the type of the operation that are processed by this operation processor
 * @param <TAnswer> class of answer returned by this processor
 * @param <TItem> the type of processing items used by this navigation processor
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public abstract class QueueOperationProcessor<TRequest extends AbstractOperation, TAnswer extends AbstractAnswer, TItem>
        extends SimpleOperationProcessor<TRequest, TAnswer> implements AsyncOperationProcessor<TRequest, TAnswer> {

    /** Internal queue of items used for processing */
    protected final Queue<TItem> processingItems;
    /** Number of items processed so far */
    //private volatile int processed;
    /** Flag whether the queue is closed, if <tt>false</tt>, i.e. getting a next element from the queue blocks and waits for the queue to fill */
    private boolean queueClosed;

    /** Flag whether the processor was initialized. Calling {@link #initProcessor()} is the first step of this processor. */
    private volatile boolean initialized = false;

    /**
     * Create a new navigation processor with a given queue instance.
     * The processor is {@link #isQueueClosed() closed} and contains only the specified processing items.
     * No additional processing items can be added.
     * @param operation the operation to process
     * @param processingItems the processing items queue
     * @param queueClosed the flag whether the given queue is closed, if <tt>false</tt>, i.e. getting a next element from the queue blocks and waits for the queue to fill
     */
    protected QueueOperationProcessor(TRequest operation, Queue<TItem> processingItems, boolean queueClosed) {
        super(operation);
        this.processingItems = processingItems;
        this.queueClosed = queueClosed;
    }

    /**
     * Create a new navigation processor.
     * The processor is {@link #isQueueClosed() closed} and contains only the specified processing items.
     * No additional processing items can be added.
     * @param operation the operation to process
     * @param processingItems the processing items for the operation
     */
    public QueueOperationProcessor(TRequest operation, Collection<? extends TItem> processingItems) {
        this(operation, new LinkedList<>(processingItems), true);
    }

    /**
     * Create a new navigation processor.
     * The processor does not contain any processing items and the processing
     * will block in method {@link #processStep()}. Additional processing items
     * must be added via {@link #addProcessingItem} methods and then {@link #finish()} closed}
     * in order to be able to finish the processing.
     *
     * @param operation the operation to process
     */
    public QueueOperationProcessor(TRequest operation) {
        this(operation, new LinkedList<>(), false);
    }


    // region ********************            Queue handling methods        ******************************** //

    /**
     * This method is always called as the first step of this processor. It is to be overriden by the implementations.
     */
    protected void initProcessor() throws AlgorithmMethodException {

    }

    /**
     * Adds a collection of processing items to this processor.
     * Note that processing items can be added only if this processor is not {@link #isQueueClosed() closed}.
     * 
     * @param itemsToAdd the collection of processing items to add
     * @throws IllegalStateException if this processor is already {@link #isQueueClosed() closed}.
     */
    public synchronized final void addProcessingItems(Collection<? extends TItem> itemsToAdd) throws IllegalStateException {
        if (queueClosed)
            throw new IllegalStateException();
        processingItems.addAll(itemsToAdd);
        //notify();
    }

    /**
     * Adds a processing item to this processor.
     * Note that processing item can be added only if this processor is not {@link #isQueueClosed() closed}.
     * 
     * @param processingItem the processing item to add
     * @return returns the added processing item
     * @throws IllegalStateException if this processor is already {@link #isQueueClosed() closed}.
     */
    public synchronized final TItem addProcessingItem(TItem processingItem) throws IllegalStateException {
        if (queueClosed)
            throw new IllegalStateException();
        processingItems.add(processingItem);
        //notify();
        return processingItem;
    }

    /**
     * Closes this processor queue.
     * That means that no additional processing items can be added and the {@link #processStep()}
     * method will no longer block and wait for additional processing items.
     * 
     * @throws IllegalStateException if this processor is already {@link #isQueueClosed() closed}.
     */
    public synchronized void queueClose() throws IllegalStateException {
        if (queueClosed)
            throw new IllegalStateException();
        queueClosed = true;
        notifyAll();
    }

    /**
     * Returns whether additional processing items can be added to this processor (<tt>false</tt>)
     * or this processor is closed (<tt>true</tt>).
     * @return <tt>true</tt> if this processor is closed and no additional processing items can be added for processing
     */
    public boolean isQueueClosed() {
        return queueClosed;
    }

    // endregion

    /**
     * This methods ends the processing of this processor. It should be called by the implementing
     *  {@link OperationProcessor#finish()} method.
     * @throws IllegalStateException
     */
    public synchronized void abort() throws IllegalStateException {
        queueClosed = true;
        processingItems.clear();
        notifyAll();
    }

    /**
     * Returns the next processing item in the queue.
     * @return the next processing item in the queue or <tt>null</tt> if the queue is empty
     * @throws InterruptedException if the waiting for the next item in the queue has been interrupted
     */
    protected synchronized TItem getNextProcessingItem(boolean isAsynchronous) throws InterruptedException {
        return processingItems.poll();
    }

    @Override
    @SuppressWarnings("unchecked")
    public final boolean processStep() throws InterruptedException, AlgorithmMethodException, CloneNotSupportedException {
        if (! initialized) {
            initProcessor();
            initialized = true;
        }
        TItem processingItem = getNextProcessingItem(false);
        if (processingItem == null)
            return false;
        processItem(processingItem);
        //processed++;
        return true;
    }

    @Override
    public final Callable<Void> processStepAsynchronously() throws InterruptedException, AlgorithmMethodException {
        if (! initialized) {
            synchronized (this) {
                if (! initialized) {
                    initProcessor();
                    initialized = true;
                }
            }
        }
        final TItem processingItem = getNextProcessingItem(true);
        if (processingItem == null)
            return null;
        return () -> {
            processItem(processingItem);
            return null;
        };
    }

    /**
     * Processes the encapsulated operation using the given processing item.
     *
     * @param processingItem the processing item using which to process the operation
     * @return the operation after processing (can be the same instance as the given operation)
     * @throws AlgorithmMethodException if an error occurred during the evaluation of the processing step
     */
    protected abstract void processItem(TItem processingItem) throws AlgorithmMethodException;

}
