/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.proxy;

import messif.record.ModifiableRecord;
import messif.record.Record;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Basic interface for the method wrapping. This wrapper allows to execute operation on proxy.
 * <p>
 * Currently we support four different MethodWrappers
 * {@link RecordMethodWrapper} - Used for methods that allow access to underlying {@link Record} data via given interface methods.
 * These methods has to be annotated with @Get, @Set or @Remove interface.
 * <p>
 * {@link DefaultMethodWrapper} - Wrapper which allows to execute default methods implemented in converted interface.
 * <p>
 * {@link ForwardMethodWrapper} - Forwards the method execution to the underlying {@link Record}. It is used to support methods like 'equals', 'hashCode' and 'toString'
 * <p>
 * {@link ExposeMethodWrapper} - Ad-hoc method wrapper that allows the converted interface to return underlying {@link Record}. Such method has to be annotated with an @Expose annotation.
 * <p>
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
interface MethodWrapper {

    /**
     * Executes wrapped method on proxied interface using underlying{@link Record}.
     *
     * @param record underlying record
     * @param proxy  proxy instance which was used to call the method
     * @param args   arguments of the method.
     * @return generic return object
     * @throws Throwable
     */
    @Nullable
    Object execute(@Nonnull ModifiableRecord record, @Nonnull Object proxy, @Nullable Object[] args) throws Throwable;
}
