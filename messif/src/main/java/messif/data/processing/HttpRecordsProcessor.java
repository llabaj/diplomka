/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.processing;

import messif.data.DataObject;
import messif.record.Record;
import messif.record.processing.RecordProcessingException;
import messif.record.processing.RecordProcessor;
import messif.utility.json.JSONWriter;
import messif.utility.net.HttpJSONClient;
import messif.utility.net.HttpStatusException;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

/**
 * Connects to a given socket(s), encapsulates the record as a JSON request, sends it to the socket, and assumes
 * that the socket service returns a modified record (encapsulated within a JSON response).
 */
public class HttpRecordsProcessor implements RecordProcessor.Multi<DataObject> {

    public static final String RECORDS_FIELD = "records";

    protected final Predicate<DataObject> preCondition;
    private final String preconditionDesc;
    private final String[] modifiedFields;

    protected final HttpJSONClient client;

    /**
     * Creates a new processor by opening connections to given URLs (round robin).
     * The precondition is empty, we don't know what the service is about to do with the records.
     * @param urls a list of URLs where HTTP services run
     */
    public HttpRecordsProcessor(final URL [] urls) {
        this(urls, object -> true, "no condition", new String [0]);
    }

    /**
     * Creates a new processor by opening connections to given URLs (round robin).
     * @param urls a list of URLs where HTTP services run
     * @param preCondition some condition to check and skip records that do not follow this condition
     * @param preconditionDesc description of the condition
     * @param modifiedFields a list of fields that we expect to be modified by the HTTP service
     */
    public HttpRecordsProcessor(final URL[] urls, Predicate<DataObject> preCondition, String preconditionDesc, String [] modifiedFields) {
        this.client = new HttpJSONClient(urls);
        this.preCondition = preCondition;
        this.preconditionDesc = preconditionDesc;
        this.modifiedFields = modifiedFields;
    }

    /**
     * Takes given a list of {@link DataObject}s and encapsulates it in to {@link #RECORDS_FIELD} of a new JSON object.
     * This object is sent to the HTTP service.
     * @param records a list record to be processed and modified
     * @return list of records modified by the HTTP service (can be in a different order, we cannot influence this)
     * @throws RecordProcessingException
     */
    @Override
    public List<DataObject> process(List<DataObject> records) throws RecordProcessingException {
        Object request = Collections.singletonMap(RECORDS_FIELD, records.toArray(new DataObject[records.size()]));
        final Record response = callHttpService(records, request);
        if (! (response.containsField(RECORDS_FIELD))) {
            throw new RecordProcessingException("error processing record by HTTP processor: "
                    + JSONWriter.writeToJSON(records, false) + "; wrong returned string: " + JSONWriter.writeToJSON(response, false));
        }
        return Arrays.asList(response.getField(RECORDS_FIELD, DataObject[].class));
    }

    /**
     * Does the actual call of the REST service and properly handle the result or error.
     * @param dataRecord the data record or records (only for error handling)
     * @param request a create JSON-like request to be sent to the HTTP service
     * @return a JSON-like response (record)
     * @throws RecordProcessingException
     */
    protected Record callHttpService(Object dataRecord, Object request) throws RecordProcessingException {
        try {
            final Object response = client.sendJSON(request);
            if (! (response instanceof Record)) {
                throw new RecordProcessingException("error processing record by HTTP processor: "
                        + JSONWriter.writeToJSON(dataRecord, false) + "; wrong returned string: " + JSONWriter.writeToJSON(response, false));
            }
            return (Record) response;
        } catch (IOException e) {
            throw new RecordProcessingException("error processing record by HTTP processor: " + JSONWriter.writeToJSON(dataRecord, false), e);
        } catch (HttpStatusException e) {
            throw new RecordProcessingException("error processing record by HTTP processor: " + JSONWriter.writeToJSON(dataRecord, false), e, e.getHttpStatusCode());
        }
    }

    @Override
    public boolean preCondition(DataObject record) {
        return preCondition.test(record);
    }

    @Override
    public String preConditionDescription() {
        return this.preconditionDesc;
    }

    @Override
    public String[] modifiedFields() {
        return this.modifiedFields;
    }

    @Override
    public String toString() {
        return "record(s) JSON processor at " + client.toString();
    }


    // *************************    Static factory methods      ************************** //

    /**
     * Create a HTTP processor that checks NON-EXISTENCE of a given field and applies the HTTP processor on the
     * ones without this field to add it.
     * @param urls a list of URLs with the "extractors"
     * @param field a field to check and add
     * @return a new instance of the HTTP record processor
     */
    public static final HttpRecordsProcessor addMissingField(final URL [] urls, final String field) {
        return new HttpRecordsProcessor(urls, record -> ! record.containsField(field),
                "record already contains field '" + field + "'", new String [] {field});
    }

    /**
     * Create a HTTP processor that checks NON-EXISTENCE of a given field and applies the HTTP processor on the
     * ones without this field to add it.
     * @param urls a list of URLs with the "extractors"
     * @param field a field to check and add
     * @return a new instance of the HTTP record processor
     */
    public static final HttpRecordsProcessor addFieldCheckExistence(final URL [] urls, final String field, final String [] fieldsToCheckBefore) {
        return new HttpRecordsProcessor(urls,
                record -> ! record.containsField(field) && Arrays.stream(fieldsToCheckBefore).anyMatch(record::containsField),
                "\"record already contains field '\" + field + \"'\" or does not contain fields '" + Arrays.toString(fieldsToCheckBefore) + "'",
                new String [] {field});
    }

}
