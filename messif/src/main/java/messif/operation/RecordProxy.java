/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation;

import messif.record.ModifiableRecord;
import messif.utility.json.JSONException;
import messif.utility.json.JSONWritable;
import messif.utility.json.JSONWriter;
import messif.utility.proxy.annotations.Expose;
import messif.record.Record;

import java.io.IOException;

/**
 * Abstract predecessor of all operations, answers and other interfaces that are to be used with the proxy over
 *  a {@link ModifiableRecord}.
 */
public interface RecordProxy extends JSONWritable {
    /**
     * Access to the underlying {@link Record} with all the parameters.
     * @return return the underlying map with all parameters.
     */
    @Expose
    ModifiableRecord getParams();

    /**
     * The JSON representation of the operation is implemented by printing all the fields.
     */
    @Override
    default JSONWriter writeJSON(JSONWriter writer) throws JSONException, IOException {
        return getParams().writeJSON(writer);
    }

    @Override
    default JSONWriter writeJSON(JSONWriter writer, int level) throws JSONException, IOException {
        return getParams().writeJSON(writer, level);
    }
}
