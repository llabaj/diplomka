/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.answer;

import messif.data.DataObject;
import messif.operation.ReturnDataOperation;
import messif.operation.helpers.AnswerProvider;
import messif.operation.helpers.ErrorCodeHelper;
import messif.operation.helpers.SimpleErrorCodeMerger;
import messif.operation.info.answer.ObjectCountAnswer;
import messif.record.ModifiableRecord;
import messif.record.Record;
import messif.utility.ProcessingStatus;
import messif.utility.proxy.ProxyConverter;
import messif.utility.proxy.annotations.Get;

import java.util.*;

/**
 * An answer that contains a list of data objects; it also has a field with the number of objects in the answer. It can
 *  be used for all CRUD operations and other operations that return objects without any ranking from a query objects.
 *  There are also static factory methods that work with the {@link ReturnDataOperation#fieldsToReturn(String[])} field
 *  to return only request fields; it uses the {@link ReturnDataOperation#transformObject(DataObject, String[])} method.
 */
public interface ListingAnswer extends ObjectCountAnswer {

    /**
     * Name of a {@link Record} (JSON) field to contain the list of objects returned by the operation.
     *  This field is not mandatory.
     */
    String ANSWER_OBJECTS_FIELD = "answer_records";

    /**
     * This method provides a convenient access to the field with name {@link #ANSWER_OBJECTS_FIELD}.
     * @return an array of {@link DataObject}s returned by the operation
     */
    @Get(field = ANSWER_OBJECTS_FIELD)
    DataObject [] getAnswerObjects();


    // region *************************            Factory methods       ************************** //

    static ListingAnswer createFailed(ProcessingStatus status) {
        return create(status, ListingAnswer.class, 0);
    }

    static <TAnswer extends ListingAnswer> TAnswer create(ProcessingStatus status, Class<TAnswer> answerClass, int answerCount) {
        ModifiableRecord fieldMap = AbstractAnswer.createMap(status);
        fieldMap.setField(ANSWER_OBJ_COUNT_FIELD, answerCount);
        return ProxyConverter.convert(fieldMap, answerClass);
    }

    static  ListingAnswer create(Collection<DataObject> objects, String[] fieldsToReturn, ProcessingStatus status) {
        return ListingAnswer.create(objects, fieldsToReturn, status, ListingAnswer.class);
    }

    static  <TAnswer extends ListingAnswer> TAnswer create(Collection<DataObject> objects, String[] fieldsToReturn,
                                                           ProcessingStatus status, Class<TAnswer> answerClass) {
        ModifiableRecord fieldMap = AbstractAnswer.createMap(status);
        final DataObject[] transformedObjects = ReturnDataOperation.transformObjects(objects, fieldsToReturn);
        if (transformedObjects != null) {
            fieldMap.setField(ANSWER_OBJECTS_FIELD, transformedObjects);
        }
        fieldMap.setField(ANSWER_OBJ_COUNT_FIELD, objects.size());
        return ProxyConverter.convert(fieldMap, answerClass);
    }
    // endregion

    /**
     * This class helps to iteratively build the answer; it is used by operation helpers and operation processors.
     */
    class Builder<TAnswer extends ListingAnswer> implements AnswerProvider<TAnswer> {
        /**
         * The continuously created answer.
         */
        protected final Collection<DataObject> answerList;

        protected final ReturnDataOperation operation;

        private final Class<TAnswer> answerClass;

        public final ErrorCodeHelper errorCodeHelper;
        //private final boolean unique;

        protected final Set<String> ids;

        public Builder(ReturnDataOperation operation, Class<TAnswer> answerClass) {
            this(operation, answerClass, false);
        }

        public Builder(ReturnDataOperation operation, Class<TAnswer> answerClass, boolean unique) {
            this(operation, answerClass, new SimpleErrorCodeMerger<>(answerClass), unique);
        }

        public Builder(ReturnDataOperation operation, Class<TAnswer> answerClass, ErrorCodeHelper errorCodeHelper, boolean unique) {
            this.operation = operation;
            this.answerClass = answerClass;
            this.errorCodeHelper = errorCodeHelper;
            this.answerList = Collections.synchronizedCollection(new ArrayList<>());
            this.ids = unique ? new HashSet<>() : null;
        }

        // region ************************    Methods that add objects to the answer (and update the error code)

        public synchronized boolean add(DataObject dataObject) {
            if (ids == null || ! ids.contains(dataObject.getID())) {
                answerList.add(dataObject);
                if (ids != null)
                    ids.add(dataObject.getID());
                return true;
            }
            return false;
        }

        public void add(DataObject dataObject, ProcessingStatus status) {
            if (add(dataObject))
                errorCodeHelper.updateErrorCode(status);
        }

        public void addAll(Collection<DataObject> dataObjects) {
            addAll(dataObjects.iterator());
        }

        public void addAll(Collection<DataObject> dataObjects, ProcessingStatus status) {
            addAll(dataObjects);
            errorCodeHelper.updateErrorCode(status);
        }

        public void addAll(Iterator<DataObject> dataObjects) {
            while (dataObjects.hasNext())
                add(dataObjects.next());
        }

        public void addAll(Iterator<DataObject> dataObjects, ProcessingStatus status) {
            addAll(dataObjects);
            errorCodeHelper.updateErrorCode(status);
        }
        // endregion

        public int getAnswerSize() {
            return answerList.size();
        }

        public DataObject [] getProcessedObjsArray() {
            return answerList.toArray(new DataObject[answerList.size()]);
        }

        @Override
        public TAnswer getAnswer() {
            ProcessingStatus status = errorCodeHelper.getStatus();
            if (! status.isSet()) {
                status = (answerList == null || answerList.isEmpty()) ? AbstractAnswer.EMPTY_ANSWER : AbstractAnswer.OPERATION_OK;
            }
            if (operation.fieldsToReturn() == null) {
                return ListingAnswer.create(status, answerClass, getAnswerSize());
            }
            return ListingAnswer.create(answerList, operation.fieldsToReturn(), status, answerClass);
        }

        @Override
        public synchronized void updateAnswer(AbstractAnswer answer) {
            errorCodeHelper.updateAnswer(answer);
            if (answer instanceof ListingAnswer) {
                addAll(Arrays.asList(((ListingAnswer) answer).getAnswerObjects()));
            }
        }

    }
}
