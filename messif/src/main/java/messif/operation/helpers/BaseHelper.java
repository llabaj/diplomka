/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.helpers;

import messif.operation.AbstractOperation;

/**
 * A base class for many operation processing helpers.
 */
public abstract class BaseHelper<TRequest extends AbstractOperation> {

    /**
     * Operation to be processed.
     */
    protected final TRequest operation;

    protected BaseHelper(TRequest operation) {
        this.operation = operation;
    }

    public TRequest getOperation() {
        return operation;
    }

}
