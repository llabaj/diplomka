/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.text;

import messif.distance.impl.JaccardDistanceFunction;
import messif.utility.SortingIterator;

/**
 * Jaccard distance function for {@link WordsProvider} objects.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class WordsJaccardDistanceFunction extends JaccardDistanceFunction<String, WordsProvider> {

    /** Class id for serialization. */
    private static final long serialVersionUID = 1L;

    public WordsJaccardDistanceFunction() {
        super((s1, s2) -> s1.compareTo(s2));
    }

    @Override
    protected SortingIterator<String> getValuesIterator(WordsProvider obj) {
        return new SortingIterator<String>().addArray(obj.getWords());
    }
    @Override
    public Class<WordsProvider> getObjectClass() {
        return WordsProvider.class;
    }

    // TODO: there is a problem that WordProvider does not extend DataObject
//    /**
//     * Creates a post-ranking collection that uses {@link WordsProvider} instances
//     * and Jaccard coefficient to define the ranking.
//     * @param queryWords the query words provider against which to rank
//     * @param originalDistanceWeight the mixing weight of the original distance and the newly computed jaccard one
//     * @return a new empty post-ranking collection
//     */
//    public static RankedSortedCollection createRerankingCollection(WordsProvider queryWords, float originalDistanceWeight) {
//        return new RankedSortedDistFunctionCollection<WordsProvider>(new WordsJaccardDistanceFunction(), queryWords, originalDistanceWeight);
//    }
//
//    /**
//     * Changes the ranking collection of the given query operation to the jaccard post-ranking on {@link WordsProvider} instances.
//     * This can be typically used by {@link Application#operationProcessByMethod(java.io.PrintStream, java.lang.String...) operationProcessByMethod}
//     * in configuration files processed by the application.
//     * Please note that this is intended to be used after the operation has been executed,
//     * thus the objects stored in the operation answer should have the words present.
//     * @param operation the operation the collection of which to change
//     * @param originalDistanceWeight the mixing weight of the original distance and the newly computed jaccard one
//     * @return the changed operation
//     */
//    public static RankedAnswer.Builder setOperationCollection(RankedAnswer.Builder answerBuilder, WordsProvider queryObject, float originalDistanceWeight) {
//        answerBuilder.setAnswerCollection(createRerankingCollection(queryObject, originalDistanceWeight));
//        return answerBuilder;
//    }
}
