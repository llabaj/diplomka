/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.processing.impl;

import messif.algorithm.AlgorithmMethodException;
import messif.data.DataObject;
import messif.distance.DistanceFunc;
import messif.operation.answer.JoinQueryAnswer;
import messif.operation.params.CandidateIDSetQueue;
import messif.operation.search.JoinOperation;

import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 * A base for refinement of the similarity join candidate set and building the answer.
 */
public abstract class RefineJoinProcessorBase extends QueueOperationProcessor<JoinOperation, JoinQueryAnswer, List<String>> { // extends SimpleOperationProcessor<JoinOperation, JoinQueryAnswer> implements AsyncOperationProcessor<JoinOperation, JoinQueryAnswer> {

    /**
     * This is the same object as {@link QueueOperationProcessor#processingItems}.
     */
    protected BlockingQueue<List<String>> idQueue;

    /**
     * Helper that helps to build the similarity join answer.
     */
    protected final JoinOperation.Helper helper;

    /**
     * Creates this processor given a query operation and a distance to be used if the query does not contain
     * a distance itself.
     *
     * @param operation
     * @param defaultDistanceFunc
     */
    protected RefineJoinProcessorBase(JoinOperation operation, CandidateIDSetQueue candidateIDSetQueue, DistanceFunc<DataObject> defaultDistanceFunc) {
        super(operation, candidateIDSetQueue.getIDSetQueue());
        this.idQueue = candidateIDSetQueue.getIDSetQueue();
        helper = new JoinOperation.Helper(operation, defaultDistanceFunc);
    }

    /**
     * Returns the next processing item in the queue.
     * @return the next processing item in the queue or <tt>null</tt> if the queue is empty
     * @throws InterruptedException if the waiting for the next item in the queue has been interrupted
     */
    protected synchronized List<String> getNextProcessingItem(boolean isAsynchronous) throws InterruptedException {
        if (idQueue == null) {
            return null;
        }
        final List<String> nextItem = idQueue.take();
        if (CandidateIDSetQueue.END_OF_STREAM.equals(nextItem)) {
            idQueue = null;
            return null;
        }
        return nextItem;
    }

    @Override
    protected void processItem(List<String> processingItem) throws AlgorithmMethodException {
        helper.evaluate(getObjects(processingItem));
    }

    /** This is the main (only) method to be implemented by a non-abstract refine processor. */
    protected abstract List<DataObject> getObjects(List<String> ids);

    @Override
    public JoinQueryAnswer finish() {
        return helper.getAnswer();
    }
}
