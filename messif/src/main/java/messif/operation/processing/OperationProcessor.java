/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.processing;

import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;

import javax.annotation.Nullable;

/**
 * // TODO: fix documentation
 * Interface for processing a single type of operation.
 * The processor should be obtained via {@link Factory},
 * The processor is then used by {@link OperationHandler} to actually run the processing.
 * The instance of {@link messif.algorithm.NavigationProcessor} is first obtained on a given {@link NavigationDirectory}
 * via {@link NavigationDirectory#getNavigationProcessor(messif.operation.AbstractOperation) getNavigationProcessor}
 * method for a given {@link AbstractOperation operation} instance.
 * The operation then can be processed iteratively by calling {@link #processStep()}
 * until there are no more directory items available. This typically means that
 * the navigation directory provides candidates where the operation
 * should be processed which are either computed before the processing (i.e. the
 * list of buckets to visit) or dynamically when the next processing
 * is requested.
 *
 * @param <TRequest> the type of the operation that are processed by this navigator processor
 * @param <TAnswer>  the type of the answer returned by this processor
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface OperationProcessor<TRequest extends AbstractOperation, TAnswer extends AbstractAnswer> {

    /**
     * Returns the operation for which this processor was created.
     *
     * @return the operation for which this processor was created
     */
    TRequest getOperation();

    /**
     * Processes the encapsulated operation by the next processing step.
     * The returned value indicates if a processing step was executed or if
     * this processor is canFinish. Note that this method may block if necessary.
     *
     * @return <tt>true</tt> if the next processing step was taken or
     * <tt>false</tt> if no processing was done and this processor is canFinish
     * @throws Exception if an error occurred during the evaluation of the processing step
     */
    boolean processStep() throws Exception;

    /**
     * Method to be called after processing all the steps which returns the final answer.
     */
    @Nullable
    TAnswer finish();

    /**
     * The processors can implement a cleaning method that is explicitly called only in case
     *    an exception was thrown and the answer was not returned by calling {@link #finish()}.
     */
    default void clean() { };

    /**
     * Returns whether this processor did all steps and can finish.
     * Note that this method should return <tt>true</tt> if and only if the next execute
     * to {@link #processStep()} returns <tt>false</tt>.
     *
     * @return <tt>false</tt> if additional processing via {@link #processStep()} is possible or
     * <tt>true</tt> if this processor has canFinish
     */
    // TODO: remove this method completely
    //boolean canFinish();

    /**
     * TODO: TESTING
     * Require that the processor can update itself from a (partial) answer of the respective type.
     */
    //void updateFrom(AbstractAnswer answer);
}
