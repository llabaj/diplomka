/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.net;

/**
 * Exception that bears information about the HTTP status code to be returned.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class HttpStatusException extends RuntimeException implements HttpStatusCodeProvider {
    /** Serial version for {@link java.io.Serializable} */
    private static final long serialVersionUID = 2L;

    /** HTTP error code from {@link HttpStatusCodeProvider} */
    private int httpErrorCode = HttpStatusCodeProvider.STATUS_CODE_NOT_SET;

    /**
     * Constructs an instance of <code>ExtractorException</code> with the
     * specified detail message and cause.
     * @param msg the detail message.
     * @param cause the cause which is saved for later retrieval by the
     *         {@link #getCause()} method); a <tt>null</tt> value is
     *         permitted, and indicates that the cause is nonexistent or
     *         unknown
     */
    public HttpStatusException(String msg, Throwable cause, int httpErrorCode) {
        super(msg, cause);
        this.httpErrorCode = httpErrorCode;
    }

    /**
     * Constructs an instance of <code>ExtractorException</code> with the specified detail message
     *  and HTTP error code to be returned.
     * @param msg the detail message.
     * @param httpErrorCode HTTP error code to be returned, if this exception is raised
     */
    public HttpStatusException(String msg, int httpErrorCode) {
        super(msg);
        this.httpErrorCode = httpErrorCode;
    }

    /**
     * Constructs an instance of <code>ExtractorException</code> with the specified detail message
     *  and HTTP error code to be returned.
     * @param httpErrorCode HTTP error code to be returned, if this exception is raised
     */
    public HttpStatusException(int httpErrorCode) {
        this.httpErrorCode = httpErrorCode;
    }

    // ************************       Interface HttpStatusCodeProvider      ************************** //
    
    @Override
    public boolean isHttpStatusSet() {
        return httpErrorCode != HttpStatusCodeProvider.STATUS_CODE_NOT_SET;
    }    
    
    @Override
    public int getHttpStatusCode() {
        return httpErrorCode;
    }

    /**
     * Sets a new HTTP code to be later returned by this provider.
     * @param httpErrorCode a new HTTP code to be later returned by this provider.
     */
    public void setHttpErrorCode(int httpErrorCode) {
        this.httpErrorCode = httpErrorCode;
    }

}
