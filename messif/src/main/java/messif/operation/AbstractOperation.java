/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation;

import messif.data.DataObject;
import messif.utility.proxy.annotations.Get;

/**
 * Abstract predecessor of all operations. All operations (can) contain a type that is either set during
 *  creation (in field {@link #TYPE_FIELD}) or it's taken from the default method of respective operation
 *  interface {@link #getInnerType()}.
 *
 *  One can use {@link OperationBuilder#create(Class)} to build an operation.
 *
 *  The low-level operation usually have also static methods like {@link messif.operation.crud.InsertOperation#create(DataObject)}
 *   that provide even more comfortable access to operation creating.
 */
public interface AbstractOperation extends RecordProxy {

    /** A field to store the operation "type string". */
    String TYPE_FIELD = "_type";

    /**
     * Gets the type of the operation either from the underlying field map or (if not set) as the innter type via
     *  {@link #getInnerType()} method.
     * @return string that determines the operation type.
     */
    default String getType() {
        String type = getFieldMapType();
        if (type == null)
            return getInnerType();
        else
            return type;
    }

    /**
     * Returns the type string of the operation as hard-coded in the operation class (interface).
     * @return string that determines the operation type.
     */
    String getInnerType();

    @Get(field = TYPE_FIELD)
    String getFieldMapType();

}
