/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.algorithm.impl;

import messif.algorithm.Algorithm;
import messif.distance.DistanceFunc;
import messif.data.DataObject;
import messif.data.util.StreamDataObjectIterator;
import messif.operation.search.KNNOperation;
import messif.operation.search.KNNRangeOperation;
import messif.operation.search.QueryObjectOperation;
import messif.operation.search.RangeOperation;
import messif.operation.answer.RankedAnswer;
import messif.operation.processing.ProcessorConfiguration;
import messif.operation.processing.impl.OneStepProcessor;

import java.io.IOException;

/**
 * Implementation of the naive sequential scan algorithm over a given file of objects.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class FileSequentialScan extends Algorithm {

    /**
     * Class serial id for serialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Number of objects read from the data file at once for special query processing.
     */
    private static final int DATA_BATCH_SIZE = 1000;

    /**
     * One instance of bucket where all objects are stored
     */
    protected final String file;
    //protected final String clazz;
    protected final int nThreads;


    /**
     * Default distance function to be used for the distance-based queries.
     */
    protected DistanceFunc<DataObject> defaultDistanceFunc;


    /**
     * Creates a new instance of SequantialScan access structure with the given bucket and filtering pivots.
     *
     * @param file file with text representation of objects
     */
    @Algorithm.AlgorithmConstructor(description = "FileSequentialScan", arguments = {"data file name", "data class"})
    public FileSequentialScan(String file, DistanceFunc<DataObject> defaultDist, int nThreads) {
        super("SequentialScan");

        // Create an empty bucket (using the provided bucket class and parameters)
        this.file = file;
        this.nThreads = nThreads;
        this.defaultDistanceFunc = defaultDist;
    }


    @Override
    protected void registerProcessors() {
        super.registerProcessors();
        this.handler.register(ProcessorConfiguration.create(QueryObjectOperation.class, RankedAnswer.class)
                .forTypeStrings(KNNOperation.TYPE_STRING, RangeOperation.TYPE_STRING, KNNRangeOperation.TYPE_STRING)
                .withProcessor(QueryObjectProcessor::new)
                .executeSequential().build());
    }


    //******* SEARCH ALGORITHMS ************************************//

    protected class QueryObjectProcessor extends OneStepProcessor<QueryObjectOperation, RankedAnswer> {

        final QueryObjectOperation.Helper helper;

        public QueryObjectProcessor(QueryObjectOperation query) {
            super(query);
            helper = new QueryObjectOperation.Helper(query, defaultDistanceFunc);
        }

        @Override
        public RankedAnswer finish() {
            return helper.getAnswer();
        }

        @Override
        protected void process() throws Exception {
            helper.evaluate(new StreamDataObjectIterator(file));
        }
    }


    /**
     * Processes the batch ranking operation; the batch is split into sub-batches which are processed 
     *  in parallel. A separate thread reads the data from the data file.
     * @param operation batch operation to be processed
     * @throws IOException if the data file cannot be read
     * @throws ClassNotFoundException if the specified data class is not valid
     */
    // TODO: add the BatchRankingQueryOperation and implement its processing here
//    public void batchQuerySearch(BatchRankingQueryOperation operation) throws IOException, ClassNotFoundException {
//        List<Future> futures = new ArrayList<>();
//
//        // list of all data queues used by individual query batches
//        List<BlockingQueue<List<LocalAbstractObject>>> queryDataQueues = new ArrayList<>(nThreads);
//        int queryBatchSize = (int) Math.ceil((float) (operation.getNOperations()) / (float) nThreads);
//        int queryCounter = 0;
//
//        // iterate over all sub-operations from the batch
//        while (queryCounter < operation.getNOperations()) {
//            List<QueryOperation> operationBatch = new ArrayList<>(queryBatchSize);
//            BlockingQueue<List<LocalAbstractObject>> dataBatch = new ArrayBlockingQueue<>(3);
//            queryDataQueues.add(dataBatch);
//            do {
//                operationBatch.add(operation.getOperation(queryCounter));
//            } while ((++ queryCounter % queryBatchSize) != 0 && queryCounter < operation.getNOperations());
//
//            // start thread that will process a sub-batch of ranking operations
//            futures.add(getOperationsThreadPool().submit(new EvaluationThread(operationBatch, dataBatch)));
//        }
//
//        futures.add(getOperationsThreadPool().submit(new DataReaderThread(queryDataQueues)));
//
//        for (Future future : futures) {
//            try {
//                future.get();
//            } catch (InterruptedException | ExecutionException ex) {
//                Logger.getLogger(FileSequentialScan.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        operation.endOperation();
//    }
//
//    /**
//     * Thread in which a sub-batch of query operations is processed.
//     */
//    private class EvaluationThread extends Thread {
//
//        private final BlockingQueue<List<LocalAbstractObject>> dataBatches;
//        private final List<QueryOperation> operationBatch;
//
//        /**
//         * Creates a new query-batch evaluation thread.
//         * @param operationBatch batch of operations to be processed within this thread
//         * @param dataBatches a queue of data batches filled from the outside and consumed by this thread
//         */
//        public EvaluationThread(List<QueryOperation> operationBatch, BlockingQueue<List<LocalAbstractObject>> dataBatches) {
//            this.dataBatches = dataBatches;
//            this.operationBatch = operationBatch;
//        }
//
//        @Override
//        public void run() {
//            while (true) {
//                try {
//                    List<LocalAbstractObject> currentBatch = dataBatches.take();
//                    if (currentBatch.isEmpty()) {
//                        return;
//                    }
//                    for (QueryOperation operation : operationBatch) {
//                        operation.evaluate(DataObjectIterator.getIterator(currentBatch.iterator()));
//                    }
//                } catch (InterruptedException ex) {    }
//            }
//        }
//    }
//
//    /**
//     * Thread that reads the data from the data file and sends this data to the operation processing threads.
//     */
//    private class DataReaderThread extends Thread {
//
//        private final List<BlockingQueue<List<LocalAbstractObject>>> queryQueues;
//
//        /**
//         * New data reading thread.
//         * @param queryQueues list of all shared queues into which this data reader should send the data.
//         */
//        public DataReaderThread(List<BlockingQueue<List<LocalAbstractObject>>> queryQueues) {
//            this.queryQueues = queryQueues;
//        }
//
//        @Override
//        public void run() {
//            try (StreamDataObjectIterator<LocalAbstractObject> it = new StreamDataObjectIterator<>(
//                    Convert.getClassForName(clazz, LocalAbstractObject.class), file)) {
//                while (it.hasNext()) {
//                    List<LocalAbstractObject> dataBatch = new ArrayList<>(DATA_BATCH_SIZE);
//                    int counter = 0;
//                    while (it.hasNext() && counter ++ < DATA_BATCH_SIZE) {
//                        dataBatch.add(it.next());
//                    }
//
//                    for (BlockingQueue<List<LocalAbstractObject>> queryQueue : queryQueues) {
//                        try {
//                            queryQueue.put(dataBatch);
//                        } catch (InterruptedException ex) {
//                            Logger.getLogger(FileSequentialScan.class.getName()).log(Level.SEVERE, null, ex);
//                        }
//                    }
//                }
//                for (BlockingQueue<List<LocalAbstractObject>> queryQueue : queryQueues) {
//                    try {
//                        queryQueue.put(Collections.EMPTY_LIST);
//                    } catch (InterruptedException ex) {   }
//                }
//
//            } catch (IllegalArgumentException | IOException | ClassNotFoundException ex) {
//                Logger.getLogger(FileSequentialScan.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }

    /**
     * Converts the object to a string representation
     *
     * @return String representation of this algorithm
     */
    @Override
    public String toString() {
        StringBuffer rtv = new StringBuffer("FileSequentialScan");
        return rtv.toString();
    }
}
