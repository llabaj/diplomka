/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.processing;

import messif.data.DataObject;
import messif.record.processing.*;
import messif.utility.json.JSONReader;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.Arrays;

/**
 * Connects to a given socket(s), sends the binary data either read from a file or from a URL (file) and sends it to the
 *  socket. Then it reads the response (text) and creates another field from this text.
 */
public class SocketProcessorAddField extends SocketProcessorBase implements RecordProcessor.Single<DataObject> {

    protected final AddFieldProcessor fieldAdder;

    public SocketProcessorAddField(final String host, final int[] ports, final int timeoutMillis, String uriField, String outputField) {
        super(host, ports, timeoutMillis);
        this.fieldAdder = new AddFieldProcessor(uriField, outputField) {
            @Override
            protected Object getFieldValue(DataObject record) {
                // Process with socket extractor
                try (Socket socket = createSocket()) {
                    // read the binary data (either from a file or a URL)
                    // TODO: more than one required field? (try one by one)?
                    BinaryDataSource dataSource = RecordProcessors.openBinarySource(record, requiredField);
                    DataOutputStream os = new DataOutputStream(socket.getOutputStream());
                    byte[] data = dataSource.getBinaryData();

                    // send the binary data to the socket
                    os.writeInt(data.length);
                    os.write(data);
                    os.flush();

                    return createField(socket.getInputStream());
                } catch (IOException e) {
                    throw new RecordProcessingException("error creating socket on '" + host + "', ports: " + Arrays.toString(ports), e);
                }
            }
        };
    }

    /**
     * Assumes that the service on the other side of the socket returns a valid JSON.
     * @param socketInputStream
     * @return a field created from the JSON string
     * @throws IOException
     */
    protected Object createField(InputStream socketInputStream) throws IOException {
        // read the text answer from the socket and create a new field from the data (JSON-formatted)
        final JSONReader jsonReader = new JSONReader(socketInputStream, false);
        return jsonReader.readNextObject();
    }

    @Override
    public boolean preCondition(DataObject record) {
        return fieldAdder.preCondition(record);
    }

    @Override
    public String preConditionDescription() {
        return fieldAdder.preConditionDescription();
    }

    @Override
    public DataObject process(DataObject record) throws RecordProcessingException {
        return fieldAdder.process(record);
    }

    @Override
    public String[] modifiedFields() {
        return fieldAdder.modifiedFields();
    }
}
