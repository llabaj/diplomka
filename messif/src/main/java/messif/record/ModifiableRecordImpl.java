/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.record;

import java.util.HashMap;
import java.util.Map;

/**
 * Basic implementation of the {@link ModifiableRecord} interface on encapsulated {@link Map}.
 * Note that this class can be used as wrapper for {@link Map}.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class ModifiableRecordImpl extends RecordImpl implements ModifiableRecord {
    /**
     * Class serial id for serialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Encapsulated {@link Map} that provides the field values
     */
    //private final Map<String, Object> modifiableMap;
    public ModifiableRecordImpl() {
        super(new HashMap<>(), true);
    }

    /**
     * Creates a new instance of ModifiableParametricBase backed-up by the given map with fields.
     * The fields are stored in a new {@link HashMap} instance.
     *
     * @param fields the map that provides the field keys &amp; values
     */
    public ModifiableRecordImpl(Map<String, Object> fields) {
        this(fields, false);
    }

    /**
     * Creates a new instance of ModifiableParametricBase backed-up by the given map with fields.
     * The fields are stored in a new {@link HashMap} instance.
     *
     * @param fields         the map that provides the field keys &amp; values
     * @param useMapDirectly flag whether the given field map is used directly (<tt>true</tt>)
     *                       or if a {@link HashMap} copy is created (<tt>false</tt>)
     */
    public ModifiableRecordImpl(Map<String, Object> fields, boolean useMapDirectly) {
        super(useMapDirectly ? fields : new HashMap<>(fields), true);
        //this.modifiableMap = getDirectMap();
    }

    /**
     * Creates a new instance of ParametricBase from another {@link Record} object.
     * The fields are stored in a new {@link HashMap} instance.
     *
     * @param record another {@link Record} object the data of which to copy
     */
    public ModifiableRecordImpl(Record record) {
        this(record.getMap(), false);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new ModifiableRecordImpl(this);
    }

    @Override
    public Object setField(String name, Object value) {
        return map.put(name, value);
    }

    @Override
    public Object removeField(String name) {
        return map.remove(name);
    }

    @Override
    public boolean addAllFields(Record record, boolean override) {
        final Map<String, Object> map = (record instanceof RecordImpl) ? ((RecordImpl) record).getDirectMap() : record.getMap();
        boolean modified = false;
        for (Map.Entry<String, Object> nameValue : map.entrySet()) {
            if (override || ! containsField(nameValue.getKey())) {
                setField(nameValue.getKey(), nameValue.getValue());
                modified = true;
            }
        }
        return modified;
    }


}
