/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.serialization;

import messif.data.DataObject;
import messif.data.util.DataObjectList;
import messif.serialization.io.BinaryInput;
import messif.serialization.io.BinaryOutput;
import messif.serialization.io.FileChannelInputStream;
import messif.serialization.io.FileChannelOutputStream;
import messif.utility.json.JSONReaderTest;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.List;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class SingleClassSerializatorTest {
    

    protected List<DataObject> objects;

    @Before
    public void setUp() throws Exception {
        try {
            objects = new DataObjectList(JSONReaderTest.getDataObjects());
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
        System.out.println("successful setUp");
    }

    /**
     * Test of readObject method, of class SingleClassSerializator.
     */
    @Test
    public void testWriteObject() throws Exception {
        System.out.println("writeObject");
        try (FileOutputStream fileOutputStream = new FileOutputStream("testfile.bin")) {
            BinaryOutput output = new FileChannelOutputStream(256, true, fileOutputStream.getChannel(), 0, Long.MAX_VALUE);
            
            SingleClassSerializator<Serializable> serializator = new SingleClassSerializator(new JavaSerializationReaderWriter(), (byte) 51);
            serializator.writeObject(output, objects.get(0));
            output.flush();
        }
    }
    
    
    /**
     * Test of readObject method, of class SingleClassSerializator.
     */
    @Test
    public void testReadObject() throws Exception {
        System.out.println("readObject");
        try (FileInputStream fileInputStream = new FileInputStream("testfile.bin")) {
            BinaryInput input = new FileChannelInputStream(256, true, fileInputStream.getChannel(), 0, Long.MAX_VALUE);
            
            SingleClassSerializator serializator = new SingleClassSerializator(new JavaSerializationReaderWriter(), (byte) 51);
            DataObject readObject = (DataObject) serializator.readObject(input);
            //assertTrue(readObject.dataEquals(objects.get(0)));
            assertTrue(readObject.getFieldNames().containsAll(objects.get(0).getFieldNames()));
        }
    }
    
}
