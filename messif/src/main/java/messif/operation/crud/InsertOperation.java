/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.crud;

import messif.bucket.BucketStorageException;
import messif.bucket.LocalBucket;
import messif.data.DataObject;
import messif.operation.OperationBuilder;
import messif.operation.crud.answer.CRUDAnswer;
import messif.operation.crud.answer.InsertAnswer;
import messif.storage.Storage;
import messif.utility.ProcessingStatus;

import java.util.List;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface InsertOperation extends ModifyingOperation {

    String TYPE_STRING = "insert";

    @Override
    default String getInnerType() {
        return TYPE_STRING;
    }

    static InsertOperation create(DataObject [] objects) {
        return OperationBuilder.create(InsertOperation.class).addParam(CRUDOperation.OBJECTS_FIELD, objects).build();
    }

    static InsertOperation create(DataObject object) {
        return OperationBuilder.create(InsertOperation.class).addParam(CRUDOperation.OBJECTS_FIELD, new DataObject [] {object}).build();
    }

    class Helper extends CRUDOperation.Helper<InsertOperation, InsertAnswer> {

        /**
         * Creates this processor given the operation to be processed.
         *
         * @param operation operation to be executed
         */
        public Helper(InsertOperation operation) {
            super(operation, InsertAnswer.class, InsertAnswer.SOME_OBJECTS_INSERTED,
                    new ProcessingStatus[] { InsertAnswer.OBJECT_INSERTED, InsertAnswer.SOFTCAPACITY_EXCEEDED },
                    new ProcessingStatus[] { InsertAnswer.HARDCAPACITY_EXCEEDED, InsertAnswer.OBJECT_DUPLICATE, InsertAnswer.OBJECT_REFUSED, CRUDAnswer.SOME_OBJECT_NOT_FOUND, InsertAnswer.SOME_OBJECTS_INSERTED});
        }

        /******************  Helper methods specific to insert operation ******************/

        @Override
        protected ProcessResult processObject(LocalBucket bucket, DataObject dataObject) {
            return new ProcessResult(dataObject, bucket.addObjectErrCode(dataObject));
        }

        /**
         * Insert all objects specified in this operation into given bucket, monitor the actually inserted objects and update the error
         * code correspondingly.
         *
         * @param bucket the bucket to insert objects into
         * @return list of ORIGINAL objects actually inserted in the same order as in parameter objects
         */
        public List<DataObject> insertAllObjects(LocalBucket bucket) {
            return insertObjects(bucket, getOperation().getObjects());
        }

        /**
         * Insert given list of objects into given bucket, monitor the actually inserted objects and update the error
         * code correspondingly.
         *
         * @param bucket  the bucket to insert objects into
         * @param objects a list of objects to be inserted into the bucket
         * @return list of ORIGINAL objects actually inserted in the same order as in parameter objects
         */
        public List<DataObject> insertObjects(LocalBucket bucket, DataObject[] objects) {
            return super.processObjects(bucket, objects);
        }

        protected ProcessResult processObject(Storage<DataObject> storage, DataObject dataObject) {
            try {
                storage.store(dataObject);
                return new ProcessResult(dataObject, InsertAnswer.OBJECT_INSERTED);
            } catch (BucketStorageException e) {
                return new ProcessResult(dataObject, InsertAnswer.OBJECT_DUPLICATE);
            }
        }
    }
}
