/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.crud.answer;

import messif.operation.answer.ProcessObjectsAnswer;
import messif.utility.ProcessingStatus;
import messif.utility.net.HttpStatusCodeProvider;

/**
 * A common predecessor of all CRUD operations.
 */
public interface CRUDAnswer extends ProcessObjectsAnswer {


    // ***************    Status codes common for (some) CRUD operations    ****************

    /**
     * A successful status code saying that the requested object was found (used for get and update operations).
     */
    ProcessingStatus OBJECT_FOUND = new ProcessingStatus("records found", 205, HttpStatusCodeProvider.STATUS_CODE_OK);

    /** Some of the objects cannot be deleted because it is not present. */
    ProcessingStatus SOME_OBJECT_NOT_FOUND = new ProcessingStatus("some of the records not found", 206, HttpStatusCodeProvider.STATUS_CODE_PARTIAL);

    /** Object cannot be deleted because it is not present. */
    ProcessingStatus OBJECT_NOT_FOUND = new ProcessingStatus("records not found", 404, HttpStatusCodeProvider.STATUS_CODE_NOT_FOUND);

}
