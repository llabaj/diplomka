/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.distance.impl;

import messif.distance.*;

/**
 * Implementation of Euclidean distance (L2) on arrays of integers (int[]).
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class L2DistanceInts implements DistanceFunc<int[]>, Metric {

    /** Class id for Java serialization. */
    private static final long serialVersionUID = 1L;
    
    @Override
    public float getDistance(int[] o1, int[] o2, float threshold) throws DistanceException {
        if (o2.length != o1.length) {
            throw new DistanceException("Cannot compute distance on different vector dimensions (" + o1.length + ", " + o2.length + ")");
        }
        float powSum = 0;
        for (int i = 0; i < o1.length; i++) {
            float dif = (o1[i] - o2[i]);
            powSum += dif * dif;
        }
        return (float) Math.sqrt(powSum);
    }

    @Override
    public Class<int[]> getObjectClass() {
        return int [].class;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
