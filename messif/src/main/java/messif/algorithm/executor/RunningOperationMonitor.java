/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.algorithm.executor;


import messif.algorithm.Algorithm;
import messif.algorithm.AlgorithmMethodException;
import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.crud.ModifyingOperation;
import messif.operation.processing.OperationExecutor;
import messif.utility.ExtendedProperties;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class RunningOperationMonitor extends ExecutorChainItem {

    //****************** Constants ******************//

    /**
     * Maximal number of currently executed operations.
     */
    public static final String CONFIG_MAX_CONCUR_OPS = "max_concurrent_ops";

    public static final String CONFIG_MAX_CONCUR_UPDATES = "max_concurrent_updates";


    //****************** Attributes ******************//

    /**
     * Number of actually running operations.
     */
    protected Semaphore runningOperationsSemaphore;

    /**
     * Number of actually running update operations.
     */
    protected Semaphore updateOperationsSemaphore;

    /**
     * List of currently running operations.
     */
    protected WeakHashMap<AbstractOperation, Thread> runningOperations;

    protected int maxConcurrentOps = 1024;

    protected int maxConcurrentUpdates = 1024;


    //****************** Constructors ******************//

    @Override
    public void init(ExtendedProperties properties, Algorithm algorithm) throws AlgorithmMethodException {
        super.init(properties, algorithm);
        int maxConcurrentOps = properties.getIntProperty(CONFIG_MAX_CONCUR_OPS, this.maxConcurrentOps);
        int maxConcurrentUpdates = properties.getIntProperty(CONFIG_MAX_CONCUR_UPDATES, this.maxConcurrentUpdates);

        this.maxConcurrentOps = maxConcurrentOps;
        this.maxConcurrentUpdates = maxConcurrentUpdates;
        runningOperationsSemaphore = new Semaphore(maxConcurrentOps, true);
        updateOperationsSemaphore = new Semaphore(maxConcurrentUpdates, true);
        runningOperations = new WeakHashMap<>(maxConcurrentOps);
    }

    //****************** Running operations info methods ******************//

    /**
     * Returns the number of currently evaluated operations.
     * Every thread inside executeOperation is counted as well as every invocation of backgroundExecute
     * that was not yet extracted by waitBackgroundExecuteOperation.
     *
     * @return the number of currently evaluated operations
     */
    public int getRunningOperationsCount() {
        return maxConcurrentOps - runningOperationsSemaphore.availablePermits();
    }

    /**
     * Returns all operations currently executed by this algorithm.
     * Note that the returned collection is new independent instance, thus any
     * modifications are not propagated, so it is <em>not</em> unmodifiable.
     * Note also that the thread processing the operation must be still alive
     * in order to be returned by this method.
     *
     * @return a collection of all operations
     */
    public Collection<AbstractOperation> getAllRunningOperations() {
        synchronized (this) { // We are synchronizing the access to the list using algorithmName so that the runningOperations can be set when deserializing
            // The list of operations must be copied to a serializable list
            return runningOperations.entrySet().stream().filter(entry -> entry.getValue().isAlive())
                    .map(Map.Entry<AbstractOperation, Thread>::getKey).collect(Collectors.toList());
        }
    }


    public void stopUpdates() {
        updateOperationsSemaphore.acquireUninterruptibly(maxConcurrentUpdates);
    }

    public void continueUpdates() {
        updateOperationsSemaphore.release(maxConcurrentUpdates);
    }

    /**
     * Execute operation with additional parameters.
     *
     * @throws AlgorithmMethodException if the execution has thrown an exception
     * @throws NoSuchMethodException    if the operation is unsupported (there is no method for the operation)
     */

    @Override
    public AbstractAnswer execute(AbstractOperation operation, Iterator<OperationExecutor> iterator) throws Exception {
        runningOperationsSemaphore.acquireUninterruptibly();
        if (operation instanceof ModifyingOperation) {
            updateOperationsSemaphore.acquireUninterruptibly();
        }
        synchronized (this) { // We are synchronizing the access to the list using algorithmName so that the runningOperations can be set when deserializing
            runningOperations.put(operation, Thread.currentThread());
        }
        try {
            return executeNext(operation, iterator);
        } catch (CloneNotSupportedException | ClassCastException | InterruptedException e) {
            throw new AlgorithmMethodException(e);
        } finally {
            runningOperationsSemaphore.release();
            if (operation instanceof ModifyingOperation)
                updateOperationsSemaphore.release();
        }
    }


    // TODO: remove compeletely
//    /**
//     * Terminates processing of the operation with given identifier.
//     * Note that the thread that executes the operation is interrupted leaving
//     * the decision on how to finish cleanly to the processing method.
//     * Each processing method thus should check the {@link Thread#isInterrupted()}
//     * regularly and act accordingly if it is set.
//     * @param operationId the identifier of the operation to terminate
//     * @return <tt>true</tt> if there was an operation for that identifier and it was interrupted;
//     *          if there was no operation or the thread executing it has already canFinish, <tt>false</tt> is returned
//     */
//    public boolean terminateOperation(UUID operationId) {
//        synchronized (lock) { // We are synchronizing the access to the list using algorithmName so that the runningOperations can be set when deserializing
//            for (Map.Entry<AbstractOperation, Thread> entry : runningOperations.entrySet()) {
//                if (entry.getKey().getOperationID().equals(operationId)) {
//                    entry.getValue().interrupt();
//                    return entry.getValue().isAlive();
//                }
//            }
//            return false;
//        }
//    }

    /**
     * Terminates processing of the given operation.
     * Note that the thread that executes the operation is interrupted leaving
     * the decision on how to finish cleanly to the processing method.
     * Each processing method thus should check the {@link Thread#isInterrupted()}
     * regularly and act accordingly if it is set.
     * @param operation the operation to terminate
     * @return <tt>true</tt> if the operation was processed by a thread and it was interrupted;
     *          if there was no thread currently processing the operation, <tt>false</tt> is returned
     */
    public boolean terminateOperation(AbstractOperation operation) {
        final Thread thread = runningOperations.get(operation);
        if (thread == null) {
            return false;
        }
        thread.interrupt();
        return thread.isAlive();
    }

}
