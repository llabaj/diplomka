/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.processing;

import messif.utility.Convert;
import messif.utility.json.JSONReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;

/**
 * Connects to a given socket(s) with an old non-JSON Caffe extractor, sends the binary data either read from a file or
 *  from a URL (file) and sends it to the socket. Then it reads the response (text) and creates another field from this
 *  text; the resulting text is modified to be a valid JSON-like int array.
 */
public class SocketProcessorCaffeExtractor extends SocketProcessorAddField {

    final Pattern pattern = Pattern.compile("^(.[0-9]+)(?!\\.)");

    public SocketProcessorCaffeExtractor(final String host, final int[] ports, final int timeoutMillis, String uriField, String outputField) {
        super(host, ports, timeoutMillis, uriField, outputField);
    }

    /**
     * The original Caffe extractor does not return a valid JSON.
     * @param socketInputStream
     * @return
     */
    @Override
    protected Object createField(InputStream socketInputStream) throws IOException {
        final StringBuilder stringBuilder = new StringBuilder("[");
        Convert.readStringData(socketInputStream, stringBuilder);
        stringBuilder.append(']');
        String value = pattern.matcher(stringBuilder).replaceFirst("$1.0");
        return JSONReader.readObjectFrom(value, false);
    }
}
