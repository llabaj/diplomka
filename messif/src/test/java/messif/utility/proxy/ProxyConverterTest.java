/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.proxy;

import messif.record.ModifiableRecord;
import messif.record.ModifiableRecordImpl;
import messif.utility.proxy.annotations.*;
import org.hamcrest.core.StringContains;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Unit test verifying the proxy conversion and testing various method conversion.
 * <p>
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class ProxyConverterTest {
    private interface GetterClass {
        @Get(field = "id")
        int getId();

        @Get(field = "valid")
        boolean isValid();

        @Get(field = "name")
        String getName();

        @Get(field = "vector")
        float[] getVector();

        @Get(field = "string_vector")
        String[] getStringVector();
    }

    @Test
    public void getField_test() {
        Map<String, Object> map = new HashMap<String, Object>() {{
            put("id", 12);
            put("valid", true);
            put("name", "Name (12)");
            put("vector", new float[]{1.0f});
            put("string_vector", new String[]{"one", "two"});
        }};
        ModifiableRecord record = new ModifiableRecordImpl(map);
        GetterClass obj = ProxyConverter.convert(record, GetterClass.class);

        Assert.assertEquals(12, obj.getId());
        Assert.assertEquals(true, obj.isValid());
        Assert.assertEquals("Name (12)", obj.getName());
        Assert.assertArrayEquals(new float[]{1.0f}, obj.getVector(), 0);
        Assert.assertArrayEquals(new String[]{"one", "two"}, obj.getStringVector());
    }

    private interface RequiredGetterClass {
        @Get(field = "id")
        int getId();

        @Get(field = "valid")
        boolean isValid();

        @Get(field = "name")
        @Required
        String getName();

        @Get(field = "vector")
        @Required
        float[] getVector();
    }

    @Test
    public void getRequiredField_test() {
        ModifiableRecord record = new ModifiableRecordImpl(new HashMap<>());
        RequiredGetterClass obj = ProxyConverter.convert(record, RequiredGetterClass.class);

        assertMissingValue(obj::getId, "id");
        assertMissingValue(obj::getVector, "vector");
        assertMissingValue(obj::getName, "name");
        assertMissingValue(obj::isValid, "valid");
    }

    private interface DefaultGetterClass {
        @Get(field = "id")
        int getId(int defaultId);

        @Get(field = "valid")
        boolean isValid(boolean defaultValid);

        @Get(field = "name")
        String getName(String defaultName);

        @Get(field = "vector")
        float[] getVector(float[] defaultVector);
    }

    @Test
    public void getFieldWithDefault_test() {
        Map<String, Object> map = new HashMap<>();
        ModifiableRecord record = new ModifiableRecordImpl(map);
        DefaultGetterClass obj = ProxyConverter.convert(record, DefaultGetterClass.class);

        Assert.assertEquals(0, obj.getId(0));
        Assert.assertEquals(true, obj.isValid(true));
        Assert.assertEquals("DefaultName", obj.getName("DefaultName"));
        Assert.assertArrayEquals(new float[0], obj.getVector(new float[0]), 0);
    }

    private interface SetterClass extends GetterClass {
        @Set(field = "id")
        void setId(int id);

        @Set(field = "valid")
        void setValid(boolean valid);

        @Set(field = "name")
        void setName(String name);

        @Set(field = "vector")
        void setVector(float[] vector);
    }

    @Test
    public void setField_test() {
        ModifiableRecord fieldMap = new ModifiableRecordImpl(new HashMap<>());
        SetterClass obj = ProxyConverter.convert(fieldMap, SetterClass.class);

        obj.setId(12);
        obj.setValid(true);
        obj.setName("Name (12)");
        obj.setVector(new float[]{1.0f});

        Assert.assertEquals(12, obj.getId());
        Assert.assertEquals(true, obj.isValid());
        Assert.assertEquals("Name (12)", obj.getName());
        Assert.assertArrayEquals(new float[]{1.0f}, obj.getVector(), 0);
    }

//    @Test(expected = ClassCastException.class)
//    public void setField_onUnmodifiableFieldMap_test() {
//        ModifiableRecord record = new ModifiableRecordImpl(new HashMap<>());
//        SetterClass obj = ProxyConverter.convert(record, SetterClass.class);
//
//        obj.setId(12);
//    }

    private interface RemoveClass extends SetterClass {
        @Remove(field = "id")
        int removeId();

        @Remove(field = "valid")
        boolean removeValid();

        @Remove(field = "name")
        String removeName();

        @Remove(field = "vector")
        float[] removeVector();
    }

    @Test
    public void removeField_test() {
        ModifiableRecord fieldMap = new ModifiableRecordImpl(new HashMap<>());
        RemoveClass obj = ProxyConverter.convert(fieldMap, RemoveClass.class);

        obj.setId(12);
        obj.setValid(true);
        obj.setName("Name (12)");
        obj.setVector(new float[]{1.0f});

        Assert.assertEquals(12, obj.removeId());
        Assert.assertEquals(true, obj.removeValid());
        Assert.assertEquals("Name (12)", obj.removeName());
        Assert.assertArrayEquals(new float[]{1.0f}, obj.removeVector(), 0);

        assertMissingValue(obj::getId, "id");
        assertMissingValue(obj::isValid, "valid");
        Assert.assertNull(obj.getName());
        Assert.assertNull(obj.getVector());
    }

    //@Test(expected = NullPointerException.class)
    @Test
    public void removeField_onUnmodifiableFieldMap_test() {
        ModifiableRecord record = new ModifiableRecordImpl(new HashMap<>());
        RemoveClass obj = ProxyConverter.convert(record, RemoveClass.class);

        obj.removeName();
    }

    private interface DefaultMethods {
        @Get(field = "MY_STRANGE_NAME")
        String getName();

        default String callName() {
            return "Call " + getName();
        }
    }

    private interface DefaultMethodsChild extends DefaultMethods {
        default String getName() {
            return "CHILD";
        }
    }

    @Test
    public void defaultMethods_test() {
        Map<String, Object> map = new HashMap<String, Object>() {{
            put("MY_STRANGE_NAME", "MyName");
        }};
        ModifiableRecord fieldMap = new ModifiableRecordImpl(map);
        DefaultMethods obj = ProxyConverter.convert(fieldMap, DefaultMethods.class);

        Assert.assertEquals("MyName", obj.getName());
        Assert.assertEquals("Call MyName", obj.callName());

        DefaultMethods child = ProxyConverter.convert(fieldMap, DefaultMethodsChild.class);

        Assert.assertEquals("CHILD", child.getName());
        Assert.assertEquals("Call CHILD", child.callName());
    }

    private interface ExposeClass {
        @Expose
        ModifiableRecord getUnderlyingRecord();
    }

    @Test
    public void underlyingFieldMap_test() {
        Map<String, Object> map = new HashMap<>();
        ModifiableRecord fieldMap = new ModifiableRecordImpl(map);

        ExposeClass obj = ProxyConverter.convert(fieldMap, ExposeClass.class);

        Assert.assertEquals(fieldMap, obj.getUnderlyingRecord());
    }

    private void assertMissingValue(Runnable runnable, String fieldName) {
        try {
            runnable.run();
            Assert.fail("Should be missing error for fieldName " + fieldName);
        } catch (IllegalArgumentException e) {
            Assert.assertThat("Field '" + fieldName + "' should be missing", e.getMessage(), StringContains.containsString(fieldName));
        }
    }

    @Test
    public void toString_hashCode_test() {
        ModifiableRecord record = new ModifiableRecordImpl(new HashMap<>());
        ExposeClass obj = ProxyConverter.convert(record, ExposeClass.class);
        String toString = obj.toString();
        Assert.assertEquals("messif.record.ModifiableRecordImpl with {}", toString);
        int hashCode = obj.hashCode();
        Assert.assertTrue(hashCode > 0);

        Assert.assertEquals(obj, obj);

        ModifiableRecord record2 = new ModifiableRecordImpl(new HashMap<>());
        ExposeClass obj2 = ProxyConverter.convert(record2, ExposeClass.class);
        Assert.assertNotEquals(obj, obj2);
    }
}
