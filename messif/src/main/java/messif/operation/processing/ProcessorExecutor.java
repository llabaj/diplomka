/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.processing;

import messif.algorithm.AlgorithmMethodException;
import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */

public interface ProcessorExecutor {

    @Nullable
    <TRequest extends AbstractOperation, TAnswer extends AbstractAnswer> TAnswer execute(OperationProcessor<TRequest, TAnswer> processor) throws Exception;

    ProcessorExecutor SEQUENTIAL_EXECUTOR = new SequentialExecutor();

    class SequentialExecutor implements ProcessorExecutor {
        @Override
        public <TRequest extends AbstractOperation, TAnswer extends AbstractAnswer> TAnswer execute(OperationProcessor<TRequest, TAnswer> processor)
                throws Exception {
            try {
                while (processor.processStep()) ; // This empty body is intended
                return processor.finish();
            } catch (Exception ex) {
                processor.clean();
                throw ex;
            }
        }
    }

    class ParallelExecutor implements ProcessorExecutor {

        private final ExecutorService pool;

        public ParallelExecutor(ExecutorService pool) {
            this.pool = pool;
        }

        @Override
        public <TRequest extends AbstractOperation, TAnswer extends AbstractAnswer> TAnswer execute(OperationProcessor<TRequest, TAnswer> processor)
                throws Exception {
            if (! (processor instanceof AsyncOperationProcessor)) {
                return SEQUENTIAL_EXECUTOR.execute(processor);
            }
            AsyncOperationProcessor<TRequest, TAnswer> asyncProcessor = (AsyncOperationProcessor<TRequest, TAnswer>) processor;
            try {
                List<Future<Void>> futures = new ArrayList<>();
                Callable<Void> callable = asyncProcessor.processStepAsynchronously();
                while (callable != null) {
                    futures.clear();
                    do {
                        futures.add(pool.submit(callable));
                        callable = asyncProcessor.processStepAsynchronously();
                    } while (callable != null);
                    // wait for all threads to finish
                    for (Iterator<Future<Void>> it = futures.iterator(); it.hasNext(); )
                        it.next().get();
                    // and check the "next" step once again
                    callable = asyncProcessor.processStepAsynchronously();
                };
                return asyncProcessor.finish();
            } catch (ExecutionException e) {
                asyncProcessor.clean();
                throw new AlgorithmMethodException(e.getCause());
            } catch (Error e) {
                asyncProcessor.clean();
                throw new AlgorithmMethodException(e);
            }
        }
    }
}

