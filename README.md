## Instructions
### Requirements
This project requires JDK 10 or later

Download JDK 10 [http://jdk.java.net/10](http://jdk.java.net/10), extract archive, set `JAVA_HOME` enviroment variable to `/full/path/to/jdk-10.0.1`

Clone the repository
```
git clone https://bitbucket.org/llabaj/diplomka.git
```

Project structure:
```
root
├── fixedpivots        # library module, containing data structures and algorithms used for searching and indexing
├── fixedpivotswebapp  # web applictaion module
├── messif             # dependency for data import/storage
└── scripts            # helper sripts used during developement/testing

```

### Build the project
*(use `./mvnw` for Linux and Unix, `./mvnw.cmd` for Windows)*

```
cd diplomka
./mvnw install -DskipTests
```
it is important to run `./mvnw install -DskipTests` command inside root directory.

**Run web application from command line** *(project needs to be built first)*
```
cd fixedpivotswebapp
./mvnw spring-boot:run
```
output `Started Application in X seconds (JVM running for Y)` indicates application has started successfully.

Application can now be accessed at `http://localhost:8080/`.

Database is initially empty, for demonstration purposes a small dataset is included and can be imported by visiting `http://localhost:8080/action/init`, import should be completed in 10 to 20 seconds.
Once import is complete, `http://localhost:8080/` shows randomly selected images, clicking on image seraches for similar images.

Default port is 8080 and can be changed in `fixedpivotswebapp/src/main/resources/application.properties`.

**Build executable jar** *(project needs to be built first)*:
```
cd fixedpivotswebapp
./mvnw package spring-boot:repackage
```
Output file is located at `fixedpivotswebapp/target/webapp-full.jar` and can be run using `java -jar webapp-full.jar`, configure port using `--server.port=<port>` option.

# Licenses
 1. Hazelcast IMDG - [hazelcast.org](http://hazelcast.org) under [Apache Licence v2](http://www.apache.org/licenses/LICENSE-2.0)
 2. MESSIF - [bitbucket.org/disalab/messif](https://bitbucket.org/disalab/messif) under [GNU General Public License](http://www.gnu.org/licenses/gpl.txt)
 3. Apache Commons Lang - [commons.apache.org/proper/commons-lang](https://commons.apache.org/proper/commons-lang/) under [Apache Licence v2](http://www.apache.org/licenses/LICENSE-2.0)
 4. SLF4J - [slf4j.org](https://www.slf4j.org/) under [MIT License](https://www.slf4j.org/license.html)
 5. OWNER - [owner.aeonbits.org](http://owner.aeonbits.org/) under [BSD license](https://raw.githubusercontent.com/lviggiano/owner/master/LICENSE)
 6. Spring Boot - [github.com/spring-projects/spring-boot](https://github.com/spring-projects/spring-boot) under [Apache Licence v2](http://www.apache.org/licenses/LICENSE-2.0) 
 
 
 