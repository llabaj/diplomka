/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.classification.impl;

import messif.classification.*;
import messif.distance.DistanceFunc;
import messif.data.util.RankedDataObject;
import messif.record.ModifiableRecord;
import messif.record.Record;

import java.util.Iterator;

/**
 * Implementation of a classifier that computes a {@link ClassificationWithConfidence}
 * from the given distance-ranked objects. The categories are derived from
 * the {@link Record} parameter with the given name. The confidences of the
 * respective categories are the distance-ranks of the respective objects that
 * provide them. The overall classification is computed as the minimal distance-rank
 * of each category.
 * 
 * @param <C> the class of instances that represent the classification categories
 * @see Classifications#convertToClassificationWithConfidence
 * @see ClassificationWithConfidenceBase#updateAllConfidences
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class ParametricRankedObjectDistanceClassifier<C> implements Classifier<Iterator<? extends RankedDataObject>, C> {

    /** Class of instances that represent the classification categories */
    private final Class<? extends C> categoriesClass;
    /** *  Name of the {@link Record} parameter that contains the classification categories */
    private final String categoriesParameterName;

    /**
     * Creates a new instance of ParametricRankedObjectDistanceClassifier.
     * @param categoriesClass the class of instances that represent the classification categories
     * @param categoriesParameterName the name of the {@link Record} parameter that contains the classification categories
     */
    public ParametricRankedObjectDistanceClassifier(Class<? extends C> categoriesClass, String categoriesParameterName) {
        this.categoriesClass = categoriesClass;
        this.categoriesParameterName = categoriesParameterName;
    }

    @Override
    public ClassificationWithConfidence<C> classify(Iterator<? extends RankedDataObject> iterator, Record parameters) throws ClassificationException {
        ClassificationWithConfidenceBase<C> ret = new ClassificationWithConfidenceBase<C>(categoriesClass, DistanceFunc.MAX_DISTANCE, DistanceFunc.MIN_DISTANCE);
        while (iterator.hasNext())
            ret.updateAllConfidences(getClassification(iterator.next(), parameters));
        return ret;
    }

    /**
     * Retrieves the classification from the object.
     * Note that the encapsulated {@link RankedDataObject#getObject() object}
     * must implement the {@link Record} interface.
     * Note also that the rank of the object is used as the confidence.
     * @param object the ranked object for which to get the classification
     * @param parameters additional parameters for the classification;
     *          the values for the parameters are specific to a given classifier
     *          implementation and can be updated during the process if they are {@link ModifiableRecord}
     * @return the classification of the given ranked object or <tt>null</tt> if
     *      the parameter is <tt>null</tt> or cannot be converted to classification
     */
    protected ClassificationWithConfidence<C> getClassification(RankedDataObject object, Record parameters) {
        return Classifications.convertToClassificationWithConfidence(((Record)object.getObject()).getField(categoriesParameterName),
            categoriesClass,
            object.getDistance(),
            DistanceFunc.MAX_DISTANCE, DistanceFunc.MIN_DISTANCE
        );
    }

    @Override
    public Class<? extends C> getCategoriesClass() {
        return categoriesClass;
    }
    
}
