/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Allows to compute an intersection of the values provided by two iterators.
 * @param <T> the type of intersecting values
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class IteratorIntersection<T> implements Comparator<T> {
    /** The first encapsulated iterator */
    private final PeekIterator<? extends T> iterator1;
    /** The second encapsulated iterator */
    private final PeekIterator<? extends T> iterator2;
    /** The comparator used to compare the provided objects */
    private final Comparator<? super T> comparator;
    /** The last value returned by the first iterator */
    private T value1;
    /** The last value returned by the second iterator */
    private T value2;
    /** The comparison result of the value1 and value2 */
    private int cmp;
    /** Flag whether the first, the second or both iterators have advanced during the last step */
    private byte iteratorAdvanced;

    /**
     * Creates a new intersection evaluator for the given two iterators.
     * @param iterator1 the first encapsulated iterator
     * @param iterator2 the second encapsulated iterator
     * @param comparator the comparator used to compare the provided objects
     */
    public IteratorIntersection(PeekIterator<? extends T> iterator1, PeekIterator<? extends T> iterator2, Comparator<? super T> comparator) {
        this.iterator1 = iterator1;
        this.iterator2 = iterator2;
        this.comparator = comparator;
    }

    @Override
    @SuppressWarnings("unchecked")
    public final int compare(T o1, T o2) {
        if (o1 == null)
            return o2 == null ? 0 : 1;
        if (o2 == null)
            return -1;
        if (comparator == null)
            return ((Comparable<? super T>)o1).compareTo(o2);
        return comparator.compare(o1, o2);
    }

    /**
     * Get the next value from the first (if comparison is smaller), the second (if comparison is bigger),
     * or both (if comparison is equal) iterators.
     * @param cmpResult the comparison value
     * @return which iterators have been advanced (3 means both)
     */
    protected byte advanceIterator(int cmpResult) {
        byte ret = 0;
        if (cmpResult <= 0 && iterator1.hasNext()) {
            value1 = iterator1.next();
            ret |= 1;
        }
        if (cmpResult >= 0 && iterator2.hasNext()) {
            value2 = iterator2.next();
            ret |= 2;
        }
        return ret;
    }

    /**
     * Perform intersection computation step and set internal variables.
     * @return <tt>true</tt> if one of the iterators have been advanced,
     *      i.e. <tt>false</tt> means that both the iterators have no more values
     */
    public boolean step() {
        if (cmp == 0 && iteratorAdvanced != 0) // If the current values are intersecting, advance accoring to the next values
            cmp = compare(iterator1.peek(), iterator2.peek());
        iteratorAdvanced = advanceIterator(cmp);
        if (iteratorAdvanced == 0)
            return false;
        cmp = compare(value1, value2);
        return true;
    }

    /**
     * Computes the objects in the intersection.
     * Note that this method only returns the intersection of
     * the non-processed data of the iterators.
     * @return a list of objects in the intersection
     */
    public List<T> intersection() {
        List<T> ret = new ArrayList<T>();
        while (step()) {
            if (isIntersecting())
                ret.add(value1);
        }
        return ret;
    }

    /**
     * Returns whether the first iterator has advanced during the last step.
     * @return <tt>true</tt> if first iterator has advanced during the last step
     */
    public boolean hasIterator1Advanced() {
        return (iteratorAdvanced & 1) == 1;
    }

    /**
     * Returns whether the second iterator has advanced during the last step.
     * @return <tt>true</tt> if second iterator has advanced during the last step
     */
    public boolean hasIterator2Advanced() {
        return (iteratorAdvanced & 2) == 2;
    }

    /**
     * Returns whether an intersection of the two values has been found.
     * @return <tt>true</tt> if the two objects discovered by the last step are the same (as specified by the comparator)
     */
    public boolean isIntersecting() {
        return cmp == 0;
    }

    /**
     * Returns the current value of the first iterator.
     * @return the current value of the first iterator
     */
    public T getValue1() {
        return value1;
    }

    /**
     * Returns the current value of the second iterator.
     * @return the current value of the second iterator
     */
    public T getValue2() {
        return value2;
    }
}
