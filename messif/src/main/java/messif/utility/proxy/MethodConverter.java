/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.proxy;

import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Remove;
import messif.utility.proxy.annotations.Required;
import messif.utility.proxy.annotations.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Class responsible for conversion of {@link Method} to {@link MethodWrapper}.
 * Holds the list of list of rules that will be used to convert proxy methods to underlying object.
 * <p>
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
class MethodConverter {

    /**
     * Converts the {@link Method} to {@link MethodWrapper} based on rules stored in {@link #rules} list.
     * Method returns NULL if there is no applicable rule.
     *
     * @param method method to be converted
     * @return MethodWrapper or NULL if the conversion fails
     */
    @Nullable
    static MethodWrapper tryToConvert(@Nonnull Method method) {
        for (MethodConversionRule rule : rules) {
            if (rule.isApplicable(method))
                return rule.createWrapper(method);
        }
        return null;
    }

    /**
     * Ordered list of conversion rules
     */
    private static final List<MethodConversionRule> rules = new LinkedList<>();

    static {
        try {
            // method prototypes
            final Predicate<Method> hasGetRemovePrototype = method -> !method.getReturnType().equals(Void.TYPE) && method.getParameterCount() == 0;
            final Predicate<Method> hasGetRequiredPrototype = method -> !method.getReturnType().equals(Void.TYPE) && method.getParameterCount() == 0 &&
                    (method.isAnnotationPresent(Required.class) || method.getReturnType().isPrimitive());
            final Predicate<Method> hasGetDefaultPrototype = method -> !method.getReturnType().equals(Void.TYPE) && method.getParameterCount() == 1 &&
                    method.getReturnType().isAssignableFrom(method.getParameterTypes()[0]);
            final Predicate<Method> hasSetPrototype = method -> method.getReturnType().equals(Void.TYPE) && method.getParameterCount() == 1;

            // class retrievers
            final Function<Method, Class> typeFromParameter = method -> method.getParameterTypes()[0];
            final Function<Method, Class> typeFromResult = Method::getReturnType;

            // Rules definition
            rules.add(new MethodConversionRule.ExposeMethodConversionRule());
            rules.add(new MethodConversionRule.DefaultMethodConversionRule());
            rules.add(new MethodConversionRule.ForwardMethodConversionRule());

            rules.add(RuleBuilder.is(Get.class).has(hasGetDefaultPrototype).call(RecordMethodWrapper::getDefaultField).expects(typeFromResult).build());
            rules.add(RuleBuilder.is(Get.class).has(hasGetRequiredPrototype).call(RecordMethodWrapper::getRequiredField).expects(typeFromResult).requiresValue().build());
            rules.add(RuleBuilder.is(Get.class).has(hasGetRemovePrototype).call(RecordMethodWrapper::getField).expects(typeFromResult).build());

            rules.add(RuleBuilder.is(Set.class).has(hasSetPrototype).call(RecordMethodWrapper::setField).expects(typeFromParameter).modifiesInput().build());

            rules.add(RuleBuilder.is(Remove.class).has(hasGetRemovePrototype).call(RecordMethodWrapper::removeField).expects(typeFromResult).modifiesInput().build());
            //rules.add(RuleBuilder.is(Remove.class).has(hasGetRemovePrototype).call(RecordMethodWrapper::removeField).expects(typeFromResult).modifiesInput().build());

        } catch (Exception e) {
            throw new IllegalStateException("Failed to initialize MethodConverter");
        }
    }
}
