/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.classification.impl;

import messif.algorithm.Algorithm;
import messif.data.DataObject;
import messif.classification.Classifier;
import messif.operation.OperationBuilder;
import messif.operation.search.KNNOperation;
import messif.operation.search.KNNOperationApprox;
import messif.operation.answer.RankedAnswer;
import messif.operation.params.ApproximateSearch;

/**
 * Implementation of a classifier by approximate k-nearest neighbors operation.
 * The classification of an object is inferred from its most similar objects.
 * The actual classification is computed from the retrieved nearest neighbors
 * by a given {@link Classifier}.
 *
 * @param <C> the class of instances that represent the classification categories
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class ApproxKNNOperationClassifier<C> extends KNNOperationClassifier<C> {

    /** Type of the local approximation parameter used. */
    private final ApproximateSearch.Type localSearchType;
    /**
     * Value of the local approximation parameter. 
     * Its interpretation depends on the value of {@link #localSearchType}.
     */
    private final int localSearchParam;
    /**
     * Radius for which the answer is guaranteed as correct.
     * It is specified in the constructor and can influence the level of approximation.
     * An algorithm evaluating this query can also change this value, so it can
     * notify about the guarantees of evaluation.
     */
    private final float radiusGuaranteed;

    /**
     * Creates a new kNN classifier.
     * @param classifier the classifier used to compute the object classification
     * @param k the number of nearest neighbors to retrieve
     * @param localSearchParam local search parameter - typically approximation parameter
     * @param localSearchType type of the local search parameter
     * @param radiusGuaranteed radius within which the answer is required to be guaranteed as correct
     * @param algorithm the algorithm that supplies the similar objects
     * @param executedOperationParameter the name of the parameter to put the executed operation into when classifying
     */
    public ApproxKNNOperationClassifier(Classifier<? super RankedAnswer, C> classifier, int k, int localSearchParam,
                                        ApproximateSearch.Type localSearchType, float radiusGuaranteed,
                                        Algorithm algorithm, String executedOperationParameter) {
        this(classifier, k, new String [] {}, localSearchParam, localSearchType, radiusGuaranteed, algorithm, executedOperationParameter);
    }

    /**
     * Creates a new kNN classifier.
     * @param classifier the classifier used to compute the object classification
     * @param k the number of nearest neighbors to retrieve
     * @param fieldsToReturn a list of data object fields to be returned for each answer object
     * @param localSearchParam local search parameter - typically approximation parameter
     * @param localSearchType type of the local search parameter
     * @param radiusGuaranteed radius within which the answer is required to be guaranteed as correct
     * @param algorithm the algorithm that supplies the similar objects
     * @param executedOperationParameter the name of the parameter to put the executed operation into when classifying
     */
    public ApproxKNNOperationClassifier(Classifier<? super RankedAnswer, C> classifier, int k, String [] fieldsToReturn, int localSearchParam,
                                        ApproximateSearch.Type localSearchType, float radiusGuaranteed,
                                        Algorithm algorithm, String executedOperationParameter) {
        super(classifier, k, algorithm, executedOperationParameter);
        this.localSearchParam = localSearchParam;
        this.localSearchType = localSearchType;
        this.radiusGuaranteed = radiusGuaranteed;
    }

    @Override
    protected KNNOperationApprox createOperation(DataObject object) {
        return OperationBuilder.create(KNNOperationApprox.class).addParam(KNNOperation.QUERY_OBJECT_FIELD, object)
                .addParam(KNNOperationApprox.K_FIELD, k).addParam(KNNOperationApprox.APPROX_TYPE_FIELD, localSearchType)
                .addParam(KNNOperationApprox.APPROX_PARAM_FIELD, localSearchParam).addParam(KNNOperationApprox.RADIUS_GUARANTEED_FIELD, radiusGuaranteed)
                .build();
    }

}
