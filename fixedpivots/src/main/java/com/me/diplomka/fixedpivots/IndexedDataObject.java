package com.me.diplomka.fixedpivots;

import messif.data.DataObject;
import messif.record.Record;
import messif.record.RecordImpl;

import java.util.stream.Stream;

public interface IndexedDataObject extends DataObject {
    String getPivotPrefix();
    String[] getTags();

    static IndexedDataObject of(DataObject dataObject, String... pivotKeys) {
        return new IndexedDataObjectImpl(dataObject, pivotKeys);
    }

    class IndexedDataObjectImpl extends RecordImpl implements IndexedDataObject {
        private String pivotPrefix;
        private String[] tags;

        private IndexedDataObjectImpl(Record record, String... pivotKeys) {
            super(record);
            pivotPrefix = String.join("_", pivotKeys);
            var tags = getField("tags", String[].class);
            if (tags != null) {
                this.tags = Stream.of(tags)
                        .map(tag -> tag.trim().toLowerCase())
                        .toArray(String[]::new);
            } else {
                this.tags = new String[0];
            }
        }

        @Override
        public String toString() {
            return "IndexedDataObjectImpl{" +
                    "ID=" + getID() +
                    ",pivotPrefix=" + pivotPrefix +
                    '}';
        }

        @Override
        public String getPivotPrefix() {
            return pivotPrefix;
        }

        @Override
        public String[] getTags() {
            return tags;
        }
    }

}
