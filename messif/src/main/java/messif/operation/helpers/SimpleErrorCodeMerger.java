/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.helpers;

import messif.operation.answer.AbstractAnswer;
import messif.utility.ProcessingStatus;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class SimpleErrorCodeMerger<TAnswer extends AbstractAnswer> implements ErrorCodeHelper, AnswerProvider<TAnswer> {

    /**
     * Continuously updated error code to be used in the final answer.
     */
    protected ProcessingStatus status = ProcessingStatus.NOT_SET;

    protected final Class<? extends TAnswer> answerClass;

    public SimpleErrorCodeMerger(Class<? extends TAnswer> answerClass) {
        this.answerClass = answerClass;
    }

    /**
     * Update the error code of this operation from another operation.
     * If the updated operation was not successful, the value overrides this
     * operation's error code (so that the errors propagate up).
     *
     * @param answer the source operation from which to get the update
     * @throws ClassCastException if the specified operation is incompatible with this operation
     */
    @Override
    public void updateAnswer(AbstractAnswer answer) {
        if (!status.isSet() || (answer.isFinished() && !answer.wasSuccessful()))
            status = answer.getStatus();
    }

    @Override
    public TAnswer getAnswer() {
        return AbstractAnswer.createAnswer(status, answerClass);
    }

    /**
     * This method simple replaces current error code with the given code.
     * @param status
     */
    @Override
    public void updateErrorCode(ProcessingStatus status) {
        this.status = status;
    }

    public ProcessingStatus getStatus() {
        return status;
    }
}
