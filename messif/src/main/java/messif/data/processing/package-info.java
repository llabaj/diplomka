/**
 * A package that contains various processors of {@link messif.data.DataObject}; it replaces package
 *  messif.objects.extraction
 */
package messif.data.processing;

