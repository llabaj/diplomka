/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.algorithm.executor;

import messif.algorithm.Algorithm;
import messif.algorithm.AlgorithmMethodException;
import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.processing.OperationExecutor;
import messif.operation.answer.ListingAnswer;
import messif.operation.answer.RankedAnswer;
import messif.statistic.*;
import messif.utility.ExtendedProperties;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * An operation processing chain item (layer) that calculates various MESSIF statistics. It also creates a "statistics"
 * field in the answer.
 */
public class StatisticsExecutor extends ExecutorChainItem {

    public static final String CONFIG_REGEXP = "statistics.regexp";

    /**
     * Name of the answer field to store the statistics.
     */
    public static final String STAT_FIELD = "statistics";

    private String bindOperationStatsRegexp = null;

    @Override
    public void init(ExtendedProperties properties, Algorithm algorithm) throws AlgorithmMethodException {
        super.init(properties, algorithm);
        setBindOperationStatsRegexp(properties.getProperty(CONFIG_REGEXP, bindOperationStatsRegexp));
    }

    @Override
    public AbstractAnswer execute(AbstractOperation operation, Iterator<OperationExecutor> iterator) throws Exception {
        StatisticTimer operationTime = OperationStatistics.getLocalThreadStatistics().getOpStatistics("OperationTime", StatisticTimer.class);
        operationTime.start();
        AbstractAnswer answer = null;
        try {
            statisticsBeforeOperation();
            answer = executeNext(operation, iterator);
            return answer;
        } finally {
            operationTime.stop();
            if (answer != null) {
                statisticsAfterOperation(answer);
            }
        }
    }

    public void setBindOperationStatsRegexp(String regexp) {
        bindOperationStatsRegexp = regexp;
    }

    /**
     * This method can be used by all algorithms before processing any operation to set default (operation) statistics.
     *
     * @throws ClassCastException if new statistic cannot be created
     */
    public void statisticsBeforeOperation() throws ClassCastException {
        OperationStatistics.resetLocalThreadStatistics();
        if (bindOperationStatsRegexp != null) {
            OperationStatistics.getLocalThreadStatistics().registerBoundAllStats(bindOperationStatsRegexp);
        } else {
            OperationStatistics.getLocalThreadStatistics().registerBoundStat(StatisticCounter.class, "DistanceComputations", "DistanceComputations");
            OperationStatistics.getLocalThreadStatistics().registerBoundStat(StatisticCounter.class, "DistanceComputations.Savings", "DistanceComputations.Savings");
            OperationStatistics.getLocalThreadStatistics().registerBoundStat(StatisticCounter.class, "BlockReads", "BlockReads");
        }
    }

    /**
     * This method can be used by all algorithms after processing any operation to set default (operation) statistics.
     * The results are right only when the {@link #statisticsBeforeOperation() } method was used before.
     *
     * @param answer (typically search) operation that was just processed
     */
    public void statisticsAfterOperation(@Nonnull AbstractAnswer answer) {
        StatisticCounter accessedObjects = OperationStatistics.getOpStatisticCounter("AccessedObjects");
        accessedObjects.set(OperationStatistics.getOpStatisticCounter("DistanceComputations").get()
                + OperationStatistics.getOpStatisticCounter("DistanceComputations.Savings").get());
        if (answer instanceof ListingAnswer) {
            OperationStatistics.getOpStatisticCounter("AnswerCount").set(((ListingAnswer) answer).getAnswerCount());
            if ((answer instanceof RankedAnswer) && (((RankedAnswer) answer).getAnswerCount() > 0)) {
                final float[] answerDistances = ((RankedAnswer) answer).getAnswerDistances();
                OperationStatistics.getLocalThreadStatistics().getStatistics("AnswerDistance", StatisticObject.class).set(answerDistances[answerDistances.length - 1]);
            }
        }
        OperationStatistics.getLocalThreadStatistics().unbindAllStats();

        // create the statistics JSON object to be added to the answer
        Map<String, Object> answerStats = (Map<String, Object>) answer.getParams().getField(STAT_FIELD, Map.class, new HashMap<>());
        final Iterator<Statistics<?>> allStatistics = OperationStatistics.getLocalThreadStatistics().getAllStatistics();
        while (allStatistics.hasNext()) {
            final Statistics<?> stat = allStatistics.next();
            answerStats.put(stat.getName(), stat.getValue());
        }
        answer.getParams().setField(STAT_FIELD, answerStats);
    }

}
