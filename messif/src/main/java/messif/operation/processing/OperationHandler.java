/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.processing;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */

import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.record.ModifiableRecord;
import messif.record.ModifiableRecordImpl;
import messif.utility.ProcessingStatus;
import messif.utility.proxy.ProxyConverter;
import messif.record.Record;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class OperationHandler implements OperationEvaluator, FieldMapEvaluator {

    /**
     * A list of {@link ProcessorConfiguration}s managed by this handler. Given an operation in method
     * {@link #evaluate(AbstractOperation)}, the operation's CLASS TYPE is checked according to configurations
     * {@link ProcessorConfiguration#getOpClass()}
     * and the first matching processor configuration is used to process the operation.
     */
    private final List<ProcessorConfiguration<? extends AbstractOperation, ? extends AbstractAnswer>> opClassConfigs = new ArrayList<>();

    /**
     * A map of {@link ProcessorConfiguration}s for individual {@link AbstractOperation#TYPE_FIELD} strings. This
     * map is used ONLY if the operation is executed via {@link #evaluate(Record)}.
     */
    private final Map<String, ProcessorConfiguration<? extends AbstractOperation, ? extends AbstractAnswer>> typeStringConfigs = new HashMap<>();


    private final List<OperationExecutor> executors;

    /**
     * Creates the {@link OperationHandler} object given executors to be applied before and after actual operation
     * processing.
     *
     * @param executors list of executors to be applied before the operation is actually processed
     */
    public OperationHandler(OperationExecutor... executors) {
        this.executors = new ArrayList<>(Arrays.asList(executors));
        this.executors.add(createTerminateExecutor());
    }

    public void register(ProcessorConfiguration<? extends AbstractOperation, ? extends AbstractAnswer> configuration) {
        // register the configuration for processing using {@link #evaluate(Record)}
        for (String typeString : configuration.getTypeStrings()) {
            typeStringConfigs.put(typeString, configuration);
        }

        boolean inserted = false;
        for (int i = 0; i < opClassConfigs.size(); i++) {
            // insert the new class right before its predecessor in the current list
            if (opClassConfigs.get(i).getOpClass().isAssignableFrom(configuration.getOpClass())) {
                // if the inserted class already IS in the list, replace it
                if (opClassConfigs.get(i).getOpClass().equals(configuration.getOpClass())) {
                    opClassConfigs.set(i, configuration);
                } else {
                    opClassConfigs.add(i, configuration);
                }
                inserted = true;
                break;
            }
        }
        if (!inserted) {
            opClassConfigs.add(configuration);
        }
    }

    public Collection<ProcessorConfiguration> getSupportedOperations() {
        return opClassConfigs.stream().collect(Collectors.toList());
    }

    @Override
    @Nullable
    public AbstractAnswer evaluate(AbstractOperation operation) throws Exception {
        if (findConfiguration(operation) == null) {
            return null;
        }
        return run(operation);
    }

    @Override
    @Nullable
    public AbstractAnswer evaluate(Record record) throws Exception {
        String type = record.getRequiredField(AbstractOperation.TYPE_FIELD, String.class);
        ProcessorConfiguration<? extends AbstractOperation, ?> configuration = typeStringConfigs.get(type);
        if (configuration == null) {
            return null;
        }

        Class<? extends AbstractOperation> operationClass = configuration.getOpClass();
        ModifiableRecord modifiableRecord = record instanceof ModifiableRecord ? (ModifiableRecord) record : new ModifiableRecordImpl(record);
        AbstractOperation operation = ProxyConverter.convert(modifiableRecord, operationClass);
        return run(operation);
    }

    private AbstractAnswer run(AbstractOperation operation) throws Exception {
        Iterator<OperationExecutor> iterator = executors.iterator();
        return iterator.next().execute(operation, iterator);
    }

    private OperationExecutor createTerminateExecutor() {
        return new OperationExecutor() {
            @Override
            @Nonnull
            public AbstractAnswer execute(AbstractOperation operation, Iterator<OperationExecutor> iterator) throws Exception {
                ProcessorConfiguration<AbstractOperation, ?> configuration = findConfiguration(operation);
                if (configuration == null) {
                    return AbstractAnswer.createAnswer(new ProcessingStatus(AbstractAnswer.ABORTED_BY_ERROR, "there is no executor for this operation"));
                }

                // call the actual processor using given executor (sequential, parallel)
                final AbstractAnswer retVal = configuration.getExecutor().execute(configuration.initProcessor(operation));

                if (retVal == null) {
                    return AbstractAnswer.createAnswer(new ProcessingStatus(AbstractAnswer.ABORTED_BY_ERROR, "empty answer object returned"));
                }
                return retVal;
            }
        };
    }

    /**
     * Find the first configuration which is defined for this operation class or its super class.
     */
    @SuppressWarnings("unchecked")
    @Nullable
    private ProcessorConfiguration<AbstractOperation, ?> findConfiguration(AbstractOperation operation) {
        for (ProcessorConfiguration<? extends AbstractOperation, ?> configuration : opClassConfigs) {
            if (configuration.getOpClass().isAssignableFrom(operation.getClass())) {
                return (ProcessorConfiguration<AbstractOperation, ?>) configuration;
            }
        }
        return null;
    }
}