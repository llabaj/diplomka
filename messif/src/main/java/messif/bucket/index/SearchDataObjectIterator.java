/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.bucket.index;

import messif.data.DataObject;
import messif.data.util.DataObjectIterator;
import messif.data.util.DataObjectList;
import messif.utility.SortedCollection;

import java.util.*;

/**
 * Provides a bridge between {@link Search} and {@link DataObjectIterator}.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
//  * @param <T> the class of the iterated objects
public class SearchDataObjectIterator extends DataObjectIterator {

    //****************** Attributes ******************//

    /** Wrapped index instance for providing search */
    private final Index<DataObject> index;
    /** Wrapped search instance */
    protected Search<DataObject> search;
    /** Flag for remembering if next() has been called on <code>search</code> and its result */
    protected int hasNext;
    /** Maximal number of iterations */
    private final int limit;
    /** Current number of iterations */
    private int count;

    //****************** Constructor ******************//

    /**
     * Creates a new instance of SearchDataObjectIterator for the specified {@link Search} instance.
     * @param search the {@link Search} instance to wrap by this iterator
     * @param limit limit the number of iterations (use -1 for unlimited number of iterations)
     */
    public SearchDataObjectIterator(Search<DataObject> search, int limit) {
        if (search == null)
            throw new NullPointerException("Search cannot be null");
        this.index = null;
        this.search = search;
        this.hasNext = -1;
        this.limit = limit;
        this.count = 0;
    }

    /**
     * Creates a new instance of SearchDataObjectIterator for the specified {@link Index} instance.
     * @param index the {@link Index} instance the search of which is wrapped by this iterator
     * @param limit limit the number of iterations (use -1 for unlimited number of iterations)
     */
    public SearchDataObjectIterator(Index<DataObject> index, int limit) {
        if (index == null)
            throw new NullPointerException("Index cannot be null");
        this.index = index;
        this.search = null;
        this.hasNext = -1;
        this.limit = limit;
        this.count = 0;
    }

    /**
     * Creates a new instance of SearchDataObjectIterator for the specified {@link Search} instance.
     * @param search the {@link Search} instance to wrap by this iterator
     */
    public SearchDataObjectIterator(Search<DataObject> search) {
        this(search, -1);
    }

    /**
     * Creates a new instance of SearchDataObjectIterator for the specified {@link Index} instance.
     * @param index the {@link Index} instance the search of which is wrapped by this iterator
     */
    public SearchDataObjectIterator(Index<DataObject> index) {
        this(index, -1);
    }


    //****************** Attribute access methods ******************//

    /**
     * Returns the current number of iterations.
     * @return the current number of iterations
     */
    public int getCount() {
        return count;
    }

    /**
     * Returns the maximal number of iterations; -1 means unlimited.
     * @return the maximal number of iterations
     */
    public int getLimit() {
        return limit;
    }

    /**
     * Returns <tt>true</tt> if the current number of iterations has reached its maximum.
     * @return <tt>true</tt> if the current number of iterations has reached its maximum
     */
    public final boolean isLimitReached() {
        return (limit != -1) && (count >= limit);
    }


    //****************** Overrides ******************//

    @Override
    public DataObject getCurrentObject() {
        if (search == null)
            throw new NoSuchElementException("There is no current object");
        return search.getCurrentObject();
    }

    @Override
    public boolean hasNext() {
        // Check limit
        if (isLimitReached())
            return false;

        // If the next was called, thus hasNext is not decided yet
        if (hasNext == -1) {
            if (search == null)
                search = index.search();
            hasNext = search.next()?1:0; // Perform search
        }

        return hasNext == 1;
    }

    @Override
    public DataObject next() throws NoSuchElementException {
        if (!hasNext())
            throw new NoSuchElementException("There are no more objects");
        hasNext = -1;
        count++;
        
        return getCurrentObject();
    }
    
    @Override
    public void remove() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("This iterator does not support removal");
    }

    @Override
    public DataObject getObjectByAnyID(Set<String> locatorURIs, boolean removeFound) throws NoSuchElementException {
        if (search == null)
            search = index.search(LocalAbstractObjectOrder.idToObjectComparator, locatorURIs);
        return super.getObjectByAnyID(locatorURIs, removeFound);
    }

    @Override
    public List<DataObject> getRandomObjects(int count, boolean unique) {
        if (search != null)
            return super.getRandomObjects(count, unique);

        // No search yet, get full search on the whole index
        int size = index.size();
        DataObjectList ret = new DataObjectList(count);
        search = index.search();
        try {
            Random generator = new Random();
            Collection<Integer> positions = new SortedCollection<Integer>(count);
            while (positions.size() < count)
                positions.add(generator.nextInt(size));
            int lastpos = 0;
            for (Integer position : positions) {
                search.skip(position - lastpos);
                ret.add(search.getCurrentObject());
                lastpos = position;
            }
        } finally {
            search.close();
            search = null;
        }
        return ret;
    }

}
