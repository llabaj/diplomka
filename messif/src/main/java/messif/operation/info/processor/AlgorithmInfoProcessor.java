/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.info.processor;

import messif.algorithm.Algorithm;
import messif.operation.info.AlgorithmInfoOperation;
import messif.operation.info.answer.InformationAnswer;
import messif.operation.processing.impl.OneStepProcessor;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class AlgorithmInfoProcessor extends OneStepProcessor<AlgorithmInfoOperation, InformationAnswer> {

    private final Algorithm algorithm;

    private String answerString;

    /**
     * Creates this processor given the operation to be processed.
     * @param operation operation to be executed
     * @param algorithm algorithm to run the operation on
     */
    public AlgorithmInfoProcessor(AlgorithmInfoOperation operation, Algorithm algorithm) {
        super(operation);
        this.algorithm = algorithm;
    }

    @Override
    protected void process() throws Exception {
        answerString = algorithm.toString();
    }

    @Override
    public InformationAnswer finish() {
        return InformationAnswer.createSuccessful(answerString);
    }
}
