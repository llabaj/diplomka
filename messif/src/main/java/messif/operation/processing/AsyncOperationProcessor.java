/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.processing;

import messif.algorithm.AlgorithmMethodException;
import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;

import javax.annotation.Nullable;
import java.util.concurrent.Callable;

/**
 * Extension of the {@link OperationProcessor} that supports asynchronous execution
 * of the processing steps via {@link Callable}s. If this interface is implemented,
 * each step of the processor must be independent and thread-safe so that
 * any parallelization can be used to process the steps. Synchronization can be used
 * when implementing by blocking the {@link #processStepAsynchronously()} method.
 *
 * <p>
 * Implementation of this interface must ensure equivalency of the thread processing
 * by calling {@link #processStepAsynchronously()} and sequential processing by calling
 * {@link #processStep()}.
 * </p>
 *
 * @param <TRequest> the type of the operation that are processed by this processor
 * @param <TAnswer> type of answer returned by this processor
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface AsyncOperationProcessor<TRequest extends AbstractOperation, TAnswer extends AbstractAnswer>
        extends OperationProcessor<TRequest, TAnswer> {

    /**
     * Returns a {@link Callable} that allows to execute the next processing step asynchronously.
     *  Note that this method may block if necessary. The evaluation of the {@Callable} can change the state of the
     *  processor in such a way that the subsequent call of {{@link #processStepAsynchronously()}} returns another
     *  {@Callable} even if the previous call returned <code>null</code>.
     * @return a {@link Callable} for next step or <tt>null</tt> if there are no more steps.
     * @throws InterruptedException if the processing thread was interrupted
     * @throws AlgorithmMethodException if the processing fails
     */
    @Nullable
    Callable<Void> processStepAsynchronously() throws InterruptedException, AlgorithmMethodException;

}
