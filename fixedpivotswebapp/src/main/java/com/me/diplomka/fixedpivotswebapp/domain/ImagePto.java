package com.me.diplomka.fixedpivotswebapp.domain;

import messif.data.DataObject;

public class ImagePto {
    private String id;
    private String title;
    private String[] tags;
    private String imgUrl;
    private float distance;

    public static ImagePto fromDataObject(DataObject dataObject, float distance) {
        var url = dataObject.getField("url", String.class);
        url = (url != null) ? "http://localhost:9914" + url : "http://disa.fi.muni.cz/profimedia/images/" + String.format("%010d", Integer.valueOf(dataObject.getID()));
        return new ImagePto(dataObject.getID(),
                dataObject.getField("title", String.class),
                dataObject.getField("tags", String[].class),
                url,
                distance);
    }

    public ImagePto(String id, String title, String[] tags, String imgUrl, float distance) {
        this.id = id;
        this.title = title;
        this.tags = tags != null ? tags : new String[0];
        this.imgUrl = imgUrl;
        this.distance = distance;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String[] getTags() {
        return tags;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public float getDistance() {
        return distance;
    }
}
