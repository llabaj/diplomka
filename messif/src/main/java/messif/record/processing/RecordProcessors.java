/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.record.processing;

import messif.record.Record;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class RecordProcessors {

    /**
     * Encapsulates a list of processors as a single processor which send a record through the whole processor chain.
     *  There is no enter condition to the whole chain, each processor has it's own condition. If the record does not
     *  match the condition, the processor is SKIPPED and the record is sent further to the next processor.
     * @param processors a list (chain) of processors to be merged
     * @param <RecordType>
     * @return
     */
    public static <RecordType extends Record> RecordProcessor<RecordType> chainProcessors(final RecordProcessor<RecordType> ... processors) {
        if (processors == null || processors.length == 0) {
            return RecordProcessor.IDENTITY;
        }
        StringBuilder stringBuilder = new StringBuilder();
        List<String> fields = new ArrayList<>();
        for (RecordProcessor<RecordType> processor : processors) {
            stringBuilder.append(processor.preConditionDescription()).append(" OR ");
            fields.addAll(Arrays.asList(processor.modifiedFields()));
        }
        final String description = stringBuilder.deleteCharAt(stringBuilder.length() - 4).toString();
        final String [] modifiedFields = fields.toArray(new String [fields.size()]);

        return new RecordProcessor<RecordType>() {

            @Override
            public RecordType process(RecordType record) throws RecordProcessingException {
                for (RecordProcessor<RecordType> processor : processors) {
                    if (processor.preCondition(record)) {
                        record = processor.process(record);
                    }
                }
                return record;
            }

            @Override
            public List<RecordType> process(List<RecordType> records) throws RecordProcessingException {
                List<RecordType> retVal = new ArrayList<>(records);
                List<RecordType> toProcess = new ArrayList<>(records.size());

                for (RecordProcessor<RecordType> processor : processors) {
                    toProcess.clear();
                    for (Iterator<RecordType> it = retVal.iterator(); it.hasNext(); ) {
                        RecordType current = it.next();
                        if (processor.preCondition(current)) {
                            toProcess.add(current);
                            it.remove();
                        }
                    }
                    retVal.addAll(processor.process(toProcess));
                }
                return retVal;
            }

            @Override
            public boolean preCondition(final RecordType record) {
                return Arrays.stream(processors).anyMatch(p -> p.preCondition(record));
            }

            @Override
            public String preConditionDescription() {
                return description;
            }

            @Override
            public String[] modifiedFields() {
                return modifiedFields;
            }
        };
    }


    public final static String FILE_FIELD = "_file";

    public final static String URL_FIELD = "_url";

    // TODO: let the binary source read the _base64 data from the JSON

    /**
     * Creates a binary input stream from a field {@link #FILE_FIELD file}, a {@link #URL_FIELD URL} (in this order).
     * @param record record to read the file name or URL from
     * @return the binary stream with the file or URL-specified file
     */
    public static BinaryDataSource openBinarySource(Record record) throws IOException {
        return openBinarySource(record, FILE_FIELD, URL_FIELD);
    }

    /**
     * Creates a binary input stream from a file, a URL (remote file) or, in general, also from any other data source
     *  the {@link BinaryDataSource} can work with (like {@code byte[]} or {@link java.io.InputStream}.
     *
     * @param record record to read the file name or URL from
     * @param fieldNames field names to try in given order
     * @return the binary stream with the file or URL-specified file
     * @throws IOException
     */
    public static BinaryDataSource openBinarySource(Record record, final String ... fieldNames) throws IOException {
        if (fieldNames == null) {
            return null;
        }
        // for every suggested fields
        for (String fieldName : fieldNames) {
            // try if it exists and convert it into File or URL if it is a String
            Object field = record.getField(fieldName);
            if (field == null) {
                continue;
            }
            if (field instanceof String) {
                File file = new File((String) field);
                if (file.exists())
                    field = file;
                else {
                    field = new URL((String) field);
                }
            }
            return BinaryDataSource.openBinarySource(field);
        }
        return null;
    }

}
