/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.serialization;

import messif.serialization.io.BinaryInput;
import messif.serialization.io.BinaryOutput;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * This is the simple serializator implementation for {@link BinarySerializable} objects.
 * It can store and restore only one specified class or the standard Java-serialized objects.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class MultiClassSerializator extends ObjectSerializator {
    /** class serial id for serialization */
    private static final long serialVersionUID = 1L;

    protected final Map<Byte,ClassReaderWriter> objectReadWriters;

    protected final Map<Class,Byte> readWriteClasses;
    
    //************************ Constructor ************************//

    /**
     * Create a new instance of BinarySerializator.
     * 
     * @param objectReadWriters
     */
    public MultiClassSerializator(Map<Byte,ClassReaderWriter> objectReadWriters) {
        this.objectReadWriters = objectReadWriters;
        this.readWriteClasses = new HashMap<>();
        for (Map.Entry<Byte, ClassReaderWriter> readWriter : objectReadWriters.entrySet()) {
            readWriteClasses.put(readWriter.getValue().getReadWriteClass(), readWriter.getKey());
        }
    }

    //************************ Serializator methods ************************//

    
    @Override
    protected byte getObjectTypeByte(Object object) {
        if ((object != null)) {
            Byte typeByte = readWriteClasses.get(object.getClass());
            if (typeByte != null) {
                return typeByte;
            }
        }
        return super.getObjectTypeByte(object);
    }
    
    @Override
    protected int write(BinaryOutput output, Object object, byte typeByte) throws BinarySerializationException, IOException {
        final ClassReaderWriter classReaderWriter = objectReadWriters.get(typeByte);
        if (classReaderWriter != null) {
            return classReaderWriter.writeBinary(object, output, this);
        }
        return super.write(output, object, typeByte);
    }
    
    @Override
    protected Object readObject(BinaryInput input, byte typeByte) throws IOException, BinarySerializationException {
        ClassReaderWriter reader = objectReadWriters.get(typeByte);
        if (reader == null) {
            return super.readObject(input, typeByte);
        }
        return reader.readBinary(input, this);
    }
}
