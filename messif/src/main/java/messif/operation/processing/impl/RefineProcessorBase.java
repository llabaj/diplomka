/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.processing.impl;

import messif.algorithm.AlgorithmMethodException;
import messif.data.DataObject;
import messif.distance.DistanceFunc;
import messif.operation.answer.RankedAnswer;
import messif.operation.params.CandidateIDQueue;
import messif.operation.processing.AsyncOperationProcessor;
import messif.operation.search.RankingOperation;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.stream.Stream;

/**
 * Base of processor to refine a candidate set for any (approximate) query-object operation.
 */
public abstract class RefineProcessorBase
        extends SimpleOperationProcessor<RankingOperation, RankedAnswer>
        implements AsyncOperationProcessor<RankingOperation, RankedAnswer> {

    protected final BlockingQueue<String> idQueue;

    // creator of the answer
    final RankingOperation.Helper helper;

    /**
     * Creates this processor given a query operation and a distance to be used if the query does not contain
     * a distance itself.
     *
     * @param query
     * @param defaultDistanceFunc
     */
    protected RefineProcessorBase(RankingOperation query, CandidateIDQueue candidateIDQueue, DistanceFunc<DataObject> defaultDistanceFunc) {
        super(query);
        this.idQueue = candidateIDQueue.getIDQueue();
        if (this.idQueue == null) {
            throw new IllegalArgumentException("the query to be processed by the " + RefineProcessorBase.class.getSimpleName()
                    + " processor must have field " + CandidateIDQueue.ID_QUEUE_PARAM);
        }
        helper = RankingOperation.getHelper(query, defaultDistanceFunc);
    }

    @Override
    public boolean processStep() throws InterruptedException, AlgorithmMethodException, CloneNotSupportedException {
        if (canFinish) {
            return false;
        }
        // starts the refinement of all IDs in the stream (sequential or parallel depends on the stream).
        getIDStream().forEach((id) -> {
            helper.addToAnswer(getObject(id));
        });
        return true;
    }

    @Override
    public Callable<Void> processStepAsynchronously() throws InterruptedException {
        if (canFinish) {
            return null;
        }
        final Stream<String> idStream = getIDStream();
        return () -> {
            idStream.parallel().forEach((id) -> {
                helper.addToAnswer(getObject(id));
            });
            return null;
        };
    }

    private Stream<String> getIDStream() {
        final ArrayList<String> currentlyAvailableIDs = new ArrayList<>();
        try {
            currentlyAvailableIDs.add(idQueue.take());
            idQueue.drainTo(currentlyAvailableIDs);
            if (CandidateIDQueue.END_OF_STREAM.equals(currentlyAvailableIDs.get(currentlyAvailableIDs.size() - 1))) {
                currentlyAvailableIDs.remove(currentlyAvailableIDs.size() - 1);
                canFinish = true;
            }
        } catch (InterruptedException e) {
            canFinish = true;
        }
        return currentlyAvailableIDs.stream();
    }

    @Override
    public RankedAnswer finish() {
        return helper.getAnswer();
    }

    /** This is the main (only) method to be implemented by a non-abstract refine processor. */
    protected abstract DataObject getObject(@Nonnull String id);

}
