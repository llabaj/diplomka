/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.algorithm.impl;

import messif.algorithm.Algorithm;
import messif.operation.crud.DeleteOperation;
import messif.operation.crud.InsertOperation;
import messif.operation.crud.UpdateOperation;
import messif.operation.processing.ProcessorConfiguration;
import messif.operation.processing.impl.AlgorithmsOperationProcessor;
import messif.operation.answer.ListingAnswer;
import messif.utility.ExtendedProperties;

import java.util.*;

/**
 * Indexing algorithm that processes operation by passing them to encapsulated collection
 * of algorithms.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class MultipleOverlaysAlgorithm extends Algorithm {
    /**
     * Class id for Java serialization.
     */
    private static final long serialVersionUID = 2L;

    /**
     * Internal list of algorithms on which the operations are actually processed
     */
    private final List<Algorithm> algorithms;

    /**
     * Creates a new multi-algorithm overlay for the given collection of algorithms.
     *
     * @param algName    string name of this algorithm to be used instead of the standard string
     * @param algorithms the algorithms on which the operations are processed
     */
    public MultipleOverlaysAlgorithm(String algName, Collection<? extends Algorithm> algorithms, ExtendedProperties configuration) {
        super(algName, configuration);
        this.algorithms = new ArrayList<>(algorithms);
    }

    /**
     * Creates a new multi-algorithm overlay for the given collection of algorithms.
     *
     * @param algName    string name of this algorithm to be used instead of the standard string
     * @param algorithms the algorithms on which the operations are processed
     */
    public MultipleOverlaysAlgorithm(String algName, Collection<? extends Algorithm> algorithms) {
        super(algName);
        this.algorithms = new ArrayList<>(algorithms);
    }

    /**
     * Creates a new multi-algorithm overlay for the given collection of algorithms.
     *
     * @param algorithms the algorithms on which the operations are processed
     */
    public MultipleOverlaysAlgorithm(Collection<? extends Algorithm> algorithms) {
        this("Multiple overlay algorithm on: " + algorithms.size() + " algorithms", algorithms);
    }


    /**
     * Creates a new multi-algorithm overlay for the given collection of algorithms.
     *
     * @param algorithms the algorithms on which the operations are processed
     */
    public MultipleOverlaysAlgorithm(Collection<? extends Algorithm> algorithms, ExtendedProperties configuration) {
        this("Multiple overlay algorithm on: " + algorithms.size() + " algorithms", algorithms, configuration);
    }

    /**
     * Creates a new multi-algorithm overlay for the given collection of algorithms.
     *
     * @param algorithms the algorithms on which the operations are processed
     */
        @AlgorithmConstructor(description = "Constructor with created algorithms", arguments = {"array of running algorithms", "properties config file (e.g. with executors)"})
    public MultipleOverlaysAlgorithm(Algorithm[] algorithms, ExtendedProperties configuration) {
        this(Arrays.asList(algorithms), configuration);
    }

    /**
     * Creates a new multi-algorithm overlay for the given collection of algorithms.
     *
     * @param algorithms the algorithms on which the operations are processed
     */
    @AlgorithmConstructor(description = "Constructor with created algorithms", arguments = {"array of running algorithms"})
    public MultipleOverlaysAlgorithm(Algorithm[] algorithms) {
        this(Arrays.asList(algorithms));
    }

//    /**
//     * Creates a new multi-algorithm overlay for an empty list of algorithms (they can be added later).
//     */
//    public MultipleOverlaysAlgorithm(String algName) {
//        this(algName, Collections.EMPTY_LIST);
//    }

    @Override
    public synchronized boolean init() {
        for (Algorithm algorithm : algorithms) {
            if (algorithm != this)
                algorithm.init();
        }
        return super.init();
    }

    @Override
    @SuppressWarnings({"FinalizeDeclaration", "FinalizeCalledExplicitly"})
    public void finalize() throws Throwable {
        for (Algorithm algorithm : algorithms)
            algorithm.finalize();
        super.finalize();
    }

    @Override
    protected void registerProcessors() {
        super.registerProcessors();
        this.handler.register(ProcessorConfiguration.create(InsertOperation.class, ListingAnswer.class)
                .forTypeStrings(InsertOperation.TYPE_STRING)
                .withProcessor(operation -> new AlgorithmsOperationProcessor<>(operation, new InsertOperation.Helper(operation), algorithms))
                .executeParallel(threadPool).build());
        this.handler.register(ProcessorConfiguration.create(DeleteOperation.class, ListingAnswer.class)
                .forTypeStrings(DeleteOperation.TYPE_STRING)
                .withProcessor(operation -> new AlgorithmsOperationProcessor<>(operation, new DeleteOperation.Helper(operation), algorithms))
                .executeParallel(threadPool).build());
        this.handler.register(ProcessorConfiguration.create(UpdateOperation.class, ListingAnswer.class)
                .forTypeStrings(UpdateOperation.TYPE_STRING)
                .withProcessor(operation -> new AlgorithmsOperationProcessor<>(operation, new UpdateOperation.Helper(operation), algorithms))
                .executeParallel(threadPool).build());

        // this is a fallback for all distance-based single-query search operations
//        this.handler.register(ProcessorConfiguration.create(QueryObjectOperation.class, RankedAnswer.class)
//                .forTypeStrings(KNNOperation.TYPE_STRING, RangeOperation.TYPE_STRING, KNNRangeOperation.TYPE_STRING)
//                .withProcessor(operation -> new AlgorithmsOperationProcessor(operation,
    //                        new RankedAnswer.Builder<>((QueryObjectOperation) operation, null, RankedAnswer.class), algorithms))
//                .executeParallel(threadPool).build());
    }

    /**
     * Adds given algorithm to the list of managed algorithms.
     * @param algorithm algorithm to add
     */
    public void addAlgorithm(Algorithm algorithm) {
        algorithms.add(algorithm);
    }

    /**
     * Returns the number of the currently encapsulated algorithms.
     *
     * @return the number of the currently encapsulated algorithms
     */
    public int getAlgorithmsCount() {
        return algorithms.size();
    }

    /**
     * Returns all the currently encapsulated algorithms.
     *
     * @return all the currently encapsulated algorithms
     */
    protected Collection<Algorithm> getAlgorithms() {
        return Collections.unmodifiableCollection(algorithms);
    }

    @Override
    public String toString() {
        return getName();
    }
}
