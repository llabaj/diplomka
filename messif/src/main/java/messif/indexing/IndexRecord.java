/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.indexing;

import messif.data.DataObject;
import messif.distance.DistanceFunc;
import messif.statistic.StatisticCounter;
import messif.utility.json.JSONWritable;

import java.util.Collection;
import java.util.Map;

/**
 * Interface for fields added to the {@link messif.data.DataObject} by an index (algorithm).
 */
public interface IndexRecord extends JSONWritable {

    // region ******************        Statistics      ******************

    /** Global counter for distance computations (any purpose) */
    //StatisticCounter counterDistanceComputations = StatisticCounter.getStatistics("DistanceComputations");

    /** Global counter for lower-bound distance computations (any purpose) */
    StatisticCounter counterLowerBoundDistanceComputations = StatisticCounter.getStatistics("DistanceComputations.LowerBound");

    /** Global counter for upper-bound distance computations (any purpose) */
    StatisticCounter counterUpperBoundDistanceComputations = StatisticCounter.getStatistics("DistanceComputations.UpperBound");

    /** Global counter for saving distance computations by using precomputed */
    StatisticCounter counterPrecomputedDistanceSavings = StatisticCounter.getStatistics("DistanceComputations.Savings");
    // endregion

    /**
     * Returns a set of additional field names present in this object.
     * @return a set of additional field names present in this object
     */
    Collection<String> getIndexFieldNames();

    /**
     * Returns whether a field with the given {@code name} exists in this parametric object.
     * @param name the name of the additional field to get
     * @return <tt>true</tt> if this object contains a field of the given {@code name} or <tt>false</tt> if it does not
     */
    boolean containsIndexField(String name);

    /**
     * Returns an additional field with the given {@code name}.
     * @param name the name of the additional field to get
     * @return the value of the field {@code name} or <tt>null</tt> if it is not set
     */
    Object getIndexField(String name);

    /**
     * Returns an additional field with the given {@code name}.
     * If the field is not set or is not an instance of {@code fieldClass},
     * the {@code defaultValue} is returned instead.
     *
     * @param <T> the class of the field
     * @param name the name of the additional field to get
     * @param fieldClass the class of the field to get
     * @return the field value or null (in case it does not exist or it does not extend the given class)
     */
    <T> T getIndexField(String name, Class<? extends T> fieldClass);

    Object setIndexField(String name, Object value);

    Object removeIndexField(String name);

    /**
     * Returns the map of all additional fields.
     * Note that the map is not modifiable.
     * @return the map of additional fields
     */
    Map<String, Object> getIndexMap();


    // region ***********************    Excluding & including using any filter

    /**
     * Returns the precomputed distance to an object.
     * If there is no distance associated with the object <tt>obj</tt>
     * the function returns {@link DistanceFunc#UNKNOWN_DISTANCE UNKNOWN_DISTANCE}.
     *
     * @param obj the object for which the precomputed distance is returned
     * @return the precomputed distance to an object
     */
    default float getPrecomputedDistance(DataObject obj) {
        for (String indexFieldName : getIndexFieldNames()) {
            final PrecomputedDistancesFilter filter = getIndexField(indexFieldName, PrecomputedDistancesFilter.class);
            if (filter == null)
                continue;

            float distance = filter.getPrecomputedDistance(obj);
            if (distance != DistanceFunc.UNKNOWN_DISTANCE) {
                counterPrecomputedDistanceSavings.add();
                return distance;
            }
        }
        return DistanceFunc.UNKNOWN_DISTANCE;
    }

    /**
     * Returns <tt>true</tt> if the <code>obj</code> has been excluded (filtered out) using stored precomputed distance.
     * Otherwise returns <tt>false</tt>, i.e. when <code>obj</code> must be checked using original distance (see {@link DistanceFunc#getDistance}).
     *
     * In other words, method returns <tt>true</tt> if <code>this</code> object and <code>obj</code> are more distant than <code>radius</code>. By
     * analogy, returns <tt>false</tt> if <code>this</code> object and <code>obj</code> are within distance <code>radius</code>. However, both this cases
     * use only precomputed distances. Thus, the real distance between <code>this</code> object and <code>obj</code> can be greater
     * than <code>radius</code> although the method returned <tt>false</tt>!
     * @param obj the object to check the distance for
     * @param radius the radius between <code>this</code> object and <code>obj</code> to check
     * @return <tt>true</tt> if the <code>obj</code> has been excluded (filtered out) using stored precomputed distance
     */
    default boolean excludeUsingPrecompDist(IndexObject obj, float radius) {
        for (String indexFieldName : getIndexFieldNames()) {
            final PrecomputedDistancesFilter filter1 = getIndexField(indexFieldName, PrecomputedDistancesFilter.class);
            final PrecomputedDistancesFilter filter2 = obj.getIndexField(indexFieldName, PrecomputedDistancesFilter.class);
            if (filter1 == null || filter2 == null)
                continue;

            // Test for the same classes
            if (! filter1.getClass().equals(filter2.getClass()))
                throw new IllegalArgumentException("Trying to use different filters to filter an object: thisFilter=" + filter1.getClass() + " objectFilter=" + filter2.getClass());
            if (filter1.excludeUsingPrecompDist(filter2, radius)) {
                counterPrecomputedDistanceSavings.add();
                return true;
            }
        }

        return false;
    }

    /**
     * Returns <tt>true</tt> if the <code>obj</code> has been included using stored precomputed distance.
     * Otherwise returns <tt>false</tt>, i.e. when <code>obj</code> must be checked using original distance (see {@link DistanceFunc#getDistance}).
     *
     * In other words, method returns <tt>true</tt> if the distance of <code>this</code> object and <code>obj</code> is below the <code>radius</code>.
     * By analogy, returns <tt>false</tt> if <code>this</code> object and <code>obj</code> are more distant than <code>radius</code>.
     * However, both this cases use only precomputed distances. Thus, the real distance between <code>this</code> object and
     * <code>obj</code> can be lower than <code>radius</code> although the method returned <tt>false</tt>!
     * @param obj the object to check the distance for
     * @param radius the radius between <code>this</code> object and <code>obj</code> to check
     * @return <tt>true</tt> if the obj has been included using stored precomputed distance
     */
    default boolean includeUsingPrecompDist(IndexObject obj, float radius) {
        for (String indexFieldName : getIndexFieldNames()) {
            final PrecomputedDistancesFilter filter1 = getIndexField(indexFieldName, PrecomputedDistancesFilter.class);
            final PrecomputedDistancesFilter filter2 = obj.getIndexField(indexFieldName, PrecomputedDistancesFilter.class);
            if (filter1 == null || filter2 == null)
                continue;

            // Test for the same classes
            if (! filter1.getClass().equals(filter2.getClass()))
                throw new IllegalArgumentException("Trying to use different filters to include an object: thisFilter=" + filter1.getClass() + " objectFilter=" + filter2.getClass());
            if (filter1.includeUsingPrecompDist(filter2, radius)) {
                counterPrecomputedDistanceSavings.add();
                return true;
            }
        }
        return false;
    }
    // endregion

}
