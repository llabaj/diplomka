/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.classification.impl;

import messif.algorithm.Algorithm;
import messif.classification.Classification;
import messif.data.DataObject;
import messif.classification.ClassificationException;
import messif.classification.Classifier;
import messif.classification.UpdatableClassifier;
import messif.operation.answer.AbstractAnswer;
import messif.operation.crud.DeleteOperation;
import messif.operation.crud.InsertOperation;
import messif.operation.search.QueryObjectOperation;
import messif.operation.answer.RankedAnswer;
import messif.record.ModifiableRecord;
import messif.record.Record;

/**
 * Abstract implementation of a classifier that executes a {@link QueryObjectOperation}
 * on an encapsulated algorithm and computes the classification using encapsulated
 * classifier that takes {@link RankedAnswer}.
 *
 * @param <C> the class of instances that represent the classification categories
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public abstract class RankingQueryOperationClassifier<C> implements UpdatableClassifier<DataObject, C> {

    /** Classifier used to compute the object classification */
    private final Classifier<? super RankedAnswer, C> classifier;
    /** Algorithm that supplies the similar objects */
    private final Algorithm algorithm;
    /** Name of the parameter to put the executed operation into when classifying */
    private final String anwerParameter;

    /**
     * Creates a new kNN classifier.
     * @param classifier the classifier used to compute the object classification
     * @param algorithm the algorithm that supplies the similar objects
     * @param anwerParameter the name of the parameter to put the executed operation into when classifying
     */
    public RankingQueryOperationClassifier(Classifier<? super RankedAnswer, C> classifier, Algorithm algorithm, String anwerParameter) {
        this.classifier = classifier;
        this.algorithm = algorithm;
        this.anwerParameter = anwerParameter;
    }

    @Override
    public Classification<C> classify(DataObject object, Record parameters) throws ClassificationException {
        try {
            final AbstractAnswer answer = algorithm.evaluate(createOperation(object));
            if (! (answer instanceof RankedAnswer)) {
                throw new ClassificationException("Evaluation of the query returned wrong type of answer: " + answer.toJSONString());
            }
            if (parameters instanceof ModifiableRecord && anwerParameter != null)
                ((ModifiableRecord)parameters).setField(anwerParameter, answer);
            return classifier.classify((RankedAnswer) answer, parameters);
        } catch (Exception e) {
            throw new ClassificationException("There was an error executing query", e.getCause());
        }
    }

    /**
     * Creates a ranking operation to be executed to get the candidate list for classification.
     * @param object the object to classify
     * @return a new instance of the ranking operation to execute
     */
    protected abstract QueryObjectOperation createOperation(DataObject object);

    @Override
    public boolean addClasifiedObject(DataObject object, C classification) throws ClassificationException {
        try {
            final AbstractAnswer answer = algorithm.evaluate(InsertOperation.create(object));
            return answer.wasSuccessful();
        } catch (Exception e) {
            throw new ClassificationException("There was an error executing insert operation", e.getCause());
        }
    }

    @Override
    public boolean removeClasifiedObject(DataObject object) throws ClassificationException {
        try {
            final AbstractAnswer answer = algorithm.evaluate(DeleteOperation.create(object));
            return answer.wasSuccessful();
        } catch (Exception e) {
            throw new ClassificationException("There was an error executing delete operation", e.getCause());
        }
    }

    @Override
    public Class<? extends C> getCategoriesClass() {
        return classifier.getCategoriesClass();
    }

    /**
     * Returns the executed operation stored by this classifier in the given parameters.
     * @param parameters the parameters to get the executed operation from
     * @return the executed operation or <tt>null</tt> if no operation was stored in the parameters
     */
    public RankedAnswer getExecutedOperation(Record parameters) {
        if (anwerParameter == null || parameters == null)
            return null;
        return parameters.getField(anwerParameter, RankedAnswer.class);
    }

}
