/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.params;

import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Required;

public interface ApproximateSearch {
    /**
     * Enumeration of types of the stop condition for approximation early termination strategy.
     */
    enum Type {
        /**
         * Stop after inspecting given percentage of data.
         * The {@link #getApproxParam() parameter} holds the value between 0-100.
         */
        PERCENTAGE,
        /**
         * Stop after inspecting the specific number of objects.
         * The {@link #getApproxParam() parameter} is the number of objects.
         */
        ABS_OBJ_COUNT,
        /**
         * Stop after the specific number of evaluations of distance functions.
         * The {@link #getApproxParam() parameter} is the threshold on the number of distance computations.
         */
        ABS_DC_COUNT,
        /**
         * Stop after a specific number of "data regions" (buckets, clusters) is accessed and searched.
         * The {@link #getApproxParam() parameter} is the limit on "data regions" (partitions, buckets, clusters) to be accessed.
         */
        DATA_PARTITIONS,
        /**
         * Stop after a specific number of I/O operations (page reads)..
         * The {@link #getApproxParam() parameter} is the limit on "data regions" (partitions, buckets, clusters) to be accessed.
         */
        BLOCK_READS,
        /**
         * The search type and parameter are not set and the default of the structure should be used.
          The {@link #getApproxParam() parameter} is ignored.
         */
        USE_STRUCTURE_DEFAULT
    }

    String APPROX_TYPE_FIELD = "approx_type";

    /**
     * Returns the {@link Type type of the local approximation} parameter used.
     * @return the {@link Type type of the local approximation} parameter used
     */
    @Get(field = APPROX_TYPE_FIELD)
    @Required
    Type getApproxType();


    String APPROX_PARAM_FIELD = "approx_param";

    /**
     * Returns the value of the local approximation parameter.
     * Its interpretation depends on the value of {@link #getApproxType() local search type}.
     * @return the value of the local approximation parameter
     */
    @Get(field = APPROX_PARAM_FIELD)
    @Required
    int getApproxParam();


    String RADIUS_GUARANTEED_FIELD = "radius_guaranteed";

    /**
     * Returns a currently set value of radius within which the results are guaranteed as correct.
     * An evaluation algorithm is completely responsible for setting the correct value.
     * @return the value of the currently guaranteed radius
     */
    @Get(field = RADIUS_GUARANTEED_FIELD)
    float getRadiusGuaranteed(float defaultRadius);

    default float getRadiusGuaranteed() {
        return getRadiusGuaranteed(0f);
    };

}
