/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.proxy;

import messif.record.ModifiableRecord;
import messif.record.ModifiableRecordImpl;
import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Remove;
import messif.utility.proxy.annotations.Set;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Unit tests verifying the conversion of the  {@link ModifiableRecord} to interface.
 * This class focuses on the verification of the incoming interface (return vs incoming types etc) as well as the runtime validation (nullity and types in respect to interface)
 * <p>
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class ProxyConverterCheckedTest {

    private interface InvalidMethodPrototypes {
        @Get(field = "something")
        void getSomething();

        @Get(field = "something1")
        void getSomething(String str);

        @Get(field = "something2")
        int getSomething(String str, int i);

        @Set(field = "something")
        String setSomething(String str);

        @Set(field = "something1")
        void setSomething(String str, int something);

        @Remove(field = "something")
        void removeSomething();

        @Remove(field = "something2")
        void removeSomething(String str);

        @Remove(field = "something3")
        int removeSomethingElse(String str);
    }

    @Test
    public void testInvalidMethodPrototypes() {
        Map<String, Object> map = new HashMap<>();
        ModifiableRecord fieldMap = new ModifiableRecordImpl(map);
        InvalidMethodPrototypes obj = ProxyConverter.convert(fieldMap, InvalidMethodPrototypes.class);

        assertUnsupportedMethod(obj::getSomething, "void getSomething()");
        assertUnsupportedMethod(() -> obj.getSomething("str"), "void getSomething(String)");
        assertUnsupportedMethod(() -> obj.getSomething("str", 2), "int getSomething(String, int)");

        assertUnsupportedMethod(() -> obj.setSomething("str"), "String setSomething(String)");
        assertUnsupportedMethod(() -> obj.setSomething("str", 1), "void setSomething(String, int)");

        assertUnsupportedMethod(obj::removeSomething, "void removeSomething()");
        assertUnsupportedMethod(() -> obj.removeSomething("str"), "void removeSomething(String)");
        assertUnsupportedMethod(() -> obj.removeSomethingElse("str"), "int removeSomethingElse(String, int)");
    }

    private interface IncompatibleTypeMethodPrototypes {
        @Get(field = "a")
        String getIntToStr(int i);

        @Get(field = "b")
        int getStrToInt(String str);
    }

    @Test
    public void testIncompatibleTypePrototypes() {
        Map<String, Object> map = new HashMap<>();
        ModifiableRecord fieldMap = new ModifiableRecordImpl(map);
        IncompatibleTypeMethodPrototypes obj = ProxyConverter.convert(fieldMap, IncompatibleTypeMethodPrototypes.class);

        assertUnsupportedMethod(() -> obj.getIntToStr(2), "String getIntToStr(int i)");
        assertUnsupportedMethod(() -> obj.getStrToInt("str"), "int getStrToInt(String str)");
    }

    private void assertUnsupportedMethod(Runnable r, String methodName) {
        try {
            r.run();
            Assert.fail("Method '" + methodName + "' should not be supported");
        } catch (UnsupportedOperationException e) {
            // do nothing
        }
    }

    private interface SetterClass {
        @Set(field = "name")
        void setName(String name);
    }

//    @Test(expected = IllegalArgumentException.class)
//    public void setField_onUnmodifiableFieldMap_test() {
//        ModifiableRecord record = new ModifiableRecordImpl(new HashMap<>());
//        SetterClass obj = ProxyConverter.convertChecked(record, SetterClass.class);
//
//        obj.setName("Hello");
//    }
}
