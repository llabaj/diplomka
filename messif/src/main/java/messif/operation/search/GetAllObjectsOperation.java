/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.search;

import messif.bucket.BucketDispatcher;
import messif.bucket.LocalBucket;
import messif.operation.ReturnDataOperation;
import messif.operation.processing.impl.OneStepProcessor;
import messif.operation.answer.ListingAnswer;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface GetAllObjectsOperation extends ReturnDataOperation {

    String TYPE_STRING = "get_all_objects";

    @Override
    default String getInnerType() {
        return TYPE_STRING;
    }

    class BucketDispatcherProcessor extends OneStepProcessor<GetAllObjectsOperation, ListingAnswer> {

        final ListingAnswer.Builder answerBuilder;
        private final BucketDispatcher dispatcher;

        public BucketDispatcherProcessor(GetAllObjectsOperation operation, BucketDispatcher dispatcher) {
            super(operation);
            this.answerBuilder = new ListingAnswer.Builder(operation, ListingAnswer.class);
            this.dispatcher = dispatcher;
        }

        @Override
        protected void process() throws Exception {
            for (LocalBucket dataObjects : dispatcher.getAllBuckets()) {
                answerBuilder.addAll(dataObjects.getAllObjects());
            }
        }

        @Override
        public ListingAnswer finish() {
            return answerBuilder.getAnswer();
        }
    }

}
