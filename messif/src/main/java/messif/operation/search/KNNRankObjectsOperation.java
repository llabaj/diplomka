/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.search;

import messif.operation.params.DataObjects;

/**
 * This operation does a KNN search not on the indexed data but on the given list of objects (typically identified
 *  by object IDs). The query object might be also identified just by its ID.
 */
public interface KNNRankObjectsOperation extends KNNOperation, DataObjects {

    String TYPE_STRING = "knn_rank_objects";

    @Override
    default String getInnerType() {
        return TYPE_STRING;
    }
}
