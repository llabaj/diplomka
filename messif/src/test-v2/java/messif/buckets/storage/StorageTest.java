/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.bucket.storage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;
import messif.bucket.TemporaryCloseable;
import messif.bucket.TemporaryCloseableThread;
import messif.objects.LocalAbstractObject;
import test.TestConstants;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class StorageTest extends TestCase {
    private static final File serfile = new File("disk-storage.ser");
    private static final File storfile = new File("disk-storage.ds");

    private final List<LocalAbstractObject> objects;

    public StorageTest(String testName) throws Exception {
        super(testName);
        objects = TestConstants.createObjects();
    }

    /**
     * Test of store method, of class Storage.
     */
    @SuppressWarnings("unchecked")
    public void testStore() throws Throwable {
        Storage<LocalAbstractObject> instance = TestConstants.createStorage();
        List<Address<LocalAbstractObject>> addrs = new ArrayList<Address<LocalAbstractObject>>();
        for (LocalAbstractObject obj : objects) {
            Address<LocalAbstractObject> addr = instance.store(obj);
            assertNotNull(addr);
            addrs.add(addr);
        }

        // Test read
        for (int i = 0; i < objects.size(); i++) {
            LocalAbstractObject origObj = objects.get(i);
            LocalAbstractObject storageObj = addrs.get(i).read();
            assertEquals("Inserted object has the same key", origObj.getObjectKey(), storageObj.getObjectKey());
            assertTrue("Inserted object has the same data", origObj.dataEquals(storageObj));
        }

        // Serialize both storage and addresses
        instance.finalize();
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(serfile));
        out.writeObject(instance);
        out.writeObject(addrs);
        out.close();

        // Restore storage and addresses
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(serfile));
        instance = (Storage)in.readObject();
        addrs = (List)in.readObject();
        in.close();
        serfile.delete();

        // Test read
        for (int i = 0; i < objects.size(); i++) {
            LocalAbstractObject origObj = objects.get(i);
            LocalAbstractObject storageObj = addrs.get(i).read();
            assertEquals("Inserted object has the same key", origObj.getObjectKey(), storageObj.getObjectKey());
            assertTrue("Inserted object has the same data", origObj.dataEquals(storageObj));
        }
        instance.destroy();
    }

    /**
     * Test of store method, of class Storage.
     */
    @SuppressWarnings("unchecked")
    public void testTemporaryClose() throws Throwable {
        Storage<LocalAbstractObject> instance = TestConstants.createStorage();
        if (!(instance instanceof TemporaryCloseable)) {
            System.out.println("Storage " + instance.getClass() + " is not temporarily closeable, skipping test");
            return;
        }
        
        TemporaryCloseableThread temporaryCloseableThread = new TemporaryCloseableThread(5);
        temporaryCloseableThread.add((TemporaryCloseable)instance);

        List<Address<LocalAbstractObject>> addrs = new ArrayList<Address<LocalAbstractObject>>();
        for (LocalAbstractObject obj : objects) {
            Address<LocalAbstractObject> addr = instance.store(obj);
            assertNotNull(addr);
            addrs.add(addr);
            Thread.sleep(1);
        }

        // Test read
        for (int i = 0; i < objects.size(); i++) {
            LocalAbstractObject origObj = objects.get(i);
            LocalAbstractObject storageObj = addrs.get(i).read();
            assertEquals("Inserted object has the same key", origObj.getObjectKey(), storageObj.getObjectKey());
            assertTrue("Inserted object has the same data", origObj.dataEquals(storageObj));
        }

        instance.destroy();
    }

}
