/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.distance.agg;

import messif.distance.DistanceFunc;

import java.io.Serializable;

/**
 * The main class for complex aggregated functions that contain several sub-distance functions and combine them.
 */
public abstract class AggregationDistanceFuncBase<T> implements AggregationDistanceFunc<T>, Serializable {

    /** Class ID for Java serialization. */
    private static final long serialVersionUID = 1L;

    /**
     * Partial distance functions to be computed on the given objects and their results aggregated.
     */
    protected final DistanceFunc<T>[] distanceFunctions;

    protected AggregationDistanceFuncBase(DistanceFunc<T>[] distanceFunctions) {
        this.distanceFunctions = distanceFunctions;
    }

    /**
     * Compute the max distance of the aggregated function. This is a default implementation that calls
     *  {@link #compute(float...)} on individual sub-distances maximal values.
     */
    protected float calculateMaxDistances() {
        float [] maxDistances = new float[distanceFunctions.length];
        for (int i = 0; i < distanceFunctions.length; i++) {
            maxDistances[i] = distanceFunctions[i].getMaxDistance();
        }
        return compute(maxDistances);
    }

    //****************** Aggregation function evaluation ******************//

    /**
     * Computes the value of the aggregate distance from the provided sub-distances.
     * The <code>distances</code> array is assumed to contain distances in the order in which the
     *  {@link #distanceFunctions} were passed to this object in the constructor.
     * @param distances the distances evaluated by the list of distance functions passed to this aggregator
     * @return the aggregate distance
     */
    public abstract float compute(float... distances);


    //****************** Distance evaluation ******************//

    /**
     * Computes (the aggregated) distance of two objects.
     * @param object1 the one object to compute distance for
     * @param object2 the other object to compute distance for
     * @return the distance between object1 and object1 using this combination function
     */
    @Override
    public float getDistance(T object1, T object2, float threshold) {
        return getDistance(object1, object2, threshold, null);
    }

    @Override
    public float getDistance(T o1, T o2, float threshold, float[] individualDistances) {
        if (individualDistances == null || individualDistances.length != distanceFunctions.length) {
            individualDistances = new float[distanceFunctions.length];
        }
        for (int i = 0; i < distanceFunctions.length; i++) {
            individualDistances[i] = distanceFunctions[i].getDistance(o1, o2);
        }

        // Compute overall distance
        return compute(individualDistances);
    }

    @Override
    public int getNumberOfSubdists() {
        return distanceFunctions.length;
    }
}
