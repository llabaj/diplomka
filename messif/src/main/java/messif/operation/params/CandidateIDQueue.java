/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.params;

import messif.operation.AbstractOperation;
import messif.operation.OperationBuilder;
import messif.operation.answer.AbstractAnswer;
import messif.operation.helpers.AnswerProvider;
import messif.record.ModifiableRecord;
import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Required;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * If a {@link messif.operation.ReturnDataOperation} also implements this interface (contains these parameters),
 *  then the results are rather stored to the given ID queue instead of returned in
 *  a {@link messif.operation.answer.ListingAnswer} or {@link messif.operation.answer.RankedAnswer}.
 */
public interface CandidateIDQueue {

    String ID_QUEUE_PARAM = "id_queue";

    @Get(field = ID_QUEUE_PARAM)
    @Required
    BlockingQueue<String> getIDQueue();

    String CANDIDATE_SIZE_PARAM = "candidate_size";
    @Get(field = CANDIDATE_SIZE_PARAM)
    @Required
    int getCandidateSize();


    String END_OF_STREAM = "";

    /**
     * Adds {@link #ID_QUEUE_PARAM} with a newly created {@link LinkedBlockingQueue} into the operation and wraps it
     *  as a new operation that has its original type (class) but it also implements {@link CandidateIDQueue} interface.
     * @param opClass type of the passed operation that is maintained also by the returned operation
     * @param operation operation to modify
     * @param defaultCandSize default candidate size to be set into {@link #CANDIDATE_SIZE_PARAM}, if not set before.
     * @param <T> type of the operation
     * @return a newly created operation over {@link ModifiableRecord parameters} from the original operation
     */
    static <T extends AbstractOperation> T addCandidateSetParams(Class<? extends T> opClass, T operation, int defaultCandSize) {
        ModifiableRecord origParams = operation.getParams();
        if (! origParams.containsField(CANDIDATE_SIZE_PARAM)) {
            origParams.setField(CANDIDATE_SIZE_PARAM, defaultCandSize);
        }
        origParams.setField(ID_QUEUE_PARAM, new LinkedBlockingQueue<>());

        return OperationBuilder.build(origParams, opClass, false, CandidateIDQueue.class);
    }

    /**
     * Helper that takes care of pushing data into the queue of candidate IDs.
     */
    class Helper implements AnswerProvider<AbstractAnswer> {

        /** ID queue. */
        protected final BlockingQueue<String> idQueue;

        /** In case the helper should check uniqueness of the IDs, this set checks them. */
        protected final Set<String> uniqueIDs;

        public Helper(CandidateIDQueue operation) {
            this(operation, false);
        }

        public Helper(CandidateIDQueue operation, boolean uniqueIDs) {
            this.idQueue = operation.getIDQueue();
            this.uniqueIDs = uniqueIDs ? Collections.synchronizedSet(new HashSet<>()) : null;
        }

        public boolean addToAnswer(String id) {
            if (this.uniqueIDs == null || this.uniqueIDs.add(id)) {
                idQueue.add(id);
                return true;
            }
            return false;
        }

        /**
         * Close the stream of IDs that is to be consumed by the refiner.
         */
        public void endProcessing() {
            idQueue.add(END_OF_STREAM);
        }

        /**
         * Do not return any data in the answer.
         * @return
         */
        @Override
        public AbstractAnswer getAnswer() {
            endProcessing();
            return AbstractAnswer.createAnswer(AbstractAnswer.OPERATION_OK, AbstractAnswer.class);
        }

        @Override
        public void updateAnswer(AbstractAnswer answer) {
            // do nothing (until we
        }
    }
}
