/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.algorithm.impl;

import messif.algorithm.Algorithm;
import messif.classification.Classification;
import messif.classification.ClassificationWithConfidence;
import messif.classification.Classifications;
import messif.classification.Classifier;
import messif.operation.answer.AbstractAnswer;
import messif.operation.processing.ProcessorConfiguration;
import messif.operation.processing.impl.OneStepProcessor;
import messif.operation.search.ClassificationOperation;
import messif.record.ModifiableRecord;
import messif.record.Record;
import messif.utility.proxy.ProxyConverter;

/**
 * Algorithm wrapper for a {@link Classifier}.
 * The algorithm process any {@link ClassificationOperation} by passing its
 * query object to the classifier and returning the list of classes.
 *
 * @param <C> the class of instances that represent the classification categories
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class ClassifierAlgorithm<C> extends Algorithm {


    public static final String CLASSIFICATION_ANSWER_FIELD = "classes";
    public static final String CONFIDENCES_FIELD = "confidences";

    /**
     * Class serial id for serialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Wrapped classifier that is used for query execution
     */
    private final Classifier<? super Record, C> classifier;

    /**
     * Creates a new instance of ClassifierAlgorithm for the given classifier.
     * The classifier must accept an instance of {@link ClassificationOperation} as its argument.
     *
     * @param classifier the wrapped classifier that is used for query execution
     */
    @AlgorithmConstructor(description = "create classifier algorithm wrapper", arguments = {"the classifier to wrap"})
    public ClassifierAlgorithm(Classifier<? super Record, C> classifier) {
        super("Annotation");
        this.classifier = classifier;
    }

    @Override
    protected void registerProcessors() {
        super.registerProcessors();
        this.handler.register(ProcessorConfiguration.create(ClassificationOperation.class, AbstractAnswer.class)
                .withProcessor(ClassificationProcessor::new)
                .executeSequential().build());
    }


    protected class ClassificationProcessor extends OneStepProcessor<ClassificationOperation, AbstractAnswer> {

        Classification<C> classification = null;

        /**
         * Creates this processor given the operation to be processed.
         *
         * @param operation operation to be executed
         */
        public ClassificationProcessor(ClassificationOperation operation) {
            super(operation);
        }

        @Override
        protected void process() throws Exception {
            classifier.classify(getOperation().getQueryObject(), getOperation().getParams());
        }

        @Override
        public AbstractAnswer finish() {
            ModifiableRecord answer = classificationToAnswer(classification);
            if (classification instanceof ClassificationWithConfidence)
                answer.setField(CONFIDENCES_FIELD, getConfidences((ClassificationWithConfidence) classification));
            return ProxyConverter.convert(answer, AbstractAnswer.class);
        }

        /**
         * Converts the given classification to the operation answer.
         * Since the classification does not provide confidences.
         *
         * @param classification the classification to convert
         */
        protected ModifiableRecord classificationToAnswer(Classification<C> classification) {
            final Object[] objectsToReturn = new Object[classification.size()];
            int i = 0;
            for (C object : classification) {
                objectsToReturn[i++] = object;
            }

            ModifiableRecord fieldMap = AbstractAnswer.createMap(AbstractAnswer.OPERATION_OK);
            fieldMap.setField(CLASSIFICATION_ANSWER_FIELD, objectsToReturn);
            return fieldMap;
        }

        /**
         * Converts the given classification to the operation answer.
         * The classification ordering is preserved according to its lowest/highest confidence.
         * Note that the resulting distance is normalized from zero to one.
         *
         * @param classification the classification to convert
         */
        protected float[] getConfidences(ClassificationWithConfidence<C> classification) {
            boolean greaterThan = classification.getLowestConfidence() < classification.getHighestConfidence();
            float lowestConfidence = Classifications.getExtremeConfidence(classification, !greaterThan, classification.getHighestConfidence());
            float highestConfidence = Classifications.getExtremeConfidence(classification, greaterThan, classification.getLowestConfidence());
            // Convert the classification items to result
            float[] confidences = new float[classification.size()];
            int i = 0;
            for (C item : classification) {
                confidences[i++] = greaterThan ?
                        1 - (classification.getConfidence(item) - lowestConfidence) / (highestConfidence - lowestConfidence) :
                        (classification.getConfidence(item) - highestConfidence) / (lowestConfidence - highestConfidence);
            }
            return confidences;
        }

    }

}
