#!/bin/bash

find $* -not -path '*/.svn' -not -path '*/.git' -not -name 'package-info.java' -name '*.java' | while read f;do
	cat >$f.tmp <<EOF
/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
EOF
	perl -e '
		my $x = 0;
		my $a = 0;
		my $as = "";
		while (<>) {
			$x = 1 if (/package /);
			if (/(^.*\@author) / || /^ \* Created by/) {
				$a = $a + 1;
				$as = $as . $_;
			} else {
				if ($a > 0) {
					if ($a < 3) {
						print " * \@author David Novak, Masaryk University, Brno, Czech Republic, david.novak\@fi.muni.cz\n";
					} else {
						print $as;
					}
					$a = 0;
				}
				print if ($x);
			}
		}
		die "No package line found in $ARGV" unless ($x);
	' $f >> $f.tmp &&\
	mv -f $f.tmp $f
done
