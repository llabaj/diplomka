/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.params;

import messif.data.DataObject;
import messif.record.Record;
import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Required;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface DataObjects {

    /** Name of a {@link Record} (JSON) field to contain the list of objects to work with. */
    String OBJECTS_FIELD = "records";

    /**
     * This method provides a convenient access to the field with name {@link #OBJECTS_FIELD}.
     * @return a collection of {@link DataObject}s to work with
     */
    @Get(field = OBJECTS_FIELD)
    @Required
    DataObject [] getObjects();

    /**
     * Return the number of objects passed by this operation
     * @return
     */
    default int getObjectCount() {
        return getObjects() == null ? 0 : getObjects().length;
    }

}
