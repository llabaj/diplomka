/**
 * New package for data manipulation and querying operations, their processors and answers.
 *   All operations from MESSIF 2.0 were moved here and transformed, but the following:
 *   <li>
 *       <ul>BatchRankingQueryOperation</ul>
 *       <ul>BatchKNNQueryOperation</ul>
 *       <ul>IncrementalNNQueryOperation</ul>
 *       <ul>SingletonQueryOperation</ul>
 *   </li>
 *
 */
package messif.operation;

