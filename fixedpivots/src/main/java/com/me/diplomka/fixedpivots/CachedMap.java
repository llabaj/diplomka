package com.me.diplomka.fixedpivots;

import com.hazelcast.core.IMap;
import com.hazelcast.map.listener.*;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class CachedMap<K, V> implements ConcurrentMap<K, V> {

    private IMap<K, V> sourceMap;
    private ConcurrentMap<K, V> cache;

    private final String addedListener;
    private final String updatedListener;
    private final String removedListener;
    private final String clearedListener;
    private final String evictedListener;

    public CachedMap(IMap<K, V> sourceMap) {
        this.sourceMap = sourceMap;
        cache = new ConcurrentHashMap<>(sourceMap);

        addedListener = sourceMap.addEntryListener((EntryAddedListener<K, V>) event -> {
            cache.put(event.getKey(), event.getValue());
        }, true);

        updatedListener = sourceMap.addEntryListener((EntryUpdatedListener<K, V>) event -> {
            cache.put(event.getKey(), event.getValue());
        }, true);

        removedListener = sourceMap.addEntryListener((EntryRemovedListener<K, V>) event -> {
            cache.remove(event.getKey());
        }, false);

        clearedListener = sourceMap.addEntryListener((MapClearedListener) event -> {
            cache.clear();
        }, false);

        evictedListener = sourceMap.addEntryListener((MapEvictedListener) event -> {
            cache.clear();
        }, false);

    }

    public void clearListeners() {
        sourceMap.removeEntryListener(addedListener);
        sourceMap.removeEntryListener(updatedListener);
        sourceMap.removeEntryListener(removedListener);
        sourceMap.removeEntryListener(clearedListener);
        sourceMap.removeEntryListener(evictedListener);
    }

    @Override
    public int size() {
        return cache.size();
    }

    @Override
    public boolean isEmpty() {
        return cache.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return cache.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return cache.containsValue(value);
    }

    @Override
    public V get(Object key) {
        return cache.get(key);
    }

    @Override
    public Set<K> keySet() {
        return cache.keySet();
    }

    @Override
    public Collection<V> values() {
        return cache.values();
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return cache.entrySet();
    }

    @Override
    public V put(K key, V value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public V remove(Object key) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public V putIfAbsent(K key, V value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(Object key, Object value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean replace(K key, V oldValue, V newValue) {
        throw new UnsupportedOperationException();
    }

    @Override
    public V replace(K key, V value) {
        throw new UnsupportedOperationException();
    }
}
