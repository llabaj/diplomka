/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation;

import messif.data.DataObject;
import messif.record.Record;
import messif.record.RecordImpl;
import messif.utility.proxy.annotations.Get;

import java.util.Arrays;
import java.util.Collection;

/**
 * An abstract predecessor of all operations that return some data objects. The only parameter of this operation
 *  specifies the fields from the data objects (field map) to be returned for each object in the answer.
 */
public interface ReturnDataOperation extends AbstractOperation {

    /** Name of a {@link Record} (JSON) field to contain a list of data fields to be returned. */
    String FIELDS_TO_RETURN_FIELD = "fields_to_return";

    @Get(field = FIELDS_TO_RETURN_FIELD)
    String [] fieldsToReturn(String [] defaultFields);

    /** The default list of fields to be returned (return all original fields). */
    String [] DEFAULT_LIST = new String [] {"*"};

    /**
     * Returns the list of fields to be returned for each object in the answer of this operation. Contract: <ul>
     *     <li>if the returned array is NULL, it means that the data objects are not returned AT ALL</li>
     *     <li>if the returned array is EMPTY, all fields in the data objects are returned (DEFAULT)</li>
     *     <li>otherwise, a new object with given list of fields is created for each data object
     *        and add {@link DataObject#ID_FIELD} if missing</li>
     * </ul>
     * This method is to be overriden to change the default list of fields.
     */
    default String[] fieldsToReturn() {
        return fieldsToReturn(DEFAULT_LIST);
    }

    /**
     * Transform given data object to contain only given fields: <ul>
     *     <li>if the returned array is NULL, it means that the data objects are not returned AT ALL (return null)</li>
     *     <li>if the returned array is ["*"] (an array with one string "*"), the original object is returned</li>
     *     <li>otherwise, a new object with given list of fields is created for each data object</li>
     * </ul>
     * @param object an object to be transformed
     * @param fieldsToReturn a list of fields to be returned for the data object (see above)
     * @return <code>null</code>, the original object, or a new object with a given list of fields
     */
    static DataObject transformObject(DataObject object, String [] fieldsToReturn) {
        if (fieldsToReturn == null) {
            return null;
        }
        if (Arrays.equals(fieldsToReturn, DEFAULT_LIST)) {
            return object;
        }
        // create a new (unmodifiable) data object only with the specified fields
        return new RecordImpl(object, fieldsToReturn);
    }

    /**
     * Given a list of objects, create an array of transformed objects, or <code>null</code> if the 2nd param is null.
     * @param objects a list of objects to be transformed
     * @param fieldsToReturn a list of fields to be returned for the data object (see above)
     * @return <code>null</code>, an array of the original objects, or an array of new objects with a given fields
     */
    static DataObject [] transformObjects (Collection<DataObject> objects, String [] fieldsToReturn) {
        if (fieldsToReturn == null) {
            return null;
        }
        final DataObject[] transformedObjects = new DataObject[objects.size()];
        int i = 0;
        for (DataObject object : objects) {
            transformedObjects[i ++] = transformObject(object, fieldsToReturn);
        }
        return transformedObjects;
    }

}
