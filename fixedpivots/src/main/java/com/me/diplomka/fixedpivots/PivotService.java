package com.me.diplomka.fixedpivots;

import com.hazelcast.core.IMap;
import com.hazelcast.core.ReplicatedMap;
import messif.data.DataObject;
import messif.distance.DataObjectDistanceFunc;
import messif.distance.impl.L2DistanceFloats;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

import static com.me.diplomka.fixedpivots.CustomCollectors.sortedLimit;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.*;

public class PivotService {
    private static final Logger logger = LoggerFactory.getLogger(PivotService.class);

    private ReplicatedMap<String, DataObject> pivots;
    private IMap<String, Integer> prefixCounts;
    private CachedMap<String, Integer> prefixCountsCache;
    private DataObjectDistanceFunc<float[]> distanceFunc;
    private int prefixLength;
    private TagService tagService;

    public PivotService(ReplicatedMap<String, DataObject> pivots, IMap<String, Integer> prefixCounts, int prefixLength, TagService tagService) {
        this.pivots = pivots;
        this.prefixCounts = prefixCounts;
        this.prefixCountsCache = new CachedMap<>(prefixCounts);
        this.prefixLength = prefixLength;
        this.tagService = tagService;
        distanceFunc = new DataObjectDistanceFunc<>("descriptor_256", new L2DistanceFloats());
    }

    public int incCount(String prefix) {
        return prefixCounts.compute(prefix, (key, value) -> value != null ? value + 1 : 1);
    }

    public int getCount(String prefix) {
        var result = prefixCounts.get(prefix);
        return result != null ? result : 0;
    }

    public String findPivotPrefix(DataObject query) {
        return pivots.values()
                .stream()
                .map(pivot -> new Score(pivot, distanceFunc.getDistance(pivot, query)))
                .collect(sortedLimit(prefixLength))
                .map(score -> score.dataObject.getID())
                .collect(joining("_"));
    }

    public String[] findCandidatePivotPrefixes(DataObject query, int threshold, String... tags) {
        var distanceMap = pivots.values()
                .stream()
                .map(pivot -> new Score(pivot, distanceFunc.getDistance(pivot, query)))
                .collect(toMap(score -> score.dataObject.getID(), score -> score.distance));

        // TODO try to limit amount of sorted prefixes
        var sortedPrefixes = prefixCountsCache.keySet()
                .stream()
                .map(prefix -> {
                    var pivotKeys = prefix.split("_");
                    var distance = calculateDistanceToPPP(pivotKeys, distanceMap, 0.75f);
                    return Pair.of(prefix, distance);
                })
                .sorted(comparing(Pair::getValue))
//                .limit(threshold)
//                .collect(sortedLimit(threshold, new PairComparator()))
                .map(Pair::getKey)
                .collect(toList());

        var candidatePrefixes = new ArrayList<String>();
        var count = 0;
        var iterator = sortedPrefixes.iterator();
        while (iterator.hasNext() && count < threshold) {
            var prefix = iterator.next();
            candidatePrefixes.add(prefix);
            if (tags.length == 0) {
                count += prefixCountsCache.get(prefix);
            } else {
                count += tagService.estimateIntersect(prefix, tags);
            }
        }

        logger.debug("searching in {} cells ({} objects) for query object {}", candidatePrefixes.size(), count, query.getID());

        return candidatePrefixes.toArray(new String[0]);
    }

    private float calculateDistanceToPPP(String[] pivotKeys, Map<String, Float> scores, float coeff) {
        var sum = 0f;
        for (var i = 0; i < pivotKeys.length; i++) {
            String key = pivotKeys[i];
            sum += Math.pow(coeff, i) * scores.get(key);
        }
        return sum;
    }

    public Map<String, Integer> getCounts() {
        return Collections.unmodifiableMap(prefixCounts);
    }

    private static class Score implements Comparable<Score> {
        DataObject dataObject;
        float distance;

        private Score(DataObject dataObject, float distance) {
            this.dataObject = dataObject;
            this.distance = distance;
        }

        @Override
        public int compareTo(Score other) {
            return Float.compare(distance, other.distance);
        }
    }

    private static class PairComparator implements Comparator<Pair<String, Float>>, Serializable {
        @Override
        public int compare(Pair<String, Float> o1, Pair<String, Float> o2) {
            return Float.compare(o1.getRight(), o2.getRight());
        }
    }
}
