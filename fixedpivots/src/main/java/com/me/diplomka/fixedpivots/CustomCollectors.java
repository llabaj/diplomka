package com.me.diplomka.fixedpivots;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Stream;

public class CustomCollectors {
    private CustomCollectors() {}

    /**
     * Collector that sorts collection then returns {@link Stream} of first {@code limit} elements.
     * <p>
     * This collector is functionally the same as {@code stream.sorted().limit()}, but more efficient.
     * @param limit number of elements to return
     * @return new instance of the {@link Collector}
     * */
    public static <T extends Comparable<T>> Collector<T, Queue<T>, Stream<T>> sortedLimit(int limit){
        return new SortedLimitCollector<T>(limit, Comparator.naturalOrder());
    }

    public static <T> Collector<T, Queue<T>, Stream<T>> sortedLimit(int limit, Comparator comparator){
        return new SortedLimitCollector<T>(limit, comparator);
    }

    private static class SortedLimitCollector<T> implements Collector<T, Queue<T>, Stream<T>> {
        private Comparator<T> comparator;

        private final int limit;
        private SortedLimitCollector(int limit, Comparator<T> comparator) {
            this.limit = limit;
            this.comparator = comparator;
        }

        @Override
        public Supplier<Queue<T>> supplier() {
            return () -> new PriorityQueue<>(limit + 1, comparator.reversed());
        }

        @Override
        public BiConsumer<Queue<T>, T> accumulator() {
            return (acc, element) -> {
                acc.add(element);
                if (acc.size() > limit) {
                    acc.poll();
                }
            };
        }

        @Override
        public BinaryOperator<Queue<T>> combiner() {
            return (q1, q2) -> {
                q1.addAll(q2);
                while (q1.size() > limit) {
                    q1.poll();
                }
                return q1;
            };
        }

        @Override
        public Function<Queue<T>, Stream<T>> finisher() {
            return queue -> queue.stream().sorted(comparator);
        }

        @Override
        public Set<Characteristics> characteristics() {
            return Set.of(Characteristics.UNORDERED);
        }
    }
}
