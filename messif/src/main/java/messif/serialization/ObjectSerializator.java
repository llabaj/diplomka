/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.serialization;

import messif.indexing.IndexObject;
import messif.record.ModifiableRecord;
import messif.record.ModifiableRecordImpl;
import messif.record.Record;
import messif.record.RecordImpl;
import messif.serialization.io.*;
import messif.utility.ArrayMap;

import java.io.IOException;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

/**
 * This class provides a framework for binary serialization of objects.
 * It operates on any {@link BinaryInput}/{@link BinaryOutput}.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @see BufferInputStream
 * @see BufferOutputStream
 * @see ChannelInputStream
 * @see ChannelOutputStream
 */
public class ObjectSerializator extends BasicTypesSerializator {

    // region ******************        Constants for object serialization (up to 50)   ****************** //
    public static final byte TYPE_INT = 1;
    public static final byte TYPE_LONG = 2;
    public static final byte TYPE_FLOAT = 3;
    public static final byte TYPE_DOUBLE = 4;
    public static final byte TYPE_BOOLEAN = 5;
    public static final byte TYPE_BYTE = 6;
    public static final byte TYPE_SHORT = 7;
    public static final byte TYPE_CHAR = 8;
    public static final byte TYPE_INTARRAY = 9;
    public static final byte TYPE_LONGARRAY = 10;
    public static final byte TYPE_SHORTARRAY = 11;
    public static final byte TYPE_BYTEARRAY = 12;
    public static final byte TYPE_FLOATARRAY = 13;
    public static final byte TYPE_DOUBLEARRAY = 14;
    public static final byte TYPE_STRING = 15;
    public static final byte TYPE_STRINGARRAY = 16;
    public static final byte TYPE_BIGINTEGER = 17;
    public static final byte TYPE_BOOLEANARRAY = 18;
    public static final byte TYPE_CHARARRAY = 19;
    public static final byte TYPE_MAP_STROBJ = 20;
    public static final byte TYPE_ARRAY_OBJS = 21;
    public static final byte TYPE_FIELD_MAP = 22;
    public static final byte TYPE_MODIF_FIELD_MAP = 23;
    //public static final byte TYPE_DATA_OBJ = 24;
    public static final byte TYPE_INDEX_OBJECT = 25;
    //public static final byte TYPE_

    public static final byte TYPE_NULL = 50;
    // endregion


    //************************     The serialization of standard objects     ************************//

    /**
     * Writes the object to the output including the type byte.
     * @param output
     * @param object
     * @return
     * @throws IOException
     * @throws BinarySerializationException
     */
    public final int writeObject(BinaryOutput output, Object object) throws IOException, BinarySerializationException {
        if (object == null) {
            return writeObjectTypeByte(output, TYPE_NULL);
        }
        byte typeByte = getObjectTypeByte(object);
        if (typeByte < 0) {
            throw new BinarySerializationException("don't know how to serialize object: " + object);        
        }
        return writeObjectTypeByte(output, typeByte) + write(output, object.getClass().cast(object), typeByte);
    }

    protected byte getObjectTypeByte(Object object) {
        if (object == null) {
            return TYPE_NULL;
        }
        
        Class<?> objectClass = object.getClass();
        if (objectClass == Integer.class) {
            return TYPE_INT;
        }
        if (objectClass == Long.class) {
            return TYPE_LONG;
        }
        if (objectClass == Float.class) {
            return TYPE_FLOAT;
        }
        if (objectClass == Double.class) {
            return TYPE_DOUBLE;
        }
        if (objectClass == Boolean.class) {
            return TYPE_BOOLEAN;
        }
        if (objectClass == Byte.class) {
            return TYPE_BYTE;
        }
        if (objectClass == Short.class) {
            return TYPE_SHORT;
        }
        if (objectClass == Character.class) {
            return TYPE_CHAR;
        }
        if (objectClass == String.class) {
            return TYPE_STRING;
        }
        if (objectClass == BigInteger.class) {
            return TYPE_BIGINTEGER;
        }

        if (IndexObject.class.isAssignableFrom(objectClass)) {
            return TYPE_INDEX_OBJECT;
        }

        if (ModifiableRecordImpl.class.isAssignableFrom(objectClass)) {
            return TYPE_MODIF_FIELD_MAP;
        }

        if (RecordImpl.class.isAssignableFrom(objectClass)) {
            return TYPE_FIELD_MAP;
        }

        // TODO: check the key type anyhow?
        if (Map.class.isAssignableFrom(objectClass)) {
            return TYPE_MAP_STROBJ;
        }

        // arrays of primitive types and string
        if (objectClass.isArray()) {
            objectClass = objectClass.getComponentType();
            if (objectClass == Integer.TYPE) {
                return TYPE_INTARRAY;
            }
            if (objectClass == Long.TYPE) {
                return TYPE_LONGARRAY;
            }
            if (objectClass == Short.TYPE) {
                return TYPE_SHORTARRAY;
            }
            if (objectClass == Byte.TYPE) {
                return TYPE_BYTEARRAY;
            }
            if (objectClass == Float.TYPE) {
                return TYPE_FLOATARRAY;
            }
            if (objectClass == Double.TYPE) {
                return TYPE_DOUBLEARRAY;
            }
            if (objectClass == Boolean.TYPE) {
                return TYPE_BOOLEANARRAY;
            }
            if (objectClass == Character.TYPE) {
                return TYPE_CHARARRAY;
            }
            if (objectClass == String.class) {
                return TYPE_STRINGARRAY;
            }
            return TYPE_ARRAY_OBJS;
        }
        return -1;
    }
       
    
    protected int writeObjectTypeByte(BinaryOutput output, byte type) throws IOException {
        return write(output, type);
    }

    protected int write(BinaryOutput output, Object object, byte type) throws IOException, BinarySerializationException {
        switch (type) {
            case TYPE_INT:
                return write(output, (int) object);
            case TYPE_LONG:
                return write(output, (long) object);
            case TYPE_FLOAT:
                return write(output, (float) object);
            case TYPE_DOUBLE:
                return write(output, (double) object);
            case TYPE_BOOLEAN:
                return write(output, (boolean) object);
            case TYPE_BYTE:
                return write(output, (byte) object);
            case TYPE_SHORT:
                return write(output, (short) object);
            case TYPE_CHAR:
                return write(output, (char) object);
            case TYPE_STRING:
                return write(output, (String) object);
            case TYPE_BIGINTEGER:
                return write(output, (BigInteger) object);
            case TYPE_INDEX_OBJECT:
                return write(output, (IndexObject) object);
            case TYPE_MODIF_FIELD_MAP:
                return write(output, (RecordImpl) object);
            case TYPE_FIELD_MAP:
                return write(output, (RecordImpl) object);
            case TYPE_MAP_STROBJ:
                return write(output, (Map<String, Object>) object);
            case TYPE_INTARRAY:
                return write(output, (int[]) object);
            case TYPE_LONGARRAY:
                return write(output, (long[]) object);
            case TYPE_SHORTARRAY:
                return write(output, (short[]) object);
            case TYPE_BYTEARRAY:
                return write(output, (byte[]) object);
            case TYPE_FLOATARRAY:
                return write(output, (float[]) object);
            case TYPE_DOUBLEARRAY:
                return write(output, (double[]) object);
            case TYPE_BOOLEANARRAY:
                return write(output, (boolean[]) object);
            case TYPE_CHARARRAY:
                return write(output, (char[]) object);
            case TYPE_STRINGARRAY:
                return write(output, (String[]) object);
            case TYPE_ARRAY_OBJS:
                return write(output, (Object[]) object);
        }
        throw new BinarySerializationException("don't know how to serialize object: " + object);        
    }

    /**
     * Read the first non-deleted object in the input stream.
     * @param input
     * @return
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws BinarySerializationException
     */
    public final Object readObject(BinaryInput input) throws IOException, IllegalArgumentException, BinarySerializationException {
        byte typeByte;
        do {
            // Read object type
            typeByte = readObjectTypeByte(input);
            // if the object is deleted, then skip it and continue to first non-deleted object
            if (typeByte < 0) {
                skipObject(input, (byte) -typeByte);
            }
        } while (typeByte < 0);

        return readObject(input, typeByte);
    }

    protected byte readObjectTypeByte(BinaryInput input) throws IOException {
        return readByte(input);
    }

    protected Object readObject(BinaryInput input, byte typeByte) throws IOException, BinarySerializationException {
        switch (typeByte) {
            case TYPE_NULL:
                return null;
            case TYPE_BOOLEAN:
                return readBoolean(input);
            case TYPE_INT:
                return readInt(input);
            case TYPE_LONG:
                return readLong(input);
            case TYPE_FLOAT:
                return readFloat(input);
            case TYPE_DOUBLE:
                return readDouble(input);
            case TYPE_BYTE:
                return readByte(input);
            case TYPE_SHORT:
                return readShort(input);
            case TYPE_CHAR:
                return readChar(input);
            case TYPE_INDEX_OBJECT:
                return readIndexObject(input);
            case TYPE_MODIF_FIELD_MAP:
                return readModifFieldMap(input);
            case TYPE_FIELD_MAP:
                return readFieldMap(input);
            case TYPE_MAP_STROBJ:
                return readMap(input, true);
            case TYPE_INTARRAY:
                return readIntArray(input);
            case TYPE_LONGARRAY:
                return readLongArray(input);
            case TYPE_SHORTARRAY:
                return readShortArray(input);
            case TYPE_BYTEARRAY:
                return readByteArray(input);
            case TYPE_FLOATARRAY:
                return readFloatArray(input);
            case TYPE_DOUBLEARRAY:
                return readDoubleArray(input);
            case TYPE_STRING:
                return readString(input);
            case TYPE_STRINGARRAY:
                return readStringArray(input);
            case TYPE_BIGINTEGER:
                return readBigInteger(input);
            case TYPE_BOOLEANARRAY:
                return readBooleanArray(input);
            case TYPE_CHARARRAY:
                return readCharArray(input);
            case TYPE_ARRAY_OBJS:
                return readObjectArray(input);
            default:
                throw new BinarySerializationException("unknown object type:" + typeByte);
        }
    }

    /**
     * Skip a single the object at the current position of the stream, even if it was deleted.
     *
     * @param input the stream in which to skip an object
     * @return true if the skipped object was a valid (NOT deleted) object, false otherwise
     * @throws IOException if there was an error reading from the input stream
     */
    public boolean skipObject(BinaryInput input) throws IOException, BinarySerializationException {
        byte typeByte = readObjectTypeByte(input);
        skipObject(input, (byte) Math.abs(typeByte));
        return typeByte >= 0;
    }

    /**
     * Skips the first non-deleted object (skip also all deleted objects before the first non-deleted)
     * @param input
     * @return true, if the object was found
     * @throws IOException
     * @throws BinarySerializationException
     */
    public boolean skipObjectIgnoreDeleted(BinaryInput input) throws IOException, BinarySerializationException {
        byte typeByte;
        do {
            // Read object type
            typeByte = readObjectTypeByte(input);
            // Skip this object (even if marked as "deleted")
            skipObject(input, (byte) Math.abs(typeByte));
        } while (typeByte < 0);
        return true;
    }

    // TODO: if the skip should work with sizes (and return them) then use this method instead of the one above
    /*Skip a single the object at the current position of the stream
    * @param input the stream in which to skip an object
    * @param skipDeleted if <tt>true</tt> the deleted object are silently skipped (their sizes are
            * not reported)
    * @return true if the (last) skipped object was a valid (NOT deleted) object, false otherwise; the return value
    * @throws IOException if there was an error reading from the input stream
    */
//    public int skipObject(BinaryInput input, boolean skipDeleted) throws IOException, BinarySerializationException {
//        byte typeByte;
//        int objectSize;
//        do {
//            // Read object type
//            typeByte = readObjectTypeByte(input);
//            // Skip this object (even if marked as "deleted")
//            objectSize = 1 + skipObject(input, Math.abs(typeByte));
//        } while (typeByte < 0 && skipDeleted);
//        return typeByte >= 0 ? objectSize : -objectSize;
//    }

    // TODO: really use skip* methods instead of read methods;
    // TODO: implement skip* methods for all types (and override this method in the undrerlying classes)
    protected void skipObject(BinaryInput input, byte typeByte) throws IOException, BinarySerializationException {
        Object ignore = readObject(input, typeByte);
//        switch (typeByte) {
//            case TYPE_NULL:
//                return 0;
//            case TYPE_BOOLEAN:
//                return skipBoolean(input);
//            case TYPE_INTARRAY:
//                return skipIntArray(input);
//            default:
//                throw new BinarySerializationException("unknown object type:" + typeByte);
//        }
    }

    /**
     * This method finds a FIRST VALID (not deleted) object in the input stream
     *  and marks it it back into the given out as DELETED, which means
     *  the type byt is negated (-typeByte).
     * @param input
     * @param output
     //* @return the size of serialized object (including the size of the type byte)
     * @return true if the object was actually deleted (if it was not already deleted)
     * @throws IOException
     * @throws BinarySerializationException
     */
    // TODO: if the skip method starts working with the sizes again, return the size here
    // TODO: this method does not work at this point like the documentation says (it only deletes the first
    public boolean deleteObject(BinaryInput input, BinaryOutput output) throws IOException, BinarySerializationException {
        byte typeByte = readObjectTypeByte(input);
        // read and find out the size of the object
        //int objectSize = 1 + skipObject(input, (byte) Math.abs(typeByte));
        if (typeByte >= 0) {
            writeObjectTypeByte(output, (byte) -typeByte);
        } else {
            skipObject(input, (byte) -typeByte);
        }
        return typeByte >= 0;
    }

    /**
     * This method rewrites given serialized object in the input stream by another given object checking that the
     *  size of the new object is EXACTLY the same as the old one.
     * @return the size of serialized object (including the size of the type byte)
     * @throws IOException
     * @throws BinarySerializationException
     */
//    public int deleteObject(BinaryInput input, BinaryOutput output) throws IOException, BinarySerializationException {
//        byte typeByte = readObjectTypeByte(input);
//        // read and find out the size of the object
//        int objectSize = 1 + skipObject(input, Math.abs(typeByte));
//        if (typeByte >= 0) {
//            writeObjectTypeByte(output, typeByte);
//        }
//        return objectSize;
//    }

    public int write(BinaryOutput output, IndexObject indexObject) throws IOException, BinarySerializationException {
        return write(output, indexObject.getMap())
                + write(output, indexObject.getIndexMap());
    }

    public IndexObject readIndexObject(BinaryInput input) throws IOException, IllegalArgumentException, BinarySerializationException {
        final Map<String, Object> dataMap = readMap(input, false);
        final Map<String, Object> indexMap = readMap(input, false);
        return new IndexObject(new RecordImpl(dataMap, true), indexMap, true);
    }


    public int write(BinaryOutput output, RecordImpl dataObject) throws IOException, BinarySerializationException {
        return write(output, dataObject.getMap());
    }

    public Record readFieldMap(BinaryInput input) throws IOException, IllegalArgumentException, BinarySerializationException {
        final Map<String, Object> stringObjectMap = readMap(input, false);
        return new RecordImpl(stringObjectMap, true);
    }

    public ModifiableRecord readModifFieldMap(BinaryInput input) throws IOException, IllegalArgumentException, BinarySerializationException {
        final Map<String, Object> stringObjectMap = readMap(input, true);
        return new ModifiableRecordImpl(stringObjectMap, true);
    }


    /**
     * Writes <code>map</code> to the provided output buffer.
     *
     * @param output the buffer to write the object to
     * @param map the map to write
     * @return the number of bytes actually written
     * @throws IOException if there was an error using flushChannel
     */
    public int write(BinaryOutput output, Map<String, Object> map) throws IOException, BinarySerializationException {
        int size = write(output, map.size());
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            size += write(output, entry.getKey());
            size += writeObject(output, entry.getValue());
        }
        return size;
    }

    public Map<String, Object> readMap(BinaryInput input) throws IOException, IllegalArgumentException, BinarySerializationException {
        return readMap(input, false);
    }

    /**
     * Reads a <code>map</code> from the provided input buffer.
     *
     * @param input the buffer to read the map from
     * @return the map read from the input
     * @throws IOException if there was an error using flushChannel
     */
    public Map<String, Object> readMap(BinaryInput input, boolean modifiable) throws IOException, IllegalArgumentException, BinarySerializationException {
        int size = readInt(input);
        Map map;
        if (modifiable) {
            map = new HashMap<>(size);
            for (int i = 0; i < size; i++) {
                map.put(readString(input), readObject(input));
            }
        } else {
            String [] keys = new String[size];
            Object [] values = new Object[size];
            for (int i = 0; i < size; i++) {
                keys[i] = readString(input).intern();
                values[i] = readObject(input);
            }
            map = new ArrayMap(keys, values, true);
        }
        return map;
    }

    public int write(BinaryOutput output, Object[] array) throws IOException, BinarySerializationException {
        int size = write(output, array.length);
        for (Object obj : array) {
            size += writeObject(output, (Object) obj);
        }
        return size;
    }

    public Object[] readObjectArray(BinaryInput input) throws IOException, IllegalArgumentException, BinarySerializationException {
        int size = readInt(input);
        Object[] retVal = new Object[size];

        for (int i = 0; i < retVal.length; i++) {
            retVal[i] = readObject(input);

        }
        return retVal;
    }

    /**
     * Returns the size of the (binary) serialized <code>object</code> in bytes. The exact size
     * including all overhead is returned. This method can be very slow if the standard Java
     * {@link java.io.Serializable serialization} is used on object, i.e. when the object does not
     * implement the {@link BinarySerializable} interface.
     *
     * @param object the object to get the size for
     * @return the size of the binary-serialized <code>object</code>
     * @throws IllegalArgumentException if there was an error using Java standard serialization on
     * the object
     */
//    public final int getBinarySize(Object object) throws IllegalArgumentException {
//        // Object is null, the size will be zero
//        if (object == null)
//            return 4;
//
//        // Object will be serialized as the binary serializable object
//        if (object instanceof BinarySerializable)
//            return getBinarySize((BinarySerializable) object) + 4;
//
//        // Object will be serialized using standard Java serialization
//        try {
//            return getBinarySize(new JavaToBinarySerializable(object)) + 4;
//        } catch (IOException e) {
//            throw new IllegalArgumentException(e);
//        }
//    }
    /**
     * Returns the size of the binary-serialized <code>object</code> in bytes.
     *
     * @param object the object to get the size for
     * @return the size of the binary-serialized <code>object</code>
     * @throws IllegalArgumentException if there was an error using Java standard serialization on
     * the object
     */
//    protected abstract int getBinarySize(BinarySerializable object) throws IllegalArgumentException;

    // **********     OTHER METHODS TO MODIFY ONCE THE SYSTEM WORKS        ********* //
    /**
     * Writes <code>object</code> to a new buffer output stream.
     *
     * @param object the object to write
     * @param bufferDirect the type of buffer to use (direct or array-backed)
     * @return the create buffer that contains the serialized object
     * @throws IOException if there was an error using flushChannel
     */
    public final BufferOutputStream write(Object object, boolean bufferDirect) throws IOException {
        try {
            BufferOutputStream buf = new GrowingBufferOutputStream(bufferDirect);
            writeObject(buf, object);
            return buf;
        } catch (BinarySerializationException e) {
            throw new IOException(e);
        }
    }

//    /**
//     * Writes <code>object</code> to a given file.
//     * If the object implements {@link BinarySerializable} interface, it
//     * is binary-serialized. Otherwise, a standard Java {@link java.io.Serializable serialization} is used.
//     *
//     * @param object the object to write
//     * @param bufferDirect the type of buffer to use (direct or array-backed)
//     * @param outputFile the file to output the data to
//     * @throws IOException if there was an error writing the data
//     */
//    public final void writeToFile(Object object, boolean bufferDirect, File outputFile) throws IOException {
//        BufferOutputStream data = write(object, bufferDirect);
//        FileOutputStream out = new FileOutputStream(outputFile);
//        try {
//            data.write(out.getChannel(), 0);
//        } finally {
//            out.close();
//        }
//    }
//
//    /**
//     * Reads an instance from the <code>input</code> using this serializator.
//     *
//     * @param <E> the class that is expected to be in the input
//     * @param expectedClass the class that is expected to be in the input
//     * @param bufferDirect the type of buffer to use (direct or array-backed)
//     * @param inputFile the file to read the instance from
//     * @return an instance of the deserialized object
//     * @throws IOException if there was an I/O error
//     * @throws IllegalArgumentException if the constructor or the factory method has a wrong prototype
//     */
//    public final <E> E readFromFile(Class<? extends E> expectedClass, boolean bufferDirect, File inputFile) throws IOException {
//        FileInputStream in = new FileInputStream(inputFile);
//        try {
//            return readObject(new FileChannelInputStream(BufferOutputStream.MINIMAL_BUFFER_SIZE, bufferDirect, in.getChannel(), 0, Long.MAX_VALUE), expectedClass);
//        } finally {
//            in.close();
//        }
//    }
//    
    /**
     * Reads one object from the input stream into a buffer (including the header), no deserialization is performed.
     * @param stream the stream to read the object from
     * @param buffer the buffer into which an object is read
     * @param copySize the maximal number of bytes read by a bulk read operation (i.e. the input stream buffer size)
     * @return the size of the object loaded into the buffer (including the header)
     * @throws IOException if there was a problem reading the stream or the given buffer is too small to hold the object data
     */
//    public int objectToBuffer(BinaryInput stream, ByteBuffer buffer, int copySize) throws IOException {
//        int objectSize;
//        try {
//            objectSize = readObjectSize(stream);
//        } catch (EOFException ignore) {
//            return 0;
//        }
//        if (objectSize == 0)
//            return 0;
//        if (objectSize + 4 > buffer.remaining())
//            throw new IOException("Buffer too small - required " + (objectSize + 4) + " bytes but available only " + buffer.remaining());
//        buffer.putInt(objectSize);
//        int remainingSize = objectSize;
//        while (remainingSize > 0) {
//            ByteBuffer data = stream.readInput(Math.min(remainingSize, copySize));
//            if (remainingSize < data.remaining()) {
//                int limit = data.limit();
//                data.limit(data.position() + remainingSize);
//                remainingSize -= data.remaining();
//                buffer.put(data);
//                data.limit(limit);
//            } else {
//                remainingSize -= data.remaining();
//                buffer.put(data);
//            }
//        }
//        return objectSize + 4;
//    }

    //************************ Search for constructor/factory ************************//
    /**
     * Returns a native-serializable constructor for <code>objectClass</code>. The constructor
     * should have the following prototype:
     * <pre>
     *      <i>ClassConstructor</i>({@link BinaryInput} input, {@link BinarySerializator} serializator) throws {@link IOException}
     * </pre>
     *
     * @param <T> the object class to construct
     * @param objectClass the object class to construct
     * @return a constructor for <code>objectClass</code> or <tt>null</tt> if there is no
     * native-serializable constructor
     */
//    protected static <T> Constructor<T> getNativeSerializableConstructor(Class<T> objectClass) {
//        try {
//            Constructor<T> constructor = objectClass.getDeclaredConstructor(BinaryInput.class, BinarySerializator.class);
//            constructor.setAccessible(true);
//            return constructor;
//        } catch (NoSuchMethodException ignore) {
//            return null;
//        }
//    }
//
//    /**
//     * Returns a native-serializable factory method for <code>objectClass</code>.
//     * The factory method should have the following prototype:
//     * <pre>
//     *      <i>ObjectClass</i> binaryDeserialize({@link BinaryInput} input, {@link BinarySerializator} serializator) throws {@link IOException}
//     * </pre>
//     * 
//     * @param objectClass the object class to construct
//     * @return a factory method for <code>objectClass</code> or <tt>null</tt> if there is no native-serializable factory method
//     */
//    protected static Method getNativeSerializableFactoryMethod(Class<?> objectClass) {
//        try {
//            Method method = objectClass.getDeclaredMethod("binaryDeserialize", BinaryInput.class, BinarySerializator.class);
//            if (!Modifier.isStatic(method.getModifiers())) {
//                return null;
//            }
//            method.setAccessible(true);
//            return method;
//        } catch (NoSuchMethodException ignore) {
//            return null;
//        }
//    }
//
//
//    //************************ Helper methods ************************//
//
//    /**
//     * Returns the value of the <code>serialVersionUID</code> field for the
//     * specified class.
//     * @param classToCheck the class for which to look-up the serial version
//     * @return the serial version of the specified class
//     * @throws NoSuchFieldException if the class does not have serial version
//     */
//    protected static long getSerialVersionUID(Class<?> classToCheck) throws NoSuchFieldException {
//        try {
//            Field field = classToCheck.getDeclaredField("serialVersionUID");
//            field.setAccessible(true);
//            return field.getLong(null);
//        } catch (IllegalAccessException ex) {
//            // This should never happen
//            return 0;
//        }
//    }
//
//    /**
//     * Returns the hash code for value of the <code>serialVersionUID</code>
//     * field of the specified class.
//     * @param classToCheck the class for which to look-up the serial version
//     * @return the serial version of the specified class
//     */
//    protected static int getSerialVersionUIDHash(Class<?> classToCheck) {
//        try {
//            long serialVersion = getSerialVersionUID(classToCheck);
//            return (int)(serialVersion >> 32) | (int)serialVersion;
//        } catch (NoSuchFieldException ignore) {
//            return -1;
//        }
//    }
    /**
     * Writes <code>object</code> to the provided output buffer. If the object implements
     * {@link BinarySerializable} interface, it is binary-serialized. Otherwise, a standard Java
     * {@link java.io.Serializable serialization} is used.
     *
     * @param output the buffer to write the object to
     * @param object the object to write
     * @return the number of bytes actually written
     * @throws IOException if there was an error using flushChannel
     */
//    public final int write(BinaryOutput output, Object object) throws IOException {
//        // Write null
//        if (object == null) {
//            return write(output, SerializationTypes.NULL);
//        }
//
//        writeObjectTypeAndData
//        
//        // Write object data to intermediate buffer
//        // TODO: guess size of this class
//        //GrowingBufferOutput growingBufferOutput = new GrowingBufferOutput(output, true);
//        //int objectSize = writeObjectData(growingBufferOutput, object);
//        
//        // write the size of the serialized object (because only now we know it)
//        //write(output, objectSize);
//        // actually write the binary data from serialization to output
//        //growingBufferOutput.flush();
//
//        //return objectSize + 4;
//    }
    /**
     * Writes <code>object</code> to this output buffer using binary serialization - just write the
     * data without the size of the data.
     *
     * @param output the buffer to write the object to
     * @param object the object to write
     * @return the number of bytes actually written
     * @throws IOException if there was an error using flushChannel
     */
    //protected abstract int writeObjectData(BinaryOutput output, Object object) throws IOException;
    /**
     * Abstract method that is to find out the object class to read from the input and then read it.
     *
     * @param input the buffer to read the instance from
     * @return an instance of the deserialized object
     * @throws IOException if there was an I/O error
     * @throws IllegalArgumentException if the constructor or the factory method has a wrong
     * prototype
     */
//    public final <E> E readObject(BinaryInput input, Class<E> expectedClass) throws IOException, IllegalArgumentException {        
//    }    
    /**
     * Reads an instance using the proper constructor/factory method as specified by this
     * serializator.
     *
     * @param <E> the class that is expected to be in the input
     * @param input the buffer to read the instance from
     * @param expectedClass the class that is expected to be in the input
     * @return an instance of the deserialized object
     * @throws IOException if there was an I/O error
     * @throws IllegalArgumentException if the constructor or the factory method has a wrong
     * prototype
     */
    //protected abstract <E> E readObjectData(BinaryInput input, Class<E> expectedClass) throws IOException, IllegalArgumentException;
}
