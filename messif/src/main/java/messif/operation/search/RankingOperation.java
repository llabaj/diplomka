/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.search;

import messif.data.DataObject;
import messif.data.util.RankedDataObject;
import messif.distance.DistanceException;
import messif.distance.DistanceFunc;
import messif.operation.answer.AbstractAnswer;
import messif.operation.ReturnDataOperation;
import messif.operation.helpers.AnswerProvider;
import messif.operation.helpers.BaseHelper;
import messif.operation.params.DistanceFunction;
import messif.operation.params.Range;
import messif.operation.answer.RankedAnswer;

import java.util.Iterator;

/**
 * A base class for all operations that return the data ranked using a distance function.
 */
public interface RankingOperation extends ReturnDataOperation, DistanceFunction {

    /** The default list of fields to be returned by all underlying operations is only the object identifier. */
    String [] DEFAULT_LIST = new String [] {DataObject.ID_FIELD };

    /** The default list of fields to be returned by all underlying operations is only the object identifier. */
    default String[] fieldsToReturn() {
        return fieldsToReturn(DEFAULT_LIST);
    }


    /**
     * This class helps the operation processors to process this type of operation by providing supporting methods and
     *  continuous building of the answer. It is an abstract class to be implemented by more specific helper classes.
     */
    abstract class Helper<TRequest extends RankingOperation, TAnswer extends RankedAnswer>
            extends BaseHelper<TRequest>
            implements AnswerProvider<TAnswer> {

        /** Builder of the ranked answer. */
        protected final RankedAnswer.Builder<TAnswer> answerBuilder;

        protected DistanceFunc<DataObject> distanceFunc;

        /**
         * A fixed radius of the query (for range query).
         */
        protected float radius = DistanceFunc.MAX_DISTANCE;


        // region  **********************      Constructors     ***************************

        /**
         * Creates the helper given all parameters.
         * @param operation the query operation
         * @param defaultDistanceFunc a default distance to be used ONLY IN CASE the
         *          {@link RankingOperation#getDistanceFunction() distance parameter} is not used
         * @param answerClass specific class of the answer
         */
        protected Helper(TRequest operation, DistanceFunc<DataObject> defaultDistanceFunc, Class<TAnswer> answerClass) {
            super(operation);

            this.answerBuilder = new RankedAnswer.Builder<>(operation, answerClass);

            if (operation.getParams().containsField(Range.RADIUS_FIELD)) {
                this.radius = operation.getParams().getField(Range.RADIUS_FIELD, Float.TYPE, radius);
            }
            this.distanceFunc = operation.getDistanceFunction() == null ? defaultDistanceFunc : operation.getDistanceFunction();
        }


        // endregion

        // region *******************       Getters, setters        *****************************
        public DistanceFunc<DataObject> getDistanceFunc() {
            return distanceFunc;
        }

        public void setDistanceFunc(DistanceFunc<DataObject> distanceFunc) {
            this.distanceFunc = distanceFunc;
        }
        // endregion

        // region ************        Operation evaluation and answer building utility methods     ******************

        /**
         * Returns the threshold distance for the current answer of this query.
         * If the answer has not reached the maximal size (specified in constructor) yet,
         * {@link DistanceFunc#MAX_DISTANCE} is returned.
         * Otherwise, the distance of the last answer's object is returned.
         *
         * @return the distance to the last object in the answer list or
         * {@link DistanceFunc#MAX_DISTANCE} if there are not enough objects.
         */
        public float getAnswerThreshold() {
            if (radius != DistanceFunc.MAX_DISTANCE) {
                return radius;
            }
            return answerBuilder.getAnswerCollection().getThresholdDistance();
        }

        /**
         * Evaluate this query on a given set of objects.
         * The objects found by this evaluation are added to answer of this query via {@link #addToAnswer}.
         *
         * @param objects the collection of objects on which to evaluate this query
         * @return number of objects satisfying the query
         */
        public int evaluate(Iterator<? extends DataObject> objects) {
            int counter = 0;

            // Iterate through all supplied objects
            while (objects.hasNext()) {
                try {
                    // Get current object
                    if (addToAnswer(objects.next(), this.getAnswerThreshold()) != null) {
                        counter++;
                    }
                } catch (DistanceException ignore) {
                }
            }

            return counter;
        }

        /**
         * Adds an object to the answer. The rank of the object is computed automatically
         * as a distance between the query object and the specified object.
         *
         * @param object the object to add
         * @return the distance-ranked object object that was added to answer or <tt>null</tt> if the object was not added
         */
        public RankedDataObject addToAnswer(DataObject object) throws DistanceException {
            return addToAnswer(object, this.getAnswerThreshold());
        }
        // endregion

        /**
         * Method to be implemented by the extending processors that should calculate the distance for given
         * object and add it into the answer.
         *
         * @param object        data object to be inserted into the answer (given the calculated distance is not over the threshold)
         * @param distThreshold threshold on the distance
         * @return the (object, distance) pair encapsulated as {@link RankedDataObject}
         */
        abstract protected RankedDataObject addToAnswer(DataObject object, float distThreshold);

        // region ********************     Delegated methods
        @Override
        public TAnswer getAnswer() {
            return answerBuilder.getAnswer();
        }

        @Override
        public void updateAnswer(AbstractAnswer answer) {
            answerBuilder.updateAnswer(answer);
        }
        // endregion
    }

    /**
     * A static method to create the right helper given {@link QueryObjectOperation} or {@link MultiQueryObjectOperation}.
     * @param operation operation to crete the helper for
     * @param defaultDistanceFunc default query-data distance function
     * @return respective helper
     */
    static RankingOperation.Helper getHelper(RankingOperation operation, DistanceFunc<DataObject> defaultDistanceFunc) {
        if (operation instanceof QueryObjectOperation) {
            return new QueryObjectOperation.Helper((QueryObjectOperation) operation, defaultDistanceFunc);
        }
        if (operation instanceof MultiQueryObjectOperation) {
            return new MultiQueryObjectOperation.Helper((MultiQueryObjectOperation) operation, defaultDistanceFunc);
        }
        throw new RuntimeException("don't know how to process the following operation " + operation.toString());
    }

}
