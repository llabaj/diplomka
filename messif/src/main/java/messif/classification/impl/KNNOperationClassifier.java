/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.classification.impl;

import messif.algorithm.Algorithm;
import messif.classification.Classifier;
import messif.data.DataObject;
import messif.operation.OperationBuilder;
import messif.operation.search.KNNOperation;
import messif.operation.search.QueryObjectOperation;
import messif.operation.answer.RankedAnswer;

/**
 * Implementation of a classifier by k-nearest neighbors operation.
 * The classification of an object is inferred from its most similar objects.
 * The actual classification is computed from the retrieved nearest neighbors
 * by a given {@link Classifier}.
 *
 * TODO: generalize this and the ApproxKNNOC classes to a single class that takes operation class and any parameters
 *
 * @param <C> the class of instances that represent the classification categories
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class KNNOperationClassifier<C> extends RankingQueryOperationClassifier<C> {

    /** Number of nearest neighbors to retrieve */
    protected final int k;

    /** A list of fields to be returned for each {@link DataObject} in the answer. Default, in this case, is ALL fields.*/
    protected final String [] fieldsToReturn;

    /**
     * Creates a new kNN classifier.
     * @param classifier the classifier used to compute the object classification
     * @param k the number of nearest neighbors to retrieve
     * @param algorithm the algorithm that supplies the similar objects
     * @param executedOperationParameter the name of the parameter to put the executed operation into when classifying
     */
    public KNNOperationClassifier(Classifier<? super RankedAnswer, C> classifier, int k,
                                  Algorithm algorithm, String executedOperationParameter) {
        this(classifier, k, new String [] {}, algorithm, executedOperationParameter);
    }

    /**
     * Creates a new kNN classifier.
     * @param classifier the classifier used to compute the object classification
     * @param k the number of nearest neighbors to retrieve
     * @param fieldsToReturn a list of data object fields to be returned for each answer object
     * @param algorithm the algorithm that supplies the similar objects
     * @param executedOperationParameter the name of the parameter to put the executed operation into when classifying
     */
    public KNNOperationClassifier(Classifier<? super RankedAnswer, C> classifier, int k, String [] fieldsToReturn,
                                  Algorithm algorithm, String executedOperationParameter) {
        super(classifier, algorithm, executedOperationParameter);
        this.k = k;
        this.fieldsToReturn = fieldsToReturn;
    }

    @Override
    protected QueryObjectOperation createOperation(DataObject object) {
        return OperationBuilder.create(KNNOperation.class).addParam(KNNOperation.QUERY_OBJECT_FIELD, object)
                .addParam(KNNOperation.K_FIELD, k).build();
    }
}
