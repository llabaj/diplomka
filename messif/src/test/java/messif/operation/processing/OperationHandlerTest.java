/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.processing;

import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.record.ModifiableRecord;
import messif.utility.ProcessingStatus;
import org.junit.Assert;
import org.junit.Test;

import java.util.Iterator;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class OperationHandlerTest {
    @Test
    public void execution_test() throws Exception {

        StringBuilder logMessage = new StringBuilder();
        OperationExecutor executor1 = new LogOperationExecutor(logMessage, 1);
        LogOperationExecutor executor2 = new LogOperationExecutor(logMessage, 2);
        OperationExecutor executor3 = new LogOperationExecutor(logMessage, 3);
        OperationHandler handler = new OperationHandler(executor1, executor2, executor3);

        ProcessorConfiguration configuration = ProcessorConfiguration.create(LogOperation.class, LogOperationAnswer.class)
                .withProcessor(operation -> new LogOperationProcessor(logMessage)).build();

        handler.register(configuration);

        LogOperation operation = new LogOperation();

        handler.evaluate(operation);
        String expectedMessage = "Before 1\nBefore 2\nBefore 3\nLog Operation Processor\nAfter 3\nAfter 2\nAfter 1\n";
        Assert.assertEquals(expectedMessage, logMessage.toString());
    }

    private static class LogOperationExecutor extends OperationExecutor {
        final StringBuilder log;
        final int id;

        private LogOperationExecutor(StringBuilder log, int id) {
            this.log = log;
            this.id = id;
        }

        @Override
        public AbstractAnswer execute(AbstractOperation operation, Iterator<OperationExecutor> iterator) throws Exception {
            try {
                log.append("Before ").append(id).append("\n");
                return executeNext(operation, iterator);
            } finally {
                log.append("After ").append(id).append("\n");
            }
        }
    }

    private static class LogOperation implements AbstractOperation {

        @Override
        public String getInnerType() {
            return "LOG OPERATION";
        }

        @Override
        public String getFieldMapType() {
            return null;
        }

        @Override
        public ModifiableRecord getParams() {
            return null;
        }
    }

    private static class LogOperationAnswer implements AbstractAnswer {

        @Override
        public ProcessingStatus getStatus() {
            return null;
        }

        @Override
        public boolean wasSuccessful() {
            return false;
        }

        @Override
        public ModifiableRecord getParams() {
            return null;
        }

    }

    private static class LogOperationProcessor implements OperationProcessor<LogOperation, LogOperationAnswer> {

        final StringBuilder log;

        private LogOperationProcessor(StringBuilder log) {
            this.log = log;
        }

        @Override
        public LogOperation getOperation() {
            return null;
        }

        @Override
        public boolean processStep() throws InterruptedException, CloneNotSupportedException {
            log.append("Log Operation Processor").append("\n");
            return false;
        }

        @Override
        public LogOperationAnswer finish() {
            return new LogOperationAnswer();
        }


    }
}