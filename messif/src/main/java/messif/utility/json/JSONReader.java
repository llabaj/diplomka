/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.json;

import messif.record.RecordImpl;

import java.io.*;
import java.util.*;

public class JSONReader implements AutoCloseable {

    // region ****************************      Fields, getters and setters    ******************************

    /**
     * Null is defined in the JSON standard.
     */
    public static final Object NULL = null;

    /**
     * The reader to read the data from.
     */
    protected final Reader reader;

    /**
     * Internal object to separate individual JSON tokens.
     */
    protected final JSONTokener tokener;

    /** Instead of <code>Map<String, Object></code>, create {@link messif.record.RecordImpl} (which implements also {@link messif.data.DataObject}). */
    protected final boolean createRecordImpl;

    /**
     * Get the underlying reader
     *
     * @return
     */
    public Reader getUnderlyingReader() {
        return reader;
    }
    //endregion

    /**
     * Create a new JSON reader for data in given reader
     *
     * @param r text reader
     * @param createRecordImpl
     */
    public JSONReader(Reader r, boolean createRecordImpl) {
        this.reader = r;
        this.createRecordImpl = createRecordImpl;
        this.tokener = new JSONTokener(reader);
    }

    // region ******************************     Constructors and static builders ******************************

    /**
     * Create a new JSON reader for data in given reader
     *
     * @param inputStream the input stream to read the data from
     * @param createRecordImpl
     */
    public JSONReader(InputStream inputStream, boolean createRecordImpl) {
        this(new InputStreamReader(inputStream), createRecordImpl);
    }

    /**
     * Creates a JSON reader for the passed text
     *
     * @param text a string with the JSON-formatted text
     * @param createRecordImpl
     */
    public JSONReader(String text, boolean createRecordImpl) {
        this(new StringReader(text), createRecordImpl);
    }

    /**
     * Returns an object as read and parsed from the text in the given string
     *
     * @return map, String, Integer, Float, int[], float[], Object[] or null
     * @throws JSONException if the passed text is not valid JSON or it contains more than one JSON object
     */
    public static Object readObjectFrom(String string, boolean createRecordImpl) throws JSONException {
        final JSONReader jsonReader = new JSONReader(string, createRecordImpl);
        try {
            return jsonReader.readNextObject();
        } finally {
            if (jsonReader.tokener.more()) {
                throw new JSONException("did not read the string fully: '" + string + "'");
            }
        }
    }
    // endregion

    // region ********************    Methods that actually parser the JSON text and create the objects

    public RecordImpl readRecord() throws JSONException {
        final Map<String, Object> map = readJSONObject();
        if (map == null)
            return null;
        return new RecordImpl(map, true);
    }

    /**
     * Returns an object as read and parsed from the text in the reader.
     *
     * @return Map (or RecordImpl), String, Integer, Float, int[], float[], Object[] or null
     */
    public Object readNextObject() throws JSONException {
        char c = tokener.nextClean();
        switch (c) {
            case '"':
            case '\'':
                return tokener.nextString(c);
            case '{':
                tokener.back();
                return createRecordImpl ? readRecord() : readJSONObject();
            case '[':
                tokener.back();
                return readJSONArray();
        }

        /*
         * Handle unquoted text. This could be the values true, false, or
         * null, or it can be a number. An implementation (such as this one)
         * is allowed to also accept non-standard forms.
         *
         * Accumulate characters until we reach the end of the text or a
         * formatting character.
         */
        String string;
        StringBuilder sb = new StringBuilder();
        while (c >= ' ' && ",:]}/\\\"[{;=#".indexOf(c) < 0) {
            sb.append(c);
            c = tokener.next();
        }
        if (c > 0)
            tokener.back();

        string = sb.toString().trim();
        if ("".equals(string)) {
            throw new JSONException("Missing value (empty string)");
        }
        return stringToValue(string);
    }

    /**
     * This method expects that the passed reader contains a JSON-formatted object (map), it reads it and returns it.
     *
     * @return a map with parsed JSON object
     * @throws JSONException if the text is not properly formatted.
     */
    protected Map<String, Object> readJSONObject() throws JSONException {
        Map<String, Object> retVal = new HashMap<>();
        String key;
        char c = tokener.nextClean();
        // tolerate a '[', ',' and ']' in the beginning (or end) - these objects might be in an array [ {...}, {...}, ]
        if (c == '[' || c == ',' || c == ']') {
            c = tokener.nextClean();
        }
        // return null if we are at the end of the file
        if (tokener.end()) return null;

        if (c != '{') {
            throw tokener.syntaxError("A JSONObject text must begin with '{'");
        }
        mainloop:
        for (; ; ) {
            c = tokener.nextClean();
            switch (c) {
                case 0:
                    throw tokener.syntaxError("A JSONObject text must end with '}'");
                case '}':
                    break mainloop;
                default:
                    tokener.back();
                    key = tokener.nextString().intern();
            }

            // The key is followed by ':'.
            c = tokener.nextClean();
            if (c != ':') {
                throw tokener.syntaxError("Expected a ':' after a key");
            }
            retVal.put(key, readNextObject());

            // Pairs are separated by ','.
            switch (tokener.nextClean()) {
                case ';':
                case ',':
                    if (tokener.nextClean() == '}') {
                        break mainloop;
                    }
                    tokener.back(); // THIS MIGHT BE A MISTAKE
                    break;
                case '}':
                    break mainloop;
                default:
                    throw tokener.syntaxError("Expected a ',' or '}'");
            }
        }
        return retVal;
    }

    protected Object readJSONArray() throws JSONException {
        if (tokener.nextClean() != '[') {
            throw tokener.syntaxError("A JSONArray text must start with '['");
        }
        if (tokener.nextClean() == ']') {
            return null;
        }
        tokener.back();
        ArrayList list = new ArrayList();
        mainloop:
        for (; ; ) {
            if (tokener.nextClean() == ',') {
                tokener.back();
                list.add(NULL);
            } else {
                tokener.back();
                list.add(readNextObject());
            }
            switch (tokener.nextClean()) {
                case ',':
                    if (tokener.nextClean() == ']') {
                        break mainloop;
                    }
                    tokener.back();
                    break;
                case ']':
                    break mainloop;
                default:
                    throw tokener.syntaxError("Expected a ',' or ']'");
            }
        }
        if (list.isEmpty()) {
            return null;
        }
        // convert the ArrayList to static array
        Object firstObject = list.get(0);
        if (firstObject instanceof Integer) {
            return toIntArray(list);
        }
        if (firstObject instanceof Float) {
            return toFloatArray(list);
        }
        if (firstObject instanceof String) {
            return toStringArray(list);
        }
        return list.toArray();
    }

    private static int[] toIntArray(List collection) throws JSONException {
        Object[] boxedArray = collection.toArray();
        int len = boxedArray.length;
        int[] array = new int[len];
        try {
            for (int i = 0; i < len; i++) {
                array[i] = (Integer) boxedArray[i];
            }
        } catch (ClassCastException e) {
            throw new JSONException("all array members must be of the same type: " + Arrays.toString(boxedArray));
        }
        return array;
    }

    private static float[] toFloatArray(List collection) throws JSONException {
        Object[] boxedArray = collection.toArray();
        int len = boxedArray.length;
        float[] array = new float[len];
        try {
            for (int i = 0; i < len; i++) {
                array[i] = ((Number) boxedArray[i]).floatValue();
            }
        } catch (ClassCastException e) {
            throw new JSONException("all array members must be of the same type: " + Arrays.toString(boxedArray));
        }
        return array;
    }

    private static String[] toStringArray(List collection) throws JSONException {
        try {
            return ((List<String>) collection).toArray(new String[collection.size()]);
        } catch (ClassCastException | ArrayStoreException e) {
            throw new JSONException("all array members must be of the same type: " + collection.toString());
        }
    }

    /**
     * Try to convert a string into a number, boolean, or null. If the string
     * can't be converted, return the string.
     *
     * @param string A String.
     * @return A simple JSON value.
     */
    protected static Object stringToValue(String string) {
        Float fl;
        if (string.equals("")) {
            return string;
        }
        if (string.equalsIgnoreCase("true")) {
            return Boolean.TRUE;
        }
        if (string.equalsIgnoreCase("false")) {
            return Boolean.FALSE;
        }
        if (string.equalsIgnoreCase("null")) {
            return NULL;
        }

        /*
         * If it might be a number, try converting it. If a number cannot be
         * produced, then the value will just be a string.
         */
        char b = string.charAt(0);
        if ((b >= '0' && b <= '9') || b == '-') {
            try {
                if (string.indexOf('.') > -1 || string.indexOf('e') > -1
                        || string.indexOf('E') > -1) {
                    fl = Float.valueOf(string);
                    if (!fl.isInfinite() && !fl.isNaN()) {
                        return fl;
                    }
                } else {
                    return Integer.parseInt(string);
//                    if (string.equals(myInt.toString())) {
//                        if (myInt == myInt.intValue()) {
//                            return myInt.intValue();
//                        } else {
//                            return myInt;
//                        }
//                    }
                }
            } catch (Exception ignore) {
            }
        }
        return string;
    }

    // ******************    All other methods from Writer are just passed to the encapsulated Writer object   ******** //
//    @Override
//    public int read(char[] cbuf, int off, int len) throws IOException {
//        return reader.read(cbuf, off, len);
//    }
//
    @Override
    public void close() throws IOException {
        reader.close();
    }

//    public boolean isEOF() {
//        return tokener.end();
//    }
}
