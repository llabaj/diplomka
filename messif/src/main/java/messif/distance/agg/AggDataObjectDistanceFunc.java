/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.distance.agg;

import messif.distance.DataObjectDistanceFunc;
import messif.distance.DistanceFunc;
import messif.data.DataObject;
import messif.record.Record;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Evaluator for basic arithmetic operators and functions applied on particular sub-distances.
 * Basic arithmetic operators (+, -, *, /, ^) and "log" and "log10" functions are
 * supported as well as numeric constants (treated as floats). Non-standard arithmetic operators are:
 * <ul>
 *  <li>@  meaning arithmetic mean</li>
 *  <li>&amp;  meaning geometric mean</li>
 *  <li>#  meaning harmonic mean</li>
 * </ul>
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class AggDataObjectDistanceFunc extends AggregationDistanceFuncBase<DataObject> {

    /**
     * Class id for object serialization.
     */
    private static final long serialVersionUID = 1L;


    //****************** Constants ******************//

    /**
     * Pattern used to retrieve tokens from the function string
     */
    private static final Pattern tokenizerPattern = Pattern.compile("\\s*(([\\w.,]+)|([-+*/\\^@&#])|(\\([^\\(\\)]*)|(\\)))\\s*");


    //****************** Attributes ******************//

    /**
     * The top level token that encapsulates the whole aggregation function string.
     */
    private final Token pattern;

    /**
     * Precomputed maximal distance calculated as the pattern applied on individual maximal distances.
     */
    protected final float maxDistance;


    //****************** Constructors & Builders ******************//


    /**
     * Creates a new instance of AggregationDistance. The specified function is parsed and compiled.
     * Basic arithmetic operations are supported as well as numeric constants.
     * Blank space is ignored and everything else is considered to be a variable.
     *
     * @param function          the function string
     * @param distanceFunctions distances to be applied on the the fields specified in the "function" string
     * @throws IllegalArgumentException if the specified function cannot be parsed
     */
    public static AggregationDistanceFuncBase<DataObject> build(String function, DistanceFunc... distanceFunctions) throws IllegalArgumentException {
        List<TokenSubdistance> subdistancesList = new ArrayList<>();
        final Token rootToken = parse(function, subdistancesList);

        if (subdistancesList.isEmpty())
            throw new IllegalArgumentException("Specified function contains no variables");
        if (subdistancesList.size() != distanceFunctions.length)
            throw new IllegalArgumentException("The number of distance functions must match the number of fields in the string");

        // for each given distance function, create a Record function on respective field
        DistanceFunc<DataObject>[] fieldMapDistanceFuncs = new DistanceFunc[distanceFunctions.length];
        // Create Distance<Record> wrappers around each distance
        for (int i = 0; i < subdistancesList.size(); i++) {
            fieldMapDistanceFuncs[i] = new DataObjectDistanceFunc<>(subdistancesList.get(i).getName(), distanceFunctions[i]);
        }

        return new AggDataObjectDistanceFunc(rootToken, fieldMapDistanceFuncs);
    }

    /**
     * Creates a new instance of AggregationDistance given a precompiled pattern and a list of distances
     * on the {@link Record} ({@link DataObject}).
     *
     * @param pattern           precompiled pattern
     * @param distanceFunctions list of distances on the {@link Record}
     */
    protected AggDataObjectDistanceFunc(Token pattern, DistanceFunc<DataObject>[] distanceFunctions) throws IllegalArgumentException {
        super(distanceFunctions);
        this.pattern = pattern;
        this.maxDistance = calculateMaxDistances();
    }


    //****************** Expression parsing ******************//

    /**
     * Internal method for parsing the aggregation function string. It is called recursively for expressions in brackets.
     *
     * @param patternString          agg. function string to parse
     * @param currentSubdistanceList list of tokens for subdistances found in the string - used internaly when
     *                               calling this method recursively
     * @return root of the parsed tree created from the expression passed
     * @throws IllegalArgumentException if the passed string is not valid
     */
    private static Token parse(String patternString, List<TokenSubdistance> currentSubdistanceList) throws IllegalArgumentException {

        // Parse pattern string
        Matcher token = tokenizerPattern.matcher(patternString);

        // the first operand of the currently constructed operation or function
        Token operand1 = null;
        String operationString = null;
        String functionString = null;

        // this counter is set positive in case we are building the "smallest string with the right brackets"
        int bracketsLevel = 0;
        StringBuffer bracketsString = new StringBuffer();

        // Get next token
        while (token.find()) {

            // if it is expression starting by "left bracket"
            String tokenString = token.group(4);
            if (tokenString != null) {
                if ((operand1 != null) && (operationString == null)) {
                    throw new IllegalArgumentException("Parsing brackets " + tokenString + " while operator should be here: " + patternString);
                }
                bracketsLevel++;
                bracketsString.append(tokenString);
                continue;
            }

            // if an operand is constructed in this iteration, it is stored here (constant, subdist, brackets=tree)
            Token operand = null;

            // if we constructing a term with the right number of brackets ((3 - (2 * log)) + 1)
            // and we have reached an expression with "right bracket" ")"
            if (bracketsLevel > 0) {
                tokenString = token.group(5);
                if (tokenString != null) {
                    bracketsLevel--;
                    bracketsString.append(tokenString);

                    // if a good-bracket string was parsed
                    if (bracketsLevel != 0) {
                        continue;
                    }
                    String bracket = bracketsString.toString();
                    bracketsString = new StringBuffer();
                    bracket = bracket.substring(1, bracket.length() - 1);
                    operand = parse(bracket, currentSubdistanceList);
                } else {
                    tokenString = token.group();
                    bracketsString.append(tokenString);
                    continue;
                }
            }
            // if the bracket-expression was NOT ended now
            if (operand == null) {
                // if the token is an operator
                tokenString = token.group(3);
                if (tokenString != null) {
                    if (operand1 == null) {
                        throw new IllegalArgumentException("Arithmetic operator " + tokenString + " must be after a meaningful first operand: " + patternString);
                    }
                    operationString = tokenString;
                    continue;
                }

                // if the token is a constant or if the token is application of a function or a subdistance identifier
                tokenString = token.group(2);
                if (tokenString != null) {
                    // if it is function (log)
                    if (TokenArithmeticFunction.isFunctionString(tokenString)) {
                        if ((operand1 != null) && (operationString == null)) {
                            throw new IllegalArgumentException("Arithmetic function parsed but operation expected: " + patternString);
                        }
                        functionString = tokenString;
                        continue;
                    } else { // this is constant or sub-distance
                        // crate the operand
                        try {
                            operand = new TokenConstant(tokenString);
                        } catch (IllegalArgumentException e) {
                            operand = new TokenSubdistance(tokenString, currentSubdistanceList);
                        }
                    }
                }
            }

            // if an operand was created then construct function and/or operation
            if (operand != null) {
                // if this operand succeeds function symbol
                if (functionString != null) {
                    if ((operand1 != null) && (operationString == null)) {
                        throw new IllegalArgumentException("Function cannot be applied where operation expected: " + patternString);
                    }
                    operand = new TokenArithmeticFunction(functionString, operand);
                    functionString = null;
                }

                // if this is second operand for an operation
                if (operationString != null) {
                    if (operand1 == null) {
                        throw new IllegalArgumentException("Second operand parsed while the first was not parsed before: " + patternString);
                    }
                    operand1 = new TokenArithmeticOperator(operand1, operationString, operand);
                    operationString = null;
                } else { // this was the first operand
                    if (operand1 != null) {
                        throw new IllegalArgumentException("First operand expected but one already created: " + operand1.toString() + ", while parsing " + tokenString + " in: " + patternString);
                    }
                    operand1 = operand;
                }
                continue;
            }

            // error parsing string - none of the groups matched???
            throw new IllegalArgumentException("None of the groups matched? Error parsing: " + patternString);
        }

        if (operand1 == null) {
            throw new IllegalArgumentException("Error parsing: " + patternString);
        }
        return operand1;
    }

    //****************** Evaluating methods ******************//

    @Override
    public float compute(float... distances) {
        return pattern.evaluate(distances);
    }


    @Override
    public float getMaxDistance() {
        return maxDistance;
    }

    @Override
    public Class<DataObject> getObjectClass() {
        return DataObject.class;
    }

    //****************** String conversion ******************//

    /**
     * Returns a string representation of the encapsulated function.
     *
     * @return a string representation of the encapsulated function
     */
    @Override
    public String toString() {
        return pattern.toString();
    }
}
