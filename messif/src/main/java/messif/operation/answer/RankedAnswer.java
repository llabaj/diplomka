/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.answer;

import messif.data.DataObject;
import messif.data.util.DistanceRankedObject;
import messif.data.util.RankedDataObject;
import messif.data.util.RankedSortedCollection;
import messif.operation.AbstractOperation;
import messif.operation.ReturnDataOperation;
import messif.operation.helpers.AnswerProvider;
import messif.operation.helpers.ErrorCodeHelper;
import messif.operation.helpers.SimpleErrorCodeMerger;
import messif.operation.params.Distances;
import messif.operation.params.KNN;
import messif.record.ModifiableRecord;
import messif.statistic.StatisticTimer;
import messif.statistic.Statistics;
import messif.utility.ProcessingStatus;
import messif.utility.proxy.ProxyConverter;

import java.util.Collection;

/**
 * An answer that contains a list of data objects as in {@link ListingAnswer} and also a list of ranking scores,
 *  typically query-object distances. These distances are stored in a separate array.
 */
public interface RankedAnswer extends ListingAnswer, Distances {

    // region *************************            Factory methods       ************************** //

    static RankedAnswer createSuccessful(Collection<? extends DistanceRankedObject<DataObject>> objects, String[] fieldsToReturn, ProcessingStatus status) {
        return RankedAnswer.createSuccessful(objects, fieldsToReturn, status, RankedAnswer.class);
    }

    static <TAnswer extends RankedAnswer> TAnswer createSuccessful(Collection<? extends DistanceRankedObject<DataObject>> objects,
                                                                   String[] fieldsToReturn, ProcessingStatus status, Class<TAnswer> answerClass) {
        float [] distances = new float[objects.size()];
        final DataObject[] objectsToReturn = new DataObject[objects.size()];
        int i = 0;
        for (DistanceRankedObject<DataObject> rankedObject : objects) {
            distances[i] = rankedObject.getDistance();
            objectsToReturn[i] = ReturnDataOperation.transformObject(rankedObject.getObject(), fieldsToReturn);
            i ++;
        }

        ModifiableRecord fieldMap = AbstractAnswer.createMap(status);
        fieldMap.setField(ANSWER_OBJECTS_FIELD, objectsToReturn);
        fieldMap.setField(ANSWER_OBJ_COUNT_FIELD, objectsToReturn.length);
        fieldMap.setField(Distances.ANSWER_DISTANCES_FIELD, distances);

        return ProxyConverter.convert(fieldMap, answerClass);
    }
    // endregion

    /**
     * This class helps to iteratively build the answer; it is used by operation helpers and operation processors.
     */
    class Builder<TAnswer extends RankedAnswer> implements AnswerProvider<TAnswer> {

        /**
         * Time spent in {@link #setAnswerCollection(RankedSortedCollection)}
         */
        private static final StatisticTimer timeSetAnswerCollection = StatisticTimer.getStatistics("OperationSetAnswerCollectionTime");

        /**
         * The continuously created answer. If "k" is specified, it is part of the answer collection. This collection
         * is internally synchronized.
         */
        protected RankedSortedCollection answerCollection;

        private final Class<TAnswer> answerClass;

        protected final ReturnDataOperation operation;

        public final ErrorCodeHelper errorCodeHelper;

        public Builder(ReturnDataOperation operation, RankedSortedCollection answerCollection, Class<TAnswer> answerClass) {
            this(operation, answerCollection, answerClass, new SimpleErrorCodeMerger<>(answerClass));
        }

        public Builder(ReturnDataOperation operation, RankedSortedCollection answerCollection, Class<TAnswer> answerClass, ErrorCodeHelper errorCodeHelper) {
            this.operation = operation;
            this.answerCollection = answerCollection;
            this.errorCodeHelper = errorCodeHelper;
            this.answerClass = answerClass;
        }

        public Builder(ReturnDataOperation operation, Class<TAnswer> answerClass) {
            this(operation, getAnswerCollection(operation), answerClass);
        }

        public static RankedSortedCollection getAnswerCollection(AbstractOperation operation) {
            final Integer k = operation.getParams().getField(KNN.K_FIELD, int.class);
            if (k != null)
                return new RankedSortedCollection(k, k);
            else
                return new RankedSortedCollection();
        }

        // region *************************         Answer manipulation methods       ********************

        /**
         * This is the main method used for adding new objects into the answer.
         * @param object the object to be added into the answer
         * @param distance distance of the added object from the query object(s) or any other
         * @param objectDistances so called sub-distances - TODO: this array is typically null now
         * @return the created instance of {@link RankedDataObject} that encapsulates all the parameters passed
         */
        public synchronized RankedDataObject add(DataObject object, float distance, float[] objectDistances) {
            return answerCollection.add(object, distance, objectDistances);
        }

        public RankedSortedCollection getAnswerCollection() {
            return answerCollection;
        }

        /**
         * Set a new collection that maintains the answer list of this ranking query.
         * Note that this method should be used only for changing the re-ranking/filtering
         * of the results.
         *
         * @param collection a new instance of answer collection
         */
        public void setAnswerCollection(RankedSortedCollection collection) {
            if (!collection.isEmpty())
                collection.clear();
            collection.setMaximalCapacity(answerCollection.getMaximalCapacity()); // Preserve maximal capacity (note that the collection can ignore it silently)
            collection.setIgnoringDuplicates(answerCollection.isIgnoringDuplicates()); // Preserve ignore duplicates flag (note that the collection can ignore it silently)
            if (Statistics.isEnabledGlobally()) {
                timeSetAnswerCollection.start();
                collection.addAll(answerCollection);
                timeSetAnswerCollection.stop();
            } else {
                collection.addAll(answerCollection);
            }
            this.answerCollection = collection; // This assignment IS intended
        }

        public Class<? extends RankedSortedCollection> getAnswerClass() {
            if (answerCollection == null)
                return RankedSortedCollection.class;
            else
                return answerCollection.getClass();
        }

        @Override
        public TAnswer getAnswer() {
            ProcessingStatus status = errorCodeHelper.getStatus();
            if (!status.isSet()) {
                status = (answerCollection == null || answerCollection.isEmpty()) ? AbstractAnswer.EMPTY_ANSWER : AbstractAnswer.OPERATION_OK;
            }
            return RankedAnswer.createSuccessful(answerCollection, operation.fieldsToReturn(), status, answerClass);
        }
        // endregion


        // region **********************       Update answer from other answers (answer builders)    ****************
        /**
         * Update the error code and answer of this operation from another operation.
         *
         * @param answer the source operation from which to get the update
         * @throws IllegalArgumentException if the answer of the specified operation is incompatible with this one
         */
        @Override
        public void updateAnswer(AbstractAnswer answer) throws IllegalArgumentException {
            errorCodeHelper.updateAnswer(answer);
            if (answer instanceof RankedAnswer) {
                updateAnswer(((RankedAnswer) answer).getAnswerObjects(), ((RankedAnswer) answer).getAnswerDistances());
            }
        }

        protected void updateAnswer(DataObject[] objects, float[] distances) throws IllegalArgumentException {
            int length = Math.min(objects.length, distances.length);
            for (int i = 0; i < length; i++) {
                this.answerCollection.add(objects[i], distances[i], null);
            }
        }

        /**
         * Update the answer from another answer builder
         *
         * @param answerBuilder the source anser builder from which to get the update
         */
        public void updateAnswer(RankedAnswer.Builder answerBuilder) {
            updateAnswer(answerBuilder.answerCollection);
        }

        /**
         * Update the answer from another ranked list.
         *
         * @param answerCollection a partial answer to update the answer from
         */
        public void updateAnswer(RankedSortedCollection answerCollection) {
            this.answerCollection.addAll(answerCollection);
        }
        // endregion

    }
}
