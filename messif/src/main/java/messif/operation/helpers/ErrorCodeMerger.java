/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.helpers;

import messif.operation.answer.AbstractAnswer;
import messif.utility.ProcessingStatus;

import javax.annotation.Nonnull;
import java.util.Arrays;

/**
 * This class merges answer codes from many operations remembering the worst and the best error codes.
 */
public class ErrorCodeMerger implements ErrorCodeHelper, AnswerProvider<AbstractAnswer> {

    /**
     * The worst error code returned for any object as processed by the storage buckets.
     */
    protected volatile ProcessingStatus worstStatus;

    /**
     * The best error code returned by the storage buckets.
     */
    protected volatile ProcessingStatus bestStatus;

    /**
     * A status that occurred in this merger but does not belong either to alright codes nor to error ones.
     */
    protected volatile ProcessingStatus someStatus;

    /**
     * The partial error code to be returned in case of some processings failed and some were alright.
     */
    private final ProcessingStatus partialStatus;

    /**
     * A list of error codes that are considered alright (success).
     */
    final ProcessingStatus[] alrightCodes;
    private final ProcessingStatus[] errorCodes;

    /**
     *
     * @param partialStatus
     * @param alrightCodes
     */
    public ErrorCodeMerger(ProcessingStatus partialStatus, ProcessingStatus [] alrightCodes, ProcessingStatus [] errorCodes) {
        this.partialStatus = partialStatus;
        this.alrightCodes = alrightCodes;
        this.errorCodes = Arrays.copyOf(errorCodes, errorCodes.length + 3);
        this.errorCodes[errorCodes.length] = AbstractAnswer.ABORTED_BY_ERROR;
        this.errorCodes[errorCodes.length + 1] = ProcessingStatus.UNKNOWN_ERROR;
        this.errorCodes[errorCodes.length + 2] = AbstractAnswer.ABORTED_BY_USER;
    }

    @Override
    public void updateAnswer(@Nonnull AbstractAnswer answer) {
        if (! answer.getStatus().isSet()) {
            return;
        }
        updateErrorCode(answer.getStatus());
    }

    @Override
    public AbstractAnswer getAnswer() {
        return AbstractAnswer.createAnswer(getStatus());
    }

    @Override
    public void updateErrorCode(ProcessingStatus status) {
        checkAndUpdateCode(status);
    }

    /**
     * Checks if the given status is correct or not, or we don't know. The status record is updated.
     * @param status a new status emerged during processing this operation
     * @return true only if given status is among the right "alright" statuses, false otherwise
     */
    public boolean checkAndUpdateCode(ProcessingStatus status) {
        if (status.in(alrightCodes)) {
            this.bestStatus = status;
            return true;
        }
        if (status.in(errorCodes)) {
            this.worstStatus = status;
            return false;
        }
        this.someStatus = status;
        return false;
    }

    public ProcessingStatus getStatus() {
        if (wasSuccess()) {
            // if at least one object processed FAILED then use partial code (e.g. "some objects inserted")
            return wasFail() ? partialStatus : bestStatus;
        }
        if (wasFail())
            return worstStatus;
        if (this.someStatus != null) {
            return someStatus;
        }
        return ProcessingStatus.NOT_SET;
    }


    /**
     * A helper method to return info if at least one of the answers were successful.
     */
    public boolean wasSuccess() {
        return bestStatus != null;
    }

    public boolean wasFail() {
        return worstStatus != null;
    }

}
