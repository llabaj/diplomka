/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.crud;

import messif.bucket.BucketStorageException;
import messif.bucket.LocalBucket;
import messif.data.DataObject;
import messif.operation.crud.answer.CRUDAnswer;
import messif.operation.crud.answer.UpdateAnswer;
import messif.record.ModifiableRecord;
import messif.record.ModifiableRecordImpl;
import messif.utility.ProcessingStatus;
import messif.utility.proxy.annotations.Get;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This operation updates each of the matching data objects in the algorithm by setting specified fields
 * to specified values. Setting a value to "NULL" means removing the key-value pair. Fields are overwritten only
 * if explicitly allowed.
 */
public interface UpdateOperation extends ModifyingOperation {

    String TYPE_STRING = "update";

    @Override
    default String getInnerType() {
        return TYPE_STRING;
    }

//    /**
//     * Name of the field to contain the JSON (key-value map) to be used for update.
//     */
//    String UPDATE_MAP_FIELD = "update_data";
//
//    /**
//     * This method provides a convenient access to the field with name {@link #UPDATE_MAP_FIELD}.
//     *
//     * @return a map with fields to be updated for each matching object
//     */
//    @Get(field = UPDATE_MAP_FIELD)
//    @Required
//    DataObject getUpdateData();


    /**
     * Name of the field to contain true/false flag to allow overwriting of existing fields.
     */
    String OVERWRITE_EXISTING = "overwrite";

    @Get(field = OVERWRITE_EXISTING)
    boolean isOverwrite(boolean defaultValue);

    default boolean isOverwrite() {
        return isOverwrite(false);
    }


    class Helper extends CRUDOperation.Helper<UpdateOperation, UpdateAnswer> {

        protected final boolean overrideFields;

        /**
         * Creates this helper given the operation to be processed.
         *
         * @param operation operation to be executed
         */
        public Helper(UpdateOperation operation) {
            super(operation, UpdateAnswer.class, CRUDAnswer.SOME_OBJECT_NOT_FOUND,
                    new ProcessingStatus[] { CRUDAnswer.OBJECT_FOUND },
                    new ProcessingStatus[] { CRUDAnswer.OBJECT_NOT_FOUND, CRUDAnswer.SOME_OBJECT_NOT_FOUND});
            this.overrideFields = operation.isOverwrite();
        }

        protected List<DataObject> findAndUpdateObjects(LocalBucket bucket) {
            return super.processObjects(bucket, getOperation().getObjects());
        }

        @Override
        protected ProcessResult processObject(LocalBucket bucket, DataObject dataObject) {
            try {
                final Collection<DataObject> deletedObjects = (dataObject.getFieldCount() > 1) ?
                        bucket.deleteObject(dataObject, 1) : bucket.deleteObject(dataObject.getID(), 1);
                // this collection must have zero or one object
                if (deletedObjects.isEmpty()) {
                    return new ProcessResult(dataObject, CRUDAnswer.OBJECT_NOT_FOUND);
                }
                DataObject updatedObject = updateOneObject(deletedObjects.iterator().next(), dataObject);
                bucket.addObject(updatedObject);
                return new ProcessResult(updatedObject, CRUDAnswer.OBJECT_FOUND);
            } catch (BucketStorageException e) {
                return new ProcessResult(dataObject, CRUDAnswer.OBJECT_NOT_FOUND);
            }
        }

        public DataObject updateOneObject(@Nonnull DataObject storedObject, DataObject updatingObject) {
            if (!(storedObject instanceof ModifiableRecord)) {
                storedObject = new ModifiableRecordImpl(new HashMap<>(storedObject.getMap()), true);
            }
            for (Map.Entry<String, Object> fieldToUpdate : updatingObject.getMap().entrySet()) {
                if (overrideFields || ! storedObject.containsField(fieldToUpdate.getKey()))
                    ((ModifiableRecord) storedObject).setField(fieldToUpdate.getKey(), fieldToUpdate.getValue());
            }
            return storedObject;
        }
    }
}
