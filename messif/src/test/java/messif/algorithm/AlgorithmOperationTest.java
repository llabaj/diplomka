/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.algorithm;

import messif.algorithm.impl.SequentialScan;
import messif.bucket.CapacityFullException;
import messif.bucket.LocalBucket;
import messif.bucket.impl.DiskBlockBucket;
import messif.data.DataObject;
import messif.data.util.DataObjectIterator;
import messif.distance.DataObjectDistanceFunc;
import messif.distance.impl.L1DistanceFloats;
import messif.distance.impl.L2DistanceFloats;
import messif.operation.AbstractOperation;
import messif.operation.OperationBuilder;
import messif.operation.ReturnDataOperation;
import messif.operation.answer.AbstractAnswer;
import messif.operation.crud.CRUDOperation;
import messif.operation.crud.DeleteOperation;
import messif.operation.crud.InsertOperation;
import messif.operation.crud.answer.InsertAnswer;
import messif.operation.info.AlgorithmInfoOperation;
import messif.operation.search.KNNOperation;
import messif.operation.search.QueryObjectOperation;
import messif.record.ModifiableRecordImpl;
import messif.record.Record;
import messif.record.RecordImpl;
import messif.utility.json.JSONWriter;
import messif.utility.proxy.ProxyConverter;
import messif.utility.proxy.annotations.Set;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Random;

public class AlgorithmOperationTest {

    private static final int DATA_OBJ_COUNT = 10;

    private static final Random random = new Random();

    private Algorithm algorithm;

    private JSONWriter stdoutJSONWriter;

    //@Test
    public void testMemoryAlg() throws Exception {

        insertAndSearchTest(null);
    }

    @Test
    public void testDiskBucketAlgorithm() throws Exception {

        LocalBucket bucket = new DiskBlockBucket(Long.MAX_VALUE, Long.MAX_VALUE, 0L, File.createTempFile("disk_bucket", ".bin", new File(System.getProperty("java.io.tmpdir"))));
        insertAndSearchTest(bucket);
    }

    void insertAndSearchTest(LocalBucket bucket) throws Exception {

        initAlgorithm(bucket);

        final DataObject[] dataObjects = initData();

        // FIRST WAY TO CREATE AN OPERATION
        final InsertOperation insertOp = OperationBuilder.create(InsertOperation.class)
                .addParam(CRUDOperation.OBJECTS_FIELD, dataObjects)
                //.modifiable(false)
                .checkFields(true).build();
        printFieldMap("insert operation", insertOp.getParams());
        final InsertAnswer insertAnswer = (InsertAnswer) this.algorithm.evaluate(insertOp);
        //System.out.println(insertAnswer.stringRepresentation());
        printFieldMap("insert answer", insertAnswer.getParams());

        // ANOTHER WAY - CREATE JUST FIELD MAP
        final Record algInfoOp = OperationBuilder.create(AlgorithmInfoOperation.TYPE_STRING).buildMap();
        //final AlgorithmInfoOperation algInfoOp = OperationBuilder.create(AlgorithmInfoOperation.class).build();
        printFieldMap("algorithm info operation", algInfoOp);
        AbstractAnswer algInfoAnswer = this.algorithm.evaluate(algInfoOp);
        printFieldMap("algorithm info answer", algInfoAnswer.getParams());

        // ANOTHER WAY - USE MODIFIABLE OPERATION
        InsertOperationModifiable modifiableInsertOp = ProxyConverter.convert(new ModifiableRecordImpl(), InsertOperationModifiable.class);
        modifiableInsertOp.setObjects(dataObjects);
        printOperation("algorithm info operation", modifiableInsertOp);
        AbstractAnswer modifiableInfoAnswer = this.algorithm.evaluate(modifiableInsertOp);
        printFieldMap("algorithm info answer", modifiableInfoAnswer.getParams());

        // SEARCH OPERATION
        final KNNOperation knnQuery = OperationBuilder.create(KNNOperation.class)
                .addParam(KNNOperation.K_FIELD, 5)
                .addParam(QueryObjectOperation.QUERY_OBJECT_FIELD, oneRandomObject())
                .addParam(ReturnDataOperation.FIELDS_TO_RETURN_FIELD, new String[]{DataObject.ID_FIELD, "descriptor"})
                .addParam(QueryObjectOperation.DISTANCE_FIELD, new DataObjectDistanceFunc<>("descriptor", new L1DistanceFloats())).build();
        printFieldMap("kNN query", knnQuery.getParams());
        //System.out.println("kNN from field: " + knnQuery.getFrom());
        final AbstractAnswer knnAnswer = this.algorithm.evaluate(knnQuery);
        printFieldMap("kNN answer", knnAnswer.getParams());

        final Record deleteOperation = OperationBuilder.create(DeleteOperation.TYPE_STRING)
        //final DeleteOperation deleteOperation = OperationBuilder.create(DeleteOperation.class)
                .addParam(CRUDOperation.OBJECTS_FIELD, initJustIDs())
                .addParam(ReturnDataOperation.FIELDS_TO_RETURN_FIELD, new String[0]).buildMap();
        printFieldMap("delete operation", deleteOperation);
        final AbstractAnswer deleteAnswer = this.algorithm.evaluate(deleteOperation);
        printFieldMap("delete answer", deleteAnswer.getParams());

        algInfoAnswer = this.algorithm.evaluate(algInfoOp);
        printFieldMap("algorithm info answer", algInfoAnswer.getParams());

        // print all objects in the bucket
        for (final DataObjectIterator allObjects = bucket.getAllObjects(); allObjects.hasNext(); ) {
            printFieldMap(null, allObjects.next());
        }
    }

    private DataObject[] initData() {
        final DataObject[] dataObjects = new DataObject[DATA_OBJ_COUNT];
        for (int i = 0; i < DATA_OBJ_COUNT; i++) {
            final HashMap<String, Object> map = new HashMap<>();
            map.put("descriptor", new float[]{1f * i, 2f * i, 3f * i});
            map.put(DataObject.ID_FIELD, "id" + i);
            map.put("second descriptor", new String[]{"kw" + i, "second"});

            dataObjects[i] = new RecordImpl(map, true);
        }
        return dataObjects;
    }

    private DataObject[] initJustIDs() {
        final DataObject[] dataObjects = new DataObject[DATA_OBJ_COUNT];
        for (int i = 0; i < DATA_OBJ_COUNT; i++) {
            final HashMap<String, Object> map = new HashMap<>();
            map.put(DataObject.ID_FIELD, "id" + i);

            dataObjects[i] = new RecordImpl(map, true);
        }
        return dataObjects;
    }

    private DataObject oneRandomObject() {
        final HashMap<String, Object> map = new HashMap<>();

        map.put("descriptor", new float[]{random.nextFloat() * 5f, random.nextFloat() * 5f, random.nextFloat() * 5f});
        map.put(DataObject.ID_FIELD, "query");
        map.put("second descriptor", new String[]{"kw", "second"});
        return new RecordImpl(map, true);
    }

    private void initAlgorithm(LocalBucket bucket) throws CapacityFullException, InstantiationException {
        L2DistanceFloats l2DistanceFloats = new L2DistanceFloats();
        DataObjectDistanceFunc<float[]> distance = new DataObjectDistanceFunc<>("descriptor", l2DistanceFloats);

        algorithm = bucket == null ? new SequentialScan(distance) : new SequentialScan(distance, bucket);
        algorithm.init();
    }

    private void printFieldMap(String label, Record record) throws IOException {
        if (this.stdoutJSONWriter == null) {
            this.stdoutJSONWriter = new JSONWriter(new BufferedWriter(new OutputStreamWriter(System.out)), true);
        }
        if (label != null) {
            stdoutJSONWriter.getUnderlyingWriter().write(label + ":\n");
        }
        stdoutJSONWriter.write(record.getMap());
        stdoutJSONWriter.getUnderlyingWriter().write("\n\n");
        stdoutJSONWriter.flush();
    }

    private void printOperation(String label, AbstractOperation operation) throws IOException {
        System.out.println(label);
        System.out.println(operation.toString());
    }

    public interface InsertOperationModifiable extends InsertOperation {
        @Set(field = OBJECTS_FIELD)
        void setObjects(DataObject[] objects);
    }
}
