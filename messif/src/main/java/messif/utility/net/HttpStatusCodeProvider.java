/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.net;

/**
 * This interface is to be implemented mainly by custom MESSIF exceptions thrown
 *  during operation processing or any other action; the implementing class provides
 *  a HTTP error code to be returned in case the exception was thrown.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface HttpStatusCodeProvider {

    /** Constant holding information that the HTTP error code was not set. */
    int STATUS_CODE_NOT_SET = -1;

    /** Generic HTTP status code saying that everything is OK. */
    int STATUS_CODE_OK = 200;

    /** A status saying that a resourse was created. */
    int STATUS_CODE_CREATED = 201;

    /** A status saying that the request was partially successful. */
    int STATUS_CODE_PARTIAL = 211;

    /** Constant holding a HTTP error code of internal error (default error code). */
    int STATUS_CODE_INTERNAL_ERROR = 500;
    
    /** Constant holding a HTTP error code of a 'conflict'; it is used when a duplicate insert appears. */
    int STATUS_CODE_CONFLICT = 409;

    /** Constant holding a HTTP error code of 'Not found'. */
    int STATUS_CODE_NOT_FOUND = 404;

    /** Constant holding a HTTP error code of 'Not found'. */
    int STATUS_CODE_INVALID_ARGUMENT = 400;

    /**
     * Returns true, if the HTTP code was set, false otherwise.
     * @return true, if the HTTP code was set, false otherwise.
     */
    default boolean isHttpStatusSet() {
        return getHttpStatusCode() != HttpStatusCodeProvider.STATUS_CODE_NOT_SET;
    }

    /**
     * Returns true if given error code is alright (starts with 2XX).
     */
    static boolean isOK(int errorCode) {
        return errorCode >= 200 && errorCode < 300;
    }

    /** 
     * Returns a HTTP error code to be returned to the HTTP client.
     * @return the HTTP error code to be returned to the HTTP client.
     */
    int getHttpStatusCode();
        
}
