/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.info.processor;

import messif.bucket.BucketDispatcher;
import messif.bucket.LocalBucket;
import messif.operation.AbstractOperation;
import messif.operation.info.answer.ObjectCountAnswer;
import messif.operation.processing.impl.SimpleOperationProcessor;

import java.io.IOException;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public abstract class ObjectCountProcessorBase<TRequest extends AbstractOperation, TAnswer extends ObjectCountAnswer> extends SimpleOperationProcessor<TRequest, TAnswer> {

    /**
     * The total count of objects in the algorithm, which is a result of this operation.
     */
    protected volatile long objectCount;

    /**
     * Creates this processor given the operation to be processed.
     *
     * @param operation operation to be executed
     */
    public ObjectCountProcessorBase(TRequest operation) {
        super(operation);
    }


    /******************  Helper methods to be used by other processors   ******************/

    /**
     * Add the specified count to the answer of this operation.
     *
     * @param objectCount the count to add
     */
    protected void addToAnswer(int objectCount) {
        this.objectCount += objectCount;
    }


    /**
     * Evaluate this search on a given bucket dispatcher.
     * Object counts stored in all buckets maintained by the provided dispatcher is added to this operation's answer.
     *
     * @param dispatcher the bucket dispatcher to update answer from
     * @return number of objects added to this operation, i.e. the actual dispatcher's object count
     */
    public int evaluate(BucketDispatcher dispatcher) throws IOException {
        int count = dispatcher.getObjectCount();
        objectCount += count;
        return count;
    }

    /**
     * Evaluate this search on a given bucket.
     * Object count stored in this bucket is added to this operation's answer.
     *
     * @param bucket the bucket to update answer from
     * @return number of objects added to this operation, i.e. the actual bucket's object count
     */
    public int evaluate(LocalBucket bucket) throws IOException {
        int count = bucket.getObjectCount();
        objectCount += count;
        return count;
    }

}
