package com.me.diplomka.fixedpivots;

import com.hazelcast.core.ReplicatedMap;

import java.util.stream.Stream;

import static java.lang.Math.ceil;
import static java.lang.Math.min;

public class TagService {
    private ReplicatedMap<String, Integer> tagsStatistics;
    private double tagIntersectCoefficient;

    public TagService(ReplicatedMap<String, Integer> tagsStatistics, double tagIntersectCoefficient) {
        this.tagIntersectCoefficient = tagIntersectCoefficient;
        this.tagsStatistics = tagsStatistics;
    }

    public int incCount(String pivotPrefix, String tag) {
        var tagKey = combine(pivotPrefix, tag);
        return tagsStatistics.compute(tagKey, (key, value) -> value != null ? value + 1 : 1);
    }

    public int getCount(String pivotPrefix, String tag) {
        var count = tagsStatistics.get(combine(pivotPrefix, tag));
        return count != null ? count : 0;
    }

    public int estimateIntersect(String pivotPrefix, String... tags) {
        return Stream.of(tags)
                .mapToInt(tag -> getCount(pivotPrefix, tag))
                .reduce((acc, count) -> (int) ceil(tagIntersectCoefficient * min(acc, count)))
                .orElse(0);
    }

    private String combine(String pivotPrefix, String tag) {
        return pivotPrefix + "+" + tag;
    }
}
