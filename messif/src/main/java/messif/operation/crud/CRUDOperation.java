/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.crud;

import messif.bucket.LocalBucket;
import messif.data.DataObject;
import messif.operation.ProcessObjectsOperation;
import messif.operation.answer.ListingAnswer;
import messif.operation.crud.answer.CRUDAnswer;
import messif.operation.helpers.ErrorCodeMerger;
import messif.utility.ProcessingStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * A common predecessor for all insert/delete/update/get operation. All the operations are designed to manipulate with
 *  a batch of objects - insert them, delete/update/get objects specified by some attributes (typically "_id").
 */
public interface CRUDOperation extends ProcessObjectsOperation {

    /**
     * A helper extended by all CRUD operations. It contains all common fields and functionality, mostly the builder
     *  of a {@link ListingAnswer} and {@link ErrorCodeMerger error code helper}.
     */
    abstract class Helper<TRequest extends CRUDOperation, TAnswer extends CRUDAnswer>
            extends ProcessObjectsOperation.Helper<TRequest, TAnswer> {

        /**
         * Creates this processor given the operation to be processed.
         *
         * @param operation    operation to be executed
         * @param partialAnswerCode code to be set in case that some operation ended with errors, but not all
         * @param alrightCodes a list of codes that are considered to be alright
         */
        protected Helper(TRequest operation, Class<? extends TAnswer> answerClass, ProcessingStatus partialAnswerCode, ProcessingStatus [] alrightCodes, ProcessingStatus [] errorCodes) {
            super(operation, answerClass, partialAnswerCode, alrightCodes, errorCodes);
        }

        //  ***********************       Helper methods for CRUD processing of objects     **************************** //

        /**
         * Process (insert/delete/update) all objects in the operation on given bucket, monitor the NUMBER of actually updated
         * objects and update the error code correspondingly.
         *
         * @param bucket  the bucket to process the objects on
         * @return list of ORIGINAL objects actually modified (inserted, deleted, found) in the same order as in
         * parameter objects
         */
        public List<DataObject> processObjects(LocalBucket bucket) {
            return processObjects(bucket, operation.getObjects());
        }

        /**
         * Process (insert/delete/update) given list of objects on given bucket, monitor the NUMBER of actually updated
         * objects and update the error code correspondingly.
         *
         * @param bucket  the bucket to process the objects on
         * @param objects a list of objects to be updated (inserted/deleted/updated) on the bucket
         * @return list of ORIGINAL objects actually modified (inserted, deleted, found) in the same order as in
         * parameter objects
         */
        public List<DataObject> processObjects(LocalBucket bucket, DataObject[] objects) {
            List<DataObject> actuallyProcessed = new ArrayList<>();
            for (DataObject dataObject : objects) {
                final ProcessResult result = processObject(bucket, dataObject);
                if (objectProcessed(result.processedObject, result.status)) {
                    actuallyProcessed.add(dataObject);
                }
            }
            return actuallyProcessed;
        }

        protected abstract ProcessResult processObject(LocalBucket bucket, DataObject dataObject);
        //protected abstract ProcessResult processObject(StorageIndexed<DataObject> storage, DataObject dataObject);

        protected static class ProcessResult {
            final DataObject processedObject;
            final ProcessingStatus status;

            protected ProcessResult(DataObject processedObject, ProcessingStatus status) {
                this.processedObject = processedObject;
                this.status = status;
            }
        }
    }
}
