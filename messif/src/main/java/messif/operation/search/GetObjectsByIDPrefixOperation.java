/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.search;

import messif.data.util.DataObjectIterator;
import messif.operation.answer.AbstractAnswer;
import messif.operation.ReturnDataOperation;
import messif.operation.helpers.AnswerProvider;
import messif.operation.helpers.BaseHelper;
import messif.operation.answer.ListingAnswer;
import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Required;

import java.util.NoSuchElementException;

/**
 * Operation tha
 */
public interface GetObjectsByIDPrefixOperation extends ReturnDataOperation {

    String TYPE_STRING = "get_by_id_prefix";

    @Override
    default String getInnerType() {
        return TYPE_STRING;
    }


    String ID_PREFIX_FIELD = "id_prefix";

    @Get(field = ID_PREFIX_FIELD)
    @Required
    String getIDPrefix();

    /**
     * This class helps the operation processors to process this type of operation by providing supporting methods and
     *  continuous building of the answer.
     */
    class Helper extends BaseHelper<GetObjectsByIDPrefixOperation> implements AnswerProvider<ListingAnswer> {

        protected final String idPrefix;

        protected final ListingAnswer.Builder answerBuilder;

        public Helper(GetObjectsByIDPrefixOperation query) {
            super(query);
            this.idPrefix = query.getIDPrefix();
            answerBuilder = new ListingAnswer.Builder(operation, ListingAnswer.class);
        }

        /**
         * Evaluate the operation on a given list of dat aobjects.
         */
        public int evaluate(DataObjectIterator objects) {
            int count = 0;
            try {
                while (objects.hasNext()) {
                    answerBuilder.add(objects.getObjectByID(idPrefix, true));
                    count++;
                }
            } catch (NoSuchElementException e) { // Search ended, there are no more objects in the bucket
            }

            return count;
        }

        // ********************      Implementation of {@link AnswerProvider} interface

        @Override
        public void updateAnswer(AbstractAnswer answer) {
            answerBuilder.updateAnswer(answer);
        }

        @Override
        public ListingAnswer getAnswer() {
            return answerBuilder.getAnswer();
        }
    }
}
