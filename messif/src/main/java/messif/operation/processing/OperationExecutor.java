/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.processing;

import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;

import javax.annotation.Nonnull;
import java.util.Iterator;

/**
 * This interface (abstract class) is to be implemented by classes that should be applied to each operation. These
 *  executors are to be chained and the last (terminate) executor is to return the final answer.
 */
public abstract class OperationExecutor {

    /**
     * Method to execute given operation and then call {@link #executeNext(AbstractOperation, Iterator)} method
     *  to use also other executors in the chain.
     * @param operation the operation to process by this chain of executors
     * @param iterator chain of executors to be applied on each operation
     * @return final answer as created by the last (terminate) executor (which does not call the {@link #executeNext(AbstractOperation, Iterator)} method
     * @throws Exception if the execution goes wrong
     */
    @Nonnull
    public abstract AbstractAnswer execute(AbstractOperation operation, Iterator<OperationExecutor> iterator) throws Exception;

    /**
     * This method takes next processor in the passed chain (iterator) and calls its {@link #execute(AbstractOperation, Iterator)} method.
     * @param operation the operation to process by this chain of executors
     * @param iterator chain of executors to be applied on each operation
     * @return final answer as created by the last (terminate) executor (which does not call the {@link #executeNext(AbstractOperation, Iterator)} method
     * @throws Exception if the execution goes wrong
     */
    @Nonnull
    protected final AbstractAnswer executeNext(AbstractOperation operation, Iterator<OperationExecutor> iterator) throws Exception {
        OperationExecutor executor = iterator.next();

        if (executor != null)
            return executor.execute(operation, iterator);

        throw new IllegalStateException("Missing OperationExecutor chain termination");
    }
}
