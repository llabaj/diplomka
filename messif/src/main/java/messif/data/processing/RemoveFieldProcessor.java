/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.processing;

import messif.data.DataObject;
import messif.record.ModifiableRecord;
import messif.record.ModifiableRecordImpl;
import messif.record.processing.RecordProcessingException;
import messif.record.processing.RecordProcessor;

/**
 * A data object processor that removes a given field from the record (if it the field exists).
 */
public class RemoveFieldProcessor implements RecordProcessor.Single<DataObject> {

    protected final String fieldToRemove;

    protected final String conditionString;

    protected final String[] modifiedFields;


    /**
     * Create a new processor given the field to be removed.
     */
    public RemoveFieldProcessor(String fieldToRemove) {
        this.fieldToRemove = fieldToRemove;
        this.conditionString = "field '" + fieldToRemove + "' required";
        this.modifiedFields = new String[]{fieldToRemove};
    }

    @Override
    public DataObject process(DataObject record) throws RecordProcessingException {
        if (! preCondition(record)) {
            return record;
        }
        if (!(record instanceof ModifiableRecord)) {
            record = new ModifiableRecordImpl(record);
        }
        ((ModifiableRecord) record).removeField(fieldToRemove);
        return record;
    }

    @Override
    public boolean preCondition(DataObject record) {
        return record.getField(fieldToRemove) != null;
    }

    @Override
    public String preConditionDescription() {
        return conditionString;
    }

    @Override
    public String[] modifiedFields() {
        return modifiedFields;
    }
}
