/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.util;

import messif.data.DataObject;
import messif.distance.DistanceFunc;
import messif.distance.DistanceException;

/**
 * Encapsulation of an object-distance pair.
 * This class holds an {@link DataObject} and its distance.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class RankedDataObject extends DistanceRankedObject<DataObject> {

    /** Class serial id for serialization */
    private static final long serialVersionUID = 1L;

    //****************** Constructor ******************//

    /**
     * Creates a new instance of RankedDataObject for an object and its measured distance.
     * @param object the ranked object
     * @param distance the measured distance of the object
     */
    public RankedDataObject(DataObject object, float distance) {
        super(object, distance);
    }

    /**
     * Creates a new instance of RankedDataObject by measuring an object's distance from the reference object
     * using a given distance function.
     * @param <T> the type of object used to measure the distance
     * @param object the ranked object
     * @param distanceFunc the distance function used for the measuring
     * @param referenceObject the reference object from which the distance is measured
     * @throws NullPointerException if the distance function is <tt>null</tt>
     */
    public <T extends DataObject> RankedDataObject(T object, DistanceFunc<? super T> distanceFunc, T referenceObject) throws NullPointerException, DistanceException {
        super(object, distanceFunc, referenceObject);
    }

    @Override
    public RankedDataObject clone(float newDistance) {
        return (RankedDataObject)super.clone(newDistance); // This cast is valid because of the cloning
    }

}
