/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.answer;

import messif.data.DataObject;
import messif.data.util.DistanceRankedSortedCollection;
import messif.data.util.RankedJoinObject;
import messif.operation.ReturnDataOperation;
import messif.operation.helpers.AnswerProvider;
import messif.operation.helpers.ErrorCodeHelper;
import messif.operation.helpers.SimpleErrorCodeMerger;
import messif.operation.params.Distances;
import messif.operation.search.JoinOperation;
import messif.record.ModifiableRecord;
import messif.record.Record;
import messif.utility.ProcessingStatus;
import messif.utility.proxy.ProxyConverter;
import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Required;

import static messif.operation.answer.ListingAnswer.ANSWER_OBJ_COUNT_FIELD;

/**
 * An answer that contains a list of PAIRs of data objects (for metric join operation) and also a list of ranking scores,
 *  typically the object-object distances. These distances are stored in a separate array.
 */
public interface JoinQueryAnswer extends AbstractAnswer, Distances {

    /**
     * Name of a {@link Record} (JSON) field to contain the list of pairs of objects returned by the
     *  join operation.
     */
    String ANSWER_PAIRS_FIELD = "answer_pairs";

    /**
     * This method provides a convenient access to the field with name {@link #ANSWER_PAIRS_FIELD}.
     * @return an array of {@link DataObject}s returned by the operation
     */
    @Get(field = ANSWER_PAIRS_FIELD)
    @Required
    DataObject [] [] getAnswerPairs();

    /**
     * This method provides a convenient access to the field with name {@link ListingAnswer#ANSWER_OBJ_COUNT_FIELD}.
     * @return the number of objects returned by the operation
     */
    @Get(field = ANSWER_OBJ_COUNT_FIELD)
    @Required
    int getAnswerCount();


    /** The default correct error code is only {@link AbstractAnswer#OPERATION_OK}. */
    @Override
    default boolean wasSuccessful() {
        return getStatus().in(AbstractAnswer.OPERATION_OK);
    }


    // region *************************            Factory methods       ************************** //

    static  JoinQueryAnswer createSuccessful(DistanceRankedSortedCollection<RankedJoinObject> pairs, String[] fieldsToReturn, ProcessingStatus status) {
        float [] distances = new float[pairs.size()];
        final DataObject[] [] pairsToReturn = new DataObject[pairs.size()][];
        int i = 0;
        for (RankedJoinObject rankedObject : pairs) {
            distances[i] = rankedObject.getDistance();
            DataObject [] objPair = new DataObject[] {
                    ReturnDataOperation.transformObject(rankedObject.getLeftObject(), fieldsToReturn),
                    ReturnDataOperation.transformObject(rankedObject.getRightObject(), fieldsToReturn)
            };
            pairsToReturn[i] = objPair;
            i ++;
        }

        ModifiableRecord fieldMap = AbstractAnswer.createMap(status);
        fieldMap.setField(ANSWER_PAIRS_FIELD, pairsToReturn);
        fieldMap.setField(ANSWER_OBJ_COUNT_FIELD, pairsToReturn.length);
        fieldMap.setField(ANSWER_DISTANCES_FIELD, distances);

        return ProxyConverter.convert(fieldMap, JoinQueryAnswer.class);
    }
    // endregion

    /**
     * This class helps to build the join answer iteratively using {@link DistanceRankedSortedCollection} and then
     *  build the actual {@link JoinQueryAnswer answer}. It is used by operation helpers and processors.
     */
    class Builder implements AnswerProvider<JoinQueryAnswer> {

        /** Set holding the answer of this query */
        private DistanceRankedSortedCollection<RankedJoinObject> answerCollection;

        protected final JoinOperation operation;

        protected final float mu;

        public final ErrorCodeHelper errorCodeHelper;

        public Builder(JoinOperation operation, DistanceRankedSortedCollection<RankedJoinObject> answerCollection) {
            this(operation, answerCollection, new SimpleErrorCodeMerger<>(JoinQueryAnswer.class));
        }

        public Builder(JoinOperation operation, DistanceRankedSortedCollection<RankedJoinObject> answerCollection, ErrorCodeHelper errorCodeHelper) {
            this.answerCollection = answerCollection;
            this.operation = operation;
            this.mu = operation.getMu();
            this.errorCodeHelper = errorCodeHelper;
        }

        public Builder(JoinOperation operation) {
            this(operation, new DistanceRankedSortedCollection<>());

        }

        // region *************************         Answer manipulation methods       ********************

        /**
         * Add a new pair of objects to the answer. The rank of the pair is computed automatically
         * as a distance between the passed objects.
         *
         * @param leftObject  left object of the pair
         * @param rightObject right object of the pair
         * @param distance distance between the left and right objects (this distance is compared against the passed threshold as well as any internal join query threshold)
         * @param distThreshold the threshold on distance;
         *      if the computed distance exceeds the threshold (sharply) or the join constraint (threshold),
         *      the pair is not added
         * @return the distance-ranked join object that was added to answer or <tt>null</tt> if the object was not added
         */
        public synchronized RankedJoinObject add(DataObject leftObject, DataObject rightObject, float distance, float distThreshold) {
            if (leftObject == null || rightObject == null)
                return null;
            if (distance > distThreshold)
                return null;

            // Create the ranked object encapsulation
            RankedJoinObject rankedObject = new RankedJoinObject(leftObject, rightObject, distance);

            // Add the encapsulated object to the answer
            if (answerCollection.add(rankedObject))
                return rankedObject;
            else
                return null;
        }

        public int getAnswerSize() {
            return answerCollection.size();
        }

        @Override
        public JoinQueryAnswer getAnswer() {
            ProcessingStatus status = errorCodeHelper.getStatus();
            if (! status.isSet()) {
                status = (answerCollection == null || answerCollection.isEmpty()) ? AbstractAnswer.EMPTY_ANSWER : AbstractAnswer.OPERATION_OK;
            }
            return JoinQueryAnswer.createSuccessful(answerCollection, operation.fieldsToReturn(), status);
        }
        // endregion


        // region **********************       Update answer from other answers (answer builders)    ****************

        /**
         * Update the error code and answer of this operation from another operation.
         *
         * @param answer the source operation from which to get the update
         * @throws IllegalArgumentException if the answer of the specified operation is incompatible with this one
         */
        @Override
        public void updateAnswer(AbstractAnswer answer) throws IllegalArgumentException {
            errorCodeHelper.updateAnswer(answer);
            if (answer instanceof JoinQueryAnswer) {
                updateFrom(((JoinQueryAnswer)answer).getAnswerPairs(), ((JoinQueryAnswer)answer).getAnswerDistances());
            }
        }

        public void updateFrom(DataObject[] [] objects, float[] distances) throws IllegalArgumentException {
            int length = Math.min(objects.length, distances.length);
            for (int i = 0; i < length; i++) {
                add(objects[i][0], objects[i][1], distances[i], mu);
            }
        }

        /**
         * Update the answer from another answer builder.
         *
         * @param answerBuildf the source processor from which to get the update
         */
        public void updateFrom(JoinQueryAnswer.Builder answerBuildf) {
            answerCollection.addAll(answerBuildf.answerCollection);
        }

        /**
         * Update the answer from another ranked list.
         *
         * @param answer a partial answer to update the answer from
         */
        public void updateFrom(DistanceRankedSortedCollection<RankedJoinObject> answer) {
            this.answerCollection.addAll(answer);
        }
        // endregion
    }


}
