/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.info.answer;

import messif.operation.answer.AbstractAnswer;
import messif.record.ModifiableRecord;
import messif.utility.ProcessingStatus;
import messif.utility.proxy.ProxyConverter;
import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Required;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface InformationAnswer extends AbstractAnswer {

    String INFO_FIELD = "info";

    @Get(field = INFO_FIELD)
    @Required
    String getInfo();

    /**
     * Returns <tt>true</tt> if this operation has canFinish successfully.
     * Otherwise, <tt>false</tt> is returned - the operation was either unsuccessful or is has not canFinish yet.
     *
     * @return <tt>true</tt> if this operation has canFinish successfully
     */
    @Override
    default boolean wasSuccessful() {
        return getStatus().in(AbstractAnswer.OPERATION_OK);
    }

    static InformationAnswer createFailed(ProcessingStatus status) {
        ModifiableRecord fieldMap = AbstractAnswer.createMap(status);
        fieldMap.setField(INFO_FIELD, "");
        return ProxyConverter.convert(fieldMap, InformationAnswer.class);
    }

    static InformationAnswer createSuccessful(String info) {
        ModifiableRecord fieldMap = AbstractAnswer.createMap(AbstractAnswer.OPERATION_OK);
        fieldMap.setField(INFO_FIELD, info);
        return ProxyConverter.convert(fieldMap, InformationAnswer.class);
    }
}
