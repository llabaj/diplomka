/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.info;

import messif.operation.AbstractOperation;
import messif.operation.ReturnDataOperation;
import messif.utility.proxy.annotations.Get;

/**
 * Operation for retrieving all objects locally stored (organized by an algorithm) and printing
 *  their text representation to specified file.
 */
public interface PrintAllObjectsOperation extends AbstractOperation, ReturnDataOperation {

    String TYPE_STRING = "print_all_records";

    @Override
    default String getInnerType() {
        return TYPE_STRING;
    }


    String OUTFILE_PARAM = "output_file_name";

    @Get(field = OUTFILE_PARAM)
    String getOutputFile();


    String OBJECTS_TO_ANSWER_PARAM = "records_to_answer";

    @Get(field = OBJECTS_TO_ANSWER_PARAM)
    boolean objectsToAnswer(boolean defaultValue);

    default boolean objectsToAnswer() {
        return objectsToAnswer(true);
    }


    String DELETE_FILE_PARAM = "delete_file_after";

    @Get(field = DELETE_FILE_PARAM)
    boolean deleteFileAfter(boolean defaultValue);

    default boolean deleteFileAfter() {
        return deleteFileAfter(true);
    }

}
