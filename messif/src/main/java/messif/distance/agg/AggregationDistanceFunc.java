/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.distance.agg;

import messif.distance.DataObjectDistanceFunc;
import messif.distance.DistanceFunc;
import messif.data.DataObject;
import messif.record.Record;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Interface for distance functions that aggregates certain distances into one (e.g. by a weighted sum). The only
 *  purpose of this interface is to return also the sub-distances together with the distance.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface AggregationDistanceFunc<T> extends DistanceFunc<T> {

    /**
     * Returns the distance between object {@code o1} and object {@code o2}.
     *
     * @param o1 the object for which to measure the distance
     * @param o2 the object for which to measure the distance
     * @param individualDistances an output array to be filled with the sub-distances (if not null and if of correct size)
     * @return the distance between object {@code o1} and object {@code o2}
     */
    default float getDistance(T o1, T o2, float[] individualDistances) {
        return getDistance(o1, o2, MAX_DISTANCE, individualDistances);
    }

    /**
     * Returns the distance between object {@code o1} and object {@code o2}.
     *
     * @param o1 the object for which to measure the distance
     * @param o2 the object for which to measure the distance
     * @param individualDistances an output array to be filled with the sub-distances (if not null and if of correct size)
     * @return the distance between object {@code o1} and object {@code o2}
     */
    float getDistance(T o1, T o2, float threshold, float[] individualDistances);

    /**
     * Get the number of subdistances used by this distance function.
     * @return the number of subdistances used by this distance function.
     */
    int getNumberOfSubdists();


    // region ******************     Creator of simple aggregation functions                 ******************

    /** Pattern used to retrieve tokens from the function string */
    Pattern tokenizerPattern = Pattern.compile("\\s*(([\\w.,]+)|([-+*/]))\\s*");

    /**
     * Creates a new instance of a simple aggregation function that combines distance functions using arithmetic functions.
     * Basic arithmetic operations (+, -, *, /) are supported as well as numeric constants (treated as floats).
     *
     * The specified function is parsed and prepared. Basic arithmetic operations
     * (+, -, *, /) are supported as well as numeric constants (float).
     * Blank space is ignored and everything else is considered to be a name of the {@link Record} field.
     * @param function the function string
     * @param distanceFunctions distances to be applied on the the fields specified in the "function" string
     * @throws IllegalArgumentException if the specified function cannot be parsed
     */
    static AggregationDistanceFuncBase<DataObject> build(String function, DistanceFunc<DataObject>... distanceFunctions) throws IllegalArgumentException {
        LinkedHashMap<String, Float> descriptorMap = parse(function);
        if (descriptorMap.isEmpty())
            throw new IllegalArgumentException("Specified function contains no variables");
        if (descriptorMap.size() != distanceFunctions.length)
            throw new IllegalArgumentException("The number of distance functions must match the number of fields in the string");

        // for each given distance function, create a Record function on respective field
        DistanceFunc<DataObject>[] fieldMapDistanceFuncs = new DistanceFunc[distanceFunctions.length];
        float [] weights = new float[descriptorMap.size()];

        // Fill internal arrays
        int i = 0;
        for (Map.Entry<String, Float> entry : descriptorMap.entrySet()) {
            fieldMapDistanceFuncs[i] = new DataObjectDistanceFunc<>(entry.getKey(), distanceFunctions[i]);
            weights[i] = entry.getValue();
            i++;
        }

        return new WeightedDistanceFuncSum<>(fieldMapDistanceFuncs, weights);
    }

    /**
     * This is simple parser of arithmetic expressions with variables.
     * Parsing only recognizes + - * / operators (no brackets!) and constant numbers.
     * Whitespace is ignored.
     * Everything else is treated as variable name.
     *
     * @param function the function to be parsed
     * @return map where the keys are variable names found in the expression and the values are variable multiplication coefficients
     */
    static LinkedHashMap<String, Float> parse(String function) {
        LinkedHashMap<String, Float> rtv = new LinkedHashMap<>();

        // Parse string function (only +-*/ operations are supported)
        Matcher token = tokenizerPattern.matcher(function);

        // Internal variables for expression
        float currentCoeffValue = 1;
        String currentCoeffName = null;
        char lastOperator = 0;
        boolean isInverted = false;

        // Get next token
        while (token.find()) {
            String operand = token.group(2);
            if (operand != null) {
                // Token is operand
                try {
                    // If token is a number, adjust current coefficient
                    float value = Float.parseFloat(operand);
                    switch (lastOperator) {
                        case '/':
                            currentCoeffValue /= value;
                            break;
                        default:
                            currentCoeffValue *= value;
                    }
                } catch (NumberFormatException e) {
                    // The token is variable name
                    currentCoeffName = operand;
                    isInverted = lastOperator == '/';
                }
            } else {
                // Token is operator
                lastOperator = token.group(3).charAt(0);

                // Addition or subtraction delimits variables
                if (lastOperator == '+' || lastOperator == '-') {
                    if (currentCoeffName != null)
                        rtv.put(currentCoeffName, isInverted?1.0f/currentCoeffValue:currentCoeffValue);
                    // Reset coefficient parameters (value & name)
                    currentCoeffValue = (lastOperator == '-')?-1:1;
                    currentCoeffName = null;
                }
            }
        }
        if (currentCoeffName != null)
            rtv.put(currentCoeffName, isInverted?1.0f/currentCoeffValue:currentCoeffValue);

        return rtv;
    }
    // endregion

}
