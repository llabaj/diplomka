/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.helpers;

import messif.operation.answer.AbstractAnswer;

/**
 * TODO: merge this interface into AnswerProvider ?
 */
public interface AnswerMerger {
    /**
     * Update the error code of this operation from another operation.
     * If the updated operation was not successful, the value overrides this
     * operation's error code (so that the errors propagate up).
     *
     * @param answer the source operation from which to get the update
     * @throws ClassCastException if the specified operation is incompatible with this operation
     */
    void updateAnswer(AbstractAnswer answer);
}
