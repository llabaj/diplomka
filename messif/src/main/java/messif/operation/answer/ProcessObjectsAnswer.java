/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.answer;

import messif.data.DataObject;
import messif.record.ModifiableRecord;
import messif.utility.ProcessingStatus;
import messif.utility.net.HttpStatusCodeProvider;
import messif.utility.proxy.ProxyConverter;
import messif.utility.proxy.annotations.Get;

/**
 * A common predecessor of all answers to operations that process some given list of records; these answers contain
 *  a field with records that were not processed (were skipped).
 */
public interface ProcessObjectsAnswer extends ListingAnswer {

    /** A field that contains information about objects that were NOT processed by the operation. */
    String SKIPPED_OBJECTS_FIELD = "skipped_records";

    @Get(field = SKIPPED_OBJECTS_FIELD)
    DataObject[] getSkippedObjects();

    default int getNumberSkipped() {
        if (getSkippedObjects() == null) {
            return 0;
        }
        return getSkippedObjects().length;
    }

    default boolean hasSkipped() {
        return getSkippedObjects() != null && getSkippedObjects().length != 0;
    }

    default ProcessingStatus getFirstSkippedStatus() {
        if (! hasSkipped()) {
            return null;
        }
        return getSkippedObjects()[0].getField(ProcessObjectsAnswer.SKIP_REASON_FIELD, AbstractAnswer.class).getStatus();
    }

    /** Each of the skipped objects may have this field with a reason (JSON). */
    String SKIP_REASON_FIELD = "_reason";

    static ProcessObjectsAnswer createFailed(ProcessingStatus status, DataObject [] skippedObjects) {
        ModifiableRecord fieldMap = AbstractAnswer.createMap(status);
        fieldMap.setField(ANSWER_OBJ_COUNT_FIELD, 0);
        fieldMap.setField(ANSWER_OBJECTS_FIELD, new DataObject [0]);
        fieldMap.setField(SKIPPED_OBJECTS_FIELD, skippedObjects);
        return ProxyConverter.convert(fieldMap, ProcessObjectsAnswer.class);
    }


    /** Object has been processed successfully. */
    ProcessingStatus OK_PROCESSED = new ProcessingStatus("records processed", 230, HttpStatusCodeProvider.STATUS_CODE_OK);

    /** Partial error code. */
    ProcessingStatus SOME_OBJECTS_PROCESSED = new ProcessingStatus("some records processed", 231, HttpStatusCodeProvider.STATUS_CODE_PARTIAL);

    /** Error processing object, or objects. */
    ProcessingStatus ERROR_PROCESSING = new ProcessingStatus("error processing", 430, HttpStatusCodeProvider.STATUS_CODE_PARTIAL);

    @Override
    default boolean wasSuccessful() {
        return getStatus().in(AbstractAnswer.OPERATION_OK, ProcessObjectsAnswer.OK_PROCESSED, ProcessObjectsAnswer.SOME_OBJECTS_PROCESSED);
    }
}
