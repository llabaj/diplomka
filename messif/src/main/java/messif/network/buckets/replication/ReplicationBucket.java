/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.network.buckets.replication;

import messif.bucket.Bucket;
import messif.bucket.BucketStorageException;
import messif.bucket.LocalBucket;
import messif.bucket.index.ModifiableIndex;
import messif.distance.DistanceFunc;
import messif.network.buckets.RemoteBucket;
import messif.network.NetworkNode;
import messif.data.DataObject;
import messif.data.util.DataObjectIterator;
import messif.operation.search.RankingOperation;
import messif.operation.answer.RankedAnswer;
import messif.statistic.OperationStatistics;
import messif.statistic.StatisticCounter;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class ReplicationBucket extends LocalBucket {
    /** class serial id for serialization */
    private static final long serialVersionUID = 1L;

    /** Statistics counter */
    protected static StatisticCounter distanceComputations = StatisticCounter.getStatistics("DistanceComputations");

    protected final ReplicationNetworkBucketDispatcher bucketDispatcher;
    protected final LocalBucket encapsulatedBucket;
    protected final List<RemoteBucket> replicas = new ArrayList<RemoteBucket>();
    protected final AtomicInteger nextReplicaForGet = new AtomicInteger(0); // zero means access local bucket
    protected final ReadWriteLock replicaManipulationLock = new ReentrantReadWriteLock(true);

    /** Creates a new instance of ReplicationBucket */
    protected ReplicationBucket(ReplicationNetworkBucketDispatcher bucketDispatcher, LocalBucket encapsulatedBucket) {
        super(Long.MAX_VALUE, Long.MAX_VALUE, 0, false, encapsulatedBucket.getObjectCount());
        this.encapsulatedBucket = encapsulatedBucket;
        this.bucketDispatcher = bucketDispatcher;
    }


    //****************** Overrides for all public methods of LocalBucket that simply execute the stub ******************//

    public void createReplica(NetworkNode atNetworkNode) throws BucketStorageException, IllegalStateException {
        replicaManipulationLock.writeLock().lock();
        
        try {
            // Create new remote bucket at specified node
            RemoteBucket replica = bucketDispatcher.createRemoteBucket(atNetworkNode);
            
            // Replicate all currently stored objects
            replica.addObjects(encapsulatedBucket.getAllObjects());
            
            // Add new replica to the internal replicas list
            replicas.add(replica);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        } finally {
            replicaManipulationLock.writeLock().unlock();
        }
    }

    /** Remove replica of this bucket from given node.
     * @return false if the bucket is not replicated at the node */
    public boolean removeReplica(NetworkNode atNetworkNode) throws IOException {
        replicaManipulationLock.writeLock().lock();
        
        try {
            // Search for replicas at specified node
            Iterator<RemoteBucket> iterator = replicas.iterator();
            while (iterator.hasNext()) {
                RemoteBucket replica = iterator.next();
                if (atNetworkNode.equals(replica.getRemoteNetworkNode())) {
                    // Found the replica, delete it...
                    iterator.remove();
                    return bucketDispatcher.removeRemoteBucket(replica);
                }
            }
            return false;
        } finally {
            replicaManipulationLock.writeLock().unlock();
        }
    }

    /** Indicate that one of the replica was migrated */
    public void replicaMigrated(NetworkNode origNode, NetworkNode newNode) {
        replicaManipulationLock.writeLock().lock();
        try {
            for (RemoteBucket replica : replicas)
                if (replica.getRemoteNetworkNode().equals(origNode))
                    replica.setRemoteNetworkNode(newNode);
        } finally {
            replicaManipulationLock.writeLock().unlock();
        }
    }

    /** Return set of all network nodes where this bucket has replicas */
    public Set<NetworkNode> getAllReplicaNodes() {
        Set<NetworkNode> retVal = new HashSet<NetworkNode>();
        for (RemoteBucket replicaBucket : replicas)
            retVal.add(replicaBucket.getRemoteNetworkNode());
        return retVal;
    }


    //****************** Overrides for all public methods of LocalBucket that simply execute the stub ******************//

    @Override
    public int getBucketID() {
        return encapsulatedBucket.getBucketID();
    }

    @Override
    public int getObjectCount() {
        return encapsulatedBucket.getObjectCount();
    }

    @Override
    public long getCapacity() {
        return encapsulatedBucket.getCapacity();
    }

    @Override
    public long getSoftCapacity() {
        return encapsulatedBucket.getSoftCapacity();
    }

    @Override
    public long getLowOccupation() {
        return encapsulatedBucket.getLowOccupation();
    }

    @Override
    public long getOccupation() {
        return encapsulatedBucket.getOccupation();
    }

    @Override
    public double getOccupationRatio() {
        return encapsulatedBucket.getOccupationRatio();
    }

    @Override
    public boolean isSoftCapacityExceeded() {
        return encapsulatedBucket.isSoftCapacityExceeded();
    }

    @Override
    public String toString() {
        return encapsulatedBucket.toString();
    }


    //****************** Overrides for all manipulation methods of LocalBucket ******************//

    @Override
    public int addObjects(Iterator<? extends DataObject> objects) throws BucketStorageException {
        replicaManipulationLock.readLock().lock();
        try {
            int ret = encapsulatedBucket.addObjects(objects);
            
            // Update all replicas
            for (RemoteBucket replica : replicas)
                replica.addObjects(objects);

            return ret;
        } finally {
            replicaManipulationLock.readLock().unlock();
        }
    }

    @Override
    public int addObjects(Collection<? extends DataObject> objects) throws BucketStorageException {
        replicaManipulationLock.readLock().lock();
        try {
            int ret = encapsulatedBucket.addObjects(objects);
            
            // Update all replicas
            for (RemoteBucket replica : replicas)
                replica.addObjects(objects);

            return ret;
        } finally {
            replicaManipulationLock.readLock().unlock();
        }
    }

    @Override
    public void addObject(DataObject object) throws BucketStorageException {
        replicaManipulationLock.readLock().lock();
        try {
            encapsulatedBucket.addObject(object);
            // Update all replicas
            for (RemoteBucket replica : replicas)
                replica.addObject(object);
        } finally {
            replicaManipulationLock.readLock().unlock();
        }
    }


    //****************** Overrides for all getter methods of LocalBucket ******************//    

    protected Bucket getOperatingBucket() {
        // Get next replica index (thread safe)
        int index;
        synchronized (nextReplicaForGet) {
            if (nextReplicaForGet.get() > replicas.size())
                nextReplicaForGet.set(0);
            index = nextReplicaForGet.getAndIncrement();
        }
        
        return (index == 0)?encapsulatedBucket:replicas.get(index - 1);
    }

    @Override
    public RankedAnswer processQuery(RankingOperation query, DistanceFunc<DataObject> defaultDistanceFunc) {
        replicaManipulationLock.readLock().lock();
        try {
            StatisticCounter threadDistComp = OperationStatistics.getOpStatisticCounter("DistanceComputations");
            threadDistComp.bindTo(distanceComputations); // Try to bind to global distance computations (if it is not)
            long currentDistComp = threadDistComp.get();
            Bucket bucket = getOperatingBucket();
            try {
                return bucket.processQuery(query, defaultDistanceFunc);
            } finally {
                if ((bucket == encapsulatedBucket) || ((RemoteBucket) bucket).isLocalBucket())
                    bucketDispatcher.bucketOperationDistcompCounter.add(query, threadDistComp.get() - currentDistComp);
                threadDistComp.unbind();
            }
        } finally {
            replicaManipulationLock.readLock().unlock();
        }
    }

    public void processQuery(RankingOperation.Helper<? extends RankingOperation, ? extends RankedAnswer> queryProcessor) {
        replicaManipulationLock.readLock().lock();
        try {
            StatisticCounter threadDistComp = OperationStatistics.getOpStatisticCounter("DistanceComputations");
            threadDistComp.bindTo(distanceComputations); // Try to bind to global distance computations (if it is not)
            long currentDistComp = threadDistComp.get();
            Bucket bucket = getOperatingBucket();
            try {
                bucket.processQuery(queryProcessor);
            } finally {
                if ((bucket == encapsulatedBucket) || ((RemoteBucket) bucket).isLocalBucket())
                    bucketDispatcher.bucketOperationDistcompCounter.add(queryProcessor.getOperation(), threadDistComp.get() - currentDistComp);
                threadDistComp.unbind();
            }
        } finally {
            replicaManipulationLock.readLock().unlock();
        }
    }


    @Override
    public DataObjectIterator getAllObjects() {
        replicaManipulationLock.readLock().lock();
        try {
            return encapsulatedBucket.getAllObjects();
        } finally {
            replicaManipulationLock.readLock().unlock();
        }
    }
//
//    @Override
//    public DataObject getObject(AbstractObjectKey key) throws NoSuchElementException {
//        replicaManipulationLock.readLock().lock();
//        try {
//            return encapsulatedBucket.getObject(key);
//        } finally {
//            replicaManipulationLock.readLock().unlock();
//        }
//    }

    @Override
    public DataObject getObject(String id) throws NoSuchElementException {
        replicaManipulationLock.readLock().lock();
        try {
            return encapsulatedBucket.getObject(id);
        } finally {
            replicaManipulationLock.readLock().unlock();
        }
    }

    @Override
    public DataObjectIterator iterator() {
        replicaManipulationLock.readLock().lock();
        try {
            return encapsulatedBucket.iterator();
        } finally {
            replicaManipulationLock.readLock().unlock();
        }
    }

    @Override
    protected ModifiableIndex<DataObject> getModifiableIndex() {
        throw new UnsupportedOperationException("This method should not be called anywhere in the replication bucket, please fix the code");
    }

}
