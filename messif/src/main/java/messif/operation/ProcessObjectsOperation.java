/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation;

import messif.data.DataObject;
import messif.operation.answer.AbstractAnswer;
import messif.operation.answer.ListingAnswer;
import messif.operation.answer.ProcessObjectsAnswer;
import messif.operation.helpers.AnswerProvider;
import messif.operation.helpers.BaseHelper;
import messif.operation.helpers.ErrorCodeMerger;
import messif.operation.params.DataObjects;
import messif.operation.params.QueryObject;
import messif.record.RecordImpl;
import messif.utility.ProcessingStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * An base operation for any kinds of processing of given objects. A typical example are CRUD operations, but also
 *  "extraction" of descriptors etc.
 */
public interface ProcessObjectsOperation extends ReturnDataOperation, DataObjects {

    String TYPE_STRING = "process";

    @Override
    default String getInnerType() {
        return TYPE_STRING;
    }

    /**
     * Creates a generic "process object operation" from an operation with a query object
     * @param qObjOperation an operation with a query object
     * @return an operation that has the {@link DataObjects#OBJECTS_FIELD} with the query object from the passed operation
     */
    static ProcessObjectsOperation createFrom(QueryObject qObjOperation) {
        return OperationBuilder.create(ProcessObjectsOperation.class)
                .addParam(DataObjects.OBJECTS_FIELD, new DataObject[]{qObjOperation.getQueryObject()}).build();
    }

    /**
     * Creates a generic "process object operation" from any operation that has a list of data objects
     * @param objOperation an operation with a list of data objects
     * @return an operation that has the {@link DataObjects#OBJECTS_FIELD} with the same list of data objects (shallow copied)
     */
    static ProcessObjectsOperation createFrom(DataObjects objOperation) {
        return OperationBuilder.create(ProcessObjectsOperation.class)
                .addParam(DataObjects.OBJECTS_FIELD, Arrays.copyOf(objOperation.getObjects(), objOperation.getObjects().length)).build();
    }

    /**
     * Creates a generic helper for this type of operation with preset error codes.
     * @param operation an operation for which we create the helper
     * @return a created helper
     */
    static Helper<ProcessObjectsOperation, ProcessObjectsAnswer> createHelper(ProcessObjectsOperation operation) {
        return new Helper<>(operation, ProcessObjectsAnswer.class, ProcessObjectsAnswer.SOME_OBJECTS_PROCESSED,
                new ProcessingStatus[] { ProcessObjectsAnswer.OK_PROCESSED},
                new ProcessingStatus[] { ProcessObjectsAnswer.ERROR_PROCESSING, ProcessObjectsAnswer.SOME_OBJECTS_PROCESSED });
    }

    /**
     * A helper that contains mostly the builder of a {@link ListingAnswer}, a list of
     * {@link ProcessObjectsAnswer#SKIPPED_OBJECTS_FIELD skipped objects} and {@link ErrorCodeMerger error code helper}.
     */
    class Helper<TRequest extends ProcessObjectsOperation, TAnswer extends ProcessObjectsAnswer> extends BaseHelper<TRequest>
            implements AnswerProvider<TAnswer> {

        /**
         * A builder of the list of objects actually modified (inserted/deleted/update) by this operation.
         */
        public final ListingAnswer.Builder<TAnswer> answerBuilder;

        /**
         * A helper class to merge properly the error codes.
         */
        public final ErrorCodeMerger errorCodeHelper;

        /**
         * A list of objects skipped by the processing, e.g. because they are not stored (and thus cannot be deleted).
         */
        protected List<DataObject> skippedObjects;

        /**
         * Creates this processor given the operation to be processed.
         *
         * @param operation    operation to be executed
         * @param partialAnswerCode code to be set in case that some operation ended with errors, but not all
         * @param alrightCodes a list of codes that are considered to be alright
         */
        protected Helper(TRequest operation, Class<? extends TAnswer> answerClass, ProcessingStatus partialAnswerCode, ProcessingStatus [] alrightCodes, ProcessingStatus [] errorCodes) {
            super(operation);
            this.errorCodeHelper =  new ErrorCodeMerger(partialAnswerCode, alrightCodes, errorCodes);
            this.answerBuilder = new ListingAnswer.Builder(operation, answerClass, errorCodeHelper, true);
        }

        //  *********************       Direct manipulation with list of processed objects and error codes     *******  //

        @Override
        public void updateAnswer(AbstractAnswer answer) {
            answerBuilder.updateAnswer(answer);
            if ((answer instanceof ProcessObjectsAnswer) && ((ProcessObjectsAnswer) answer).getSkippedObjects() != null) {
                if (skippedObjects == null) {
                    skippedObjects = new ArrayList<>();
                }
                this.skippedObjects.addAll(Arrays.asList(((ProcessObjectsAnswer) answer).getSkippedObjects()));
            }
        }

        /**
         * Method saying that given object was processed with given processing status. The object is either
         *  stored in the answer or in the skipped objects.
         * @param processedObject
         * @param status
         * @return
         */
        public boolean objectProcessed(DataObject processedObject, ProcessingStatus status) {
            if (errorCodeHelper.checkAndUpdateCode(status) && (processedObject != null)) {
                answerBuilder.add(processedObject);
                return true;
            }
            skipObject(processedObject, status);
            return false;
        }

        private static final String [] skippedFields = new String [] { DataObject.ID_FIELD, ProcessObjectsAnswer.SKIP_REASON_FIELD };

        /**
         * Adds given object among the objects that were to be processed, but were skipped for a given reason
         *  (expressed by a processing status).
         * @param object data object skipped from processing
         * @param status a processing status to express the reason why the object was skipped
         */
        public void skipObject(DataObject object, ProcessingStatus status) {
            skipObject(object, AbstractAnswer.createAnswer(status));
        }

        /**
         * Adds given object among the objects that were to be processed, but were skipped for a given reason
         *  (expressed by a sub-query answer).
         * @param object data object skipped from processing
         * @param subAnswer a reason why the object was skipped
         */
        public void skipObject(DataObject object, AbstractAnswer subAnswer) {
            if (skippedObjects == null) {
                skippedObjects = new ArrayList<>();
            }

            // create a new (unmodifiable) data object only with the specified fields
            Object [] skippedData = new Object [] { object.getID(), subAnswer};
            skippedObjects.add(new RecordImpl(skippedFields, skippedData));
        }

        //  *********************       Getting info about processed objects and error codes     ********************* //

        public int getNumberProcessedObjects() {
            return answerBuilder.getAnswerSize();
        }

        @Override
        public TAnswer getAnswer() {
            final TAnswer answer = answerBuilder.getAnswer();
            if (skippedObjects != null) {
                answer.getParams().setField(ProcessObjectsAnswer.SKIPPED_OBJECTS_FIELD, skippedObjects.toArray(new DataObject[skippedObjects.size()]));
            }
            return answer;
        }

    }
}
