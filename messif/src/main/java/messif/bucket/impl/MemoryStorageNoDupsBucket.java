/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.bucket.impl;

import messif.data.DataObject;

import java.io.Serializable;
import messif.bucket.BucketDispatcher;
import messif.bucket.BucketStorageException;
import messif.bucket.DuplicateObjectException;
import messif.bucket.LocalBucket;
import messif.bucket.OrderedLocalBucket;
import messif.bucket.index.LocalAbstractObjectOrder;
import messif.bucket.index.ModifiableOrderedIndex;
import messif.bucket.index.impl.IntStorageIndex;
import messif.storage.impl.MemoryStorage;


/**
 * A volatile implementation of {@link LocalBucket}.
 * It stores all objects in a {@link messif.bucket.storage.impl.MemoryStorage memory storage}.
 * Objects are indexed by their {@link LocalAbstractObject#dataHashCode() data} and
 * does not allow to store two objects with the same data. If an object that is
 * {@link messif.objects.LocalAbstractObject#dataEquals dataEquals} to
 * any object actually stored in the bucket, the <tt>addObject</tt> method will
 * throw {@link DuplicateObjectException}.
 * 
 * <p>
 * This bucket has an efficient {@link LocalBucket#deleteObject(messif.objects.LocalAbstractObject, int)} implementation
 * at the cost of additional memory overhead for maintaining the index.
 * If fast {@code deleteObject} implementation is not required and
 * the iteration over all objects is used frequently, consider using
 * {@link MemoryStorageBucket}.
 * </p>
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @see BucketDispatcher
 * @see LocalBucket
 */
public class MemoryStorageNoDupsBucket extends OrderedLocalBucket<DataObject> implements Serializable {
    /** class serial id for serialization */
    private static final long serialVersionUID = 1L;

    //****************** Data storage ******************//

    /** Object storage with object-id index */
    protected ModifiableOrderedIndex<DataObject, DataObject> objects =
            new IntStorageIndex<DataObject, DataObject>(
                    new MemoryStorage<DataObject>(DataObject.class),
                    LocalAbstractObjectOrder.DATA
            ) {
                /** class serial id for serialization */
                private static final long serialVersionUID = 1L;

                @Override
                protected int insertionPoint(DataObject object) throws BucketStorageException {
                    int pos = binarySearch(object, 0, size() - 1, true);
                    if (pos < 0)
                        return -pos - 1;
                    // Object was found in the index, throw duplicate exception
                    throw new DuplicateObjectException();
                }
            };


    /****************** Constructors ******************/

    /**
     * Constructs a new instance of MemoryStorageNoDupsBucket.
     * 
     * @param capacity maximal capacity of the bucket - cannot be exceeded
     * @param softCapacity maximal soft capacity of the bucket
     * @param lowOccupation a minimal occupation for deleting objects - cannot be lowered
     * @param occupationAsBytes flag whether the occupation (and thus all the limits) are in bytes or number of objects
     */
    public MemoryStorageNoDupsBucket(long capacity, long softCapacity, long lowOccupation, boolean occupationAsBytes) {
        super(capacity, softCapacity, lowOccupation, occupationAsBytes, 0);
    }


    //****************** Overrides ******************//

    @Override
    protected ModifiableOrderedIndex<DataObject, DataObject> getModifiableIndex() {
        return objects;
    }

    @Override
    public void finalize() throws Throwable {
        objects.finalize();
        super.finalize();
    }

    @Override
    public void destroy() throws Throwable {
        objects.destroy();
        super.destroy();
    }

}
