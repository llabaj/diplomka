/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.bucket;

import java.util.List;
import junit.framework.TestCase;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import test.TestConstants;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class LocalBucketBenchmarkTest extends TestCase {

    public LocalBucketBenchmarkTest(String testName) {
        super(testName);
    }

    public void testBenchmark() throws Throwable {
        List<LocalAbstractObject> objs = TestConstants.createBenchmarkObjects();
        if (objs == null)
            return;

        LocalBucket bucket = TestConstants.createBucket();
        try {
            // Add objects to the bucket
            long time = System.currentTimeMillis();
            for (LocalAbstractObject object : objs)
                bucket.addObject(object);
            time = System.currentTimeMillis() - time;
            System.out.println("Benchmark of adding " + objs.size() + " objects run in " + time + "ms");

            // Read all objects
            time = System.currentTimeMillis();
            AbstractObjectIterator<LocalAbstractObject> allObjects = bucket.getAllObjects();
            while (allObjects.hasNext()) {
                LocalAbstractObject obj = allObjects.next();
                obj.getLocatorURI(); // This is to actualy use the created object
            }
            time = System.currentTimeMillis() - time;
            System.out.println("Benchmark of reading: " + objs.size() + " objects run in " + time + "ms");

            // Delete all objects
            time = System.currentTimeMillis();
            bucket.deleteAllObjects();
            time = System.currentTimeMillis() - time;
            System.out.println("Benchmark of deleting: " + objs.size() + " objects run in " + time + "ms");
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            bucket.destroy();
        }
    }

}
