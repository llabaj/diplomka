/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.distance.agg;

import messif.distance.DistanceFunc;

import java.text.NumberFormat;
import java.util.Arrays;

/**
 * A distance which, given two objects, calculates a minimum of a previously given list of distance functions on the two objects.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @param <T> Type of the object on which this distance can evaluate {@link DistanceFunc#getDistance(Object, Object)
 */
public abstract class WeightedDistanceFunc<T> extends AggregationDistanceFuncBase<T> {

    /** Class id for Java serialization. */
    private static final long serialVersionUID = 1L;

    protected final float [] weights;

    protected final float maxDistance;

    protected WeightedDistanceFunc(DistanceFunc<T>[] distanceFunctions, float [] weights) {
        super(distanceFunctions);
        if (distanceFunctions == null || weights == null || distanceFunctions.length != weights.length) {
            throw new IllegalArgumentException("the distances and weights lists must have the same lenght: " +
                    Arrays.toString(distanceFunctions) + ", " + Arrays.toString(weights));
        }
        this.weights = weights;
        this.maxDistance = calculateMaxDistances();
    }

    @Override
    public float getMaxDistance() {
        return maxDistance;
    }

    @Override
    public Class<T> getObjectClass() {
        if (distanceFunctions.length == 0) {
            return null;
        }
        return distanceFunctions[0].getObjectClass();
    }

    /**
     * Returns a string representation of the encapsulated function.
     * @return a string representation of the encapsulated function
     */
    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder("f = ");
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(5);
        for (int i = 0; i < weights.length; i++) {
            buf.append(nf.format(weights[i])).append('*').append(distanceFunctions[i].toString());
            if (i < weights.length - 1)
                buf.append(" + ");
        }
        return buf.toString();
    }
}
