/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.processing;

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.reflections.Reflections;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class ReflectionTest {

    @BeforeClass
    public static void setUp() {

    }

    private interface A {
        default String getType() {
            return getInnerType();
        }

        String getInnerType();
    }

    private interface B extends A {
        String TYPE = "B";

        @Override
        default String getInnerType() {
            return TYPE;
        }
    }

    private interface C extends A {
        String TYPE = "C";

        @Override
        default String getInnerType() {
            return TYPE;
        }
    }

    private interface BB extends B {
        String TYPE = "BB";

        @Override
        default String getInnerType() {
            return TYPE;
        }
    }

    private interface BBB extends B {
    }

    @Test
    public void testClassLoader() throws NoSuchMethodException, NoSuchFieldException, IllegalAccessException {
        assertClassLoader(C.class, "C");
        assertClassLoader(BB.class, "BB");
        assertClassLoader(BBB.class);
        assertClassLoader(B.class, "BB", "B");
        assertClassLoader(A.class, "B", "BB", "C");
    }


    //    @Test
    public void testClassPath() throws NoSuchMethodException, NoSuchFieldException, IllegalAccessException {
        assertClassPath(A.class, "B", "BB", "C");
        assertClassPath(B.class, "BB", "B");
        assertClassPath(C.class, "C");
        assertClassPath(BB.class, "BB");
        assertClassPath(BBB.class);
    }

    private void assertClassLoader(Class<?> clazz, String... expectedTypes) throws NoSuchFieldException, IllegalAccessException {

        List<String> types = Lists.newArrayList();
        for (Object subTypeObj : listClasses()) {
            Class subType = (Class) subTypeObj;
            if (clazz.isAssignableFrom(subType)) {
                String type = verifyMethod(subType);
                if (type != null)
                    types.add(type);
            }
        }

        types.sort(String::compareTo);
        List<String> expectedTypeList = Arrays.asList(expectedTypes);
        expectedTypeList.sort(String::compareTo);
        System.out.println("Class actual:" + clazz.getTypeName() + types.toString());
        System.out.println("Class expected:" + clazz.getTypeName() + expectedTypeList.toString());

        Assert.assertArrayEquals(expectedTypeList.toArray(), types.toArray());
    }

    private void assertClassPath(Class<?> clazz, String... expectedTypes) throws NoSuchFieldException, IllegalAccessException {

        List<String> types = Lists.newArrayList();
        for (Class<?> subType : getReflections().getSubTypesOf(clazz)) {
            String type = verifyMethod(subType);
            if (type != null)
                types.add(type);
        }

        String type = verifyMethod(clazz);
        if (type != null)
            types.add(type);

        types.sort(String::compareTo);
        List<String> expectedTypeList = Arrays.asList(expectedTypes);
        expectedTypeList.sort(String::compareTo);
        System.out.println("Class actual:" + clazz.getTypeName() + types.toString());
        System.out.println("Class expected:" + clazz.getTypeName() + expectedTypeList.toString());

        Assert.assertArrayEquals(expectedTypeList.toArray(), types.toArray());
    }

    static Reflections reflections = null;

    private static Reflections getReflections() {
        if (reflections == null)
            reflections = new Reflections();
        return reflections;
    }

    private String verifyMethod(Class<?> clazz) {
        try {
            Method method = clazz.getDeclaredMethod("getInnerType");
            if (method.isDefault())
                return (String) clazz.getField("TYPE").get(null);
        } catch (NoSuchMethodException | NoSuchFieldException | IllegalAccessException e) {
        }
        return null;
    }

    private static Collection listClasses() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        Class clClass = cl.getClass();
        while (clClass != java.lang.ClassLoader.class) {
            clClass = clClass.getSuperclass();
        }
        java.lang.reflect.Field classesField = clClass.getDeclaredField("classes");
        classesField.setAccessible(true);
        Vector classes = (Vector) classesField.get(cl);
        return classes;
    }
}


