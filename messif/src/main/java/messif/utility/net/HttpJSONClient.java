/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.net;

import messif.utility.json.JSONReader;
import messif.utility.json.JSONWriter;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Connects to a given URL (round robin given several URLs) and sends given json text as a POST into via the HTTP
 * protocol. Returns the parsed JSON returned from the service.
 */
public class HttpJSONClient extends HttpClient {

    public HttpJSONClient(URL url) {
        super(url);
    }

    public HttpJSONClient(String url) throws MalformedURLException {
        super(url);
    }

    public HttpJSONClient(URL [] urls) {
        super(urls);
    }


    public Object sendJSON(Object jsonObject) throws IOException, HttpStatusException {
        final String jsonString = JSONWriter.writeToJSON(jsonObject, false);
        final String response = super.sendRequest(jsonString);
        return JSONReader.readObjectFrom(response, true);
    }

    @Override
    protected void setHeaders(HttpURLConnection connection) {
        super.setHeaders(connection);
        connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
    }
}
