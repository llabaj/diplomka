/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.processing;

import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.record.Record;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.function.Function;

/**
 * This class mainly says what {@link OperationProcessor} should be used for which operation. The decision can
 *  be done either based on operation class or based on its type string.
 */
public class ProcessorConfiguration<TOperation extends AbstractOperation, TAnswer extends AbstractAnswer> {

    /**
     * Type of operation for which this config is valid; it fires also for all subclasses of this type.
     */
    private final Class<TOperation> operationClass;

    /**
     * Value of type string (stored in attributed {@link AbstractOperation#TYPE_FIELD} of a {@link Record})
     * for which this processor config is valid. The list can be empty.
     */
    private final Collection<String> typeStrings;

    //private final Class<TAnswer> answerClass;

    /**
     * A factory to actually create a processor for given operation.
     */
    private Function<TOperation, OperationProcessor<? super TOperation, ? extends TAnswer>> factory;

    /**
     * Executor to be used on the processor (typically either sequential or parallel).
     */
    private final ProcessorExecutor executor;

    /**
     * A full constructor to set all the fields, it is typically called from a builder.
     * @param typeStrings a list of type strings to match {@link Record}.
     * @param operationClass operation class to match incomming {@link AbstractOperation}
     * @param factory a function to create a processor given an operation
     * @param executor a seq/parallel executor to be used with this processor
     */
    public ProcessorConfiguration(Collection<String> typeStrings, Class<TOperation> operationClass,
                                  Function<TOperation, OperationProcessor<? super TOperation, ? extends TAnswer>> factory, ProcessorExecutor executor) {
        this.operationClass = operationClass;
        //this.answerClass = answerClass;
        this.factory = factory;
        this.executor = executor;
        if (typeStrings.isEmpty()) {
            try {
                typeStrings.add((String) operationClass.getField("TYPE_STRING").get(null));
            } catch (NoSuchFieldException | IllegalAccessException ignore) {
            }
        }
        this.typeStrings = Collections.unmodifiableCollection(typeStrings);
    }

    public Class<TOperation> getOpClass() {
        return operationClass;
    }

    public Collection<String> getTypeStrings() {
        return typeStrings;
    }

    public OperationProcessor<? super TOperation, ? extends TAnswer> initProcessor(TOperation operation) {
        return factory.apply(operation);
    }

    public ProcessorExecutor getExecutor() {
        return executor;
    }

    @Override
    public String toString() {
        return "operation of class " + operationClass.toString() + " or " + Record.class.getSimpleName() +
                " with field in \"" + AbstractOperation.TYPE_FIELD + "\": " +
                "\"" + Arrays.deepToString(typeStrings.toArray()) + '\"';
    }

    public static <TOperation extends AbstractOperation, TAnswer extends AbstractAnswer> ProcessorConfigurationBuilder<TOperation, TAnswer> create(Class<TOperation> operationClass, Class<TAnswer> aClazz) {
        return new ProcessorConfigurationBuilder<>(operationClass, aClazz);
    }

    public static class ProcessorConfigurationBuilder<TOperation extends AbstractOperation, TAnswer extends AbstractAnswer> {
        private Collection<String> typeStrings;
        private Class<TOperation> operationClass;
        private ProcessorExecutor executor;
        private Function<TOperation, OperationProcessor<? super TOperation, ? extends TAnswer>> factory;
        //private Class<TAnswer> answerClass;

        public ProcessorConfigurationBuilder(Class<TOperation> operationClass, Class<TAnswer> answerClass) {
            this.operationClass = operationClass;
            //this.answerClass = answerClass;
            this.typeStrings = new ArrayList<>();
            this.executor = ProcessorExecutor.SEQUENTIAL_EXECUTOR;
        }

        public ProcessorConfigurationBuilder<TOperation, TAnswer> forTypeStrings(String... typeStrings) {
            for (String typeString : typeStrings) {
                this.typeStrings.add(typeString);
            }
            return this;
        }

        public ProcessorConfigurationBuilder<TOperation, TAnswer> withProcessor(Function<TOperation, OperationProcessor<? super TOperation, ? extends TAnswer>> factory) {
            this.factory = factory;
            return this;
        }

        public ProcessorConfigurationBuilder<TOperation, TAnswer> executeSequential() {
            this.executor = ProcessorExecutor.SEQUENTIAL_EXECUTOR;
            return this;
        }

        public ProcessorConfigurationBuilder<TOperation, TAnswer> executeParallel(ExecutorService pool) {
            this.executor = new ProcessorExecutor.ParallelExecutor(pool);
            return this;
        }

        public ProcessorConfigurationBuilder<TOperation, TAnswer> executeWith(ProcessorExecutor executor) {
            this.executor = executor;
            return this;
        }

        public ProcessorConfiguration<TOperation, TAnswer> build() {
            return new ProcessorConfiguration<>(typeStrings, operationClass, factory, executor);
        }
    }
}