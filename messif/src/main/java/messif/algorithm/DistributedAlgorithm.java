/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.algorithm;

import messif.network.netcreator.Startable;
import messif.network.*;
import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.helpers.AnswerProvider;
import messif.statistic.OperationStatistics;
import messif.statistic.StatisticCounter;
import messif.statistic.StatisticRefCounter;
import messif.statistic.Statistics;

import java.io.IOException;
import java.util.*;
import java.util.logging.Level;

/**
 *  Abstract distributed algorithm framework with support for request/reply messaging and respective operation executive.
 *
 *  Distributed algorithm subclass should override createRequestMessage
 *  and createResponseMessage to add algorithm specific message extensions.
 *
 *  Operation methods should have two arguments - the operation (a subclass of AbstractOperation) and
 *  the request message (a subclass of DistAlgRequestMessage). Operation method should process
 *  the operation and use standard methods of message dispatcher (sendMessage family) to process the operation.
 *  The original message that triggered this processing is the second argument of the executed
 *  methods and can be null if the method was called locally (i.e. processing start).
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public abstract class DistributedAlgorithm extends Algorithm implements Startable {

    /** Class id for serialization. */
    private static final long serialVersionUID = 1L;

    //****************** Attributes ******************//

    /** Message dispatcher for this distributed algorithm */
    protected final MessageDispatcher messageDisp;


    //****************** Constructors ******************//

    /**
     * Creates a new instance of DistributedAlgorithm.
     * @param algorithmName the name of this algorithm
     * @param port the TCP/UDP port on which this distributed algorithm communicates
     * @param broadcastPort the UDP multicast port that this distributed algorithm uses for broadcast
     * @throws IllegalArgumentException if there was a problem starting message dispatcher
     */
    public DistributedAlgorithm(String algorithmName, int port, int broadcastPort) throws IllegalArgumentException {
        super(algorithmName);

        try {
            // Start Message dispatcher
            this.messageDisp = new MessageDispatcher(port, broadcastPort);
        } catch (IOException e) {
            log.log(Level.SEVERE, e.getClass().toString(), e);
            throw new IllegalArgumentException("Can't start message dispatcher: " + e.getMessage());
        }
    }

    /**
     * Creates a new instance of DistributedAlgorithm without broadcast capabilities.
     * @param algorithmName the name of this algorithm
     * @param port the TCP/UDP port on which this distributed algorithm communicates
     * @throws IllegalArgumentException if there was a problem starting message dispatcher
     */
    public DistributedAlgorithm(String algorithmName, int port) throws IllegalArgumentException {
        this(algorithmName, port, 0);
    }

    /**
     * Creates a new instance of DistributedAlgorithm without broadcast capabilities.
     * The TCP/UDP port on which this distributed algorithm communicates is selected by the
     * operating system and can be queried through {@link #getThisNode()}.
     * @param algorithmName the name of this algorithm
     * @throws IllegalArgumentException if there was a problem starting message dispatcher
     */
    public DistributedAlgorithm(String algorithmName) throws IllegalArgumentException {
        this(algorithmName, 0);
    }

    /**
     * Creates a new instance of DistributedAlgorithm with a higher-level message dispatcher queue.
     * The TCP/UDP port on which this distributed algorithm communicates as well as
     * the broadcast capabilities are linked to the specified message dispatcher.
     * @param algorithmName the name of this algorithm
     * @param parentDispatcher the higher level dispatcher this algorithm's dispacher is connected to
     * @param nodeID the sub-identification of this algorithm's dispatcher for the higher level
     * @throws IllegalArgumentException if there was a problem starting message dispatcher
     */
    public DistributedAlgorithm(String algorithmName, MessageDispatcher parentDispatcher, int nodeID) throws IllegalArgumentException {
        super(algorithmName);

        // Start Message dispatcher
        this.messageDisp = new MessageDispatcher(parentDispatcher, nodeID);
    }

    @Override
    protected void registerProcessors() {
        super.registerProcessors();
//        this.handler.register(ProcessorConfiguration.create(ObjectCountOperation.TYPE_STRING, ObjectCountOperation.class)
//                .withProcessor(operation -> new SequentialScan.ObjectCountProcessor((ObjectCountOperation) operation))
//                .executeSequential().build());
    }

    //****************** Destructor ******************//

    @Override
    public void finalize() throws Throwable {
        messageDisp.closeSockets();
        super.finalize();
    }

    @Override
    public void destroy() throws Throwable {
        messageDisp.closeSockets();
        // Do not execute super.destroy(), since algorithm needs to differentiate between finalizing and destroying
    }

        //****************** Name enrichment ******************//

    /**
     * Returns the name of this algorithm with host:port of its message dispatcher
     * @return the name of this algorithm with host:port of its message dispatcher
     */
    @Override
    public String getName() {
        return super.getName() + " at " + getThisNode().getHost().getHostName() + ":" + getThisNode().getPort();
    }


    //****************** Message dispatcher ******************//

    /**
     * Returns the network node of this distributed algorithm.
     * @return the network node of this distributed algorithm
     */
    public NetworkNode getThisNode() {
        return messageDisp.getNetworkNode();
    }

    /**
     * Returns the message dispatcher of this distributed algorithm.
     * @return the message dispatcher of this distributed algorithm
     */
    public MessageDispatcher getMessageDispatcher() {
        return messageDisp;
    }


    // *******************     Helper methods to be used in the actual operation processors    ********************** //

    /**
     * Given a just-arrived message, this method registers (binds) DC, DC.Savings and BlockReads statistics
     *   for current thread.
     * @param msg new arrived message
     * @return collection of registered statistics
     * @throws InstantiationException
     */
    protected Collection<Statistics<?>> setupMessageStatistics(Message msg) throws InstantiationException {
        Collection<Statistics<?>> registeredStats = new ArrayList<Statistics<?>>();
        registeredStats.add(msg.registerBoundStat("DistanceComputations"));
        registeredStats.add(msg.registerBoundStat("DistanceComputations.Savings"));
        //registeredStats.add(msg.registerBoundStat("BlockReads"));

        return registeredStats;
    }

    /**
     * Unbind given statistics.
     * @param stats set of stats to unbind
     */
    protected void deregisterMessageStatistics(Collection<Statistics<?>> stats) {
        if (stats == null)
            return;
        for (Statistics<?> statistics : stats) {
            statistics.unbind();
        }
    }


    //****************** Navigation processing support ******************//

    /**
     * Creates a request message used by this algorithm.
     * @param operation the operation for which to create the request message
     * @return a new request message with the specified operation
     */
    protected DistAlgRequestMessage createRequestMessage(AbstractOperation operation) {
        return new DistAlgRequestMessage(operation);
    }

    /**
     * Creates a reply message used by this algorithm.
     * @param msg the request message for which to create a response
     * @return a new reply message for the specified request message
     */
   protected DistAlgReplyMessage createReplyMessage(DistAlgRequestMessage msg, AbstractAnswer answer) {
        return new DistAlgReplyMessage(msg, answer);
    }

    /**
     * Processes navigation when there will be no local processing.
     * If the request message is specified, it is simply forwarded to the specified node.
     * Otherwise, a new {@link DistAlgRequestMessage} is created, sent to the specified node
     * and the method waits for all the responses. This method blocks until all the
     * responses are gathered.
     * @param processor operation processor with the operation that is processed
     * @param request the request from which the the operation arrived (the first node has <tt>null</tt> request)
     * @param node the destination node where to forward the request
     * @throws IOException if there was an I/O error during sending or receiving messages
     */
    protected void navigationNoProcessing(AnswerProvider<AbstractAnswer> merger, AbstractOperation operation, DistAlgRequestMessage request, NetworkNode node) throws IOException {
        // If there is no processing, the forward node cannot be null
        if (node == null)
            throw new IOException("Navigation processing error while evaluating " + operation + ": forwarding to null node requested");

        // If the request is null, this is a first node and thus we will wait for replies
        if (request == null) {
            navigationAfterProcessing(merger, null, messageDisp.sendMessageWaitReply(createRequestMessage(operation), DistAlgReplyMessage.class, true, node));
        } else {
            // Otherwise, just forward the messages
            messageDisp.sendMessage(request, node, true);
        }
    }

    /**
     * Processes navigation when there will be no local processing.
     * If the request message is specified, it is simply forwarded to all the nodes.
     * Otherwise, a new {@link DistAlgRequestMessage} is created, sent to the specified nodes
     * and the method waits for all the responses. This method blocks until all the
     * responses are gathered.
     * @param merger operation processor with the operation that is processed
     * @param request the request from which the the operation arrived (the first node has <tt>null</tt> request)
     * @param nodes the destination nodes where to forward the request
     * @throws IOException if there was an I/O error during sending or receiving messages
     */
    protected void navigationNoProcessing(AnswerProvider<AbstractAnswer> merger, AbstractOperation operation, DistAlgRequestMessage request, Collection<NetworkNode> nodes) throws IOException {
        // If there is no processing, the forward node cannot be null
        if (nodes == null || nodes.isEmpty())
            throw new IOException("Navigation processing error while evaluating " + operation + ": forwarding to null nodes requested");

        // If the request is null, this is a first node and thus we will wait for replies
        if (request == null) {
            navigationAfterProcessing(merger, null, messageDisp.sendMessageWaitReply(createRequestMessage(operation), DistAlgReplyMessage.class, true, nodes));
        } else {
            // Otherwise, just forward the messages
            messageDisp.sendMessage(request, nodes, true);
        }
    }

    /**
     * Processes navigation before the local processing.
     * If the request message is specified, it is simply forwarded to all the nodes.
     * Otherwise, a new {@link DistAlgRequestMessage} is created, sent to the specified nodes
     * and the reply receiver for waiting for their responses is returned (i.e. the processing is not blocked).
     * @param operation the operation that is processed
     * @param request the request from which the the operation arrived (the first node has <tt>null</tt> request)
     * @param nodes the destination nodes where to forward the request
     * @return receiver that allows waiting for the responses
     * @throws IOException if there was an I/O error during sending messages
     */
    protected ReplyReceiver<? extends DistAlgReplyMessage> navigationBeforeProcessing(AbstractOperation operation, DistAlgRequestMessage request, Collection<NetworkNode> nodes) throws IOException {
        if (nodes == null) {
            // If the request is null, this is a first node and thus we will wait for replies
            if (request == null) {
                return messageDisp.sendMessageWaitReply(createRequestMessage(operation), DistAlgReplyMessage.class, true, nodes);
            } else {
                // Otherwise, just forward the messages
                messageDisp.sendMessage(request, nodes, false);
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Processes navigation before the local processing.
     * If the request message is specified, it is simply forwarded to the specified node.
     * Otherwise, a new {@link DistAlgRequestMessage} is created, sent to the specified node
     * and the reply receiver for waiting for their responses is returned (i.e. the processing is not blocked).
     * @param operation the operation that is processed
     * @param request the request from which the the operation arrived (the first node has <tt>null</tt> request)
     * @param node the destination node where to forward the request
     * @return receiver that allows waiting for the responses
     * @throws IOException if there was an I/O error during sending messages
     */
    protected ReplyReceiver<? extends DistAlgReplyMessage> navigationBeforeProcessing(AbstractOperation operation, DistAlgRequestMessage request, NetworkNode node) throws IOException {
        if (node != null) {
            // If the request is null, this is a first node and thus we will wait for replies
            if (request == null) {
                return messageDisp.sendMessageWaitReply(createRequestMessage(operation), DistAlgReplyMessage.class, true, node);
            } else {
                // Otherwise, just forward the messages
                messageDisp.sendMessage(request, node, false);
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Processes navigation after the local processing.
     * If the receiver is <tt>null</tt> and the request is not <tt>null</tt>, a reply is created and sent back.
     * Otherwise, the receiver is used to wait for all the responses and this method blocks until they are gathered.
     * @param merger the processor to merge the partial answers
     * @param request the request from which the the operation arrived (the first node has <tt>null</tt> request)
     * @param receiver the receiver that is used for waiting for messages (can be <tt>null</tt>)
     * @return the number of messages received and processed
     * @throws IOException if there was an I/O error during sending messages
     */
    protected int navigationAfterProcessing(AnswerProvider<AbstractAnswer> merger, DistAlgRequestMessage request, ReplyReceiver<? extends DistAlgReplyMessage> receiver) throws IOException {
        if (receiver != null) {
            try {
                List<? extends DistAlgReplyMessage> replies = receiver.getReplies();
                mergeOperationsFromReplies(merger, replies);
                mergeStatisticsFromReplies(OperationStatistics.getLocalThreadStatistics(), replies);
                return replies.size();
            } catch (InterruptedException e) {
                throw new IOException("Interrupted while waiting for replies (" + receiver + ")");
            }
        } else {
            if (request != null)
                messageDisp.replyMessage(createReplyMessage(request, merger.getAnswer()));
            return 0;
        }
    }


    //****************** Reply merging functions ******************//

    /** Update supplied operation answer with partial answers from reply messages
     *  @param merger to merge the partial answers
     *  @param replyMessages the list of reply messages received with partial answers
     */
    public static void mergeOperationsFromReplies(AnswerProvider<AbstractAnswer> merger, Collection<? extends DistAlgReplyMessage> replyMessages) {
        for (DistAlgReplyMessage msg : replyMessages) {
            merger.updateAnswer(msg.getAnswer());
        }
    }

    /** Update supplied statistics with partial statistics from reply messages
     *  @param targetStatistics the operation statistics object that should be updated
     *  @param replyMessages the list of reply messages received with partial statistics
     */
    public void mergeStatisticsFromReplies(OperationStatistics targetStatistics, Collection<? extends ReplyMessage> replyMessages) {
        mergeStatisticsFromReplies(targetStatistics, replyMessages, OperationStatistics.getLocalThreadStatistics());
    }
    
    /** Update supplied statistics with partial statistics from reply messages
     *  @param targetStatistics the operation statistics object that should be updated
     *  @param replyMessages the list of reply messages received with partial statistics
     *  @param localStats statistics of local processing of the operation on this node
     */
    public void mergeStatisticsFromReplies(OperationStatistics targetStatistics, Collection<? extends ReplyMessage> replyMessages, OperationStatistics localStats) {
        if (replyMessages.isEmpty())
            return;

        StatisticCounter totalMsgs = targetStatistics.getStatisticCounter("Total.Messages");
        
        // first, create the nodesMap - maximum over node's DCs on given positions in all replies
        SortedMap<Integer, Map<NetworkNode, OperationStatistics>> nodesMap = new TreeMap<Integer, Map<NetworkNode, OperationStatistics>>();
        if (localStats != null) {
            Map<NetworkNode, OperationStatistics> statsOfFirstSender = new HashMap<NetworkNode, OperationStatistics>();
            statsOfFirstSender.put(getThisNode(), localStats);
            nodesMap.put(0, statsOfFirstSender);
        }
        for (ReplyMessage reply : replyMessages) {
            // create a map of node->DC taken as max over all nodes in all replies
            int i = 0;
            NetworkNode previousNode = null;
            for (Iterator<NavigationElement> it = reply.getPathElements(); it.hasNext(); i++) {
                NavigationElement el = it.next();
                Map<NetworkNode, OperationStatistics> positionMap = nodesMap.get(i);
                if (positionMap == null) {
                    positionMap = new HashMap<NetworkNode, OperationStatistics>();
                    nodesMap.put(i, positionMap);
                }
                
                NetworkNode node = el.getSender();
                OperationStatistics actualNodeStatistics = positionMap.get(node);
                if (actualNodeStatistics != null) { // sum the DC of peers that appear more than once in the navigation path
                    //positionMap.put(node, Math.max(actualNodeStatistics, (el.getStatistics()==null)?0:el.getStatistics().getStatisticCounter("NavigationElement.DistanceComputations").get()));
                    if ((el.getStatistics() != null) && 
                        (actualNodeStatistics.getStatisticCounter("NavigationElement.DistanceComputations").get() < el.getStatistics().getStatisticCounter("NavigationElement.DistanceComputations").get())) {
                        positionMap.put(node, el.getStatistics());
                    }
                } else {
                    //positionMap.put(node, (el.getStatistics()==null)?0:el.getStatistics().getStatisticCounter("NavigationElement.DistanceComputations").get());
                    if (el.getStatistics() != null) {
                        positionMap.put(node, el.getStatistics());
                    }
                    if ((previousNode != null) && ! previousNode.equalsIgnoreNodeID(node))
                        totalMsgs.add();
                }
                previousNode = node;
            }
            // add the reply messages (if not from the same host)
            if ((previousNode != null) && ! previousNode.equalsIgnoreNodeID(getThisNode()))
                totalMsgs.add();
        }
        
        // calculate sum of DCs computed on the whole PEER
        StatisticRefCounter peersDC = targetStatistics.getStatisticRefCounter("Peers.DistanceComputations");
        StatisticCounter totalDC = targetStatistics.getStatisticCounter("Total.DistanceComputations");
        StatisticCounter totalDCSavings = targetStatistics.getStatisticCounter("Total.DistanceComputations.Savings");
        StatisticCounter totalBlockReads = targetStatistics.getStatisticCounter("Total.BlockReads");
        for (Map<NetworkNode, OperationStatistics> position : nodesMap.values()) {
            for (Map.Entry<NetworkNode, OperationStatistics> nodeMaxDC : position.entrySet()) {
                long value = nodeMaxDC.getValue().getStatisticCounter("NavigationElement.DistanceComputations").get();
                peersDC.add(new NetworkNode(nodeMaxDC.getKey(), false), value);
                totalDC.add(value);
                value = nodeMaxDC.getValue().getStatisticCounter("NavigationElement.DistanceComputations.Savings").get();
                totalDCSavings.add(value);
                value = nodeMaxDC.getValue().getStatisticCounter("NavigationElement.BlockReads").get();
                totalBlockReads.add(value);
            }
            // calculate total messages as sum of sizes of the positions in the "nodesMap"
        }

        // statistics
        StatisticRefCounter peersParDC = targetStatistics.getStatisticRefCounter("PeersParallel.DistanceComputations");
        StatisticCounter hopCount = targetStatistics.getStatisticCounter("HopCount");
        
        // iterate over the replies again and calculate parallel DC for all hosts
        for (ReplyMessage reply : replyMessages) {
            // create a map of node->DC because messages can create loops (in the terms of "hosts")
            Set<NetworkNode> visitedPeers = new HashSet<NetworkNode>();
            long parDCs = 0;
            int replyLength = 0;
            NetworkNode previousHost = null;
            for (Iterator<NavigationElement> it = reply.getPathElements(); it.hasNext(); ) {
                NavigationElement el = it.next();
                NetworkNode peer = new NetworkNode(el.getSender(), false);
                if (visitedPeers.add(peer))
                    peersParDC.min(peer, peersDC.get(peer) + parDCs);
                parDCs += (el.getStatistics()==null)?0:el.getStatistics().getStatisticCounter("NavigationElement.DistanceComputations").get();
                
                // messaging
                if ((previousHost != null) && (! previousHost.equals(el.getSender())))
                    replyLength++;
                previousHost = peer;
            }
            if (! getThisNode().equals(previousHost))
                replyLength++;
            hopCount.max(replyLength);
        }
        
        // create the ParallelDC statics now as a max over all hostsParallel DCs
        StatisticCounter parDC = targetStatistics.getStatisticCounter("Parallel.DistanceComputations");
        for (Object obj : peersParDC.getKeys())
            parDC.max(peersParDC.get(obj));
                
        // the peers that answered (are at the end of the paths) are put into a separate map
        StatisticRefCounter answeringPeers = targetStatistics.getStatisticRefCounter("AnsweringPeers.DistanceComputations");
        for (ReplyMessage reply : replyMessages) {
            NetworkNode peer = new NetworkNode(reply.getSender(), false);
            answeringPeers.add(peer, peersDC.get(peer));
        }
        if ((localStats != null) && (localStats.getStatisticCounter("DistanceComputations").get() > 0)) {
            answeringPeers.add(getThisNode(), localStats.getStatisticCounter("DistanceComputations").get());
        }
        // add statistic "AnsweringNodes"
        targetStatistics.getStatisticCounter("AnsweringPeers").add(answeringPeers.getKeyCount());
        
        // remove the hosts parallel statistic and
        targetStatistics.removeStatistic("PeersParallel.DistanceComputations");
        targetStatistics.removeStatistic("Peers.DistanceComputations");        
    }
    
}
