/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.record.processing;

import messif.record.Record;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Iterator that takes all {@link Record records} from a given iterator and applies given processor(s) on them.
 *
 * @param <RecordType> a subtype of {@link Record} - the class of objects returned by the encapsulated iterator
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class RecordProcessorIterator<RecordType extends Record> implements Iterator<RecordType> {

    /** Encapsulated iterator that provides the records  to convert */
    private final Iterator<? extends RecordType> iterator;

    /** Convertor to apply to iterated items */
    private final RecordProcessor<RecordType> processor;

    /**
     * If true, the records that return false for {@link RecordProcessor#preCondition(Record)} are skipped from the
     *  iterator. Otherwise, these records are returned without processing.
     */
    protected final boolean skipNonMatching;

    /** Next record to be returned by the iterator. */
    private RecordType nextRecord;

    /**
     * Creates a new iterator converts all objects from the encapsulated iterator.
     * @param iterator the encapsulated iterator that provides the objects to convert
     * @param processor the processor to apply to iterated items
     * @param skipNonMatching if true, the records that return false for {@link RecordProcessor#preCondition(Record)}
     *                        are skipped from the iterator; otherwise, these records are returned without processing.
     */
    public RecordProcessorIterator(Iterator<? extends RecordType> iterator, RecordProcessor<RecordType> processor, boolean skipNonMatching) {
        this.iterator = iterator;
        this.processor = processor;
        this.skipNonMatching = skipNonMatching;
    }

    public RecordProcessorIterator(Iterator<? extends RecordType> iterator, RecordProcessor<RecordType> [] processors, boolean skipNonMatching) {
        this(iterator, RecordProcessors.chainProcessors(processors), skipNonMatching);
    }


    @Override
    public boolean hasNext() {
        while (nextRecord == null) {
            if (!iterator.hasNext())
                return false;
            nextRecord = iterator.next();
            if (skipNonMatching && ! processor.preCondition(nextRecord))
                nextRecord = null;
        }
        return true;
    }

    @Override
    public RecordType next() {
        try {
            if (nextRecord == null && ! hasNext()) {
                throw new NoSuchElementException(RecordProcessorIterator.class.getSimpleName() + " has no more elements");
            }
            if (! skipNonMatching && ! processor.preCondition(nextRecord))
                return nextRecord;
            return processor.process(nextRecord);
        } catch (Exception e) {
            throw new IllegalStateException("Error converting object: " + e, e);
        } finally {
            nextRecord = null;
        }
    }
}
