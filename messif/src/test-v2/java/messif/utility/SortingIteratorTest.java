/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility;

import java.util.Arrays;
import junit.framework.TestCase;
import messif.objects.text.WordsProvider;
import messif.objects.text.impl.WordsJaccardDistanceFunction;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class SortingIteratorTest extends TestCase {

    public SortingIteratorTest(String testName) {
        super(testName);
    }

    public WordsProvider createWordsProvider(final String... words) {
        return new WordsProvider() {
            @Override
            public String[] getWords() {
                return words;
            }
        };
    }

    public SortingIterator<String> createIterator(final String... words) {
        return new SortingIterator<String>().addArray(words);
    }

    private void intersectionBothWays(String[] expected, String[] words1, String[] words2) {
        IteratorIntersection<String> intersection = new IteratorIntersection<>(createIterator(words1), createIterator(words2), null);
        assertEquals(Arrays.asList(expected), intersection.intersection());
        intersection = new IteratorIntersection<>(createIterator(words2), createIterator(words1), null);
        assertEquals(Arrays.asList(expected), intersection.intersection());
    }

    public void testIntersection() {
        intersectionBothWays(new String[] {"ahoj", "ahoj", "cau", "cau", "nazdar"}, new String[] { "ahoj", "ahoj", "cau", "cau", "nazdar" }, new String[] {"ahoj", "cau", "nazdar"});
        intersectionBothWays(new String[] {"cau", "cau", "nazdar", "nazdar"}, new String[] { "ahoj", "cau", "nazdar", "nazdar" }, new String[] {"cau", "cau", "nazdar"});
        intersectionBothWays(new String[] {"ahoj", "zdar"}, new String[] { "ahoj", "cau", "nazdar", "zdar" }, new String[] {"a", "ahoj", "zdar"});
    }

    public void testWordsJaccardDistanceFunction() {
        WordsJaccardDistanceFunction d = new WordsJaccardDistanceFunction();
        assertEquals(0f, d.getDistance(createWordsProvider("ahoj", "cau", "nazdar"), createWordsProvider("ahoj", "cau", "nazdar")));
        assertEquals(0f, d.getDistance(createWordsProvider("ahoj", "ahoj", "cau", "cau", "nazdar"), createWordsProvider("ahoj", "ahoj", "cau", "cau", "nazdar")));
        assertEquals(1f, d.getDistance(createWordsProvider("ahoj", "cau", "nazdar"), createWordsProvider()));
        assertEquals(1f, d.getDistance(createWordsProvider(), createWordsProvider("ahoj", "cau", "nazdar")));
        assertEquals(1f, d.getDistance(createWordsProvider("a", "b", "c"), createWordsProvider("ahoj", "cau", "nazdar")));
        assertEquals(1f, d.getDistance(createWordsProvider("ahoj", "cau", "nazdar"), createWordsProvider("a", "b", "c")));
        assertEquals(1.0f - 2.0f/7.0f, d.getDistance(createWordsProvider("ahoj", "cau", "nazdar", "zdar"), createWordsProvider("a", "ahoj", "zdar")), 0.001);
        assertEquals(1.0f - 3.0f/8.0f, d.getDistance(createWordsProvider("ahoj", "cau", "nazdar", "zdar"), createWordsProvider("a", "ahoj", "ahoj", "zdar")), 0.001);
    }
}
