/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.search;

import messif.distance.DistanceException;
import messif.distance.DistanceFunc;
import messif.distance.multi.DistanceMultiObject;
import messif.data.DataObject;
import messif.data.util.DataObjectList;
import messif.data.util.RankedDataObjectSubdists;
import messif.data.util.RankedDataObject;
import messif.operation.answer.RankedAnswer;
import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Required;
import messif.record.Record;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;


/**
 * A search operation (kNN, range...) that has multiple query objects. It specifies a {@link DistanceMultiObject function}
 *  to calculate a distance from a single data object to all the query objects.
 */
public interface MultiQueryObjectOperation extends RankingOperation {

    String QUERY_OBJECTS_FIELD = "query_records";

    @Get(field = QUERY_OBJECTS_FIELD)
    @Required
    DataObject [] getQueryObjects();

    default int getQueryObjectCount() {
        return getQueryObjects().length;
    }


    String DISTANCE_MULTI_FIELD = "multi_distance_function";

    @Get(field = DISTANCE_MULTI_FIELD)
    @Required
    DistanceMultiObject<DataObject> getMultiDistanceFunction();


    String STORE_INDI_DISTS_FIELD = "store_individual_distances";

    /**
     * Flag whether the distances between the data object and all query objects are
     * stored in {@link RankedDataObjectSubdists sub-distances}
     */
    @Get(field = STORE_INDI_DISTS_FIELD)
    boolean isStoreIndividualDistances(boolean defaultValue);

    default boolean isStoreIndividualDistances() {
        return isStoreIndividualDistances(false);
    };


    // region ****************** Utility method for creating array of objects ******************

    /**
     * Creates an array of {@link Record}s (data objects) from the given iterator.
     * @param iterator the iterator that provides objects
     * @param count the number of objects to retrieve; negative number means unlimited
     * @return a new array of {@link Record}s
     */
    static Record[] loadObjects(Iterator<? extends Record> iterator, int count) {
        ArrayList<Record> data = new ArrayList<>(count > 0 ? count : 0);
        while (iterator.hasNext() && count-- != 0)
            data.add(iterator.next());
        return data.toArray(new Record[data.size()]);
    }


    /**
     * Creates an array of {@link DataObject}s from a given instance.
     * The instance can be:
     * <ul>
     * <li>{@link DataObject} - an array with this single object is returned</li>
     * <li>static array - an array with copied and cast objects is returned</li>
     * <li>{@link Iterator} - an array with all objects from the iterator is returned</li>
     * <li>{@link Collection} - an array with all objects from the collection is returned</li>
     * </ul>
     * @param objects the object, array, iterator, or collection to convert
     * @return an array of objects
     */
    @SuppressWarnings({"unchecked", "SuspiciousToArrayCall"})
    static DataObject[] toObjectArray(Object objects) {
        if (objects == null)
            return null;
        if (objects instanceof Record)
            return new DataObject[] { (DataObject)objects };
        if (objects instanceof DataObject[])
            return (DataObject[])objects;
        if (objects.getClass().isArray()) {
            DataObject[] ret = new DataObject[Array.getLength(objects)];
            for (int i = 0; i < ret.length; i++) {
                ret[i] = (DataObject)Array.get(objects, i);
            }
            return ret;
        }
        if (objects instanceof Iterator)
            objects = new DataObjectList((Iterator)objects);
        Collection<?> col = (Collection<?>)objects;
        return col.toArray(new DataObject[col.size()]);
    }
    // endregion


    /**
     * This class helps the operation processors to process this type of operation by providing supporting methods and
     *  continuous building of the answer.
     */
    class Helper extends RankingOperation.Helper<MultiQueryObjectOperation, RankedAnswer> {

        protected final Collection<DataObject> queryObjects;

        protected DistanceMultiObject<DataObject> multiDistFunction;

        protected final boolean storeIndividualDistances;

        /**
         * Creates this processor given a query operation and a distance to be used if the query does not contain
         * a distance itself.
         *
         * @param operation the query operation
         * @param defaultDistanceFunc a default distance to be used ONLY IN CASE the
         *          {@link RankingOperation#getDistanceFunction() distance parameter} is not used
         */
        public Helper(MultiQueryObjectOperation operation, DistanceFunc<DataObject> defaultDistanceFunc, DistanceMultiObject<DataObject> multiDistFunction) {
            super(operation, defaultDistanceFunc, RankedAnswer.class);
            this.queryObjects = Arrays.asList(operation.getQueryObjects());
            this.storeIndividualDistances = operation.isStoreIndividualDistances();
            this.multiDistFunction = multiDistFunction;
        }

        public Helper(MultiQueryObjectOperation query, DistanceFunc<DataObject> defaultDistanceFunc) {
            this(query, defaultDistanceFunc, query.getMultiDistanceFunction());
        }

        /**
         * Adds an object to the answer. The rank of the object is computed automatically
         * as a distance between the query object and the specified object.
         *
         * @param object the object to add
         * @return the distance-ranked object object that was added to answer or <tt>null</tt> if the object was not added
         */
        @Override
        protected RankedDataObject addToAnswer(DataObject object, float threshold) throws DistanceException {
            return addToAnswer(object, null, threshold);
        }

        /**
         * Adds an object to the answer. The rank of the object is computed automatically
         * as a distance between all the {@link #queryObjects} and the specified object
         * using the specified {@link #multiDistFunction aggretate distance function}. The
         * passed array {@code individualDistances} will be filled with the distances
         * of the individual query objects.
         *
         * @param object the object to add
         * @param individualDistances the array to fill with the distances to the respective query objects;
         *          if not <tt>null</tt>, it must have the same number of allocated elements as the number of query objects
         * @param distThreshold the threshold on distance;
         *      if the computed distance exceeds the threshold (sharply),
         *      the object is not added to the answer
         * @return the distance-ranked object object that was added to answer or <tt>null</tt> if the object was not added
         * @throws IndexOutOfBoundsException if the passed {@code individualDistances} array is not big enough
         */
        protected RankedDataObject addToAnswer(DataObject object, float[] individualDistances, float distThreshold) throws IndexOutOfBoundsException, DistanceException {
            if (object == null)
                return null;
            if (individualDistances == null)
                individualDistances = new float[queryObjects.size()];
            // TODO: solve
            float distance = multiDistFunction.getDistanceMultiObject(distanceFunc, object, queryObjects, individualDistances);

            if (distance > distThreshold)
                return null;

            return answerBuilder.add(object, distance, storeIndividualDistances ? individualDistances : null);
        }
    }
}
