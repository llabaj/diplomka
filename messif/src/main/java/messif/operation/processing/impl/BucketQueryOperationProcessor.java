/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.processing.impl;

import messif.algorithm.AlgorithmMethodException;
import messif.bucket.Bucket;
import messif.operation.search.QueryObjectOperation;
import messif.operation.search.RankingOperation;
import messif.operation.answer.RankedAnswer;

import java.util.Collection;

/**
 * Implementation of {@link OneStepProcessor} that processes any {@link QueryObjectOperation}
 * on a set of {@link Bucket}s.
 * The buckets where the operation should be processed is provided via constructor.
 * <p>
 * <h4>Parallelism</h4>
 * The implementation is thread-safe, if operation cloning is enabled. Otherwise,
 * the operation answer needs to be synchronized.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class BucketQueryOperationProcessor
        extends QueueOperationProcessor<RankingOperation, RankedAnswer, Bucket> {

    protected final RankingOperation.Helper<? extends RankingOperation, ? extends RankedAnswer> actualOperationProcessor;

    /**
     * Create a new bucket navigation processor.
     * The processor is {@link #isQueueClosed() closed} and contains only the specified buckets.
     * No additional buckets can be added.
     *
     * @param actualOperationProcessor the operation processor to be executed on each bucket
     * @param buckets                  the buckets on which to process
     */
    public BucketQueryOperationProcessor(RankingOperation.Helper<? extends RankingOperation, ? extends RankedAnswer> actualOperationProcessor, Collection<? extends Bucket> buckets) {
        super(actualOperationProcessor.getOperation(), buckets);
        this.actualOperationProcessor = actualOperationProcessor;
    }

    /**
     * Create a new bucket navigation processor.
     * The processor does not contain any buckets and the processing
     * will block in method {@link #processStep()}. Additional buckets must be
     * added via {@link #addProcessingItem} methods and then {@link #queueClose() closed}
     * in order to be able to finish the processing.
     *
     * @param actualOperationProcessor the operation processor to be executed on each bucket
     */
    public BucketQueryOperationProcessor(RankingOperation.Helper<? extends RankingOperation, ? extends RankedAnswer> actualOperationProcessor) {
        super(actualOperationProcessor.getOperation());
        this.actualOperationProcessor = actualOperationProcessor;
    }


    @Override
    protected void processItem(Bucket bucket) throws AlgorithmMethodException {
        bucket.processQuery(actualOperationProcessor);
    }

    @Override
    public RankedAnswer finish() {
        return actualOperationProcessor.getAnswer();
    }
}
