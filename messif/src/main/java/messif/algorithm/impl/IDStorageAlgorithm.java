/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.algorithm.impl;

import messif.algorithm.Algorithm;
import messif.bucket.BucketStorageException;
import messif.bucket.index.LocalAbstractObjectOrder;
import messif.data.DataObject;
import messif.operation.answer.AbstractAnswer;
import messif.operation.OperationBuilder;
import messif.operation.ReturnDataOperation;
import messif.operation.crud.DeleteOperation;
import messif.operation.crud.GetObjectsOperation;
import messif.operation.crud.InsertOperation;
import messif.operation.crud.answer.CRUDAnswer;
import messif.operation.crud.answer.DeleteAnswer;
import messif.operation.crud.answer.GetObjectsAnswer;
import messif.operation.crud.answer.InsertAnswer;
import messif.operation.info.ObjectCountOperation;
import messif.operation.info.answer.ObjectCountAnswer;
import messif.operation.processing.ProcessorConfiguration;
import messif.operation.processing.impl.OneStepProcessor;
import messif.operation.search.*;
import messif.operation.answer.RankedAnswer;
import messif.serialization.ObjectSerializator;
import messif.storage.StorageIndexed;
import messif.storage.StorageSearch;
import messif.storage.impl.DatabaseStorage;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Wrapper for any {@link Algorithm} that stores all the inserted objects into additional
 * storage. The insert/delete operations are intercepted by the wrapper algorithm and
 * used to update the local storage. The modification operations are then passed the encapsulated
 * algorithm for processing.
 * <p>
 * The {@link messif.operation.crud.GetObjectsOperation} is handled
 * by the wrapper by accessing the indexed storage. All the other query operations
 * are handled by the encapsulated algorithm.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class IDStorageAlgorithm extends Algorithm {
    /**
     * Class serial id for serialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Encapsulated algorithm that handles regular queries
     */
    protected final Algorithm algorithm;
    /**
     * Internal indexed storage for handling locator-related queries
     */
    protected final StorageIndexed<DataObject> storage;

    /**
     * Fields to be used for each object inserted into the algorithm (all others are removed). Default: String [] {"*"}
     * which means keep all fields {@link ReturnDataOperation#transformObject(DataObject, String[])}.
     */
    protected final String[] algorithmDataFields;

    // region **************************         Constructors        ************************************* //

    /**
     * Creates a new locator-storage algorithm wrapper for the given algorithm.
     * Note that the locator-storage stores the objects as-is while the
     * algorithm can index only one of the encapsulated objects if the
     * {@code metaobjectName} parameter is set.
     *
     * @param encapsulatedAlgorithm the algorithm to wrap
     * @param storage               the storage used for indexed retrieval of the objects
     */
    @AlgorithmConstructor(description = "creates locator-storage wrapper for the given algorithm", arguments = {"algorithm to encapsulate", "metaobject sub-object name to index", "internal locator-based indexed storage"})
    public IDStorageAlgorithm(Algorithm encapsulatedAlgorithm, StorageIndexed<DataObject> storage) {
        this(encapsulatedAlgorithm, storage, new String[] {"*"});
    }

    /**
     * Creates a new locator-storage algorithm wrapper for the given algorithm.
     * Note that the locator-storage stores the objects as-is while the
     * algorithm can index only one of the encapsulated objects if the
     * {@code metaobjectName} parameter is set.
     *
     * @param encapsulatedAlgorithm the algorithm to wrap
     * @param storage               the storage used for indexed retrieval of the objects
     * @param algorithmDataFields
     */
    @AlgorithmConstructor(description = "creates locator-storage wrapper for the given algorithm", arguments = {"algorithm to encapsulate", "metaobject sub-object name to index", "internal locator-based indexed storage"})
    public IDStorageAlgorithm(Algorithm encapsulatedAlgorithm, StorageIndexed<DataObject> storage, String[] algorithmDataFields) {
        super("LocatorStorage-" + encapsulatedAlgorithm.getName());
        this.algorithm = encapsulatedAlgorithm;
        this.storage = storage;
        this.algorithmDataFields = algorithmDataFields;
    }

    /**
     * Creates a new locator-db-storage algorithm wrapper for the given algorithm.
     * The database for the given connection URL must contain a table with auto-generated
     * identifier "id", the string "locator" column with index (so that the locator-based
     * queries are fast), and the "binobj" LOB column for binary serialization of the descriptor.
     * <p>
     * <p>
     * Note that the locator-storage stores the objects as-is while the
     * algorithm can index only one of the encapsulated objects if the
     * {@code metaobjectName} parameter is set.
     * </p>
     *
     * @param encapsulatedAlgorithm the algorithm to wrap
     * @param dbConnUrl             the database connection URL (JDBC)
     * @param tableName             the name of the table in which to store the data
     * @param cacheClasses          the classes for the binary serialization - THIS PARAMETER IS NOT USED IN MESSIF >= 3.0
     * @throws IllegalArgumentException if the column names and column convertors do not match
     * @throws SQLException             if there was an error connecting to the database
     */
    @AlgorithmConstructor(description = "creates locator-storage wrapper for the given algorithm", arguments = {"algorithm to encapsulate", "db connection URL", "db table name", "serialization cache classes"})
    public IDStorageAlgorithm(Algorithm encapsulatedAlgorithm, String dbConnUrl, String tableName, Class<?>[] cacheClasses) throws IllegalArgumentException, SQLException {
        this(encapsulatedAlgorithm, dbConnUrl, tableName, cacheClasses, new String[0]);
    }

    /**
     * Creates a new locator-db-storage algorithm wrapper for the given algorithm.
     * The database for the given connection URL must contain a table with auto-generated
     * identifier "id", the string "locator" column with index (so that the locator-based
     * queries are fast), and the "binobj" LOB column for binary serialization of the descriptor.
     * <p>
     * <p>
     * Note that the locator-storage stores the objects as-is while the
     * algorithm can index only one of the encapsulated objects if the
     * {@code metaobjectName} parameter is set.
     * </p>
     *
     * @param encapsulatedAlgorithm the algorithm to wrap
     * @param dbConnUrl             the database connection URL (JDBC)
     * @param tableName             the name of the table in which to store the data
     * @param cacheClasses          the classes for the binary serialization - THIS PARAMETER IS NOT USED IN MESSIF >= 3.0
     * @param algorithmDataFields
     * @throws IllegalArgumentException if the column names and column convertors do not match
     * @throws SQLException             if there was an error connecting to the database
     */
    @AlgorithmConstructor(description = "creates locator-storage wrapper for the given algorithm", arguments = {"algorithm to encapsulate", "db connection URL", "db table name", "serialization cache classes"})
    public IDStorageAlgorithm(Algorithm encapsulatedAlgorithm, String dbConnUrl, String tableName, Class<?>[] cacheClasses, String[] algorithmDataFields) throws IllegalArgumentException, SQLException {
        this(encapsulatedAlgorithm, new DatabaseStorage<>(DataObject.class, dbConnUrl, null, null, tableName, "id", getDatabaseMap(cacheClasses)), algorithmDataFields);
    }
    // endregion

    /**
     * Creates column convertor mappings for the database storage.
     *
     * @param cacheClasses the classes for the binary serialization - THIS PARAMETER IS NOT USED IN MESSIF >= 3.0
     * @return column convertor mappings for the database storage
     */
    private static Map<String, DatabaseStorage.ColumnConvertor<DataObject>> getDatabaseMap(Class<?>[] cacheClasses) {
        Map<String, DatabaseStorage.ColumnConvertor<DataObject>> ret = new HashMap<>();
        ret.put(DataObject.ID_FIELD, DatabaseStorage.getIDColumnConvertor(true, false, true));
        ret.put("binobj", new DatabaseStorage.BinarySerializableColumnConvertor<>(DataObject.class, new ObjectSerializator()));
        return ret;
    }

    @Override
    public synchronized boolean init() {
        algorithm.init();
        return super.init();
    }

    @Override
    @SuppressWarnings({"FinalizeNotProtected", "FinalizeCalledExplicitly"})
    public void finalize() throws Throwable {
        algorithm.finalize();
        storage.finalize();
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void destroy() throws Throwable {
        algorithm.destroy();
        storage.destroy();
        super.destroy(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Returns the encapsulated algorithm.
     *
     * @return the encapsulated algorithm
     */
    public Algorithm getAlgorithm() {
        return algorithm;
    }

    @Override
    protected void registerProcessors() {
        super.registerProcessors();

        this.handler.register(ProcessorConfiguration.create(ObjectCountOperation.class, ObjectCountAnswer.class)
                .forTypeStrings(ObjectCountOperation.TYPE_STRING)
                .withProcessor(ObjectCountProcessor::new)
                .executeSequential().build());

        this.handler.register(ProcessorConfiguration.create(InsertOperation.class, InsertAnswer.class)
                .forTypeStrings(InsertOperation.TYPE_STRING)
                .withProcessor(InsertObjectsProcessor::new)
                .executeSequential().build());

        this.handler.register(ProcessorConfiguration.create(DeleteOperation.class, DeleteAnswer.class)
                .forTypeStrings(DeleteOperation.TYPE_STRING)
                .withProcessor(DeleteObjectsProcessor::new)
                .executeSequential().build());

        this.handler.register(ProcessorConfiguration.create(GetObjectsOperation.class, GetObjectsAnswer.class)
                .forTypeStrings(GetObjectsOperation.TYPE_STRING)
                .withProcessor(GetObjectProcessor::new)
                .executeSequential().build());

        // all distance-based single-query search operations
        this.handler.register(ProcessorConfiguration.create(RankingOperation.class, RankedAnswer.class)
                .forTypeStrings(KNNOperation.TYPE_STRING, RangeOperation.TYPE_STRING, KNNRangeOperation.TYPE_STRING, MultiKNNOperation.TYPE_STRING)
                .withProcessor(QueryObjectProcessor::new)
                .executeSequential().build());
    }

    /**
     * Simple one-step processor to return the number of objects in this algorithm.
     */
    protected class ObjectCountProcessor extends OneStepProcessor<ObjectCountOperation, ObjectCountAnswer> {

        final ObjectCountOperation.Helper base;

        ObjectCountProcessor(ObjectCountOperation operation) {
            super(operation);
            base = new ObjectCountOperation.Helper();
        }

        @Override
        public ObjectCountAnswer finish() {
            return base.getAnswer();
        }

        @Override
        protected void process() throws Exception {
            base.addToAnswer(storage.size());
        }
    }

    /**
     * Processor of the insert operation.
     */
    protected class InsertObjectsProcessor extends OneStepProcessor<InsertOperation, InsertAnswer> {

        final InsertOperation.Helper helper;

        public InsertObjectsProcessor(InsertOperation operation) {
            super(operation);
            this.helper = new InsertOperation.Helper(operation);
        }

        @Override
        protected void process() throws Exception {
            // Store the objects into the storage first
            for (DataObject object : operation.getObjects()) {
                try {
                    storage.store(object);
                    helper.answerBuilder.add(ReturnDataOperation.transformObject(object, IDStorageAlgorithm.this.algorithmDataFields),
                            InsertAnswer.OBJECT_INSERTED);
                } catch (BucketStorageException e) {
                    helper.answerBuilder.add(object, InsertAnswer.OBJECT_DUPLICATE);
                }
            }

            // Execute the bulk insert operation
            if (helper.getNumberProcessedObjects() > 0) {
                final InsertOperation insertOperation =
                        OperationBuilder.create(InsertOperation.class).addParam(InsertOperation.OBJECTS_FIELD, helper.answerBuilder.getProcessedObjsArray()).build();
                helper.errorCodeHelper.updateErrorCode(algorithm.evaluate(insertOperation).getStatus());
            }
        }

        @Override
        public InsertAnswer finish() {
            return helper.getAnswer();
        }
    }

    /**
     * Processor of the delete operation.
     */
    protected class DeleteObjectsProcessor extends OneStepProcessor<DeleteOperation, DeleteAnswer> {

        final DeleteOperation.Helper helper;

        public DeleteObjectsProcessor(DeleteOperation operation) {
            super(operation);
            helper = new DeleteOperation.Helper(operation);
        }

        @Override
        protected void process() throws Exception {
            // delete objects from the index
            helper.errorCodeHelper.updateErrorCode(algorithm.evaluate(operation).getStatus());

            // and delete also objects from the storage (independently of the algorithm)
            List<String> ids = new ArrayList<>(operation.getObjects().length);
            for (DataObject object : operation.getObjects()) {
                ids.add(object.getID());
            }
            StorageSearch<DataObject> search = storage.search(LocalAbstractObjectOrder.idToObjectComparator, ids);
            if (search.next()) {
                helper.answerBuilder.add(search.getCurrentObject(), DeleteAnswer.OBJECT_DELETED);
                search.remove();
            }
        }

        @Override
        public DeleteAnswer finish() {
            return helper.getAnswer();
        }
    }

    /**
     * Processor of the get object (by ID or data) operation.
     */
    protected class GetObjectProcessor extends OneStepProcessor<GetObjectsOperation, GetObjectsAnswer> {

        final GetObjectsOperation.Helper helper;

        public GetObjectProcessor(GetObjectsOperation operation) {
            super(operation);
            helper = new GetObjectsOperation.Helper(operation);
        }

        @Override
        protected void process() throws Exception {
            List<String> ids = new ArrayList<>(operation.getObjects().length);
            for (DataObject object : operation.getObjects()) {
                ids.add(object.getID());
            }
            StorageSearch<DataObject> search = storage.search(LocalAbstractObjectOrder.idToObjectComparator, ids);
            while (search.next()) {
                helper.answerBuilder.add(search.getCurrentObject(), CRUDAnswer.OBJECT_FOUND);
            }
            if (operation.getObjectCount() > helper.getNumberProcessedObjects()) {
                helper.errorCodeHelper.updateErrorCode(CRUDAnswer.OBJECT_NOT_FOUND);
            }
        }

        @Override
        public GetObjectsAnswer finish() {
            return helper.getAnswer();
        }
    }

    protected class QueryObjectProcessor extends OneStepProcessor<RankingOperation, RankedAnswer> {

        RankedAnswer answer = null;

        public QueryObjectProcessor(RankingOperation query) {
            super(query);
        }

        @Override
        public RankedAnswer finish() {
            if (answer == null) {
                return AbstractAnswer.createAnswer(AbstractAnswer.ABORTED_BY_ERROR, RankedAnswer.class);
            }
            return answer;
        }

        @Override
        protected void process() throws Exception {
            answer = (RankedAnswer) algorithm.evaluate(operation);
        }
    }

//    /**
//     * Implementation of the get-random-objects operation
//     * The objects are retrieved using the internal storage.
//     * @param op the get-random-objects operation to execute
//     */
//    public void getRandomObjects(GetRandomObjectsQueryOperation op) {
//        op.evaluate(new SearchDataObjectIterator<>(storage));
//        op.endOperation();
//    }

    @Override
    public String toString() {
        return "ID-object wrapper " + storage + " over this algorithm: " + algorithm.toString();
    }
}
