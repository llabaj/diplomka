/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.algorithm.impl;

import messif.algorithm.Algorithm;
import messif.bucket.BucketDispatcher;
import messif.bucket.CapacityFullException;
import messif.bucket.LocalBucket;
import messif.bucket.impl.MemoryStorageBucket;
import messif.data.DataObject;
import messif.distance.DistanceFunc;
import messif.operation.crud.*;
import messif.operation.info.ObjectCountOperation;
import messif.operation.info.answer.ObjectCountAnswer;
import messif.operation.processing.ProcessorConfiguration;
import messif.operation.processing.impl.OneStepProcessor;
import messif.operation.search.KNNOperation;
import messif.operation.search.KNNRangeOperation;
import messif.operation.search.QueryObjectOperation;
import messif.operation.search.RangeOperation;
import messif.operation.answer.ListingAnswer;
import messif.operation.answer.RankedAnswer;

import java.util.Map;

/**
 * Implementation of the naive sequential scan algorithm.
 * <p>
 * It uses one bucket to store objects and performs operations on the bucket.
 * It also supports pivot-based filtering. The pivots can be specified in a constructor.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class SequentialScan extends Algorithm {
    /**
     * class id for serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * One instance of bucket where all objects are stored
     */
    protected final LocalBucket bucket;

    /**
     * Default distance function to be used for the distance-based queries.
     */
    protected DistanceFunc<DataObject> defaultDistanceFunc;

    // region  ***************************     Constructors     *************************************

    /**
     * Creates a new instance of SequantialScan access structure with the given bucket.
     *
     * @param defaultDist default distance function to be used for evaluation of distance-based search queries
     * @param bucket      the bucket used for the sequential scan
     */
    public SequentialScan(DistanceFunc<DataObject> defaultDist, LocalBucket bucket) {
        super("sequential scan");
        this.defaultDistanceFunc = defaultDist;
        // Create an empty bucket (using the provided bucket class and parameters)
        this.bucket = bucket;
    }

    /**
     * Creates a new instance of SequantialScan access structure with specific bucket class and filtering pivots.
     * Additional parameters for the bucket class constructor can be passed.
     *
     * @param defaultDist       default distance function to be used for evaluation of distance-based search queries
     * @param bucketClass       the class of the storage bucket
     * @param bucketClassParams additional parameters for the bucket class constructor in the name->value form
     * @throws CapacityFullException  if the maximal number of buckets is already allocated
     * @throws InstantiationException if <ul><li>the provided storageClass is not a part of LocalBucket hierarchy</li>
     *                                <li>the storageClass does not have a proper constructor (String,long,long)</li>
     *                                <li>the correct constructor of storageClass is not accessible</li>
     *                                <li>the constructor of storageClass has failed</li></ul>
     */
    @AlgorithmConstructor(description = "SequentialScan Access Structure", arguments = {"bucket class", "bucket class params", "pivots", "pivot count", "pivotDistsValidIfGiven"})
    public SequentialScan(DistanceFunc<DataObject> defaultDist, Class<? extends LocalBucket> bucketClass, Map<String, Object> bucketClassParams) throws CapacityFullException, InstantiationException {
        this(defaultDist, BucketDispatcher.createBucket(bucketClass, Long.MAX_VALUE, Long.MAX_VALUE, 0, true, bucketClassParams));
    }

    /**
     * Creates a new instance of SequantialScan access structure with specific bucket class.
     *
     * @param defaultDist default distance function to be used for evaluation of distance-based search queries
     * @param bucketClass The class of the storage bucket
     * @throws CapacityFullException  if the maximal number of buckets is already allocated
     * @throws InstantiationException if <ul><li>the provided storageClass is not a part of LocalBucket hierarchy</li>
     *                                <li>the storageClass does not have a proper constructor (String,long,long)</li>
     *                                <li>the correct constructor of storageClass is not accessible</li>
     *                                <li>the constructor of storageClass has failed</li></ul>
     */
    @AlgorithmConstructor(description = "SequentialScan Access Structure", arguments = {"bucket class"})
    public SequentialScan(DistanceFunc<DataObject> defaultDist, Class<? extends LocalBucket> bucketClass) throws CapacityFullException, InstantiationException {
        this(defaultDist, bucketClass, null);
    }

    /**
     * Creates a new instance of SequantialScan access structure with the default MemoryStorageBucket class.
     *
     * @param defaultDist default distance function to be used for evaluation of distance-based search queries
     * @throws CapacityFullException  if the maximal number of buckets is already allocated
     * @throws InstantiationException if <ul><li>the provided storageClass is not a part of LocalBucket hierarchy</li>
     *                                <li>the storageClass does not have a proper constructor (String,long,long)</li>
     *                                <li>the correct constructor of storageClass is not accessible</li>
     *                                <li>the constructor of storageClass has failed</li></ul>
     */
    @AlgorithmConstructor(description = "SequentialScan Access Structure", arguments = {})
    public SequentialScan(DistanceFunc<DataObject> defaultDist) throws CapacityFullException, InstantiationException {
        this(defaultDist, MemoryStorageBucket.class);
    }

    /**
     * Creates a new instance of SequantialScan access structure with the default MemoryStorageBucket class.
     *
     * @throws CapacityFullException  if the maximal number of buckets is already allocated
     * @throws InstantiationException if <ul><li>the provided storageClass is not a part of LocalBucket hierarchy</li>
     *                                <li>the storageClass does not have a proper constructor (String,long,long)</li>
     *                                <li>the correct constructor of storageClass is not accessible</li>
     *                                <li>the constructor of storageClass has failed</li></ul>
     */
    @AlgorithmConstructor(description = "SequentialScan Access Structure", arguments = {})
    public SequentialScan() throws CapacityFullException, InstantiationException {
        this(null);
    }
    // endregion

    @Override
    public void finalize() throws Throwable {
        bucket.finalize();
        super.finalize();
    }

    @Override
    public void destroy() throws Throwable {
        bucket.destroy();
        // Do not execute super.destroy(), since algorithm needs to differentiate between finalizing and destroying
    }

    @Override
    protected void registerProcessors() {
        super.registerProcessors();

        this.handler.register(ProcessorConfiguration.create(ObjectCountOperation.class, ObjectCountAnswer.class)
                .forTypeStrings(ObjectCountOperation.TYPE_STRING)
                .withProcessor((operation1) -> new ObjectCountProcessor(operation1))
                .executeSequential().build());

        this.handler.register(ProcessorConfiguration.create(InsertOperation.class, ListingAnswer.class)
                .forTypeStrings(InsertOperation.TYPE_STRING)
                .withProcessor(operation -> new CRUDProcessor(operation, new InsertOperation.Helper(operation)))
                .executeSequential().build());

        this.handler.register(ProcessorConfiguration.create(DeleteOperation.class, ListingAnswer.class)
                .forTypeStrings(DeleteOperation.TYPE_STRING)
                .withProcessor(operation -> new CRUDProcessor(operation, new DeleteOperation.Helper(operation)))
                .executeSequential().build());

        this.handler.register(ProcessorConfiguration.create(UpdateOperation.class, ListingAnswer.class)
                .forTypeStrings(UpdateOperation.TYPE_STRING)
                .withProcessor(operation -> new CRUDProcessor(operation, new UpdateOperation.Helper(operation)))
                .executeSequential().build());

        this.handler.register(ProcessorConfiguration.create(GetObjectsOperation.class, ListingAnswer.class)
                .forTypeStrings(GetObjectsOperation.TYPE_STRING)
                .withProcessor(operation -> new CRUDProcessor(operation, new GetObjectsOperation.Helper(operation)))
                .executeSequential().build());

        // all distance-based single-query search operations
        this.handler.register(ProcessorConfiguration.create(QueryObjectOperation.class, RankedAnswer.class)
                .forTypeStrings(KNNOperation.TYPE_STRING, RangeOperation.TYPE_STRING, KNNRangeOperation.TYPE_STRING)
                .withProcessor(QueryObjectProcessor::new)
                .executeSequential().build());
    }

    /**
     * Simple one-step processor to return the number of objects in this algorithm.
     */
    private class ObjectCountProcessor extends OneStepProcessor<ObjectCountOperation, ObjectCountAnswer> {

        final ObjectCountOperation.Helper helper;

        ObjectCountProcessor(ObjectCountOperation operation) {
            super(operation);
            helper = new ObjectCountOperation.Helper();
        }

        @Override
        public ObjectCountAnswer finish() {
            return helper.getAnswer();
        }

        @Override
        protected void process() throws Exception {
            helper.evaluate(bucket);
        }
    }

    /**
     * Processor of the insert/update/delete/get operation.
     */
    protected class CRUDProcessor extends OneStepProcessor<CRUDOperation, ListingAnswer> {

        final CRUDOperation.Helper helper;

        public CRUDProcessor(CRUDOperation operation, CRUDOperation.Helper helper) {
            super(operation);
            this.helper = helper;
        }

        @Override
        public ListingAnswer finish() {
            return helper.getAnswer();
        }

        @Override
        protected void process() throws Exception {
            helper.processObjects(bucket);
        }
    }

    protected class QueryObjectProcessor extends OneStepProcessor<QueryObjectOperation, RankedAnswer> {

        final QueryObjectOperation.Helper helper;

        public QueryObjectProcessor(QueryObjectOperation query) {
            super(query);
            helper = new QueryObjectOperation.Helper(query, defaultDistanceFunc);
        }

        @Override
        public RankedAnswer finish() {
            return helper.getAnswer();
        }

        @Override
        protected void process() throws Exception {
            helper.evaluate(bucket.getAllObjects());
        }
    }


    /**
     * Converts the object to a string representation
     *
     * @return String representation of this algorithm
     */
    @Override
    public String toString() {
        String lineSeparator = System.getProperty("line.separator", "\n");

        return "Algorithm: " + getName() + lineSeparator +
                "Default distance function: " + defaultDistanceFunc + lineSeparator +
                "Bucket Class: " + bucket.getClass().getName() + lineSeparator +
                "Bucket Occupation: " + bucket.getOccupation() + " bytes" + lineSeparator +
                "Bucket Occupation: " + bucket.getObjectCount() + " objects" + lineSeparator;
    }

}
