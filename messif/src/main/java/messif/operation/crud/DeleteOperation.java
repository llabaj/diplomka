/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.crud;

import messif.bucket.BucketStorageException;
import messif.bucket.LocalBucket;
import messif.data.DataObject;
import messif.operation.OperationBuilder;
import messif.operation.crud.answer.CRUDAnswer;
import messif.operation.crud.answer.DeleteAnswer;
import messif.utility.ProcessingStatus;

import java.util.Collection;
import java.util.List;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface DeleteOperation extends ModifyingOperation {

    String TYPE_STRING = "delete";

    @Override
    default String getInnerType() {
        return TYPE_STRING;
    }

    static DeleteOperation create(DataObject[] objects) {
        return OperationBuilder.create(DeleteOperation.class).addParam(CRUDOperation.OBJECTS_FIELD, objects).build();
    }

    static DeleteOperation create(DataObject object) {
        return OperationBuilder.create(DeleteOperation.class).addParam(CRUDOperation.OBJECTS_FIELD, new DataObject [] {object}).build();
    }

    /**
     * Created by Telcontar on 28. 9. 2016.
     */
    class Helper extends CRUDOperation.Helper<DeleteOperation, DeleteAnswer> {

        /**
         * Creates this helper given the operation to be processed.
         *
         * @param operation operation to be executed
         */
        public Helper(DeleteOperation operation) {
            super(operation, DeleteAnswer.class, CRUDAnswer.SOME_OBJECT_NOT_FOUND,
                    new ProcessingStatus[] {DeleteAnswer.OBJECT_DELETED},
                    new ProcessingStatus[] {CRUDAnswer.OBJECT_NOT_FOUND, CRUDAnswer.SOME_OBJECT_NOT_FOUND});
        }


        /******************  Helper methods to be used by other processors   ******************/

        /**
         * Delete given list of objects from given bucket, monitor the actually deleted objects and
         * update the error code correspondingly.
         *
         * @param bucket  the bucket to process the objects on
         * @param objects a list of objects to be deleted from the bucket
         * @return list of ORIGINAL objects actually deleted in the same order as in parameter objects
         */
        public List<DataObject> deleteObjects(LocalBucket bucket, DataObject[] objects) {
            return super.processObjects(bucket, objects);
        }

        /**
         * Delete all objects specified in this operation from given bucket, monitor the actually deleted objects and
         * update the error code correspondingly.
         *
         * @param bucket the bucket to process the objects on
         * @return list of ORIGINAL objects actually deleted in the same order as in parameter objects
         */
        public List<DataObject> deleteObjects(LocalBucket bucket) {
            return deleteObjects(bucket, getOperation().getObjects());
        }

        @Override
        protected ProcessResult processObject(LocalBucket bucket, DataObject dataObject) {
            try {
                // if the object contains only _id then delete the object by this ID, otherwise compare all data fields
                final Collection<DataObject> deletedObjects = (dataObject.getFieldCount() > 1) ?
                        bucket.deleteObject(dataObject, 1) : bucket.deleteObject(dataObject.getID(), 1);
                if (deletedObjects.isEmpty()) {
                    return new ProcessResult(dataObject, CRUDAnswer.OBJECT_NOT_FOUND);
                }
                return new ProcessResult(deletedObjects.iterator().next(), DeleteAnswer.OBJECT_DELETED);
            } catch (BucketStorageException e) {
                return new ProcessResult(dataObject, e.getErrorCode());
            }
        }
    }
}
