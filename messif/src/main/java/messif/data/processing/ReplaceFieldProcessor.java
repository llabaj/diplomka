/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.processing;

/**
 * A data object processor that replaces one field with another value.
 */
abstract class ReplaceFieldProcessor extends AddFieldProcessor {

    private final String [] fieldToReplace;

    /**
     * Create a new processor given the field to be replaced.
     */
    protected ReplaceFieldProcessor(String fieldToReplace) {
        super(fieldToReplace, fieldToReplace, false);
        this.fieldToReplace = new String [] {fieldToReplace};
    }

    @Override
    public String[] modifiedFields() {
        return fieldToReplace;
    }
}
