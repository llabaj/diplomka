/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.serialization;

import messif.serialization.io.BinaryInput;
import messif.serialization.io.BinaryOutput;
import java.io.IOException;

/**
 * The <code>BinarySerializable</code> interface marks the implementing
 class to be able to writeBinary itself into a stream of bytes provided
 by the {@link ObjectSerializator}.
 * 
 * <p>
 * The class should be able to reconstruct itself from these data by
 * providing either a constructor or a factory method.
 * The factory method should have the following prototype:
 * <pre>
 *      <i>ObjectClass</i> binaryDeserialize({@link BinaryInput} input, {@link ObjectSerializator} serializator) throws {@link IOException}
 * </pre>
 * The constructor should have the following prototype:
 * <pre>
 *      <i>ClassConstructor</i>({@link BinaryInput} input, {@link ObjectSerializator} serializator) throws {@link IOException}
 * </pre>
 * The access specificator of the construtor or the factory method is not
 * important and can be even <tt>private</tt>.
 * </p>
 * 
 * @param <T> Class which can be (de)serialized by the class implementing this interface.
 * @see ObjectSerializator
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface ClassReaderWriter<T> {

    /**
     * Binary-writeBinary given object into the <code>output</code>.
     * @param object object to writeBinary
     * @param output the binary output that this object is serialized into
     * @param serializator the serializator used to write objects
     * @return the number of bytes written
     * @throws IOException if there was an I/O error during serialization
     * @throws BinarySerializationException if the serialization meets an object that it cannot write
     */
    int writeBinary(T object, BinaryOutput output, ObjectSerializator serializator) throws IOException, BinarySerializationException;

    /**
     * Binary-writeBinary this object into the <code>output</code>.
     * @param input the binary input from which to read an object
     * @param serializator the serializator used to write objects
     * @return the deserialized object
     * @throws IOException if there was an I/O error during deserialization
     * @throws BinarySerializationException if the serialization meets an object that it cannot read
     */
    T readBinary(BinaryInput input, ObjectSerializator serializator) throws IOException, IllegalArgumentException, BinarySerializationException;
    
    Class<? extends T> getReadWriteClass();
    
}
