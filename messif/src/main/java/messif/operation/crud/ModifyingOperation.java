/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.crud;

import messif.data.DataObject;

/**
 * A common predecessor for all operations that MODIFY the data in the index (insert/delete/update).
 */
public interface ModifyingOperation extends CRUDOperation {

    /** The default fields returned for the {@link messif.operation.crud.ModifyingOperation}s is just {@link messif.data.DataObject#ID_FIELD} */
    String [] DEFAULT_LIST = new String [] {DataObject.ID_FIELD};

    @Override
    default String[] fieldsToReturn() {
        return fieldsToReturn(DEFAULT_LIST);
    }

}
