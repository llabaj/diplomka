/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.info.answer;

import messif.operation.answer.AbstractAnswer;
import messif.operation.info.PrintAllObjectsOperation;
import messif.operation.answer.ListingAnswer;
import messif.record.ModifiableRecord;
import messif.utility.ProcessingStatus;
import messif.utility.json.JSONException;
import messif.utility.json.JSONWritable;
import messif.utility.json.JSONWriter;
import messif.utility.proxy.ProxyConverter;
import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Required;

import java.io.*;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface PrintAllObjectsAnswer extends ListingAnswer {

    @Get(field = PrintAllObjectsOperation.OUTFILE_PARAM)
    @Required
    String getOutputFile();


    /**
     * The JSON representation of the operation is implemented by printing all the fields and, additionally,
     *   print content of the output file as a
     */
    @Override
    default JSONWriter writeJSON(JSONWriter writer) throws JSONException, IOException {
        return getParams().writeJSON(writer);
    }

    /**
     * A special class that writes content of the all-objects file into the JSON-like output
     */
    class FileContent implements JSONWritable {

        protected final File file;
        private final boolean deleteFileAfter;

        public FileContent(String fileName, boolean deleteFileAfter) {
            this.file = new File(fileName);
            this.deleteFileAfter = deleteFileAfter;
        }

        @Override
        public JSONWriter writeJSON(JSONWriter writer) throws JSONException, IOException {
            if (writer.getMaxDepth() != Integer.MAX_VALUE) {
                writer.write((Object) "records...");
                return writer;
            }
            writer.write('[');
            if (file.exists()) {
                char[] buffer = new char[4 * 4096];
                try (Reader reader = new BufferedReader(new FileReader(file))) {
                    while (reader.ready()) {
                        int length = reader.read(buffer);
                        writer.write(buffer, 0, length);
                    }
                }
                if (deleteFileAfter) {
                    file.delete();
                }
            }
            writer.write(']');
            return writer;
        }
    }


    static PrintAllObjectsAnswer createFailed(ProcessingStatus status) {
        return ObjectCountAnswer.createFailed(status, PrintAllObjectsAnswer.class);
    }

    static PrintAllObjectsAnswer createSuccessful(long objectCount, String outputFileName, boolean objectsInAnswer, boolean deleteFileAfter) {
        ModifiableRecord fieldMap = AbstractAnswer.createMap(AbstractAnswer.OPERATION_OK);
        fieldMap.setField(ObjectCountAnswer.ANSWER_OBJ_COUNT_FIELD, objectCount);
        fieldMap.setField(PrintAllObjectsOperation.OUTFILE_PARAM, outputFileName);
        if (objectsInAnswer) {
            fieldMap.setField(ListingAnswer.ANSWER_OBJECTS_FIELD, new FileContent(outputFileName, deleteFileAfter));
        }
        return ProxyConverter.convert(fieldMap, PrintAllObjectsAnswer.class);
    }

}
