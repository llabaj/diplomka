/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.search;

import messif.distance.DistanceFunc;
import messif.data.DataObject;
import messif.data.util.RankedDataObject;
import messif.operation.params.KNN;
import java.util.Arrays;

/**
 * Standard distance-based kNN query.
 */
public interface KNNOperation extends QueryObjectOperation, KNN {

    String TYPE_STRING = "knn_query";

    @Override
    default String getInnerType() {
        return TYPE_STRING;
    }

    /**
     * The {@link KNNOperation} does not have a standard helper, please use {@link QueryObjectOperation.Helper} instead.
     * This class is a special helper that keeps information about some threshold distances...
     */
    class HelperWithThresholds extends QueryObjectOperation.Helper {

        /** Array of last k seen distances used to compute threshold */
        private float[] thresholdDistances;

        public HelperWithThresholds(KNNOperation query, DistanceFunc<DataObject> defaultDistanceFunc) {
            super(query, defaultDistanceFunc);
        }

        /**
         * Set the operation {@link #getAnswerThreshold() answer threshold distance} computation.
         * By default, the original threshold distance is used.
         * If the {@code useOriginalThresholdDistance} is <tt>false</tt>, the threshold
         * distance is always {@link DistanceFunc#MAX_DISTANCE} if {@code ignoreThresholdDistance}
         * is <tt>true</tt>. Or the threshold is computed from the first {@code computeSize} objects.
         * @param useOriginalThresholdDistance the flag whether the operation answer collection computes the threshold distance
         * @param ignoreThresholdDistance the flag whether the operation ignores answer threshold distance completely
         * @param computeSize the number of candidate objects to compute the threshold distance to;
         *          effective only if both {@code useOriginalThresholdDistance} and {@code ignoreThresholdDistance} are <tt>false</tt>
         * @throws IllegalStateException if some answer has been computed already
         */
        public void setAnswerThresholdComputation(boolean useOriginalThresholdDistance, boolean ignoreThresholdDistance, int computeSize) throws IllegalStateException {
            if (!answerBuilder.getAnswerCollection().isEmpty())
                throw new IllegalStateException("Cannot modify threshold computation when answer is already computed");
            if (useOriginalThresholdDistance) {
                thresholdDistances = null;
            } else if (ignoreThresholdDistance) {
                thresholdDistances = new float[0];
            } else {
                thresholdDistances = new float[computeSize];
                Arrays.fill(thresholdDistances, DistanceFunc.MAX_DISTANCE);
            }
        }

        /**
         * Returns the threshold distance for the current answer of this query.
         * If the answer has not reached the maximal size (specified in constructor) yet,
         * {@link DistanceFunc#MAX_DISTANCE} is returned.
         * Otherwise, the distance of the last answer's object is returned.
         * @return the distance to the last object in the answer list or
         *         {@link DistanceFunc#MAX_DISTANCE} if there are not enough objects.
         */
        @Override
        public float getAnswerThreshold() {
            // No threshold distances are stored, use the collection
            if (thresholdDistances == null)
                return super.getAnswerThreshold();
            // Zero threshold distances means no threshold distances are returned
            if (thresholdDistances.length == 0)
                return DistanceFunc.MAX_DISTANCE;
            return thresholdDistances[thresholdDistances.length - 1];
        }


        /**
         * Add a distance-ranked object to the answer.
         * Preserve the information about distances of the respective sub-objects.
         * @param object the object to add
         * @param distance the distance of object
         * @param objectDistances the array of distances to the respective sub-objects (can be <tt>null</tt>)
         * @return the distance-ranked object object that was added to answer or <tt>null</tt> if the object was not added
         * @throws IllegalArgumentException if the answer type of this operation requires cloning but the passed object cannot be cloned
         */
        @Override
        public RankedDataObject addToAnswer(DataObject object, float distance, float[] objectDistances) throws IllegalArgumentException {
            if (thresholdDistances != null && thresholdDistances.length > 0) {
                synchronized (thresholdDistances) { // This synchronization should be safe, since the threshold computation setup is allowed only if answer is empty
                    int pos = Arrays.binarySearch(thresholdDistances, distance); // Returns the insertion point
                    insert(thresholdDistances, distance, pos < 0 ? -(pos + 1) : pos);
                }
            }
            return answerBuilder.add(object, distance, objectDistances);
        }

    //    /**
    //     * Update the answer of this operation from a {@link RankingQueryOperation}.
    //     * @param answer the source operation from which to get the update
    //     */
    //    protected void updateFrom(RankedAnswer answer) {
    //        super.updateFrom(answer);
    //        if (thresholdDistances != null && thresholdDistances.length > 0) {
    //            synchronized (thresholdDistances) { // This synchronization should be safe, since the threshold computation setup is allowed only if answer is empty
    //                synchronized (answer.thresholdDistances) { // This is necessary, however, deadlock would be present only two operations update each other
    //                    if (answer.thresholdDistances != null && answer.thresholdDistances.length > 0) {
    //                        int tdIndex = 0;
    //                        forloop:for (int i = 0; i < answer.thresholdDistances.length; i++) { // Insert sort all values of the copied operation into this
    //                            while (thresholdDistances[tdIndex] < answer.thresholdDistances[i]) {
    //                                tdIndex++;
    //                                if (tdIndex < thresholdDistances.length)
    //                                    break forloop;
    //                            }
    //                            insert(thresholdDistances, answer.thresholdDistances[i], tdIndex);
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    }
    //

        /**
         * Insert value into the array on position {@code pos}.
         * Note that if position is beyond the array length, the array is not modified.
         * @param array the array into which to insert the value
         * @param value the value to insert
         * @param pos the position in the array
         */
        private static void insert(float[] array, float value, int pos) {
            if (pos < array.length) {
                System.arraycopy(array, pos, array, pos + 1, array.length - pos - 1);
                array[pos] = value;
            }
        }
    }
}
