/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.search;

import messif.data.DataObject;
import messif.data.util.DataObjectIterator;
import messif.operation.answer.AbstractAnswer;
import messif.operation.ReturnDataOperation;
import messif.operation.helpers.AnswerProvider;
import messif.operation.helpers.BaseHelper;
import messif.operation.answer.ListingAnswer;
import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Required;

import java.util.Collection;
import java.util.List;

/**
 * Operation that requests a given number of random objects from the algorithm.
 */
public interface GetRandomObjectsOperation extends ReturnDataOperation {

    String TYPE_STRING = "get_random";

    @Override
    default String getInnerType() {
        return TYPE_STRING;
    }


    String OBJECT_COUNT_FIELD = "count";

    @Get(field = OBJECT_COUNT_FIELD)
    @Required
    int getObjectCount(int defaultCount);

    /**
     * Returns the number of random objects to be returned by this operation.
     * @return the number of random objects to be returned by this operation
     */
    default int getObjectCount() {
        return getObjectCount(1);
    };


    /**
     * This class helps the operation processors to process this type of operation by providing supporting methods and
     *  continuous building of the answer.
     */
    class Helper extends BaseHelper<GetRandomObjectsOperation> implements AnswerProvider<ListingAnswer> {

        protected int remainingCount;

        protected final ListingAnswer.Builder answerBuilder;

        public Helper(GetRandomObjectsOperation operation) {
            super(operation);
            this.remainingCount = operation.getObjectCount();
            this.answerBuilder = new ListingAnswer.Builder(operation, ListingAnswer.class);
        }

        public void add(DataObject dataObject) {
            answerBuilder.add(dataObject);
        }

        public void addAll(Collection<DataObject> dataObjects) {
            answerBuilder.addAll(dataObjects);
        }

        /**
         * Selects a given number of objects at random from given iterator and add them to the answer.
         * @param objects list of objects to select random objects from
         * @param count number of objects to be selected from given list
         * @return the number of objects actually selected at random
         */
        public int evaluate(DataObjectIterator objects, int count) {
            if (count <= 0)
                return 0;
            List<DataObject> randomObjects = objects.getRandomObjects(count, false);
            answerBuilder.addAll(randomObjects);
            remainingCount -= randomObjects.size();

            return randomObjects.size();
        }

        public int evaluate(DataObjectIterator objects) {
            return evaluate(objects, remainingCount);
        }

        // ********************      Implementation of {@link AnswerProvider} interface

        @Override
        public void updateAnswer(AbstractAnswer answer) {
            answerBuilder.updateAnswer(answer);
        }

        @Override
        public ListingAnswer getAnswer() {
            return answerBuilder.getAnswer();
        }
    }

}
