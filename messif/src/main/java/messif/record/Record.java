/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.record;

import messif.utility.json.JSONException;
import messif.utility.json.JSONWritable;
import messif.utility.json.JSONWriter;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 * Interface for records that have multiple named fields.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface Record extends Cloneable, JSONWritable, Serializable {
    /**
     * Returns the number of additional fields.
     * @return the number of additional fields
     */
    int getFieldCount();

    /**
     * Returns a set of additional field names present in this object.
     * @return a set of additional field names present in this object
     */
    Collection<String> getFieldNames();

    /**
     * Returns whether a field with the given {@code name} exists in this parametric object.
     * @param name the name of the additional field to get
     * @return <tt>true</tt> if this object contains a field of the given {@code name} or <tt>false</tt> if it does not
     */
    boolean containsField(String name);

    /**
     * Returns an additional field with the given {@code name}.
     * @param name the name of the additional field to get
     * @return the value of the field {@code name} or <tt>null</tt> if it is not set
     */
    Object getField(String name);

    /**
     * Returns an additional field with the given {@code name}.
     * If the field with the given {@code name} is not set, an exception is thrown.
     * @param name the name of the additional field to get
     * @return the value of the field {@code name}
     * @throws IllegalArgumentException if the field with the given {@code name} is not set
     */
    Object getRequiredField(String name) throws IllegalArgumentException;

    /**
     * Returns an additional field with the given {@code name}.
     * If the field with the given {@code name} is not set or is not an
     * instance of {@code fieldClass}, an exception is thrown.
     *
     * @param <T> the class of the field
     * @param name the name of the additional field to get
     * @param fieldClass the class of the field to get
     * @return the field value
     * @throws IllegalArgumentException if the field with the given {@code name} is not set
     * @throws ClassCastException if the field with the given {@code name} is not an instance of {@code fieldClass}
     */
    @SuppressWarnings("unchecked")
    default <T> T getRequiredField(String name, Class<? extends T> fieldClass) throws IllegalArgumentException, ClassCastException {
        T field = getField(name, fieldClass, null);
        if (field == null)
            throw new IllegalArgumentException("The field '" + name + "' is not set");
        return field;
    }

    /**
     * Returns an additional field with the given {@code name}.
     * If the field is not set or is not an instance of {@code fieldClass},
     * the {@code defaultValue} is returned instead.
     *
     * @param <T> the class of the field
     * @param name the name of the additional field to get
     * @param fieldClass the class of the field to get
     * @param defaultValue the default value to use if the field is <tt>null</tt>
     * @return the field value
     */
    <T> T getField(String name, Class<? extends T> fieldClass, T defaultValue);

    /**
     * Returns an additional field with the given {@code name}.
     * If the field {@code name} exists but it is not an instance of
     * {@code fieldClass}, <tt>null</tt> is returned.
     *
     * @param <T> the class of the field
     * @param name the name of the additional field to get
     * @param fieldClass the class of the field to get
     * @return the value of the field {@code name} or <tt>null</tt> if it is not set
     */
    default <T> T getField(String name, Class<? extends T> fieldClass) {
        return getField(name, fieldClass, null);
    }

    /**
     * Returns the map of all additional fields.
     * Note that the map is not modifiable.
     * @return the map of additional fields
     */
    Map<String, Object> getMap();

    /**
     * Clones this record.
     * @return
     */
    Object clone() throws CloneNotSupportedException;

    /**
     * Check the record equality based on the actual data stored in individual fields.
     * @param second the second record to compare with
     * @return true only if the records contain the same fields that are equal according to {@link Object#equals(Object)}.
     */
    boolean dataEquals(Record second);

    /**
     * Return a hashcode of this record based on the data stored in the fields.
     * @return
     */
    int dataHashCode();

    /**
     * Stores this object into JSON.
     * @param writer an output writer to write the JSON into
     * @return the writer (for chaining)
     * @throws JSONException if the JSON serialization fails
     * @throws IOException if writing into the output stream within {@link JSONWriter} fails
     */
    @Override
    default JSONWriter writeJSON(JSONWriter writer) throws JSONException, IOException {
        writer.write(getMap());
        writer.flush();
        return writer;
    }
}
