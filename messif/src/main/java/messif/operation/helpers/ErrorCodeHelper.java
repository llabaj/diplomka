/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.helpers;

import messif.operation.answer.AbstractAnswer;
import messif.utility.ProcessingStatus;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface ErrorCodeHelper extends AnswerMerger {

    /**
     * Update the stored error code from given error code.
     * @param status
     */
    void updateErrorCode(ProcessingStatus status);

    /**
     * Returns current error code after all updates from {@link #updateErrorCode(ProcessingStatus)} and {@link #updateAnswer(AbstractAnswer)}
     * @return
     */
    ProcessingStatus getStatus();

}
