package com.me.diplomka.fixedpivotswebapp.controller.api;

import com.hazelcast.core.IMap;
import com.me.diplomka.fixedpivots.IndexedDataObject;
import com.me.diplomka.fixedpivots.KNNService;
import com.me.diplomka.fixedpivotswebapp.AppConfig;
import messif.data.DataObject;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/api")
public class KNNRestController {
    private static final Logger logger = LoggerFactory.getLogger(KNNRestController.class);

    @Autowired
    private KNNService knnService;

    @Autowired
    private IMap<String, IndexedDataObject> data;

    @Autowired
    private AppConfig config;

    @GetMapping("/search")
    public ResponseEntity<List<DataObject>> knnSearch(@RequestParam String query,
                                                      @RequestParam(defaultValue = "10") int limit,
                                                      @RequestParam(required = false) Integer candidateSet,
                                                      @RequestParam(defaultValue = "") String tags) {

        logger.debug("search request for '{}' with k = '{}', candidate set = '{}', tags = '{}'", query, limit, candidateSet, tags);
        var queryObject = data.get(query);

        logger.debug("got query object '{}'", queryObject);

        if (queryObject == null) {
            return ResponseEntity.notFound().build();
        }

        var tagArray = !tags.isEmpty() ? tags.split(" ") : new String[0];
        var result = knnService.search(queryObject, limit, candidateSet != null ? candidateSet : config.candidateSetThreshold(), tagArray)
                .stream()
                .map(Pair::getLeft)
                .collect(toList());

        return ResponseEntity.ok(result);
    }

    @GetMapping("/full-scan")
    public ResponseEntity<List<DataObject>> knnFullScan(@RequestParam String query,
                                                        @RequestParam(defaultValue = "10") int limit,
                                                        @RequestParam(defaultValue = "") String tags) {

        logger.debug("full scan request for '{}' with k = '{}', tags = '{}'", query, limit, tags);
        var queryObject = data.get(query);

        logger.debug("got query object '{}'", queryObject);

        if (queryObject == null) {
            return ResponseEntity.notFound().build();
        }

        var tagArray = !tags.isEmpty() ? tags.split(" ") : new String[0];
        var result = knnService.fullScan(queryObject, limit, tagArray)
                .stream()
                .map(pair -> (DataObject) pair.getLeft())
                .collect(toList());

        return ResponseEntity.ok(result);
    }
}
