/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.algorithm.executor;

import messif.algorithm.Algorithm;
import messif.algorithm.AlgorithmMethodException;
import messif.operation.answer.AbstractAnswer;
import messif.operation.AbstractOperation;
import messif.operation.processing.OperationExecutor;
import messif.statistic.OperationStatistics;
import messif.statistic.StatisticTimer;
import messif.utility.ExtendedProperties;
import messif.utility.json.JSONWriter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;

import static messif.algorithm.executor.StatisticsExecutor.STAT_FIELD;

/**
 * Logs every processed operation into a standard text log. Measures also the running time and writes the time
 *  into the answer statistics.
 */
public class OperationLogger extends ExecutorChainItem {

    //******************   Constants  (config items)     ******************//

    public static final String CONFIG_VERBOSITY = "oplogger.verbosity";

    public static final String CONFIG_PRETTY = "oplogger.pretty";


    //****************** Attributes ******************//

    /**
     * Verbosity of the logging of the last executed operation; it corresponds to the level of JSON written out.
     */
    protected int opLogVerbosity = 1;

    protected boolean prettyFormatting = false;

    protected boolean measureOpTime = true;

    @Override
    public void init(ExtendedProperties properties, Algorithm algorithm) throws AlgorithmMethodException {
        super.init(properties, algorithm);
        this.opLogVerbosity = properties.getIntProperty(CONFIG_VERBOSITY, opLogVerbosity);
        this.prettyFormatting = properties.getBoolProperty(CONFIG_PRETTY, prettyFormatting);
        // find out, if one of the subsequent executors computes statistics (including the operation time)
        this.measureOpTime = algorithm.getStatisticsExecutor() == null;
    }

    @Override
    public AbstractAnswer execute(AbstractOperation operation, Iterator<OperationExecutor> iterator) throws Exception {
        long startTimeStamp = measureOpTime ? System.currentTimeMillis() : 0;
        AbstractAnswer answerToReturn = null;
        try {
            return answerToReturn = executeNext(operation, iterator);
        } finally {
            // log the operation processing information
            long runningTime = measureOpTime ? System.currentTimeMillis() - startTimeStamp : OperationStatistics.getOpStatistics("OperationTime", StatisticTimer.class).get();
            if (opLogVerbosity > 0) {
                // int jsonDepth = (opLogVerbosity > 2) ? opLogVerbosity - 1 : 1;
                String opString = JSONWriter.writeToJSON(operation, prettyFormatting, opLogVerbosity);
                String answerString = ((answerToReturn == null) ? "null" : JSONWriter.writeToJSON(answerToReturn, prettyFormatting, opLogVerbosity));
                Algorithm.log.log(Level.INFO, algorithmName + " processed operation: " + opString + "; answer: " + answerString + "; Time: " + runningTime);
            }
            // if the operation statistics are actually not computed, set the only statistic with the running time
            if (measureOpTime && answerToReturn != null) {
                Map<String, Object> answerStats = (Map<String, Object>) answerToReturn.getParams().getField(STAT_FIELD, Map.class, new HashMap<>());
                answerStats.put("OperationTime", runningTime);
                answerToReturn.getParams().setField(STAT_FIELD, answerStats);
            }
        }
    }

}
