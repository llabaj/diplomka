/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.serialization;

import messif.serialization.io.BinaryInput;
import messif.serialization.io.BinaryOutput;

import java.io.*;

/**
 * This is a helper serialized any {@link Serializable} object using the MESSIF-like binary serialization.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class JavaSerializationReaderWriter implements ClassReaderWriter<Serializable> {

    @Override
    public int writeBinary(Serializable object, BinaryOutput output, ObjectSerializator serializator) throws IOException, BinarySerializationException {
        try (final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                ObjectOutputStream objectStream = new ObjectOutputStream(byteArrayOutputStream)) {
            objectStream.writeObject(object);
            objectStream.flush();
            final byte[] byteArray = byteArrayOutputStream.toByteArray();

            // write the Java-base binary representation (first size and than the byte array)
            return serializator.write(output, byteArray);
        } catch (NotSerializableException e) {
            throw new BinarySerializationException(e.toString());
        }
    }

    @Override
    public Serializable readBinary(BinaryInput input, ObjectSerializator serializator) throws IOException, IllegalArgumentException, BinarySerializationException {
        try {
            // read the size of the byte array to be read and deserialized
            final byte[] bytes = serializator.readByteArray(input);
            return (Serializable) new ObjectInputStream(new ByteArrayInputStream(bytes)).readObject();
        } catch (ClassNotFoundException e) {
            throw new IOException(e.toString());
        }
    }

    @Override
    public Class<Serializable> getReadWriteClass() {
        return Serializable.class;
    }

}
