/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.processing;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * A base class that connects to a given socket(s), sends the binary data either read from a file or from a URL (file)
 *  and sends it to the socket.
 */
public abstract class SocketProcessorBase {

    private final String host;
    private final int[] ports;
    private final int timeoutMillis;

    private int currentIndex;

    public SocketProcessorBase(final String host, final int[] ports, final int timeoutMillis) {
        this.host = host;
        this.ports = ports;
        this.timeoutMillis = timeoutMillis;
    }

    protected synchronized Socket createSocket() throws IOException {
        int startIndex = currentIndex;
        for (; ; ) {
            currentIndex = (currentIndex + 1) % ports.length;
            try {
                Socket socket = new Socket(host, ports[currentIndex]);
                if (timeoutMillis > 0)
                    socket.setSoTimeout(timeoutMillis);
                return socket;
            } catch (UnknownHostException e) {
                throw e;
            } catch (IOException e) {
                if (currentIndex == startIndex)
                    throw e;
            }
        }
    }

}
