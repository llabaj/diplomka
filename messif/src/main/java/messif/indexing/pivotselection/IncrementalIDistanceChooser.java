/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.indexing.pivotselection;

import messif.data.DataObject;
import messif.data.util.DataObjectIterator;
import messif.data.util.DataObjectList;
import messif.distance.DistanceFunc;
import messif.utility.ListUtils;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;


/**
 * Chooses pivots according to a generalized iDistance clustering strategy.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class IncrementalIDistanceChooser extends AbstractPivotChooser implements Serializable {
    /** Class version id for serialization */
    private static final long serialVersionUID = 1L;

    /** Size of the sample set used to test the candidate pivot (used to estimate mu_d) */
    private final int sampleSetSize;
    
    /** Size of the candidate set of pivots from which one pivot will be picked. */
    private final int samplePivotSize;
    
    /** Creates a new instance of IncrementalPivotChooser */
    public IncrementalIDistanceChooser(DistanceFunc<DataObject> distanceFunc) {
        this(10000, 100, distanceFunc);
    }

    /**
     * Creates a new instance of IncrementalPivotChooser.
     * @param sampleSetSize the size of the sample set used to test the candidate pivot (used to estimate mu_d)
     * @param samplePivotSize the size of the candidate set of pivots from which one pivot will be picked
     */
    public IncrementalIDistanceChooser(int sampleSetSize, int samplePivotSize, DistanceFunc<DataObject> distanceFunc) {
        super(distanceFunc);
        this.sampleSetSize = sampleSetSize;
        this.samplePivotSize = samplePivotSize;
    }

    /**
     * Select a pivot closes to <code>object</code>.
     * @param object the object for which to get the closest pivot
     * @param pivotIter pivots iterator
     * @return the closest pivot for the passed object
     */
    private DataObject getClosestPivot(DataObject object, Iterator<? extends DataObject> pivotIter) {
        float minVal = Float.MAX_VALUE;
        float tmpDist;
        DataObject preselectedPivot;
        DataObject pivotX = pivotIter.hasNext() ? pivotIter.next():null;
        preselectedPivot = pivotX;
        for (; pivotIter.hasNext(); pivotX = pivotIter.next()) {
            tmpDist = distanceFunc.getDistance(object, pivotX);
            if (minVal > tmpDist) {
                minVal = tmpDist;
                preselectedPivot = pivotX;
            }
        }
        return preselectedPivot;
    }
    
    /**
     * Selects new pivots.
     * Implementation of the incremental pivot selection algorithm.
     * This method is not intended to be called directly. It is automatically called from getPivot(int).
     *
     * This pivot selection technique depends on previously selected pivots. The DataObjectList
     * with such the pivots can be passed in getPivot(position,addInfo) in addInfo parameter
     * (preferable way) or directly set using setAdditionalInfo() method.
     * If the list of pivots is not passed it is assumed that no pivots were selected.
     *
     * Statistics are maintained automatically.
     * @param pivots number of pivots to generate
     * @param objectIter Iterator over the sample set of objects to choose new pivots from
     */
    @Override
    protected void selectPivot(int pivots, DataObjectIterator objectIter) {
        // Store all passed objects temporarily
        DataObjectList objectList = new DataObjectList(objectIter);
        
        int sampleSize = (Math.sqrt(sampleSetSize) > objectList.size()) ? objectList.size() * objectList.size() : sampleSetSize;

        // Select objects randomly
        List<DataObject> leftPair  = ListUtils.randomList(objectList, sampleSize, false);
        List<DataObject> rightPair = ListUtils.randomList(objectList, sampleSize, false);
        
        
        DataObject leftObj;
        DataObject rightObj;
        
        DataObject tmpPivot = null;                    // temporary pivot
        
        float[] distsLeftClosest = new float[sampleSize];      // stored distances between left objects and the best pivot
        float[] distsRightClosest = new float[sampleSize];     // stored distances between right objects and the best pivot
        
        // initialize array of distances to former pivots
        Iterator<DataObject> leftIter = leftPair.iterator();
        Iterator<DataObject> rightIter= rightPair.iterator();
        
        for (int i = 0; i < sampleSize; i++) {
            leftObj = leftIter.next();
            rightObj = rightIter.next();
            
            tmpPivot = getClosestPivot(leftObj, preselectedPivots.iterator());
            if (tmpPivot != null) {
                distsLeftClosest[i] = distanceFunc.getDistance(leftObj, tmpPivot);//Math.abs(leftObj.getDistanceFastly(tmpPivot) - rightObj.getDistanceFastly(tmpPivot));
                distsRightClosest[i] = distanceFunc.getDistance(rightObj, tmpPivot);
            } else {
                distsLeftClosest[i] = Float.MAX_VALUE;
                distsRightClosest[i] = Float.MAX_VALUE;
            }
        }
        
        
        // Select required number of pivots
        for (int p = 0; p < pivots; p++) {
            System.out.println("Selecting pivot number "+p);//", DistanceComputations: "+Statistics.printStatistics("DistanceComputations"));

            List<DataObject> candidatePivots = ListUtils.randomList(objectList, samplePivotSize, true);
            
            float[] distsLeftToBestCand = new float[sampleSize];      // stored distances between left objects and the best pivot
            float[] distsRightToBestCand = new float[sampleSize];     // stored distances between right objects and the best pivot
            float[] distsLeftToCand = new float[sampleSize];      // stored distances between left objects and the pivot candidate
            float[] distsRightToCand = new float[sampleSize];     // stored distances between right objects and the pivot candidate
            for (int i = 0; i < sampleSize; i++) {
                distsLeftToBestCand[i] = Float.MAX_VALUE;
                distsRightToBestCand[i] = Float.MAX_VALUE;
            }
            
            float bestPivotMu = 0;                                 // mu_D of the best pivot candidate
            DataObject bestPivot = null;                   // the best pivot candidate until now
            
            // go through all candidate pivots and compute their mu
            System.out.print("Candidates: "); int iter = 1;
            for (Iterator<DataObject> pivotIter = candidatePivots.iterator(); pivotIter.hasNext(); ) {
                System.out.print(iter++ +", "); System.out.flush();
                DataObject pivot = pivotIter.next();
                
                // compute distance between sample objects and pivot
                leftIter = leftPair.iterator();
                rightIter = rightPair.iterator();
                float mu = 0;
                for (int i = 0; i < sampleSize; i++) {
                    leftObj = leftIter.next();
                    rightObj = rightIter.next();
                    
                    //for (int i = 0; i < sampleSize; i++) {
                    float distLeftToCand = distanceFunc.getDistance(leftObj, pivot);
                    if (distLeftToCand < distsLeftClosest[i]) {
                        distsLeftToCand[i] = distLeftToCand;
                        distsRightToCand[i] = distanceFunc.getDistance(rightObj, pivot);
                    } else {
                        distsLeftToCand[i] = distsLeftClosest[i];
                        distsRightToCand[i] = distsRightClosest[i];
                    }
                    mu += Math.abs(distsLeftToCand[i] - distsRightToCand[i]);
                }
                mu /= (float)sampleSize;
                
                if (mu > bestPivotMu) {     // the current pivot is the best one until now, store it
                    // store mu and pivot
                    bestPivotMu = mu;
                    bestPivot = pivot;
                    // store distances from left/right objects to this pivot
                    for (int i = 0; i < sampleSize; i++) {
                        distsLeftToBestCand[i] = distsLeftToCand[i];
                        distsRightToBestCand[i] = distsRightToCand[i];
                    }
                }
            }
            System.out.println();
            // append the selected pivot
            preselectedPivots.add(bestPivot);
            // store distances from left/right objects to this pivot
            for (int i = 0; i < sampleSize; i++) {
                distsLeftClosest[i] = distsLeftToBestCand[i];
                distsRightClosest[i] = distsRightToBestCand[i];
            }
        }
    }
    
}
