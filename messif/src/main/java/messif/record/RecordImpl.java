/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.record;

import messif.data.DataObject;
import messif.utility.ArrayMap;
import messif.utility.Convert;
import messif.utility.json.JSONException;
import messif.utility.json.JSONWriter;
import messif.utility.proxy.ProxyConverter;

import java.io.IOException;
import java.util.*;

/**
 * Basic implementation of the {@link Record} interface on encapsulated {@link Map}.
 * Note that this class can be used as wrapper for {@link Map}.
 *
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class RecordImpl implements Record, DataObject {

    /**
     * Class serial id for Java serialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Encapsulated {@link Map} that provides the field values
     */
    protected final Map<String, Object> map;

    /**
     * Creates a new instance of RecordImpl from the given map of fields.
     *
     * @param fields         the map that provides the field values
     * @param useMapDirectly flag whether the given field map is used directly (<tt>true</tt>)
     *                       or if a {@link ArrayMap} copy is created (<tt>false</tt>)
     */
    public RecordImpl(Map<String, Object> fields, boolean useMapDirectly) {
        this.map = useMapDirectly ? fields : new ArrayMap<>(fields);
    }

    /**
     * Creates a new instance of RecordImpl from the given map of fields.
     * The fields are stored in a new {@link ArrayMap} instance.
     *
     * @param fields the map that provides the field values
     */
    public RecordImpl(Map<String, Object> fields) {
        this(fields, false);
    }

    /**
     * Create this static {@link Record} given keys and value arrays that are used directly by {@link ArrayMap}.
     * @param keys string keys of the record
     * @param values corresponding values
     */
    public RecordImpl(String[] keys, Object[] values) {
        this(new ArrayMap<>(keys, values, true), true);
    }

    /**
     * Creates a new instance of RecordImpl from another {@link Record} object.
     * The fields are stored in a new {@link ArrayMap} instance.
     *
     * @param record another {@link Record} object the data of which to copy
     */
    public RecordImpl(Record record) {
        this(record.getMap(), false);
    }

    /**
     * Creates a new instance of RecordImpl from another {@link Record} object.
     * The fields are stored in a new {@link ArrayMap} instance.
     *
     * @param fieldMap another {@link Record} object the data of which to copy
     */
    public RecordImpl(RecordImpl fieldMap) {
        this(fieldMap.getDirectMap(), false);
    }

    /**
     * Creates a new instance of RecordImpl from another {@link Record} object copying only the given list of fields.
     * The fields are stored in a new {@link ArrayMap} instance.
     *
     * @param fields       a map of name-data to copy
     * @param fieldsToCopy a list of field names to be copied
     */
    public RecordImpl(Map<String, Object> fields, String[] fieldsToCopy) {
        List<String> names = new ArrayList<>(fieldsToCopy.length);
        List<Object> values = new ArrayList<>(fieldsToCopy.length);
        for (String name : fieldsToCopy) {
            final Object value = fields.get(name);
            if (value != null) {
                names.add(name);
                values.add(value);
            }
        }
        this.map = new ArrayMap<>(names.toArray(new String[names.size()]), values.toArray(), true);
    }

    /**
     * Creates a new instance of RecordImpl from another {@link Record} object copying only the given list of fields.
     * The fields are stored in a new {@link ArrayMap} instance.
     *
     * @param record       another {@link Record} object the data of which to copy
     * @param fieldsToCopy a list of field names to be copied
     */
    public RecordImpl(Record record, String[] fieldsToCopy) {
        this(record.getMap(), fieldsToCopy);
    }

    /**
     * Creates a new instance of RecordImpl from another {@link Record} object copying only the given list of fields.
     * The fields are stored in a new {@link ArrayMap} instance.
     *
     * @param fieldMap     another {@link Record} object the data of which to copy
     * @param fieldsToCopy a list of field names to be copied
     */
    public RecordImpl(RecordImpl fieldMap, String[] fieldsToCopy) {
        this(fieldMap.getDirectMap(), fieldsToCopy);
    }

    /**
     * Creates a new instance of RecordImpl from another {@link Record} object copying only the given list of fields.
     * The fields are stored in a new {@link ArrayMap} instance.
     *
     * @param original       a source map to copy from
     * @param field a field name to be added or modified
     * @param value a new value to be set (added) for the field
     */
    public RecordImpl(Map<String, Object> original, String field, Object value) {
        if (original.containsKey(field)) {
            final ArrayMap<String, Object> copy = new ArrayMap<>(original);
            copy.replace(field, value);
            this.map = copy;
            return;
        }
        String [] fields = new String[original.size() + 1];
        Object [] values = new Object[original.size() + 1];
        int i = 0;
        for (Map.Entry<String, Object> entry : original.entrySet()) {
            fields[i] = entry.getKey();
            values[i] = entry.getValue();
            i++;
        }
        fields[i] = field;
        values[i] = value;
        this.map = new ArrayMap<>(fields, values, true);
    }

    public RecordImpl(Record original, String field, Object value) {
        this(original.getMap(), field, value);
    }

    public RecordImpl(RecordImpl original, String field, Object value) {
        this(original.getDirectMap(), field, value);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new RecordImpl(this);
    }

    @Override
    // TODO: This data equals does not work, e.g. [1.0, 1.0] is not .equal  another [1.0, 1.0]
    public boolean dataEquals(Record secondRecord) {
//        if (!(second instanceof Record)) {
//            return false;
//        }
//        Record secondRecord = (Record) second;
        if (getFieldCount() != secondRecord.getFieldCount()) {
            return false;
        }
        for (Map.Entry<String, Object> param : getDirectMap().entrySet()) {
            if (!secondRecord.containsField(param.getKey())) {
                return false;
            }
            Object compareParam = secondRecord.getField(param.getKey());
            if (compareParam == null && param.getValue() == null) {
                continue;
            }
            if (compareParam == null || !compareParam.equals(param.getValue())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int dataHashCode() {
        int rtv = 0;
        for (Map.Entry<String, Object> param : getDirectMap().entrySet()) {
            rtv += param.getKey().hashCode() + param.getValue().hashCode();
        }
        return rtv;
    }

    //************ Parametric interface ************//

    @Override
    public int getFieldCount() {
        return map != null ? map.size() : 0;
    }

    @Override
    public Collection<String> getFieldNames() {
        if (map == null)
            return Collections.emptyList();
        return Collections.unmodifiableCollection(map.keySet());
    }

    @Override
    public boolean containsField(String name) {
        return map != null && map.containsKey(name);
    }

    @Override
    public Object getField(String name) {
        return map != null ? map.get(name) : null;
    }

    @Override
    public Object getRequiredField(String name) throws IllegalArgumentException {
        Object field = getField(name);
        if (field == null)
            throw new IllegalArgumentException("The field '" + name + "' is not set");
        return field;
    }

    @Override
    public <T> T getField(String name, Class<? extends T> fieldClass, T defaultValue) {
        Object value = getField(name);
        if (value == null) {
            // if the map explicitly contains null value for given key, return it
            if (containsField(name)) {
                if (fieldClass.isArray()) {
                    return (T) Convert.createGenericArray(fieldClass.getComponentType(), 0);
                }
                return null;
            }
            return defaultValue;
        }
        if (fieldClass.isInstance(value))
            return fieldClass.cast(value); // This cast IS checked by isInstance

        try { // handle the primitive types stored in Map
            if (fieldClass.isPrimitive() && fieldClass == value.getClass().getField("TYPE").get(null))
                return (T) value; // This cast IS checked by previous line
        } catch (IllegalAccessException | NoSuchFieldException ignore) {
        }

        // convert (and replace) the value, if we can
        if (value instanceof String && fieldClass != String.class) {
            try {
                // in case the value is "null", the Convert method returns null
                Object retVal = Convert.stringToType((String) value, fieldClass);
                if (retVal != null) {
                    value = retVal;
                    map.replace(name, value);
                }
            } catch (InstantiationException e) {
                throw new ClassCastException(e.toString());
            }
        }
//        if (value instanceof Iterator && DataObject.class.isAssignableFrom(fieldClass)) {
//            //System.out.println("reading an object from the stream");
//            value = ((Iterator<?>) value).next();
//            map.replace(name, value);
//        }
        //
        if (fieldClass.isArray() && value.getClass().isArray()) {
            value = Arrays.copyOf((Object []) value, ((Object []) value).length, (Class) fieldClass);
            map.replace(name, value);
//            final Class<?> componentType = fieldClass.getComponentType();
//            for (String s : map.keySet()) {
//
//            }
        }

        if (fieldClass.isInstance(value))
            return fieldClass.cast(value); // This cast IS checked by isInstance

        return defaultValue;
    }

    @Override
    public Map<String, Object> getMap() {
        if (map == null)
            return Collections.emptyMap();
        return Collections.unmodifiableMap(map);
    }

    /**
     * Returns the actual map object, but this getter is only protected and should stay so.
     *
     * @return the actual map that stores the objects
     */
    protected Map<String, Object> getDirectMap() {
        return map;
    }


    //****************** String conversion support ******************//

    /**
     * Appends one field with a given {@code fieldName} from the {@link Record} instance to the {@code str}.
     * The field is printed as &lt;field-name&gt;&lt;&lt;{@code nameValueSeparator}&gt;&lt;field-value&gt;.
     *
     * @param str                the string builder to append the parametric values to
     * @param parametric         the parametric instance the values of which to append to the string
     * @param fieldName          the name of the field to append
     * @param nameValueSeparator string that is placed between the field name and the field value
     * @return the given string builder with the appended field
     */
    public static StringBuilder appendField(StringBuilder str, Record parametric, String fieldName, String nameValueSeparator) {
        return str.append(fieldName).append(nameValueSeparator).append(parametric.getField(fieldName));
    }

    /**
     * Appends the given {@link Record} instance to the given string.
     * Each field from the {@code parametric} is printed as the
     * &lt;field-name&gt;&lt;&lt;{@code nameValueSeparator}&gt;&lt;field-value&gt;.
     * The respective fields are separated using the given {@code fieldSeparator}.
     *
     * @param str                    the string builder to append the parametric values to
     * @param parametric             the parametric instance the values of which to append to the string
     * @param nameValueSeparator     string that is placed between the field name and the field value
     * @param fieldSeparator         string that is placed between each field record
     * @param addFirstFieldSeparator flag whether to add the {@code fieldSeparator} for the first field or not
     * @return the given string builder with the appended fields
     */
    public static StringBuilder append(StringBuilder str, Record parametric, String nameValueSeparator, String fieldSeparator, boolean addFirstFieldSeparator) {
        for (String fieldName : parametric.getFieldNames()) {
            if (addFirstFieldSeparator) {
                str.append(fieldSeparator);
            } else {
                addFirstFieldSeparator = true;
            }
            appendField(str, parametric, fieldName, nameValueSeparator);
        }
        return str;
    }

    /**
     * Converts the given object to string.
     * If the object implements a {@link Record} interface, the fields are
     * {@link #append(java.lang.StringBuilder, Record, java.lang.String, java.lang.String, boolean) appended}
     * to the string after the specified separator.
     *
     * @param object             the object to convert to string
     * @param separator          the string to append after the object and before the fields
     * @param nameValueSeparator string that is placed between the field name and the field value
     * @param fieldSeparator     string that is placed between each field record
     * @return the object converted to string
     */
    public static String toStringWithCast(Object object, String separator, String nameValueSeparator, String fieldSeparator) {
        if (object == null)
            return null;

        if (object instanceof Record) {
            Record parametric = (Record) object;
            if (parametric.getFieldCount() > 0) {
                StringBuilder str = new StringBuilder(parametric.toString());
                if (separator != null)
                    str.append(separator);
                append(str, parametric, nameValueSeparator, fieldSeparator, false);
                return str.toString();
            }
        }

        return object.toString();
    }

    @Override
    public String toString() {
        return append(new StringBuilder().append(getClass().getName()).append(" with {"), this, " = ", ", ", false).append('}').toString();
    }

    @Override
    public JSONWriter writeJSON(JSONWriter writer, int level) throws JSONException, IOException {
        writer.write(getDirectMap(), level);
        //writer.flush();
        return writer;
    }

    @Override
    public JSONWriter writeJSON(JSONWriter writer) throws JSONException, IOException {
        return writeJSON(writer, 0);
    }

    @Override
    public boolean equals(Object obj) {
        if (ProxyConverter.isProxy(obj))
            return super.equals(ProxyConverter.resolveUnderlyingRecord(obj));
        else
            return super.equals(obj);
    }
}
