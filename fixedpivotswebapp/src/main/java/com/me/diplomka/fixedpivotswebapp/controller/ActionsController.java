package com.me.diplomka.fixedpivotswebapp.controller;

import com.hazelcast.core.IMap;
import com.hazelcast.core.ReplicatedMap;
import com.me.diplomka.fixedpivots.IndexedDataObject;
import com.me.diplomka.fixedpivots.PivotService;
import com.me.diplomka.fixedpivots.TagService;
import com.me.diplomka.fixedpivotswebapp.AppConfig;
import com.me.diplomka.fixedpivotswebapp.Initializer;
import messif.data.DataObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

@RestController
@RequestMapping("/action")
public class ActionsController {

    @Autowired
    private IMap<String, IndexedDataObject> dataMap;
    @Autowired
    private ReplicatedMap<String, DataObject> pivotMap;
    @Autowired
    private PivotService pivotService;
    @Autowired
    private AppConfig config;
    @Autowired
    private TagService tagService;

    @GetMapping("/init")
    public String initAll() {
        var start = System.nanoTime();
        try {
            Initializer.initData(dataMap, pivotMap, pivotService, config.pivotCount(), tagService);
        } catch (IOException e) {
            return e.getMessage();
        }
        var end = System.nanoTime();
        return "ok " + (end - start) / 1_000_000_000 + "s";
    }

    @GetMapping("/init-ff")
    public String initFromFiles(@RequestParam String pivots,
                                @RequestParam String data,
                                @RequestParam(defaultValue = "0") int offset,
                                @RequestParam(defaultValue = "-1") int size) throws FileNotFoundException {

        var pivotReader = new BufferedReader(new FileReader(pivots));
        var dataReader = new BufferedReader(new FileReader(data));
        var start = System.nanoTime();
        try {
            Initializer.initData(dataMap, pivotMap, pivotService, config.pivotCount(), tagService, pivotReader, dataReader, offset, size);
        } catch (IOException e) {
            return e.getMessage();
        }

        var end = System.nanoTime();
        return "ok " + (end - start) / 1_000_000_000 + "s";
    }


}
