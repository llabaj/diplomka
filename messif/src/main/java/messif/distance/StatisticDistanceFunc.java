/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.distance;

import messif.data.DataObject;
import messif.statistic.StatisticCounter;
import messif.statistic.Statistics;

/**
 * A distance function that counts the distance computations using a global statistics counter "DistanceComputations".
 *  The actual distance function (that takes e.g. a float [] array or {@link DataObject}) is passed to this class.
 *
 * @param <T> the type of the distance function arguments
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class StatisticDistanceFunc<T> implements DistanceFunc<T> {

    /**
     * Class id for serialization.
     */
    private static final long serialVersionUID = 1L;

    public static final String DC_STAT_NAME = "DistanceComputations".intern();

    /** Statistic counter to be used for this distance function. */
    protected final StatisticCounter statistic;

    /**
     * Distance to be calculated.
     */
    private final DistanceFunc<T> distanceFunc;

    /**
     * Creates a new instance of this distance function given the actual distance function; the
     * statistic name is default: DistanceComputations
     * @param distanceFunc the actual distance function to be evaluated.
     */
    public StatisticDistanceFunc(DistanceFunc<T> distanceFunc) {
        this(distanceFunc, DC_STAT_NAME);
    }

    /**
     * Creates a new instance of this distance function given the actual distance function.
     * @param distanceFunc the actual distance function to be evaluated.
     * @param statName name of the statistics to be used for this function
     */
    public StatisticDistanceFunc(DistanceFunc<T> distanceFunc, String statName) {
        if (distanceFunc == null) {
            throw new IllegalArgumentException(StatisticDistanceFunc.class.toString() + " cannot be constructed with null argument");
        }
        this.distanceFunc = distanceFunc;
        this.statistic = StatisticCounter.getStatistics(statName);
    }


    @Override
    public float getDistance(T o1, T o2, float threshold) {
        // This check is to enhance performance when statistics are disabled
        if (Statistics.isEnabledGlobally())
            statistic.add();
        return distanceFunc.getDistance(o1, o2, threshold);
    }

    @Override
    public float getMaxDistance() {
        return distanceFunc.getMaxDistance();
    }

    @Override
    public Class<T> getObjectClass() {
        return distanceFunc.getObjectClass();
    }

    @Override
    public String toString() {
        return new StringBuilder(distanceFunc.toString()).append(" with DC statistics").toString();
    }

    /**
     * A factory method.
     * @param distanceFunc the actual distance function
     * @param <T> type of the arguments of the distance function
     * @return a newly created object encapsulating given distance function and counting distance computations
     */
    public static <T> StatisticDistanceFunc<T> create(DistanceFunc<T> distanceFunc) {
        return new StatisticDistanceFunc<T>(distanceFunc);
    }

}
