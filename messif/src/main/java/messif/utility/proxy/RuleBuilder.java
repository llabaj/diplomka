/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.proxy;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Simple builder class helping to create {@link MethodConversionRule} object in readable way.
 * <p>
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
class RuleBuilder {
    private Class<? extends Annotation> type;
    private Predicate<Method> condition = method -> true;
    private RecordMethodWrapper.RecordMethodCaller methodHandle;
    private Function<Method, Class> classRetriever;
    private boolean requiresValue = false;
    private boolean modifiedInput = false;

    static RuleBuilder is(Class<? extends Annotation> type) {
        RuleBuilder builder = new RuleBuilder();
        builder.type = type;
        return builder;
    }

    /**
     * Condition that defines if the incoming proxy method can be transferred to underlying method
     *
     * @param condition method condition
     * @return builder
     */
    RuleBuilder has(Predicate<Method> condition) {
        this.condition = condition;
        return this;
    }

    /**
     * Defines callback to underlying object
     *
     * @param methodHandle callback
     * @return builder
     */
    RuleBuilder call(RecordMethodWrapper.RecordMethodCaller methodHandle) {
        this.methodHandle = methodHandle;
        return this;
    }

    /**
     * Defines the class of the bean property
     *
     * @param classRetriever bean class
     * @return builder
     */
    RuleBuilder expects(Function<Method, Class> classRetriever) {
        this.classRetriever = classRetriever;
        return this;
    }

    /**
     * Used to determine if the value is required to be present in the underlying object
     *
     * @return builder
     */
    RuleBuilder requiresValue() {
        this.requiresValue = true;
        return this;
    }

    /**
     * Used to determine if the method modifies input
     *
     * @return builder
     */
    RuleBuilder modifiesInput() {
        this.modifiedInput = true;
        return this;
    }

    /**
     * Creates constructed conversion rule
     *
     * @return conversion rule
     */
    MethodConversionRule build() {
        return new MethodConversionRule.RecordMethodConversionRule(type, condition, methodHandle, classRetriever, requiresValue, modifiedInput);
    }
}