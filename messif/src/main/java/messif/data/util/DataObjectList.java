/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.util;

import messif.data.DataObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;


/**
 * Resizable-array for storing AbstractObjects or their descendants.
 * All list operations are implemented and additional support for
 * building randomly selected AbstracObject lists is provided.
 *
 * Additionally, the list returns <code>GenericObjectIterator</code>
 * through <code>iterator</code> method.
 * 
 */
public class DataObjectList extends ArrayList<DataObject> implements Serializable, Iterable<DataObject> {
    
    /** Class serial id for serialization */
    private static final long serialVersionUID = 1L;
    
    /****************** Constructors ******************/
    
    /**
     * Constructs an empty DataObject list with the specified initial capacity.
     *
     * @param capacity the initial capacity of the list
     * @exception IllegalArgumentException if the specified initial capacity is negative
     */
    public DataObjectList(int capacity) throws IllegalArgumentException {
        super(capacity);
    }
    
    /**
     * Constructs an empty DataObject list with an initial capacity of ten.
     */
    public DataObjectList() {
    }
    
    /**
     * Constructs an DataObject list containing the elements of the specified
 collection, in the order they are returned by the collection's
 iterator.
     *
     * @param source the collection whose elements are to be placed into this list
     * @throws NullPointerException if the specified collection is null
     */
    public DataObjectList(Collection<DataObject> source) throws NullPointerException {
        super(source);
    }
    
    /**
     * Constructs an DataObject list containing maximally <code>count</code>
     * elements returned by the specified iterator (in that order).
     *
     * @param iterator the iterator returing elements that are to be placed into this list
     * @param count maximal number of objects that are placed from iterator
     *              (negative number means unlimited)
     * @throws NullPointerException if the specified iterator is null
     */
    public DataObjectList(Iterator<DataObject> iterator, int count) throws NullPointerException {
        super((count > 0)?count:10);
        while (iterator.hasNext() && (count-- != 0))
            add(iterator.next());
    }

    /**
     * Constructs an DataObject list containing all
 elements returned by the specified iterator (in that order).
     *
     * @param iterator the iterator returing elements that are to be placed into this list
     * @throws NullPointerException if the specified iterator is null
     */
    public DataObjectList(Iterator<DataObject> iterator) throws NullPointerException {
        this(iterator, -1);
    }

    
    /****************** Additional access methods ******************/

    /**
     * Returns an iterator over the elements in this list in proper sequence.
     *
     * @return an iterator over the elements in this list in proper sequence
     */
    @Override
    public DataObjectIterator iterator() {
        final Iterator<DataObject> iterator = super.iterator();
        return new DataObjectIterator() {
            protected DataObject currentObject = null;
            @Override
            public DataObject getCurrentObject() throws NoSuchElementException {
                if (currentObject == null)
                    throw new NoSuchElementException("Can't execute getCurrentObject before next was called");
                return currentObject;
            }
            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }
            @Override
            public DataObject next() {
                return currentObject = iterator.next();
            }
            @Override
            public void remove() {
                iterator.remove();
            }
        };
    }

    /**
     * Appends all of the elements that can be retrieved from the specified
     * iterator to the end of this list.
     *
     * @param iterator iterator over elements to be added to this list
     * @return the number of objects added
     */
    public int addAll(Iterator<DataObject> iterator) {
        int ret = 0;
        if (iterator != null)
            while (iterator.hasNext())
                if (add(iterator.next()))
                    ret++;
        return ret;
    }


    /****************** Equality driven by object data ******************/

    /** 
     * Indicates whether some other object has the same data as this one.
     * @param   obj   the reference object with which to compare.
     * @return  <code>true</code> if this object has the same data as the obj
     *          argument; <code>false</code> otherwise.
     */
    public boolean dataEquals(Object obj) {
        // We must compare with other collection
        if (!(obj instanceof Collection))
            return false;
        Collection<?> castObj = (Collection<?>)obj;

        // Check size
        if (size() != castObj.size())
            return false;

        // Iterate through objects in this list and the target collection
        Iterator<?> iterator = castObj.iterator();
        for (DataObject localObject : this) {
            Object otherObject = iterator.next();

            // Check their data equality
            if (otherObject instanceof DataObject) {
                if (!localObject.dataEquals((DataObject) otherObject))
                    return false;
            } else {
                if (!localObject.equals(otherObject))
                    return false;
            }
        }

        return true;
    }

    /**
     * Returns a hash code value for the data of this list.
     * @return a hash code value for the data of this list
     */
    public int dataHashCode() {
	int hashCode = 1;
	for (DataObject obj : this) {
	    hashCode = 31*hashCode + (obj == null ? 0 : obj.dataHashCode());
	}
	return hashCode;
    }


    /****************** String representation ******************/    
    
    /**
     * Returns a string representation of this collection of objects.
     *
     * @return a string representation of this collection of objects
     */
    @Override
    public String toString() {
        return "DataObjectList " + super.toString();
    }

//
//    // ******************* Random selection *************************
//
//    /**
//     * Returns one object selected from the list at random.
//     *
//     * @return An object selected at random or null if the list is empty.
//     */
//    public DataObject randomObject() {
//        int l = size();
//
//        return ((l == 0) ? null : get( (int)(Math.random() * (double)l) ));
//    }
//
//    /** Returns a list containing randomly choosen objects from this list.
//     * If the uniqueness of objects in the retrieved list is not required, the number of objects
//     * in the response is equal to 'count'. If unique list is requested the number of objects
//     * can vary from 0 to 'count' and depends on the number of objects in this list. When this list
//     * consists of fewer objects than 'count' the whole list is returned at any case.
//     * The returned instance is exactly the same as passed in the parameter list. Chosen objects are
//     * only added to this list.
//     * If the passed list contains some objects they are left untouched.
//     *
//     * @param <T> the list class that receives random objects
//     * @param count   Number of object to return.
//     * @param unique  Flag if returned list contains each object only once.
//     * @param list    An instance of a class extending ObjectList<E> used to carry selected objects.
//     *
//     * @return the instance passed in list which contains randomly selected objects as requested
//     */
//    public <T extends List<DataObject>> T randomList(int count, boolean unique, T list) {
//        if (list == null)
//            return null;
//
//        // Ignore all previous elements in the list, just use it as it is empty.
//        List<DataObject> resList = list.subList(list.size(), list.size());
//
//        // unique list is requested
//        if (unique) {
//            if (count >= size()) {
//                resList.addAll(this);
//                return list;
//            }
//
//            // now, count is less than the length of this list
//
//            // we must pick fewer objects than the remaining objects
//            if (count <= size() - count) {
//                while (count > 0) {
//                    DataObject obj = get( (int)(Math.random() * (double)size()) );
//
//                    if (!resList.contains(obj)) {          // selected object is not in the response, add it
//                        resList.add(obj);
//                        --count;
//                    }
//                }
//            }
//            // we do not pick fewer objects than the number of objects we must pick (it is better to delete objects than to insert them).
//            else {
//                int[] toAdd = new int[size()];
//                Arrays.fill(toAdd, 1);
//
//                count = size() - count;          // how many objects to delete
//                while (count > 0) {
//                    int idx = (int)(Math.random() * (double)size());
//
//                    if (toAdd[idx] != 0) {
//                        toAdd[idx] = 0;
//                        --count;
//                    }
//                }
//
//                for (int i = 0; i < toAdd.length; i++) {
//                    if (toAdd[i] != 0)
//                        resList.add( get(i) );
//                }
//            }
//        }
//        // duplicate scan appear
//        else {
//            while (count > 0) {
//                resList.add( get( (int)(Math.random() * (double)size()) ) );
//                --count;
//            }
//        }
//
//        // return the list of selected objects
//        return list;
//    }
//
//
//    /** Returns a list containing randomly choosen objects from the passed iterator.
//     * If the uniqueness of objects in the retrieved list is not required, the number of objects
//     * in the response is equal to 'count'. If a unique list is requested, the number of objects
//     * can vary from zero to 'count' and depends on the number of objects in the passed iterator.
//     * When the iterator consists of fewer objects than 'count', all objects are returned at any case.
//     *
//     * The returned instance is exactly the same as passed in the parameter 'list'. Chosen objects are
//     * only added to this list. If the passed list contains some objects they are left untouched.
//     *
//     * @param <T> the list class that receives random objects
//     * @param count      Number of object to return.
//     * @param unique     Flag if returned list contains each object only once.
//     * @param list       An instance of a class extending ObjectList<E> used to carry selected objects.
//     * @param iterSource Iterator from which objects are randomly picked.
//     *
//     * @return the instance passed in list which contains the randomly selected objects as requested
//     */
//    public static <T extends List<DataObject>> T randomList(int count, boolean unique, T list, Iterator<DataObject> iterSource) {
//        if (list == null || iterSource == null)
//            return null;
//
//        // Ignore all previous elements in the list, just use it as it is empty.
//        List<DataObject> resList = list.subList(list.size(), list.size());
//
//        // Append objects upto count
//        while (count > 0 && iterSource.hasNext()) {
//            resList.add(iterSource.next());
//            count--;
//        }
//
//        // the iterator didn't contain more elements than count
//        if (count > 0)
//            return list;
//        // Reset count back (this value is exactly the same as the passed one!!!!)
//        count = resList.size();
//
//
//        // unique list is requested
//        if (unique) {
//            // Number of objects in the iterator
//            int iterCount = count;
//
//            // First, test the uniqueness of objects already in the list
//            Iterator<DataObject> it = resList.iterator();
//            for ( ; it.hasNext(); iterCount++) {
//                DataObject o = it.next();
//                if (resList.contains(o))
//                    it.remove();
//            }
//
//            // Append objects from iterator until the quorum is achieved.
//            for ( ; resList.size() < count && iterSource.hasNext(); iterCount++) {
//                DataObject o = iterSource.next();
//                if (! resList.contains(o))
//                    resList.add(o);
//            }
//
//            // If we have not enough object, exit prematurely.
//            if (resList.size() != count)
//                return list;
//
//            // We added count objects. Continue replacing them while there are some objects in the iterator.
//            for ( ; iterSource.hasNext(); iterCount++) {
//                DataObject o = iterSource.next();            // Get an object and move to next
//                int idx = (int)(Math.random() * (double)(iterCount + 1));
//                if (idx == iterCount) {
//                    int replace = (int)(Math.random() * count);
//                    if (! resList.contains(o))
//                        resList.set( replace, o );
//                }
//            }
//        }
//        // duplicate scan appear
//        else {
//            // Number of objects in the iterator
//            int iterCount = count;
//
//            for ( ; iterSource.hasNext(); iterCount++) {
//                DataObject o = iterSource.next();            // Get an object and move to next
//                int idx = (int)(Math.random() * (double)(iterCount + 1));
//                if (idx == iterCount) {
//                    int replace = (int)(Math.random() * count);
//                    resList.set( replace, o );
//                }
//            }
//        }
//
//        // return the list of selected objects
//        return list;
//    }
//
//    /**
//     * Selector of random data from an iterator source with known number of objects in the iterator. The
//     *  objects are selected uniformly from the iterator.
//     *
//     * The returned instance is exactly the same as passed in the parameter 'list'. Chosen objects are
//     * only added to this list. If the passed list contains some objects they are left untouched.
//     *
//     * @param <T> the list class that receives random objects
//     * @param count      Number of object to return.
//     * @param unique     Flag if returned list contains each object only once.
//     * @param list       An instance of a class extending ObjectList<E> used to carry selected objects.
//     * @param iterSource Iterator from which objects are randomly picked.
//     * @param sizeOfSource number of objects in the iterator
//     */
//    public static <T extends List<DataObject>> T randomList(int count, boolean unique, T list, Iterator<DataObject> iterSource, int sizeOfSource) {
//        if (list == null || iterSource == null)
//            return null;
//        if (count <= 0) {
//            return list;
//        }
//
//        // Ignore all previous elements in the list, just use it as it is empty.
//        List<DataObject> resList = list.subList(list.size(), list.size());
//
//        // Append objects upto count
//        if (count >= sizeOfSource && unique) {
//            while (count > 0 && iterSource.hasNext()) {
//                resList.add(iterSource.next());
//                count--;
//            }
//            return list;
//        }
//
//        Random random = new Random();
//        Collection<Integer> selected = unique ? new TreeSet<Integer>() : new SortedCollection<Integer>(count);
//
//        while (selected.size() < count) {
//            selected.add(random.nextInt(sizeOfSource));
//        }
//
//        int i = 0;
//        Iterator<Integer> iterator = selected.iterator();
//        Integer currentRandomPosition = iterator.next();
//        DATAITERATOR:
//        while (iterSource.hasNext()) {
//            DataObject next = iterSource.next();
//            while (i == currentRandomPosition) {
//                resList.add(next);
//                if (! iterator.hasNext()) {
//                    break DATAITERATOR;
//                }
//                currentRandomPosition = iterator.next();
//            }
//            i++;
//        }
//
//        // return the list of selected objects
//        return list;
//    }
//
//    /**
//     * Returns a list containing randomly choosen objects from the passed iterator.
//     * If the uniqueness of objects in the retrieved list is not required, the number of objects
//     * in the response is equal to 'count'. If a unique list is requested, the number of objects
//     * can vary from zero to 'count' and depends on the number of objects in the passed iterator.
//     * When the iterator consists of fewer objects than 'count', all objects are returned at any case.
//     *
//     * The returned instance is a new DataObjectList with the iterator's type of objects.
//     *
//     * @param count      Number of object to return.
//     * @param unique     Flag if returned list contains each object only once.
//     * @param iterSource Iterator from which objects are randomly picked.
//     *
//     * @return the instance passed in list which contains the randomly selected objects as requested
//     */
//    public static DataObjectList randomList(int count, boolean unique, Iterator<DataObject> iterSource) {
//        return randomList(count, unique, new DataObjectList(count), iterSource);
//    }
//
//    /**
//     * Selector of random data from an iterator source with known number of objects in the iterator. The
//     *  objects are selected uniformly from the iterator.
//     *
//     * The returned instance is a new DataObjectList with the iterator's type of objects.
//     *
//     * @param count      Number of object to return.
//     * @param unique     Flag if returned list contains each object only once.
//     * @param iterSource Iterator from which objects are randomly picked.
//     * @param sizeOfSource number of objects in the iterator
//     *
//     * @return the instance passed in list which contains the randomly selected objects as requested
//     */
//    public static DataObjectList randomList(int count, boolean unique, Iterator<DataObject> iterSource, int sizeOfSource) {
//        return randomList(count, unique, new DataObjectList(count), iterSource, sizeOfSource);
//    }
//
}
