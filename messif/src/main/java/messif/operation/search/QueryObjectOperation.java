/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.search;

import messif.distance.DistanceFunc;
import messif.distance.DistanceException;
import messif.data.DataObject;
import messif.data.util.RankedDataObject;
import messif.operation.answer.RankedAnswer;
import messif.operation.params.QueryObject;

/**
 * A base class of all single-query object operations (kNN, range, etc.)
 */
public interface QueryObjectOperation extends RankingOperation, QueryObject {

    /**
     * This class helps the operation processors to process this type of operation by providing supporting methods and
     *  continuous building of the answer.
     */
    class Helper extends RankingOperation.Helper<QueryObjectOperation, RankedAnswer> {

        protected final DataObject queryObject;

        /**
         * Creates this processor given a query operation and a distance to be used if the query does not contain
         * a distance itself.
         *
         * @param operation the query operation
         * @param defaultDistanceFunc a default distance to be used ONLY IN CASE the
         *          {@link RankingOperation#getDistanceFunction() distance parameter} is not used
         */
        public Helper(QueryObjectOperation operation, DistanceFunc<DataObject> defaultDistanceFunc) {
            super(operation, defaultDistanceFunc, RankedAnswer.class);
            this.queryObject = operation.getQueryObject();
        }


        // region  ***********  Utilities that work with the query object distance function        ********************* //

        /**
         * Adds an object to the answer. The rank of the object is computed automatically
         * as a distance between the query object and the specified object.
         *
         * @param object        the object to add
         * @param distThreshold the threshold on distance;
         *                      if the computed distance exceeds the threshold (sharply),
         *                      the object is not added to the answer
         * @return the distance-ranked object object that was added to answer or <tt>null</tt> if the object was not added
         */
        @Override
        protected RankedDataObject addToAnswer(DataObject object, float distThreshold) throws DistanceException {
            if (object == null)
                return null;
            if (operation.getQueryObject() == null)
                return answerBuilder.add(object, DistanceFunc.UNKNOWN_DISTANCE, null);

            // TODO: solve meta distances
            float[] metaDistances = null;
            //float[] metaDistances = storeMetaDistances ? queryObject.createMetaDistancesHolder() : null;
            float distance = this.distanceFunc.getDistance(queryObject, object, distThreshold);
            if (distance > distThreshold)
                return null;
            return addToAnswer(object, distance, metaDistances);
        }

        /**
         * Adds an object given its distance from the query.
         *  This method is necessary here in this helper since it is overloaded by underlying helpers.
         *
         * @param object the object to add
         * @return the distance-ranked object object that was added to answer or <tt>null</tt> if the object was not added
         */
        protected RankedDataObject addToAnswer(DataObject object, float distance, float[] objectDistances) throws IllegalArgumentException {
            return answerBuilder.add(object, distance, objectDistances);
        }
        // endregion
    }
}
