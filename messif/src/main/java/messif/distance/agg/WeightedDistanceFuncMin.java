/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.distance.agg;

import messif.distance.DistanceFunc;

/**
 * A distance which, given two objects, calculates a minimum of a previously given list of distance functions on the two objects.
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @param <T> Type of the object on which this distance can evaluate {@link DistanceFunc#getDistance(java.lang.Object, java.lang.Object)
 */
public class WeightedDistanceFuncMin<T> extends WeightedDistanceFunc<T> {

    /** Class id for Java serialization. */
    private static final long serialVersionUID = 1L;

    public WeightedDistanceFuncMin(DistanceFunc<T>[] distanceFunctions, float [] weights) {
        super(distanceFunctions, weights);
    }

    @Override
    public float compute(float... distances) {
        float rtv = DistanceFunc.MAX_DISTANCE;
        for (int i = 0; i < distances.length; i++) {
            float dist = weights[i] * distances[i];
            if (dist < rtv) {
                rtv = dist;
            }
        }
        return rtv;
    }

}
