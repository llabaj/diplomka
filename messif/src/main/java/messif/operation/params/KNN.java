/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.params;

import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Required;
import messif.utility.proxy.annotations.Set;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public interface KNN {

    String K_FIELD = "k";

    String FROM_FIELD = "from";

    @Get(field = K_FIELD)
    @Required
    int getK();

    /**
     * The "k" parameter can be also directly set.
     */
    @Set(field = K_FIELD)
    void setK(int newK);

    /**
     * An optional parameter that says that the "k" objects should not be taken from the beginning (the closest object)
     *  but from a "from"-nearest neighbor. Default is 0.
     * @return the {@link #FROM_FIELD} (if set) or the default value <code>0</code>
     */
    @Get(field = FROM_FIELD)
    int getFrom(int defaultValue);

    default int getFrom() {
        return getFrom(0);
    }
}
