/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.proxy;

import messif.record.ModifiableRecord;
import messif.utility.proxy.annotations.Expose;
import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Remove;
import messif.utility.proxy.annotations.Set;

import javax.annotation.Nonnull;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Abstract class holding the rule how to convert particular method to underlying class method.
 * <p>
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
abstract class MethodConversionRule {

    /**
     * Returns true if the given rule is applicable to given method
     *
     * @param method tested method
     * @return true if the rule can be applied to given method
     */
    abstract boolean isApplicable(@Nonnull Method method);

    /**
     * Creates the {@link MethodWrapper} for given {@link Method}
     *
     * @param method method to be converted
     * @return method wrapper
     */
    abstract MethodWrapper createWrapper(@Nonnull Method method);

    static class DefaultMethodConversionRule extends MethodConversionRule {

        @Override
        boolean isApplicable(@Nonnull Method method) {
            return method.isDefault();
        }

        @Override
        MethodWrapper createWrapper(@Nonnull Method method) {
            return new DefaultMethodWrapper(method);
        }
    }

    static class ExposeMethodConversionRule extends MethodConversionRule {

        final Predicate<Method> exposePrototype = method -> method.getReturnType().equals(ModifiableRecord.class) && method.getParameterCount() == 0;

        @Override
        boolean isApplicable(@Nonnull Method method) {
            return method.isAnnotationPresent(Expose.class) && exposePrototype.test(method);
        }

        @Override
        MethodWrapper createWrapper(@Nonnull Method method) {
            return new ExposeMethodWrapper();
        }
    }

    static class ForwardMethodConversionRule extends MethodConversionRule {

        @Override
        boolean isApplicable(@Nonnull Method method) {
            return method.getDeclaringClass().equals(Object.class);
        }

        @Override
        MethodWrapper createWrapper(@Nonnull Method method) {
            return new ForwardMethodWrapper(method);
        }
    }

    static class RecordMethodConversionRule extends MethodConversionRule {
        @Nonnull
        private final Class<? extends Annotation> type;
        @Nonnull
        private final Predicate<Method> condition;
        @Nonnull
        private final RecordMethodWrapper.RecordMethodCaller caller;

        @Nonnull
        private final Function<Method, Class> classRetriever;
        private final boolean requiresValue;
        private final boolean modifiedInput;

        RecordMethodConversionRule(@Nonnull Class<? extends Annotation> type, @Nonnull Predicate<Method> condition, @Nonnull RecordMethodWrapper.RecordMethodCaller caller, @Nonnull Function<Method, Class> classRetriever, boolean requireValue, boolean modifiesInput) {
            this.type = type;
            this.condition = condition;
            this.caller = caller;
            this.classRetriever = classRetriever;
            this.requiresValue = requireValue;
            this.modifiedInput = modifiesInput;
        }

        @Override
        boolean isApplicable(@Nonnull Method method) {
            return (method.isAnnotationPresent(type) && condition.test(method));
        }

        @Nonnull
        @Override
        MethodWrapper createWrapper(@Nonnull Method method) {
            RecordMethodWrapper.MethodVerifier verifier = new RecordMethodWrapper.MethodVerifier(classRetriever.apply(method), requiresValue, modifiedInput);
            String fieldName = getFieldName(method);
            return new RecordMethodWrapper(method, caller, fieldName, verifier);
        }

        @Nonnull
        private String getFieldName(@Nonnull Method method) {
            if (method.isAnnotationPresent(Get.class))
                return method.getDeclaredAnnotation(Get.class).field();
            if (method.isAnnotationPresent(Set.class))
                return method.getDeclaredAnnotation(Set.class).field();
            if (method.isAnnotationPresent(Remove.class))
                return method.getDeclaredAnnotation(Remove.class).field();
            throw new IllegalStateException("Missing annotation containing field: " + method.getName());
        }
    }
}
