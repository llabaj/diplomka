/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility;

import java.util.*;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class ListUtils {

    public static <E> E [] createArray(Iterator<? extends E> iterator) {
        final ArrayList<E> list = new ArrayList<>();
        while (iterator.hasNext())
            list.add(iterator.next());
        return list.toArray((E []) new Object [list.size()]);
    }

    // ******************* Random selection *************************

    /**
     * Returns one object selected from the list at random.
     *
     * @return An object selected at random or null if the list is empty.
     */
    public static <E> E randomObject(List<E> list) {
        int l = list.size();

        return ((l == 0) ? null : list.get( (int)(Math.random() * (double)l) ));
    }

    /** Returns a list containing randomly choosen objects from this list.
     * If the uniqueness of objects in the retrieved list is not required, the number of objects
     * in the response is equal to 'count'. If unique list is requested the number of objects
     * can vary from 0 to 'count' and depends on the number of objects in this list. When this list
     * consists of fewer objects than 'count' the whole list is returned at any case.
     * The returned instance is exactly the same as passed in the parameter list. Chosen objects are
     * only added to this list.
     * If the passed list contains some objects they are left untouched.
     *
     * @param <E> type of the objects to work with
     * @param count   Number of object to return.
     * @param unique  Flag if returned list contains each object only once.
     *
     * @return the instance passed in list which contains randomly selected objects as requested
     */
    public static <E> List<E> randomList(List<E> list, int count, boolean unique) {
        List<E> retVal = new ArrayList<E>(count);

        int listSize = list.size();
        double listSizeDouble = (double) listSize;
        // unique list is requested
        if (unique) {
            if (count >= listSize) {
                retVal.addAll(list);
                return retVal;
            }

            // now, count is less than the length of this list

            // we must pick fewer objects than the remaining objects
            if (count <= listSize - count) {
                while (count > 0) {
                    E obj = list.get( (int)(Math.random() * listSizeDouble) );

                    if (!retVal.contains(obj)) {          // selected object is not in the response, add it
                        retVal.add(obj);
                        --count;
                    }
                }
            }
            // we do not pick fewer objects than the number of objects we must pick (it is better to delete objects than to insert them).
            else {
                int[] toAdd = new int[listSize];
                Arrays.fill(toAdd, 1);

                count = listSize - count;          // how many objects to delete
                while (count > 0) {
                    int idx = (int)(Math.random() * listSizeDouble);

                    if (toAdd[idx] != 0) {
                        toAdd[idx] = 0;
                        --count;
                    }
                }

                for (int i = 0; i < toAdd.length; i++) {
                    if (toAdd[i] != 0)
                        retVal.add( list.get(i) );
                }
            }
        }
        // duplicate scan appear
        else {
            while (count > 0) {
                retVal.add( list.get( (int)(Math.random() * listSizeDouble) ) );
                --count;
            }
        }

        // return the list of selected objects
        return retVal;
    }

    // TODO: Make all these method generic (no DataObject)

    /** Returns a list containing randomly choosen objects from the passed iterator.
     * If the uniqueness of objects in the retrieved list is not required, the number of objects
     * in the response is equal to 'count'. If a unique list is requested, the number of objects
     * can vary from zero to 'count' and depends on the number of objects in the passed iterator.
     * When the iterator consists of fewer objects than 'count', all objects are returned at any case.
     *
     * The returned instance is exactly the same as passed in the parameter 'list'. Chosen objects are
     * only added to this list. If the passed list contains some objects they are left untouched.
     *
     * @param <E> type of the objects to work with
     * @param count      Number of object to return.
     * @param unique     Flag if returned list contains each object only once.
     * @param list       An instance of a class extending ObjectList<E> used to carry selected objects.
     * @param iterSource Iterator from which objects are randomly picked.
     *
     * @return the instance passed in list which contains the randomly selected objects as requested
     */
    public static <E> List<E> randomList(int count, boolean unique, List<E> list, Iterator<E> iterSource) {
        if (list == null || iterSource == null)
            return null;

        // Ignore all previous elements in the list, just use it as it is empty.
        List<E> resList = list.subList(list.size(), list.size());

        // Append objects upto count
        while (count > 0 && iterSource.hasNext()) {
            resList.add(iterSource.next());
            count--;
        }

        // the iterator didn't contain more elements than count
        if (count > 0)
            return list;
        // Reset count back (this value is exactly the same as the passed one!!!!)
        count = resList.size();


        // unique list is requested
        if (unique) {
            // Number of objects in the iterator
            int iterCount = count;

            // First, test the uniqueness of objects already in the list
            Iterator<E> it = resList.iterator();
            for ( ; it.hasNext(); iterCount++) {
                E o = it.next();
                if (resList.contains(o))
                    it.remove();
            }

            // Append objects from iterator until the quorum is achieved.
            for ( ; resList.size() < count && iterSource.hasNext(); iterCount++) {
                E o = iterSource.next();
                if (! resList.contains(o))
                    resList.add(o);
            }

            // If we have not enough object, exit prematurely.
            if (resList.size() != count)
                return list;

            // We added count objects. Continue replacing them while there are some objects in the iterator.
            for ( ; iterSource.hasNext(); iterCount++) {
                E o = iterSource.next();            // Get an object and move to next
                int idx = (int)(Math.random() * (double)(iterCount + 1));
                if (idx == iterCount) {
                    int replace = (int)(Math.random() * count);
                    if (! resList.contains(o))
                        resList.set( replace, o );
                }
            }
        }
        // duplicate scan appear
        else {
            // Number of objects in the iterator
            int iterCount = count;

            for ( ; iterSource.hasNext(); iterCount++) {
                E o = iterSource.next();            // Get an object and move to next
                int idx = (int)(Math.random() * (double)(iterCount + 1));
                if (idx == iterCount) {
                    int replace = (int)(Math.random() * count);
                    resList.set( replace, o );
                }
            }
        }

        // return the list of selected objects
        return list;
    }

    /**
     * Selector of random data from an iterator source with known number of objects in the iterator. The
     *  objects are selected uniformly from the iterator.
     *
     * The returned instance is exactly the same as passed in the parameter 'list'. Chosen objects are
     * only added to this list. If the passed list contains some objects they are left untouched.
     *
     * @param <E> type of the objects to work with
     * @param count      Number of object to return.
     * @param unique     Flag if returned list contains each object only once.
     * @param list       An instance of a class extending ObjectList<E> used to carry selected objects.
     * @param iterSource Iterator from which objects are randomly picked.
     * @param sizeOfSource number of objects in the iterator
     */
    public static <E> List<E> randomList(int count, boolean unique, List<E> list, Iterator<E> iterSource, int sizeOfSource) {
        if (list == null || iterSource == null)
            return null;
        if (count <= 0) {
            return list;
        }

        // Ignore all previous elements in the list, just use it as it is empty.
        List<E> resList = list.subList(list.size(), list.size());

        // Append objects upto count
        if (count >= sizeOfSource && unique) {
            while (count > 0 && iterSource.hasNext()) {
                resList.add(iterSource.next());
                count--;
            }
            return list;
        }

        Random random = new Random();
        Collection<Integer> selected = unique ? new TreeSet<Integer>() : new SortedCollection<Integer>(count);

        while (selected.size() < count) {
            selected.add(random.nextInt(sizeOfSource));
        }

        int i = 0;
        Iterator<Integer> iterator = selected.iterator();
        Integer currentRandomPosition = iterator.next();
        DATAITERATOR:
        while (iterSource.hasNext()) {
            E next = iterSource.next();
            while (i == currentRandomPosition) {
                resList.add(next);
                if (! iterator.hasNext()) {
                    break DATAITERATOR;
                }
                currentRandomPosition = iterator.next();
            }
            i++;
        }

        // return the list of selected objects
        return list;
    }

    /**
     * Returns a list containing randomly choosen objects from the passed iterator.
     * If the uniqueness of objects in the retrieved list is not required, the number of objects
     * in the response is equal to 'count'. If a unique list is requested, the number of objects
     * can vary from zero to 'count' and depends on the number of objects in the passed iterator.
     * When the iterator consists of fewer objects than 'count', all objects are returned at any case.
     *
     * The returned instance is a new DataObjectList with the iterator's type of objects.
     *
     * @param count      Number of object to return.
     * @param unique     Flag if returned list contains each object only once.
     * @param iterSource Iterator from which objects are randomly picked.
     *
     * @return the instance passed in list which contains the randomly selected objects as requested
     */
    public static <E> List<E> randomList(int count, boolean unique, Iterator<E> iterSource) {
        return randomList(count, unique, new ArrayList<E>(count), iterSource);
    }

    /**
     * Selector of random data from an iterator source with known number of objects in the iterator. The
     *  objects are selected uniformly from the iterator.
     *
     * The returned instance is a new DataObjectList with the iterator's type of objects.
     *
     * @param count      Number of object to return.
     * @param unique     Flag if returned list contains each object only once.
     * @param iterSource Iterator from which objects are randomly picked.
     * @param sizeOfSource number of objects in the iterator
     *
     * @return the instance passed in list which contains the randomly selected objects as requested
     */
    public static <E> List<E> randomList(int count, boolean unique, Iterator<E> iterSource, int sizeOfSource) {
        return randomList(count, unique, new ArrayList<E>(count), iterSource, sizeOfSource);
    }

}
