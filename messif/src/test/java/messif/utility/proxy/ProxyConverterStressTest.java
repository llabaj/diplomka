/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility.proxy;

import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Set;
import messif.record.ModifiableRecord;
import messif.record.ModifiableRecordImpl;
import org.junit.Ignore;
import org.junit.Test;

import java.util.*;

/**
 * Unit test to measure performance of instantiation and accessing of the proxy object and to compare the result to direct access to underlying object.
 * <p>
 * This test class is ignored by default.
 * <p>
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
@Ignore
public class ProxyConverterStressTest {
    private static final String FIRST_FIELD = "first";
    private static final String SECOND_FIELD = "second";
    private static final String RESULT_FIELD = "result";

    private long plainDuration = 0;
    private long conversionDuration = 0;
    private long proxyDuration = 0;

    @Test
    public void test() throws InterruptedException {
        //Thread.sleep(20*1000);
        int count = 100000;
        for (int i = 0; i < 50; i++) {
            System.out.println("------------------ Step " + (i + 1));
            testForCount(count);
        }

        System.out.println("------------------ FINAL RESULTS -------");
        System.out.println("Plain duration for count(" + count + "): " + plainDuration);
        System.out.println("Conversion duration for count(" + count + "): " + conversionDuration);
        System.out.println("Proxy duration for count(" + count + "): " + proxyDuration);
        System.out.println("Overall ratio (Proxy/Plain): " + ((float) (proxyDuration + conversionDuration) / plainDuration));
        System.out.println("Execution ratio (Proxy/Plain): " + ((float) proxyDuration / plainDuration));
    }

    private void testForCount(int count) {
        List<ModifiableRecord> fieldMaps = initMaps(count);
        List<Wrapper> wrappers = new ArrayList<>(count);

        System.out.println("Starting...");

        long localPlainDuration = runPlainOperation(fieldMaps);
        System.out.println("Plain duration for count(" + count + "): " + localPlainDuration);

        long localConversionDuration = runConversion(fieldMaps, wrappers);
        System.out.println("Conversion duration for count(" + count + "): " + localConversionDuration);

        long localProxyDuration = runProxyOperation(wrappers);
        System.out.println("Proxy duration for count(" + count + "): " + localProxyDuration);

        System.out.println("Proxy/Plain: " + ((float) (localProxyDuration + localConversionDuration) / localPlainDuration));

        plainDuration += localPlainDuration;
        conversionDuration += localConversionDuration;
        proxyDuration += localProxyDuration;
    }

    private long runConversion(List<ModifiableRecord> fieldMaps, List<Wrapper> wrappers) {
        long start = System.currentTimeMillis();
        for (ModifiableRecord map : fieldMaps) {
            wrappers.add(ProxyConverter.convert(map, Wrapper.class));
        }
        return System.currentTimeMillis() - start;
    }

    private long runProxyOperation(List<Wrapper> wrappers) {
        long start = System.currentTimeMillis();
        for (Wrapper obj : wrappers) {
            int first = obj.getFirst();
            int second = obj.getSecond();
            obj.setResult(first + second);
        }
        return System.currentTimeMillis() - start;
    }

    private long runPlainOperation(List<ModifiableRecord> fieldMaps) {
        long start = System.currentTimeMillis();
        for (ModifiableRecord map : fieldMaps) {
            int first = (Integer) map.getField(FIRST_FIELD);
            int second = (Integer) map.getField(SECOND_FIELD);
            map.setField(RESULT_FIELD, first + second);
        }
        return System.currentTimeMillis() - start;
    }

    private List<ModifiableRecord> initMaps(int count) {
        Random random = new Random(new Date().getTime());
        List<ModifiableRecord> maps = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            Map<String, Object> map = new HashMap<>();
            map.put(FIRST_FIELD, random.nextInt());
            map.put(SECOND_FIELD, random.nextInt());
            maps.add(new ModifiableRecordImpl(map));
        }
        return maps;
    }

    private interface Wrapper {
        @Get(field = FIRST_FIELD)
        int getFirst();

        @Get(field = SECOND_FIELD)
        int getSecond();

        @Set(field = RESULT_FIELD)
        void setResult(int result);
    }
}
