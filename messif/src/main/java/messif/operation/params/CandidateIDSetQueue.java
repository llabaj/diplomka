/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.params;

import messif.operation.answer.AbstractAnswer;
import messif.operation.helpers.AnswerProvider;
import messif.utility.proxy.annotations.Get;
import messif.utility.proxy.annotations.Required;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 * Parameter containing a queue of Collections of IDs as candidates for Join operation.
 */
public interface CandidateIDSetQueue {

    String ID_SET_QUEUE_PARAM = "id_set_queue";

    @Get(field = ID_SET_QUEUE_PARAM)
    @Required
    BlockingQueue<List<String>> getIDSetQueue();


    /** Name of the parameter to restrict the number of candidate sets to be retrieved by this operation. */
    String MAX_SETS_PARAM = "max_join_sets";

    @Get(field = MAX_SETS_PARAM)
    int getMaxJoinSets(int defaultNumber);
    default int getMaxJoinSets() {
        return getMaxJoinSets(Integer.MAX_VALUE);
    }


    /** A constant that marks the end of the candidate set stream. */
    List<String> END_OF_STREAM = Collections.singletonList(CandidateIDQueue.END_OF_STREAM);

    /**
     * Processor base that can be used to implement the process of getting candidates for join query.
     */
    class Helper implements AnswerProvider<AbstractAnswer> {

        protected final BlockingQueue<List<String>> idQueue;

        /** The maximal number of candidate sets to be returned (used mainly for testing purposes). */
        protected int maxSets = Integer.MAX_VALUE;

        /** The actual number of candidate sets returned. */
        protected int setsGenerated = 0;

        /**
         * Creates this processor given a join query operation.
         * @param query
         */
        public Helper(CandidateIDSetQueue query) {
            this.idQueue = query.getIDSetQueue();
            this.maxSets = query.getMaxJoinSets();
        }

        public boolean continueProcessing() {
            return setsGenerated < maxSets;
        }


        public void addToAnswer(List<String> ids) {
            idQueue.add(ids);
            setsGenerated ++;
        }

        /**
         * Close the stream of IDs that is to be consumed by the refiner.
         */
        public void endProcessing() {
            idQueue.add(END_OF_STREAM);
        }

        /**
         * Do not return any data in the answer.
         * @return
         */
        @Override
        public AbstractAnswer getAnswer() {
            endProcessing();
            return AbstractAnswer.createAnswer(AbstractAnswer.OPERATION_OK, AbstractAnswer.class);
        }

        @Override
        public void updateAnswer(AbstractAnswer answer) {

        }
    }
}
