/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.data.util;

import messif.data.DataObject;
import messif.distance.DistanceFunc;
import messif.distance.DistanceException;
import messif.record.Record;
import messif.utility.SortedCollection;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Specialization of {@link SortedCollection} that is specific for distance-ranked objects.
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class RankedSortedCollection extends DistanceRankedSortedCollection<RankedDataObject>  {
    /** class serial id for serialization */
    private static final long serialVersionUID = 1L;

    //****************** Attributes******************//

    /** Flag whether the add method checks if the object that {@link #isEqual} exists already in the collection */
    private boolean ignoringDuplicates;


    //****************** Constructors ******************//

    /**
     * Constructs an empty collection with the specified initial and maximal capacity.
     * @param initialCapacity the initial capacity of the collection
     * @param maximalCapacity the maximal capacity of the collection
     * @param comparator the comparator that defines ordering
     * @throws IllegalArgumentException if the specified initial or maximal capacity is invalid
     */
    public RankedSortedCollection(int initialCapacity, int maximalCapacity, Comparator<? super RankedDataObject> comparator) throws IllegalArgumentException {
        super(initialCapacity, maximalCapacity, comparator);
    }

    /**
     * Constructs an empty collection with the specified initial and maximal capacity.
     * The order is defined using the natural order of items.
     * @param initialCapacity the initial capacity of the collection
     * @param maximalCapacity the maximal capacity of the collection
     * @throws IllegalArgumentException if the specified initial or maximal capacity is invalid
     */
    public RankedSortedCollection(int initialCapacity, int maximalCapacity) throws IllegalArgumentException {
        super(initialCapacity, maximalCapacity, null);
    }

    /**
     * Constructs an empty collection.
     * The order is defined using the natural order of items.
     * The initial capacity of the collection is set to {@link #DEFAULT_INITIAL_CAPACITY}
     * and maximal capacity is not limited.
     */
    public RankedSortedCollection() {
        super();
    }

    /**
     * Creates a new collection filled with objects provided by the {@code iterator}.
     * Objects are ranked by the distance measured by the {@code distanceFunction}
     * from the given {@code referenceObject}.
     * @param <T> the type of object used to measure the distance
     * @param distanceFunc the distance function used for the measuring
     * @param referenceObject the reference object from which the distance is measured
     * @param iterator the iterator on objects to add to the collection
     */
    public <T extends DataObject> RankedSortedCollection(DistanceFunc<? super T> distanceFunc, T referenceObject, Iterator<? extends T> iterator) throws DistanceException {
        while (iterator.hasNext())
            add(new RankedDataObject(iterator.next(), distanceFunc, referenceObject));
    }

    /**
     * Creates a new collection filled with objects provided by the {@code objectProvider}.
     * Objects are ranked by the distance measured by the {@code distanceFunction}
     * from the given {@code referenceObject}.
     * @param <T> the type of object used to measure the distance
     * @param distanceFunc the distance function used for the measuring
     * @param referenceObject the reference object from which the distance is measured
     * @param objectProvider the provider of objects to add to the collection
     */
    public <T extends DataObject> RankedSortedCollection(DistanceFunc<? super T> distanceFunc, T referenceObject, Iterable<? extends T> objectProvider) throws DistanceException {
        this(distanceFunc, referenceObject, (Iterator<? extends T>)objectProvider.iterator());
    }

    // TODO: remove or solve
//    /**
//     * Constructor from an existing operation - all parameters are copied from the operation answer.
//     * @param operation operation with collection to copy all parameters from
//     */
//    public RankedSortedCollection(RankingQueryOperation operation) {
//        super(operation.getAnswerCount(), operation.getAnswerMaximalCapacity(), operation.getAnswerComparator());
//    }
//

    //****************** Attribute access methods ******************//

    @Override
    public void setMaximalCapacity(int capacity) {
        super.setMaximalCapacity(capacity);
    }

    /**
     * Set the flag whether the add method checks if the object that {@link #isEqual} exists already in the collection.
     * @param ignoringDuplicates the flag whether the add method checks for duplicate objects
     */
    public void setIgnoringDuplicates(boolean ignoringDuplicates) {
        this.ignoringDuplicates = ignoringDuplicates;
    }

    /**
     * Returns the flag whether the add method checks if the object that {@link #isEqual} exists already in the collection.
     * @return the flag whether the add method checks for duplicate objects
     */
    public boolean isIgnoringDuplicates() {
        return ignoringDuplicates;
    }


    //****************** Overrides with synchronization ******************//

    @Override
    public synchronized boolean add(RankedDataObject e) {
        return super.add(e);
    }

    @Override
    protected synchronized final boolean add(RankedDataObject e, int index) {
        if (ignoringDuplicates && !isEmpty()) {
            for (int i = index; i < size(); i++) {
                RankedDataObject objI = get(i);
                if (objI.getDistance() != e.getDistance())
                    break;
                if (isEqual(e.getObject(), objI.getObject()))
                    return false;
            }
            for (int i = index - 1; i >= 0; i--) {
                RankedDataObject objI = get(i);
                if (objI.getDistance() != e.getDistance())
                    break;
                if (isEqual(e.getObject(), objI.getObject()))
                    return false;
            }
        }
        return super.add(e, index);
    }

    /**
     * Given two objects, this method compares them according to locator (if not null) and, if both locators null,
     *  then according to {@link Record#dataEquals}.
     * @param e1 first object
     * @param e2 second object
     * @return <b>true</b> only if the locators are not null and equal or both null and data are equal
     */
    protected boolean isEqual(DataObject e1, DataObject e2) {
        String e1Locator = e1.getID();
        String e2Locator = e2.getID();
        if (e1Locator != null && e2Locator != null) {
            return e1Locator.equals(e2Locator);
        }
        if (e1Locator == null && e2Locator == null) {
            return e1.dataEquals(e2);
        }
        return false;
    }

    @Override
    public synchronized void clear() {
        super.clear();
    }

    @Override
    public synchronized boolean remove(Object o) {
        return super.remove(o);
    }

    @Override
    protected synchronized boolean remove(int index) {
        return super.remove(index);
    }

    @Override
    public synchronized RankedDataObject removeLast() throws NoSuchElementException {
        return super.removeLast();
    }

    @Override
    public synchronized RankedDataObject removeFirst() throws NoSuchElementException {
        return super.removeFirst();
    }

    @Override
    public synchronized Object[] toArray() {
        return super.toArray();
    }

    @Override
    public synchronized <E> E[] toArray(E[] array) {
        return super.toArray(array);
    }


    //****************** Distance ranked add ******************//

    /**
     * Add a distance-ranked object to this collection.
     * The information about distances of the respective sub-objects is preserved
     * using {@link RankedDataObjectSubdists} if the given {@code objectDistances}
     * array is not <tt>null</tt>.
     * @param object the object to add
     * @param distance the distance of object
     * @param objectDistances the array of distances to the respective sub-objects (can be <tt>null</tt>)
     * @return the distance-ranked object that was added to this collection or <tt>null</tt> if the object was not added
     * @throws IllegalArgumentException if the answer type of this operation requires cloning but the passed object cannot be cloned
     */
    public RankedDataObject add(DataObject object, float distance, float[] objectDistances) {
        RankedDataObject rankedObject = rankObject(object, distance, objectDistances);
        return add(rankedObject) ? rankedObject : null;
    }

    /**
     * Internal method that creates a distance-ranked object for adding to this collection.
     * The information about distances of the respective sub-objects is preserved
     * using {@link RankedDataObjectSubdists} if the given {@code objectDistances}
     * array is not <tt>null</tt>.
     * 
     * @param object the object to add
     * @param distance the distance of object
     * @param objectDistances the array of distances to the respective sub-objects (can be <tt>null</tt>)
     * @return the distance-ranked object that can be added to this collection
     * @throws IllegalArgumentException if the answer type of this operation requires cloning but the passed object cannot be cloned
     */
    protected RankedDataObject rankObject(DataObject object, float distance, float[] objectDistances) {
        if (object == null)
            return null;

        // Create the ranked object encapsulation
        return (objectDistances == null) ?
            new RankedDataObject(object, distance) :
            new RankedDataObjectSubdists(object, distance, objectDistances);
    }
}
