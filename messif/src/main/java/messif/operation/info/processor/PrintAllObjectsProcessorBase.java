/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operation.info.processor;

import messif.bucket.BucketDispatcher;
import messif.bucket.LocalBucket;
import messif.data.DataObject;
import messif.operation.ReturnDataOperation;
import messif.operation.info.PrintAllObjectsOperation;
import messif.operation.info.answer.PrintAllObjectsAnswer;
import messif.utility.ProcessingStatus;
import messif.utility.json.JSONWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

/**
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public abstract class PrintAllObjectsProcessorBase extends ObjectCountProcessorBase<PrintAllObjectsOperation, PrintAllObjectsAnswer> {

    /**
     * File to print to the data into.
     */
    protected String outputFileName;

    /**
     * Output stream into the temporary file.
     */
    private JSONWriter output;

    protected final String[] fieldsToReturn;

    /**
     * Creates this processor given the operation to be processed.
     *
     * @param operation operation to be executed
     */
    public PrintAllObjectsProcessorBase(PrintAllObjectsOperation operation) throws IOException {
        super(operation);
        this.fieldsToReturn = operation.fieldsToReturn();
        this.outputFileName = operation.getOutputFile();
        if (outputFileName == null) {
            outputFileName = File.createTempFile("all-records-", ".json", new File(".")).getAbsolutePath();
        } else {
            outputFileName = new File(outputFileName).getAbsolutePath();
        }
        this.output = new JSONWriter(new FileWriter(outputFileName));
    }

    @Override
    public PrintAllObjectsAnswer finish() {
        try {
            output.close();
            return PrintAllObjectsAnswer.createSuccessful(objectCount, outputFileName, operation.objectsToAnswer(), operation.deleteFileAfter());
        } catch (IOException ignore) {
            return PrintAllObjectsAnswer.createFailed(ProcessingStatus.UNKNOWN_ERROR);
        }
    }


    /******************  Helper methods to be used by other processors   ******************/

    /**
     * Print give object (or just its ID) to specified file output.
     *
     * @param object object to print
     * @throws java.io.IOException if the write operation fails
     */
    public void print(DataObject object) throws IOException {
        ReturnDataOperation.transformObject(object, fieldsToReturn).writeJSON(output);
    }

    /**
     * Prints all given objects to the specified file output.
     *
     * @param objects the collection of objects on which to evaluate this query
     * @return number of objects printed
     * @throws java.io.IOException if the write operation fails
     */
    public int evaluate(Iterator<DataObject> objects) throws IOException {
        int count = 0;
        while (objects.hasNext()) {
            print(objects.next());
            if (objects.hasNext()) output.write(",\n");
            count++;
        }
        addToAnswer(count);
        return count;
    }

    /**
     * Prints all given object IDs to the specified file output.
     * @param ids the collection of object IDs
     * @return number of objects printed
     * @throws java.io.IOException if the write operation fails
     */
    public int addIDs(Iterator<String> ids) throws IOException {
        int count = 0;
        while (ids.hasNext()) {
            DataObject.objectWithID(ids.next()).writeJSON(output);
            if (ids.hasNext()) output.write(",\n");
            count++;
        }
        addToAnswer(count);
        return count;
    }

    /**
     * Evaluate this search on a given bucket.
     * Object count stored in this bucket is added to this operation's answer.
     *
     * @param bucket the bucket to update answer from
     * @return number of objects added to this operation, i.e. the actual bucket's object count
     */
    public int evaluate(LocalBucket bucket) throws IOException {
        return evaluate(bucket.getAllObjects());
    }

    /**
     * Evaluate this search on a given bucket dispatcher.
     * Object counts stored in all buckets maintained by the provided dispatcher is added to this operation's answer.
     *
     * @param dispatcher the bucket dispatcher to update answer from
     * @return number of objects added to this operation, i.e. the actual dispatcher's object count
     */
    public int evaluate(BucketDispatcher dispatcher) throws IOException {
        int count = 0;
        for (LocalBucket bucket : dispatcher.getAllBuckets()) {
            count += evaluate(bucket);
        }
        return count;
    }
}
