/*
 *  This file is part of MESSIF library: https://bitbucket.org/disalab/messif
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility;

import java.io.File;
import java.io.InputStream;
import java.util.Collection;
import junit.framework.TestCase;
import test.TestConstants;

/**
 *
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public class DirectoryInputStreamTest extends TestCase {

    public DirectoryInputStreamTest(String testName) {
        super(testName);
    }

    /**
     * Test of searchFiles method, of class DirectoryInputStream.
     */
    public void testSearchFiles() throws Exception {
        if (TestConstants.directoryForInputStreamTest == null || !TestConstants.directoryForInputStreamTest.exists())
            return;
        Collection<File> files = DirectoryInputStream.searchFiles(null, TestConstants.directoryForInputStreamTest, null, false);
        assertEquals("Shallow files found", 3, files.size());
        files = DirectoryInputStream.searchFiles(null, TestConstants.directoryForInputStreamTest, null, true);
        assertEquals("Deep files found", 7, files.size());
        for (File file : files) {
            assertNotNull("File is null", file);
            assertFalse("File is dir", file.isDirectory());
        }
    }

    /**
     * Test of read method, of class DirectoryInputStream.
     */
    public void testOpen() throws Exception {
        if (TestConstants.directoryForInputStreamTest == null || !TestConstants.directoryForInputStreamTest.exists())
            return;
        InputStream instance = DirectoryInputStream.open(TestConstants.directoryForInputStreamTest.getPath() + File.separatorChar + "*.txt");
        int result = instance.read();
        int bytes = 0;
        while (result >= 0) {
            bytes++;
            assertTrue("Correct data", result < 256);
            assertTrue("Correct data", result == 'a' || result == 'b' || result == 'c' || result == 'd' || result == '\r' || result == '\n');
            result = instance.read();
        }
        assertEquals("Last read", -1, result);
        assertEquals("Number of bytes", 7*10, bytes);
    }

    /**
     * Test of read method, of class DirectoryInputStream.
     */
    public void testRead_0args() throws Exception {
        if (TestConstants.directoryForInputStreamTest == null || !TestConstants.directoryForInputStreamTest.exists())
            return;
        DirectoryInputStream instance = new DirectoryInputStream(TestConstants.directoryForInputStreamTest, null, true);
        int result = instance.read();
        int bytes = 0;
        while (result >= 0) {
            bytes++;
            assertTrue("Correct data", result < 256);
            assertTrue("Correct data", result == 'a' || result == 'b' || result == 'c' || result == 'd' || result == '\r' || result == '\n');
            result = instance.read();
        }
        assertEquals("Last read", -1, result);
        assertEquals("Number of bytes", 7*10, bytes);
    }

}
